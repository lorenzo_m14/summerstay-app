module Constants
  module Mailer
    NO_REPLY_MAILER="no-reply@thesummerstay.com"
    DEFAULT_MAILER_NAME="SummerStay"
  end

  module Events
    EMAIL_OFFSET_TIME = 6.hours
  end

  module EmailId
    # Account Mailer
    ACCOUNT_WELCOME_ID="d-12e669d90c7b4d91b7e6340b868f79f9"
    ACCOUNT_CONFIRMATION_ID="d-6fd5f1f513f04099add4f163da3302d7"
    ACCOUNT_RESET_PASSWORD_ID="d-2dd2ea7d8a6f4ccda82334984630f94e"
    ACCOUNT_MESSAGE_ID="d-cfdf18cedfda45fa9c58e5527ab0237f"
    ACCOUNT_CONNECT_VERIFIED_ID="d-d030ffb8612e4f07a4daff053af94a56"
    ACCOUNT_CONNECT_BLOCKED_ID="d-cdea371dfb964919b4705c166f60c226"
    ACCOUNT_CONNECT_DUESOON_ID="d-d422dd5b7c4e49bb9214da63d7f7ffeb"

    # Reservation Mailer
    BOOKING_HOST_REQUEST_ID="d-e360ffaff3b14068abeec246fdca87b0"
    BOOKING_HOST_ACCEPTED_ID="d-b702679a5fa54aad873619f1814f4007"
    BOOKING_RENTER_ACCEPTED_STANDARD_ID="d-6e9286ef01764fafb1c36e8e0e535f12"
    BOOKING_RENTER_ACCEPTED_PAYS_ID="d-131951d508974a9cb9a3381b22b1717f"
    BOOKING_RENTER_DECLINED_ID="d-8c183d6547224321bd9c31f980678aed"

    RESERVATION_HOST_RENTERCANCEL_ID="d-012a13f9f5494738bc03c0786b6a7adb"
    RESERVATION_RENTER_RENTERCANCEL_ID="d-3e601498d9e5441cb8deec725100b0ef"

    RESERVATION_HOST_HOSTCANCEL_ID="d-4e43d430e3494a3b912e7921898a7570"
    RESERVATION_RENTER_HOSTCANCEL_ID="d-5fd4722e148f46799260fe014043827b"

    PRE_MOVE_RENTER_ID="d-3bb377b3fe3e4291b53537be6f9aff39"
    PRE_MOVE_HOST_ID="d-53a56fa5562e4f70b79918a8d1d987cc"
    MOVE_IN_RENTER_ID="d-52be7b0ad5c5404c9aa9ddf1cd0806ca"
    MOVE_IN_HOST_ID="d-7ab124616f8941b8ab9270e854ffb2e3"
    MOVE_OUT_RENTER_ID="d-f025a965d1b44480803cc8961ce4f9ca"
    MOVE_OUT_HOST_ID="d-016d228723cb4bb7916cecae555e6fb0"

    REQUEST_EXPIRY_HOST_ID="d-4efaa5d472c5478a88d0266909c6a03e"

    # Listing Mailer
    COMPLETED_LISTING_ID="" # NOTE: NEED EMAIL ID!
    UNFINISHED_LISTING_ID="" # NOTE: NEED EMAIL ID!

    # Payments Mailer
    PAYMENT_RENTER_UPCOMING_ID="d-6e647bb82c3942f48677782b9c4770b0"
    PAYMENT_RENTER_DECLINED_ID="d-f948c3dd7d0d488c98c5ab51fe23d370"
    RECEIPT_EMAIL_ID='d-eb7479b35e354cf395ba6b5c121514e5'    # NOTE: Placeholder receipt email
    PAYOUT_HOST_SUCCESS_ID='d-fcd9420b70324520b67bc8fda1a61bdd'
    PAYOUT_HOST_VERIFICATION_ID='d-5a106647a9b3460dbb310fe244d2d311'
  end

  module EmailCategory 
    # Product Areas
    PRODUCT_AREA_LISTING = "PRODUCT_AREA_LISTING"
    PRODUCT_AREA_BOOKING = "PRODUCT_AREA_BOOKING"
    PRODUCT_AREA_ACCOUNT = "PRODUCT_AREA_ACCOUNT"
    PRODUCT_AREA_PAYMENT = "PRODUCT_AREA_PAYMENT"

    # User Type
    USER_TYPE_HOST = "USER_TYPE_HOST"
    USER_TYPE_RENTER = "USER_TYPE_RENTER"
    USER_TYPE_GENERAL = "USER_TYPE_GENERAL"

    # Importance
    PRIORITY_CRITICAL = "PRIORITY_CRITICAL"
    PRIORITY_HIGH = "PRIORITY_HIGH"
    PRIORITY_MEDIUM = "PRIORITY_MEDIUM"
    PRIORITY_LOW = "PRIORITY_LOW"
  end

  module Payments
      DEFAULT_CURRENCY = 'usd'.freeze

      PAYMENT_TYPES = {
        "card": "card",
        "bank": "bank_account"
      }

      CHARGE_TYPES = {
        "upfront": 0,
        "continuous": 1
      }

      # pay as you stay params
      CHARGE_PERIOD = 2   # 2 weeks per charge period
      DELAY_PERIOD = 1    # first payment delay period

      TRANSACTION_FEE = 25 # 25c transaction fee
      INIT_CHARGE_WEEKS = 2
      STRIPE_CURRENCY_ADJ = 100

      # Buyer side constants
      SS_RENTER_PERC = 0.05              # SummerStay 5% Buyer
      SS_CONTINUOUS_PERC = 0.1          # Continuous payment fee

      STRIPE_CARD_PERC = 0.03           # Stripe 3%
      STRIPE_ACH_FEE = 500              # $5 ACH Direct Debit
      DEPOSIT_WEEKS = 1                 # Deposit (equivalent weeks of rent)
      TAX_RATE = 0                      # Occupancy tax

      # Host side constants
      SS_HOST_PERC = 0.05                # SummerStay 5% Host
      STRIPE_ADJ_PAYOUT_PERC = 0.01     # Stripe (our percentage shown)
      STRIPE_REAL_PAYOUT_PERC = 0.0025  # Stripe (real percentage)
      PAYOUT_WEEKLY_INTERVAL = 2        # number of weeks per payout

      HOST_DEPOSIT = 10000              # $100 host deposit

      REFERRAL_PAYOUT = 2500            # $25 referral payout for host

      COST_PARAMS = {
        stripe_fee: STRIPE_CARD_PERC,
        service_fee: SS_RENTER_PERC,
        continuous_fee: SS_CONTINUOUS_PERC,
        tax: TAX_RATE,
        deposit_weeks: DEPOSIT_WEEKS,
      } 

  end

  module Listings
    DEFAULT_SCOPE = 50
    MIN_RESERVATION_LENGTH = 21
    MAX_LISTINGS_PER_USER = 10
    STAY_TYPES = ["Entire Place", "Shared Room", "Single Room"]
  end

  module Reservations
    RESERVATION_EXPIRY_DAYS = 5         # days til expiry of reservation from creation
    STANDARD_PAYMENT_DUE_DAYS = 14          # should pay second payment 14 days before, or charge automatically

    # email schedule constants
    PRE_MOVE_EMAIL_DAYS = 7             # days in advance of start date to send email 
    UPCOMING_PAYMENT_DAYS = 3           # days in advance to alert of stripe payment

    MIN_RESERVATION_LENGTH = 21
  end

  module NotificationTypes
    # reservation notifications
    RESERVATION_REQUEST = "res_request"
    RESERVATION_EXPIRING = "res_expiring"
    RESERVATION_ACCEPTED = "res_accepted"
    RESERVATION_DECLINED = "res_declined"
    RESERVATION_CANCELLED = "res_cancelled"

    # listing notifications
    LISTING_COMPLETED = "lis_completed"
    LISTING_UNFINISHED = "lis_unfinished"

    # payment notifications
    PAYMENT_UPCOMING = "pay_upcoming"
    PAYMENT_DECLINED = "pay_declined"
    PAYMENT_OVERDUE = "pay_overdue"
    PAYOUT_SUCCESS = "payout_success"

    # reminder
    REMINDER_PREMOVE = "rem_premove"
    REMINDER_MOVEIN = "rem_movein"
    REMINDER_MOVEOUT = "rem_moveout"
    REMINDER_FEEDBACK = "rem_feedback"

    # messages
    MESSAGE_SENT = "mes_sent"
    MESSAGE_RECEIVED = "mes_received"

    # claim notifications
    CLAIM_FILED = "claim_filed"

    # connect account status
    CONNECT_DUE = "con_due"
    CONNECT_VERIFIED = "con_verified"
    CONNECT_BLOCKED = "con_blocked"
  end

  module ErrorCodes
    STRIPE_CARD_ERROR = "card_error"
    STRIPE_INVALID_REQUEST_ERROR = "invalid_request_error"
    STRIPE_API_CONNECTION_ERROR = "api_connection_error"
  end
end