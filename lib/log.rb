module Log
  extend self

  [:debug, :info, :warn, :error, :fatal, :unknown].each do |severity|
    define_method severity do |message, params = {}|
      raise ArgumentError, "Hash is expected as 'params'" unless params.is_a?(Hash)
      Rails.logger.public_send(severity, {
        message: message
      }.merge(params).to_json)
    end
  end

  private
end