module Utilities
    def convert_to_dollars(cents)
        return '%.2f' % (cents/100.0)
    end

    def format_date(date)
        date.strftime("%B %d, %Y")
    end
end
