# DEPRECATED IN FAVOR OF sidekiq-cron
# Used by Heroku scheduler

# desc "This task runs daily and expires/declines old reservations"
# task :expire_old_reservations => :environment do
#     # expiry is 5 days

#     # check if created_at is less than expiry length
#     # also check if any waiting reservations have start dates in <1 day
#     # if so, cancel it
#     Reservation.where(status: :Waiting).where("expires_at < ? OR start_date >= ?", DateTime.current, Date.current).each do |r|
#         Log.info("Expiring reservation #{r.id}", level: "INFO", type: "Scheduler", action: "expire_old_reservations")
#         r.decline
#         r.Expired! # set status to expired
#         DeclinedRequestWorker.new.perform(r.id)
#     end
# end

# desc "This task runs every 3 day and checks for stale/unbookable listings"
# task :check_stale_listings => :environment do
#     # check if listing still has valid windows for reservations
#     Listing.where(status: [:Ready]).each do |listing|
#         if listing&.end_date.nil?
#             Log.error("Listing #{listing.id} is invalid", level: "ERROR", type: "Scheduler", action: "check_stale_listings")
#         elsif listing&.end_date < Date.today
#             Log.info("Archiving listing #{listing.id}", level: "INFO", type: "Scheduler", action: "check_stale_listings")
#             listing.update(bookable: false, status: Archived) 
#         else
#             Log.info("Listing #{listing.id} has no more bookable windows", level: "INFO", type: "Scheduler", action: "check_stale_listings")
#             listing.update(bookable: false) if !listing.has_bookable_windows && listing.bookable
#         end
#     end
# end