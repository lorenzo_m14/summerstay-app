source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2'

# Use postgres as the database for Active Record
gem 'pg', '~> 0.18.4'

# Use Puma as the app server
gem 'puma', '~> 3.11'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use ActiveAdmin gem for admin console
# gem 'activeadmin'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', '< 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# Use Faker to generate fake data
gem 'faker'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  # Load environment variables from .env int ENV in development
  gem 'dotenv-rails'

  # for testing
  gem 'rspec-rails'
  gem 'factory_bot_rails'
  gem 'database_cleaner'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# user and authenticatoin
gem 'devise', '~> 4.2'
gem 'omniauth', '~> 1.6'
# gem 'simple_token_authentication', '~> 1.15.1'

# address geocoding gem
gem 'geocoder', '~> 1.4'

# facebook graph api
gem 'koala', '~> 3.0'   

# json serializer
gem 'active_model_serializers', require: true

# payments
gem 'stripe', '~> 5.12.0'
gem 'plaid', '~> 8.3.0'

# http gem
gem 'httparty', '~> 0.17.0'

# transactional emails
gem 'sendgrid-ruby'

# aws storage
gem 'aws-sdk', '~> 3'
gem 'image_processing',  '~>1.10.3'

# delayed job w/ redis cache
# sidekiq-cron for recurring cron jobs
gem 'sidekiq', '~> 6.0.4'
gem "sidekiq-cron", "~> 1.1"

# pagination
gem 'will_paginate', '~> 3.2.1'

# 2fa
gem 'twilio-ruby', '~> 5.31.1'

# logs
gem 'lograge'
gem 'ddtrace'

# gem 'elastic-apm'

gem "bugsnag", "~> 6.13"

gem 'prerender_rails'
