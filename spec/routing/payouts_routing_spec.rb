require "rails_helper"

RSpec.describe PayoutsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/payouts").to route_to("payouts#index")
    end

    it "routes to #show" do
      expect(:get => "/payouts/1").to route_to("payouts#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/payouts").to route_to("payouts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/payouts/1").to route_to("payouts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/payouts/1").to route_to("payouts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/payouts/1").to route_to("payouts#destroy", :id => "1")
    end
  end
end
