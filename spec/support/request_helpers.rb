module Requests
  module JsonHelpers
    def json
      JSON.parse(response.body)
    end
  end

  module AuthenticationHelpers
    def authenticate(user)
      user.generate_authentication_token
      headers = {
        "X-User-Email": user.email,
        "X-User-Token": user.authentication_token
      }
      return headers
    end
  end
end