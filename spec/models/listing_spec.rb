require 'rails_helper'

RSpec.describe Listing, :type => :model do
  let!(:user) { FactoryBot.create(:user) }
  subject { described_class.new(
    listing_type: "Apartment",
    address: "6643 Shepley Dr, Clayton, MO 63105",
    latitude: 47.59949775267777,
    longitude: -122.24941429191826,
    user: user
  )}

  let!(:listing) { FactoryBot.create(:listing)}

  # LISTING CREATION
  describe 'listing creation' do
    it "is valid with valid attributes" do
        expect(subject).to be_valid
    end

    it "is not valid without a listing_type" do
        subject.listing_type = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without a address" do
        subject.address = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without a latitude" do
        subject.latitude = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without a longitude" do
        subject.longitude = nil
        expect(subject).to_not be_valid
    end

    it "is not valid without a user" do
        subject.user = nil
        expect(subject).to_not be_valid
    end
  end

  # LISTING EDIT
  describe 'listing edit' do
    it "is valid with valid attributes" do
        expect(listing).to be_valid
    end

  end
end