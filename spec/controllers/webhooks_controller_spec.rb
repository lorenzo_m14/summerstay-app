require 'rails_helper'

# TODO: FIX THIS!
class WebhooksControllerTest < ActionDispatch::IntegrationTest
    # Stripe webook params copied from <https://dashboard.stripe.com/test/webhooks>
    STRIPE_INVOICE_SUCCEEDED_PARAMS = {
      id: 'invoice.payment_00000000000000',
      type: 'invoice.payment_succeeded',
      data: {
        object: {
          customer: 'cus_00000000000000',
        }
      },
    }.freeze
  
    setup do
        @user = users(:one_advocate)
    end
    
    test 'Stripe hook should add premium days to the given user' do
        old = user.premium_until
        post hooks_stripe_url, params: STRIPE_INVOICE_SUCCEEDED_PARAMS
        assert_response :success
        user.reload
        assert_operator old, :<=, user.premium_until
    end
  end
