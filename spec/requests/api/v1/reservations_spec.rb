require 'rails_helper'

RSpec.describe 'Reservations API', :type => :request do
    num_listings = 5
    # let!(:listing_user) { FactoryBot.create(:user) }
    let!(:request_user) { FactoryBot.create(:other_user) }
    let!(:listing) { FactoryBot.create(:active_listing, price: 300 * 100) }
    # let!(:listings) { FactoryBot.create_list(:active_listing, num_listings, user_id: user.id) }
    let!(:listing_id) { listing.id }

    future_start_date = Date.current + 10.days


    # Test suite for creating reservations
    describe 'POST /reservations' do
        before (:each) do
            request_login
        end

        context 'make valid booking request' do
            request_params = create_req_params(listing_id, future_start_date, future_start_date + 34.days, 0)
            post "/api/v1/reservations", params: request_params

            it 'returns created reservation' do
                puts json
                expect(json['reservation']).not_to be_empty
            end

            it 'returns status code 201' do
                expect(response).to have_http_status(201)
            end
        end

        context 'make invalid booking request' do
            invalid_request_params = create_req_params(listing_id, future_start_date, future_start_date + 20.days, 0)
            post "/api/v1/reservations", params: invalid_request_params

            it 'returns status code 400' do
                expect(response).to have_http_status(400)
            end
        end

        context 'make 7 week Standard booking request' do
            standard_request_params = create_req_params(listing_id, future_start_date, future_start_date + 7.weeks, 0)
            post "/api/v1/reservations", params: standard_request_params

            it 'returns proper payment schedule' do
                expect(json['reservation']).not_to be_empty
                

            end

            it 'returns status code 201' do
                expect(response).to have_http_status(201)
            end
        end

        context 'make 7 week Continuous booking request' do
            # TODO:
        end
    end

    describe 'POST /reservations/approve' do
        context 'approve booking request' do
            # TODO: ensure payments are correct via Stripe
            # TODO: ensure payouts have correct dates
            # TODO: ensure other reservations are declined
        end
    end

    describe 'POST /reservations/decline' do
    end

    describe 'POST /reservations/cancel' do
    end

    # AUTH HEADERS METHODS
    def listing_login
        post user_session_path, params: {user: { email: listing_user.email, password: "Password1!"}}.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end

    # login for other user
    def request_login
        post user_session_path, params: {user: { email: request_user.email, password: "Password1!"}}.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end

    def create_req_params(listing_id, start_date, end_date, charge_type)
        request_params = {
            "reservation": {
                "listing_id": listing_id,
                "start_date": start_date,
                "end_date": end_date,
                "charge_type": charge_type,
            }
        }
    end

    def check_reservation(reservation_id)
        r = Reservation.find_by_id(reservation_id)

        return r.reservation_cost, r.charges
    end

    def check_charges
    end

    def check_payouts
    end
end