require 'rails_helper'

RSpec.describe 'Authentication API', :type => :request do
    let!(:user) { FactoryBot.create(:user) }
    let!(:other_user) { FactoryBot.create(:random_user) }
    let!(:unconfirmed_user) { FactoryBot.create(:unconfirmed_user) }
    let!(:valid_user_login) {{user: { email: user.email, password: "password"}}}
    let!(:invalid_user_login) {{user: { email: user.email, password: "invalid password"}}}
    let!(:unconfirmed_user_login) {{user: { email: unconfirmed_user.email, password: "password"}}}

    # Test suite for POST /users/sign_in
    describe 'POST /users/sign_in' do
        describe 'when user is confirmed' do
            context 'when proper credentials are added' do
                before (:each) do
                    post '/api/v1/users/sign_in', params: valid_user_login.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
                end

                it 'returns user' do
                    expect(json).not_to be_empty
                end

                it 'returns auth headers' do
                    expect(response.headers).not_to be_empty
                    expect(response.headers["X-User-Token"]).not_to be_empty
                    expect(response.headers["X-User-Email"]).not_to be_empty
                end
    
                it 'returns status code 201' do
                    expect(response).to have_http_status(201)
                end
            end

            context 'when improper credentials are added' do
                before (:each) do
                    post '/api/v1/users/sign_in', params: invalid_user_login.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
                end
    
                it 'returns status code 401' do
                    expect(response).to have_http_status(:unauthorized)
                end
    
                it 'returns a not found message' do
                    expect(json["error"]).to match(/Invalid Email or password./)
                end
            end
        end

        describe 'when user is not confirmed' do
            before (:each) do
                post '/api/v1/users/sign_in', params: unconfirmed_user_login.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
            end

            it 'returns status code 401' do
                expect(response).to have_http_status(:unauthorized)
            end

            it 'returns an unconfirmed account message' do
                expect(json["error"]).to match(/You have to confirm your email address before continuing./)
            end
        end
    end

    # Test suite for POST /users
    describe 'POST /users' do
        user_params = {
            "user": {
                "email": "new_user@email.com",
                "password": "password",
                "first_name": "Another",
                "last_name": "Test"
            }
        }

        incomplete_user_params = {
            "user": {
                "email": "another_new_user@email.com",
                "password": "password",
                "last_name": "Test"
            }
        }

        context 'with required params (email, pass, first_name, last_name) are present' do
            before (:each) do
                post '/api/v1/users', params: user_params.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
            end

            it 'returns user' do
                expect(json).not_to be_empty
            end

            # because needs confirmation
            it 'returns no auth headers' do
                expect(response.headers["X-User-Token"]).to be_nil
                expect(response.headers["X-User-Email"]).to be_nil
            end

            it 'returns status code 201' do
                expect(response).to have_http_status(201)
            end
        end

        context 'with required params missing' do
            before (:each) do
                post '/api/v1/users', params: incomplete_user_params.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
            end

            it 'returns errors instead of user' do
                expect(json["errors"]).not_to be_empty
            end

            it 'returns no auth headers' do
                expect(response.headers["X-User-Token"]).to be_nil
                expect(response.headers["X-User-Email"]).to be_nil
            end

            it 'returns status code 422' do
                expect(response).to have_http_status(422)
            end
        end
    end


    # Test suite for GET /users/logout
    describe 'GET /users/logout' do
        context 'with valid auth headers' do
            before (:each) do
                post '/api/v1/users/sign_in', params: valid_user_login.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
                auth_params = get_auth_headers_from_login(response)
                get '/api/v1/users/logout', headers: auth_params
            end

            it 'returns status code 204' do
                expect(response).to have_http_status(:no_content)
            end
        end

        context 'with invalid auth headers' do
            before (:each) do
                get '/api/v1/users/logout'
            end
            it 'returns status code 401' do
                expect(response).to have_http_status(:unauthorized)
            end

            it 'returns a not found message' do
                expect(json["error"]).to match(/Not Authenticated/)
            end
        end
    end

    ### FACEBOOK AUTH ###

    Test suite for POST /users/facebook
    describe 'POST /users/facebook' do
        context 'with new facebook account' do
            before (:each) do
                    Koala::Facebook::TestUsers.new(app_id: env['FACEBOOK_KEY'] , secret: env['FACEBOOK_SECRET'])
            end

            it 'confirms user'
                expect(response).to have_http_status(:no_content)
            end 

            it 'returns status code 204' do
                expect(response).to have_http_status(:no_content)
            end
        end

        context 'with existing facebook account' do

        end
    end

    # # AUTH HEADERS METHODS
    def facebook_create(params: account_params)
        post users_facebook_params, params: {facebook_access}
    end

    def facebook_login(some_user: user)
        post user_session_path, params: {user: { email: some_user.email, password: "password"}}.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end


    # Test Confirmations emails

    # 



    def get_auth_headers_from_login(response)
        auth_params = {
                      'X-User-Email' => response.headers['X-User-Email'],
                      'X-User-Token' => response.headers['X-User-Token'],
                    }
        auth_params
    end
end