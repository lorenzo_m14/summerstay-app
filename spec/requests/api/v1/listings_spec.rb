require 'rails_helper'

RSpec.describe 'Listings API', :type => :request do
    num_listings = 5
    let!(:user) { FactoryBot.create(:user) }
    let!(:other_user) { FactoryBot.create(:random_user) }
    let!(:listings) { FactoryBot.create_list(:active_listing, num_listings, user_id: user.id) }
    let(:listing_id) { listings.first.id }

    # Test suite for GET /listings
    describe 'GET /listings' do
        before (:each) do
            login
            get '/api/v1/listings'
        end


        context 'when no filter params added' do
            it 'returns listings' do
                expect(json).not_to be_empty
                expect(json['listings'].size).to eq(num_listings)
            end

            it 'returns status code 200' do
                expect(response).to have_http_status(200)
            end
        end
    end

    # Test suite for POST /listings
    describe 'POST /listings' do
        listing_params = {
            "listing": {
                "listing_type": "Apartment",
                "address": "6643 Shepley Dr, Clayton, MO 63105",
                "latitude": 47.59949775267777,
                "longitude": -122.24941429191826
            }
        }
        invalid_listing_params = {
            "listing": {
                "listing_type": "Apartment"
            }
        }

        describe 'with succesful login session' do
            context 'when params are present' do
                before(:each) do
                    login
                    post "/api/v1/listings", params: listing_params
                end

                it 'creates the listing' do
                    expect(json['listing']).not_to be_empty
                end

                it 'returns status code 201' do
                    expect(response).to have_http_status(201)
                end
            end

            context 'when params are absent' do
                before(:each) do
                    login
                    post "/api/v1/listings", params: invalid_listing_params
                end

                it 'returns status code 400' do
                    expect(response).to have_http_status(:bad_request)
                end
            end
        end

        context 'with invalid session' do
            before(:each) do
                post "/api/v1/listings", params: listing_params
            end

            it 'returns status code 401 unauthorized' do
                expect(response).to have_http_status(:unauthorized)
            end

            it 'returns a not found message' do
                expect(json["error"]).to match(/Not Authenticated/)
            end
        end

    end

    # Test suite for GET /listings/:id
    describe 'GET /listings/:id' do
        before { get "/api/v1/listings/#{listing_id}" }

        context 'when the record exists' do
            it 'returns the listing' do
                expect(json['listing']).not_to be_empty
                expect(json['listing']['id']).to eq(listing_id)
            end

            it 'returns status code 200' do
                expect(response).to have_http_status(200)
            end
        end

        context 'when the record does not exist' do
            let(:listing_id) { 1 }

            it 'returns status code 400' do
                expect(response).to have_http_status(:bad_request)
            end

            it 'returns a not found message' do
                expect(json["error"]).to match("Invalid listing ID")
            end
        end
    end

    # Test suite for GET /my_listings  (host's own listings)
    describe 'GET /my_listings' do
        context 'when user has listings' do
            before(:each) do
                login
                get "/api/v1/my_listings"
            end

            it 'returns only listings owned by user' do
                expect(json['listings']).to all(include("user_id" => user.id))
            end

            it 'returns status code 200' do
                expect(response).to have_http_status(:ok)
            end
        end

        context 'when user has no listings' do
            before(:each) do
                other_login
                get "/api/v1/my_listings"
            end

            it 'returns empty listings' do
                expect(json['listings']).to be_empty
            end

            it 'returns status code 200' do
                expect(response).to have_http_status(:ok)
            end
        end
    end

    # Test suite for PUT /listings/:id
    describe 'PUT /listings/:id' do
        new_listing_params = {
            "listing": {
                "building_type": "House",
                "persons": "4",
                "bedroom": "4",
                "beds": "4",
                "bathroom": "1",
                "listing_type": "Apartment",
                "listing_name": "Nemerov Suite 1000",
                "summary": "This is comfy suite for 400 situated in a WashU dorm.",
                "address": "6643 Shepley Dr, Clayton, MO 63105",
                "price": "4800"
            }
        }
            
        describe 'with valid auth headers' do
            context 'when params are present' do
                before(:each) do
                    login
                    put "/api/v1/listings/#{listing_id}", params: new_listing_params
                end

                it 'updates the listing' do
                    expect(json['listing']).not_to be_empty
                end

                it 'returns status code 200' do
                    expect(response).to have_http_status(:ok)
                end
            end

            # TODO
            # context 'when listing is active'

            context 'when params are absent' do
                before(:each) do
                    login
                    put "/api/v1/listings/#{listing_id}", params: new_listing_params
                end

                it 'returns status code 401' do
                    expect(response).to have_http_status(401)
                end

                it 'returns a not authenticated message' do
                    expect(json["error"]).to match(/Not Authenticated/)
                end
            end

            context 'when listing is not own' do
                before(:each) do
                    other_listing = FactoryBot.create(:active_listing, user_id: other_user.id)
                    login
                    auth_params = get_auth_headers_from_login(response)
                    put "/api/v1/listings/#{other_listing.id}", params: new_listing_params
                end

                it 'returns status code 400' do
                    expect(response).to have_http_status(:bad_request)
                end
            end
        end

        context 'with invalid auth headers' do
            before(:each) do
                post "/api/v1/listings", params: new_listing_params
            end

            it 'returns status code 401' do
                expect(response).to have_http_status(:unauthorized)
            end

            it 'returns a not authenticated message' do
                expect(json["error"]).to match(/Not Authenticated/)
            end
        end
    end

    # Test suite for GET /listings/:id
    describe 'DELETE /listings/:id' do
        before { destroy "/api/v1/listings/#{listing_id}" }

        context 'when the listing exists' do
            it 'returns the listing' do
                expect(json['listing']).to be_empty
                expect(json['success']).to match(true)
            end

            it 'returns status code 200' do
                expect(response).to have_http_status(200)
            end
        end

        context 'when the listing has active reservations'
            it 'returns status code 400' do
                expect(response).to have_http_status(400)
            end

            it 'returns a cannot delete listing message' do
                expect(json["error"]).to match("Cannot delete listing!")
            end
        end

        context 'when the record does not exist' do
            let(:listing_id) { 1 }

            it 'returns status code 400' do
                expect(response).to have_http_status(:bad_request)
            end

            it 'returns a not found message' do
                expect(json["error"]).to match("Invalid listing ID")
            end
        end
    end


    # AUTH HEADERS METHODS
    def login
        post user_session_path, params: {user: { email: user.email, password: "password"}}.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end

    # login for other user
    def other_login
        post user_session_path, params: {user: { email: other_user.email, password: "password"}}.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end

    def get_auth_headers_from_login(response)
        auth_params = {
                      'X-User-Email' => response.headers['X-User-Email'],
                      'X-User-Token' => response.headers['X-User-Token'],
                    }
        auth_params
    end
end