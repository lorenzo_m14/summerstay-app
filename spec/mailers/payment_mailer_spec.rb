require "rails_helper"

RSpec.describe PaymentMailer, type: :model do
  let!(:reservation) { FactoryBot.create(:waiting_reservation) }

  describe 'PaymentMailer emails' do
    # tests all instance methods in Reservation Mailer
    mailer = PaymentMailer.new({send_test: true})
    PaymentMailer.public_instance_methods(false).each do |method|
      it "sends successful #{method} mail" do
        response = mailer.public_send("#{method}", reservation.second_charge)
        expect(response.status_code).to eq("200").or eq("202")
      end
    end
  end
end