require "rails_helper"

RSpec.describe ReservationMailer, type: :model do
  let!(:reservation) { FactoryBot.create(:waiting_reservation) }

  describe 'ReservationMailer emails' do
    # tests all instance methods in Reservation Mailer
    mailer = ReservationMailer.new({send_test: true})
    # TODO: why doesn't this work properly? we don't want to recreate reservation
    # reservation = FactoryBot.create(:waiting_reservation)
    ReservationMailer.public_instance_methods(false).each do |method|
      it "sends successful #{method} mail" do
        response = mailer.public_send("#{method}", reservation)
        expect(response.status_code).to eq("200").or eq("202")
      end
    end
  end
end
