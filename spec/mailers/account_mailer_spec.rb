require "rails_helper"

RSpec.describe AccountMailer, type: :model do
  
  let!(:user) { FactoryBot.create(:user) }
  let!(:unconfirmed_user) { FactoryBot.create(:unconfirmed_user) }

  describe 'AccountMailer emails' do
    # tests all instance methods in Account Mailer
    mailer = AccountMailer.new({send_test: true})
    context 'welcome email' do
      it "sends successful mail" do
        response = mailer.welcome(user)
        expect(response.status_code).to eq("200").or eq("202")
      end
    end

    context 'confirmation email' do
      it "sends successful mail" do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        response = mailer.confirmation(unconfirmed_user, random_token)
        expect(response.status_code).to eq("200").or eq("202")
      end
    end

    context 'reset password email' do
      it "sends successful mail" do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        response = mailer.reset_password(unconfirmed_user, random_token)
        expect(response.status_code).to eq("200").or eq("202")
      end
    end

    # context 'message sent email' do
    #   it "sends successful mail" do
    #     response = mailer.reset_password(unconfirmed_user, random_token)
    #     expect(response.status_code).to eq("200").or eq("202")
    #   end
    # end
  end
end
