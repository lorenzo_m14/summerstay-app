FactoryBot.define do  
  center_point_list = [
                { lat: 47.6062, lng: -122.3321 }, # Seattle
                { lat: 38.6270, lng: -90.1994 }, # St. Louis
                { lat: 40.7128, lng: -74.0060 }, # New York
                { lat: 42.3601, lng: -71.0589 }, # Boston
                ]

  amenities = ["tv", "kitchen", "air", "heating", "internet", "furnished", "laundry", "parking", "gym", "pool", "pet"]

  addr = Faker::Address
  full_address = {
            line_1: addr.street_address,
            line_2: addr.secondary_address,
            city: addr.city,
            state: addr.state,
            zip_code: addr.zip_code,
            country: addr.country
        }
  bedroom = rand(1..4)
  beds = rand(bedroom..2*bedroom)
  bathroom = rand(1..2) + 0.5*(rand < 0.25 ? 1 : 0)
  listing_type_list = ["Entire Place", "Shared Room", "Single Room"]
  center_point = center_point_list.sample
  start_date = Date.current + 4.weeks
  end_date = start_date + 15.weeks

  factory :listing_creation, class: Listing do
    listing_type { listing_type_list.sample }
    longitude {center_point[:lng]}
    latitude {center_point[:lat]}
    address { addr.full_address }
    full_address { full_address }
    user
  end
  
  factory :active_listing, class: Listing do
    listing_type { listing_type_list.sample }
    bedroom { bedroom }
    beds { beds }
    bathroom { bathroom }
    listing_name { addr.street_address }
    summary { Faker::Lorem.paragraph(8) }
    address { addr.full_address }
    full_address { full_address }
    user
    price {rand(300..500) * 100}

    longitude {center_point[:lng] + rand(-0.1..0.1)}
    latitude {center_point[:lat] + rand(-0.1..0.1)}

    start_date { start_date }
    end_date { end_date }

    amenities { amenities.sample(rand(amenities.length) + 1) }

    # trait: 
    active { true }
    status { 1 }
    after(:build) do |listing|
      listing.images.attach(io: File.open(Rails.root.join('spec', 'factories', 'images', '1.jpg')), filename: '1.jpg', content_type: 'image/jpg')
      listing.images.attach(io: File.open(Rails.root.join('spec', 'factories', 'images', '2.jpg')), filename: '2.jpg', content_type: 'image/jpg')
    end
  end
end
