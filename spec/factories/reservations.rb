FactoryBot.define do
    # listing = FactoryBot.create :active_listing
    # user = FactoryBot.create :other_user
    # listing_start_date = listing.start_date
    # listing_end_date = listing.end_date

    factory :waiting_reservation, class: Reservation do
        # start_date { listing_start_date }
        # end_date { listing_end_date }
        association :user, factory: :other_user
        association :listing, factory: :active_listing
        # host { listing.user }
        expires_at { DateTime.current + 5.days }
        status { 0 }
        after(:build) do |r|
            r.host = r.listing.user
            r.start_date = r.listing.start_date
            r.end_date = r.listing.end_date
        end
        after(:create) do |r|
            PaymentServices.new(r).create_reservation_charges
        end
    end

    five_week_start_date = Date.current + 10.days

    factory :five_week_reservation, class: Reservation do
        start_date { five_week_start_date }
        end_date { five_week_start_date + (7*5-1).days }
        association :user, factory: :other_user
        association :listing, factory: :active_listing
        expires_at { DateTime.current + 5.days }
        status { 0 }
        after(:build) do |r|
            r.host = r.listing.user
        end
        after(:create) do |r|
            PaymentServices.new(r).create_reservation_charges(1)
        end
    end
  
  end
  