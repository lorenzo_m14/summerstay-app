FactoryBot.define do
    factory :user, class: User do
        first_name {'John'}
        last_name {'Doe'}
        password {'Password1!'}
        email {'john.doe@mailcatch.com'}
        confirmed_at {Time.now}
        stripe_id { 'cus_Ge8ezgWYlYLmjr' }
    end

    factory :other_user, class: User do
        first_name {'Skip'}
        last_name {'User'}
        password {'Password1!'}
        email {'skipuser@mailcatch.com'}
        confirmed_at {Time.now}
        stripe_id { 'cus_Ge8ezgWYlYLmjr' }
    end

    
    factory :random_user, class: User do
        first_name { Faker::Name.first_name }
        last_name { Faker::Name.last_name }
        password {'Password1!'}
        email { Faker::Internet.safe_email }
        confirmed_at {Time.now}
    end

    factory :unconfirmed_user, class: User do
        first_name { Faker::Name.first_name }
        last_name { Faker::Name.last_name }
        password {'Password1!'}
        email { Faker::Internet.safe_email }
        after(:create) do |u|
            u.confirmation_token = SecureRandom.urlsafe_base64
        end
    end
  end
  