# SummerStay
## Project Scope
- I (Lorenzo Melendez) built this project along with Will Tong, Alex Dees & Daniel Holiday
- The goal of SummerStay was to fill a gap in the student sublet market during internship and non school months. This is a fully functional housing marketplace equipt with payments and ID verification.
- We terminated the production deployment of this project on Jan 30, 2021 because of COVID-19 and lack of time.
- This repository is only intended to showcase the work we did. 

## File BreakDown
- All React code is located in client/src/
- The main Ruby on Rails code is in app/

## Local Development
- Make sure you have all dependencies (ruby, rails, postgres, node, npm) installed (see bottom of page for version numbers).
- ```yarn --cwd client``` to build node modules
- ```bundle``` to install gems, migrate and seed db with ```bin/rake db:migrate db:seed```
- Use ```bin/rake start``` to boot up server, client, and redis/sidekiq together.
- Server (Rails) only startup: Use ```bin/rails s -p 3001``` to boot up server on http://localhost:3001
    - Note: Make sure Postgres is running in the background
- Client (React) only startup: Use ```yarn --cwd client start``` (from base directory) to build node modules and boot up client on http://localhost:3000
- Redis and sidekiq (redis is installed):  In separate terminal windows start ```redis-server``` and ```bundle exec sidekiq```
- Stripe Local Webhooks (stripe cli is set up): To receive Stripe Webhook events locally, run `stripe listen --load-from-webhooks-api --forward-to localhost:3001`

## Installation and Setup

- **Homebrew** (for Mac) 
    - https://brew.sh/
- **RVM** 
    - https://rvm.io/rvm/install
- **Ruby** (v2.6.3)
    - `rvm install ruby-2.6.3`
- **Bundler** (v2.0.2)
    - `gem install bundler`
- **Rails** (v5.2.2)
    - `gem install rails --version=5.2.2`
- **Postgres** 
    - https://postgresapp.com/
- **Yarn, Node, npm**
    - `brew install yarn` (this will also install node along with npm, if not already installed)
- **Heroku CLI**
    - `brew install heroku`
- **Redis**
    - `brew install redis`
- **Stripe CLI** 
    - `brew install stripe/stripe-cli/stripe`
    - For more details see https://stripe.com/docs/stripe-cli


## Tools

- Visual Studio Code
- Slack (summerstay.slack.com)
- Trello (https://trello.com/b/XKkZf3GR/summerstay)
- Postman (API Tool)
    - https://www.getpostman.com/collections/e86d06d4f19655df5cbb
- PSequel (DB viewer)
    

### Versioning
- Ruby 2.6.3
- Rails 5.2.2
- Bundler 2.0.2
- Check ```client/package.json``` for node modules


## API Documentation (Coming Soon)
- Refer to Postman endpoint tests for now (https://www.getpostman.com/collections/e86d06d4f19655df5cbb)


## Services Overview
- Bitbucket (Code Repo)
- Heroku (Hosting)
- Stripe (Payments and Payouts)
- LeadDyno (Campus Reps)
- Sendgrid (Transactional Emails)
- Datadog (Logging + Error Monitoring): 
- AWS S3 (File/Image Storage)
- Redis + Sidekiq (Jobs)


# Services Guide

## Payments
### Payment plan:
- Fees
    - 3% card fee
    - 5% renter service fee
    - 6% host service fee
    - `optional` 10% pay-as-you-stay fee (on top of renter service fee)
### Charge Types
- Standard: Two upfront payments
    - Initial Payment
        - Charged at time of approval
        - Costs: 
            - 2 weeks rent
            - deposit (1 week rent)
            - 5% renter service fee 
            - 3% card fee (for sum of the above)
    - Final Payment
        - Charged one week before move-in. (Note: Occurs at time of approval if less than one week til move-in).
        - Costs: 
            - (total weeks - 2 weeks) rent
            - 3% card fee
- Pay-As-You-Stay: Upfront payment, then recurring bi-weekly subscription (starting week 3)
    - Initial Payment
        - Charged at time of approval
        - Costs: 
            - 2 weeks rent
            - deposit (1 week rent)
            - 5% renter service fee + 10% Pay-As-You-Stay fee
            - 3% card fee (for sum of the above)
    - Recurring Payments
        - Starting week 3, charge biweekly til move out (costs are adjusted to number of recurring charges)
        - Costs:
            - (total weeks - 2 weeks) rent
            - 3% card fee

### Payouts
- Payouts to the host occur via Stripe Connect platform (see Stripe sections for more details)
- Desination amount (94% of total rent cost) is calculated at time of reservation creation
- Destination amount is divided evenly across payouts for a reservation
- Database Payout objects are instantiated at time of approval and put into Sidekiq job scheduler to be paid out starting week 2, and biweekly til move out (similar to recurring charges)

___
## Stripe 
- Important Links
    - Dashboard (https://dashboard.stripe.com/test/)
    - API Docs (https://stripe.com/docs/api/)
- Stripe Payments 
    - Stripe::Invoices (https://stripe.com/docs/api/invoices)
        - All payments are processed as Invoices and associated with a database Charge
        - A successfully paid Invoice creates a Stripe Charge 
            - We save Stripe Charge receipt with db Charge
        - Most invoices are **automatic** invoices, meaning they are billed and charged automatically with the Customer's 
        - Standard Charge's final payment is created as a **manual** invoice, meaning that a link is emailed to the user to manually pay the invoice before a due date.
    - Stripe::Subscriptions and Stripe::SubscriptionSchedules
        - For Pay-As-You-Stay recurring payments *ONLY*, we create a Stripe SubscriptionSchedule which contains a Stripe Plan, defining the cost and number of charges, as well as the start and end date of the payment cycle.
        - We associate the subscription schedule with a database ReservationCost object.
        - When the subscription starts, we receive a `subscription.updated` webhook from Stripe, and save the newly created Stripe Subscription with the ReservationCost
        - Each recurring payment is a database Charge and should be associated one-to-one with Invoices released by the Subscription.
- Stripe Connect (Payouts)
    - Stripe::Transfers
        - Transfers are payments from SummerStay platform account to host's Stripe Connect account
        - Each Stripe Transfer is associated with a database Payout object
    - Stripe::Payouts
        - Stripe Payouts are Stripe's way of physically paying out balance from host's Stripe Connect account to bank account
- Payment Methods
    - Cards
        - Stripe Card Elements (from Javascript) will send server a Stripe token for payment method, and then that is saved in database and Stripe
        - All cards charge 3% on payments
    - ACH (Coming Soon)
        - Connected with Plaid, service to verify bank accounts. Seems like pretty straightforward integration.
        - Charges 0.8% up to $5 on each payment
- Webhooks 
    - Stripe sends back info on invoice completion, subscription release/completion, and transfer/payout completion
    - Set up webhooks 
    - Can use Stripe CLI to redirect webhooks to localhost for testing

___
## Heroku
- Setup
    - Install Heroku cli ()
    - Deployments with Heroku remote (ie `git push heroku master`)
- Structure
    - Environments
        - Staging (https://summerstay-staging.herokuapp.com)
        - Production (coming soon!)
    - Dynos (hobby tier)
        - web (rails app)
        - worker (sidekiq)
    - Add-ons
        - Scheduler (Cron Jobs)
        - Redis
        - Postgres
        - DataDog (https://app.datadoghq.com/signup/agent#heroku)
- Key commands
    - Reset postgres db: `heroku pg:reset`
    - Migrate and seed: `heroku rake db:migrate` `heroku rake db:seed`

___
## Redis and Sidekiq
- Run with `redis-server` command
- Sidekiq runs on redis and handles job logic for future payouts and emails
- View Sidekiq dashboard endpoint at `/api/v1/sidekiq`

___
## LeadDyno
- Campus Reps sign up as affiliates, and are created in the backend as LeadDyno affiliates
- Affiliates have their own dashboard to view their leads and purchases
- Lead: When a listing is posted with valid referral code
- Purchase: When a listing with referral code is booked
- We can go into admin portal to see all traffic and also manually payout rewards

___
## Sendgrid
- Build email templates with template id, send emails via Sendgrid Web API 

___
## Datadog
- Add buildpack to Heroku with API key
- Install agent on local computer

