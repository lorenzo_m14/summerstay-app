class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations, id: :uuid do |t|
      t.integer :status, default: 0

      t.references :user, type: :uuid, foreign_key: true
      t.references :listing, type: :uuid, foreign_key: true
      t.uuid :host_id

      t.uuid :canceller_id

      t.date :start_date
      t.date :end_date

      t.datetime :expires_at

      t.timestamps
    end
  end
end
