class CreateListings < ActiveRecord::Migration[5.2]
  def change
    create_table :listings, id: :uuid do |t|
      t.integer :status, default: 0

      t.string :listing_type
      t.integer :bedroom
      t.integer :beds
      t.float :bathroom
      t.string :listing_name
      t.text :summary
      t.string :address

      t.string :amenities, array: true, default: []

      # t.boolean :is_tv
      # t.boolean :is_kitchen
      # t.boolean :is_air
      # t.boolean :is_heating
      # t.boolean :is_internet
      # t.boolean :is_laundry
      # t.boolean :is_furnished
      # t.boolean :is_parking
      # t.boolean :is_gym
      # t.boolean :is_pool

      t.date :start_date
      t.date :end_date

      t.integer :price

      t.boolean :active, default: false

      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
