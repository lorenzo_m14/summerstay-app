class AddIdVerifyTokenToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :id_verify_token, :string
  end
end
