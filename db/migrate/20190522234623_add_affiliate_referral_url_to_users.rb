class AddAffiliateReferralUrlToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :referral_url, :string
  end
end
