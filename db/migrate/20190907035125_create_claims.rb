class CreateClaims < ActiveRecord::Migration[5.2]
  def change
    create_table :claims, id: :uuid do |t|

      t.integer :user_type
      t.integer :claim_type
      t.text :content

      t.references :user, type: :uuid, foreign_key: true
      t.references :reservation, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
