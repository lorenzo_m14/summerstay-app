class CreateConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations, id: :uuid do |t|
      t.uuid :sender_id
      t.uuid :recipient_id

      t.timestamps
    end

    add_index :conversations, %i[sender_id recipient_id]
  end
end
