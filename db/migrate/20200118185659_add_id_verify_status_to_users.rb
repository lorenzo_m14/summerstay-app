class AddIdVerifyStatusToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :id_verify_status, :integer, default: 0
  end
end
