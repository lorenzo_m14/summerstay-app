class AddAptNumberToListings < ActiveRecord::Migration[5.2]
  def change
    add_column :listings, :apt_number, :string
  end
end
