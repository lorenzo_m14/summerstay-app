class AddReferralFieldToPayouts < ActiveRecord::Migration[5.2]
  def change
    add_column :payouts, :is_referral, :boolean, default: false
  end
end
