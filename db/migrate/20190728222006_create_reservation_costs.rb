class CreateReservationCosts < ActiveRecord::Migration[5.2]
  def change
    create_table :reservation_costs do |t|
      t.integer :price
      t.integer :renter_service_fee
      t.integer :total_rent
      t.integer :num_weeks

      t.integer :deposit

      t.integer :stripe_fee
      t.integer :continuous_fee
      t.integer :tax

      t.integer :discount

      t.integer :total_cost

      t.integer :destination_amount

      t.integer :charge_type
      t.string :payment_method_type
      t.string :payment_method_id
      
      t.integer :deposit_status, default: 0

      t.references :reservation, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
