class CreateReferralCode < ActiveRecord::Migration[5.2]
  def change
    create_table :referral_codes, id: :uuid do |t|
      t.string :code, unique: true
      t.references :user, type: :uuid, foreign_key: 'campus_rep_id'
    end
  end
end
