class AddReadyAtToListings < ActiveRecord::Migration[6.0]
  def change
    add_column :listings, :ready_at, :datetime
  end
end
