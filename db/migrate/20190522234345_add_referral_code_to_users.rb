class AddReferralCodeToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :referral_code, type: :uuid, foreign_key: 'referral_code_id'
  end
end
