class AddBookingFieldsToListings < ActiveRecord::Migration[5.2]
  def change
    add_column :listings, :ready, :boolean, default: false
    add_column :listings, :booked, :boolean, default: false
    add_column :listings, :bookable, :boolean, default: true
    add_column :listings, :first_booked_at, :datetime
  end
end
