class AddReferralCodeToReservations < ActiveRecord::Migration[5.2]
  def change
    add_reference :reservations, :referral_code, type: :uuid, foreign_key: 'referral_code_id', null: true
  end
end
