class AddDatetimeFieldsToReservations < ActiveRecord::Migration[6.0]
  def change
    add_column :reservations, :approved_at, :datetime
    add_column :reservations, :declined_at, :datetime
  end
end
