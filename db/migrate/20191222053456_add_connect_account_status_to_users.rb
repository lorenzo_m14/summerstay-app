class AddConnectAccountStatusToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :connect_account_status, :integer, default: 0
  end
end
