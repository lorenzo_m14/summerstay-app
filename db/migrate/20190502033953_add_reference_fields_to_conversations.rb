class AddReferenceFieldsToConversations < ActiveRecord::Migration[5.2]
  def change
    add_reference :conversations, :listing, type: :uuid
    add_reference :conversations, :reservation, type: :uuid
  end
end
