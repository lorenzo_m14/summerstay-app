class CreateCharges < ActiveRecord::Migration[5.2]
  def change
    create_table :charges, id: :uuid do |t|
        t.integer :status, default: 0
        t.date :due_by

        t.integer :stripe_fee
        t.integer :platform_cost
        t.integer :deposit
        t.integer :total_cost
        t.integer :tax
        t.integer :discount

        t.integer :charge_type
        t.integer :charge_num
        t.string :payment_method_type
        t.string :payment_method_id

        t.string :stripe_charge_id
        t.string :invoice_url
        t.string :receipt_url
        t.datetime :paid_at

        t.references :reservation, type: :uuid, foreign_key: true
        t.references :user, type: :uuid, foreign_key: true
  
        t.timestamps
    end
  end
end
