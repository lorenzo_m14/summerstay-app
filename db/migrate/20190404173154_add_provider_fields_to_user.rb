class AddProviderFieldsToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :fb_image, :string
    add_column :users, :phone_number, :string
    add_column :users, :description, :text
  end
end
