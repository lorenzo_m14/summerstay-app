class AddRequestMessageToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :request_message, :text
  end
end
