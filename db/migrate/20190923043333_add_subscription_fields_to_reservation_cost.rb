class AddSubscriptionFieldsToReservationCost < ActiveRecord::Migration[5.2]
  def change
    add_column :reservation_costs, :subscription_id, :string
    add_column :reservation_costs, :sub_schedule_id, :string
    add_column :reservation_costs, :sub_status, :integer
    add_column :reservation_costs, :sub_start_date, :datetime
  end
end
