class CreatePayouts < ActiveRecord::Migration[5.2]
  def change
    create_table :payouts, id: :uuid do |t|
      t.integer :status, default: 0
      t.date :due_by

      t.integer :payout_amount
      t.integer :payout_num

      t.integer :ss_fee

      t.string :stripe_transfer_id
      t.string :stripe_payout_id
      t.string :merchant_id

      t.references :reservation, type: :uuid, foreign_key: true
      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
