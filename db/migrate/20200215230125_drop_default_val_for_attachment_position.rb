class DropDefaultValForAttachmentPosition < ActiveRecord::Migration[6.0]
  def change
    change_column_default :active_storage_attachments, :position, nil
  end
end
