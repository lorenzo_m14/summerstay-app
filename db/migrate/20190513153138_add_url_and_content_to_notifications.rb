class AddUrlAndContentToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :content, :string
    add_column :notifications, :notification_url, :string
  end
end
