class AddDisplayPriceToListing < ActiveRecord::Migration[6.0]
  def change
    add_column :listings, :display_price, :integer
  end
end
