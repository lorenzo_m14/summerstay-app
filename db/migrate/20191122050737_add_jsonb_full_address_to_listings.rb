class AddJsonbFullAddressToListings < ActiveRecord::Migration[5.2]
  def change
        add_column :listings, :full_address, :jsonb, null: false, default: {}
        add_index  :listings, :full_address, using: :gin
  end
end
