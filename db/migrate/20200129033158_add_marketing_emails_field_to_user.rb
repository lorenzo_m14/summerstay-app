class AddMarketingEmailsFieldToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :receive_marketing, :boolean, default: true
  end
end
