class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications, id: :uuid do |t|
      t.string :notification_type, null: false
      t.string :notifiable_type
      t.datetime :read_at, default: nil

      t.uuid :recipient_id, null: false
      t.uuid :actor_id
      t.uuid :notifiable_id

      t.timestamps
    end

    add_index :notifications, %i[recipient_id notification_type]
    add_index :notifications, [:recipient_id]
  end


end
