# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
include NotificationsHelper::Reservations
include Constants::Reservations

dummy_pass = "Password1!"

puts "Generating users"
confirmed_user = User.new(first_name: 'Test', last_name: 'User', email: 'testuser@mailcatch.com', password: dummy_pass)
confirmed_user.skip_confirmation!
confirmed_user.save!

affiliate_user = User.new(first_name: 'Affiliate', last_name: 'User', email: 'affiliateuser@mailcatch.com', password: dummy_pass)
affiliate_user.skip_confirmation!
affiliate_user.save!

skip_confirm_user = User.new(first_name: 'Skip', last_name: 'confirm', email: 'skip@mailcatch.com', password: dummy_pass)
skip_confirm_user.skip_confirmation!
skip_confirm_user.save!

unconfirmed_user = User.new(first_name: 'Confirm', last_name: 'Me!', email: 'confirm@mailcatch.com', password: dummy_pass)
unconfirmed_user.skip_confirmation!
unconfirmed_user.save!

payment_test_user = User.new(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email, password: dummy_pass)
payment_test_user.skip_confirmation!
payment_test_user.save!

user_list = []
1.upto(2) do |i|
    user = User.new(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email, password: dummy_pass)
    user.skip_confirmation!
    user.save!
    user_list.append(user)
end
user_list.push(skip_confirm_user)

center_point_list = [
                { lat: 47.6062, lng: -122.3321 }, # Seattle
                { lat: 34.0395, lng: -118.2734 }, # Los Angeles
                { lat: 38.6488, lng: -90.3108 }, # St. Louis
                { lat: 42.3601, lng: -71.0589 }, # Boston
                ]

listing_type_list = ["Entire Place", "Shared Room", "Single Room"]
listings_start_begin = DateTime.current - 2.weeks
listings_start_end = DateTime.current + 3.weeks

photos = [
    { path: "#{Rails.root}/client/src/Assets/listing.png", file: "listing.png" },
    { path: "#{Rails.root}/client/src/Assets/house.jpg", file: "house.jpg" },
    { path: "#{Rails.root}/client/src/Assets/homePage.png", file: "homePage.png" },
    { path: "#{Rails.root}/client/src/Assets/stay.png", file: "stay.png" },
    { path: "#{Rails.root}/client/src/Assets/girls.jpg", file: "tester.jpg" },
    { path: "#{Rails.root}/client/src/Assets/payment.png", file: "campus-managers-boys-laptop.jpg" }
]

amenities = ["tv", "kitchen", "air", "heating", "internet", "furnished", "laundry", "parking", "gym", "pool", "pet"]

# affiliate code stuff
# rc = ReferralCode.create!(code: "8ZJnz0cD", user_id: affiliate_user.id)
# affiliate_user.update!(referral_code: rc)
# confirmed_user.generate_referral_code

# create payment method for affiliate account
customer = Stripe::Customer.create(
    name: "#{payment_test_user.first_name} #{payment_test_user.last_name}",
    email: payment_test_user.email
)
payment_test_user.stripe_id = customer.id
payment_test_user.save
source = customer.sources.create(source: 'tok_visa')
Stripe::Customer.update(customer.id, default_source: source.id)

# fake payout accounts
confirmed_user.merchant_id = "acct_1G68uCG1YykHM6ND"
skip_confirm_user.merchant_id = "acct_1G68rTIq289dNptb"

confirmed_user.save
skip_confirm_user.save


puts "Generating listings"
# fake listing info, generate 16 listings per city
# "Ready" listings (status=1)
num_listings = 5
first_city = true
for center_point in center_point_list do
    if first_city || Rails.env.staging?
        num_listings = 25
    else
        first_city = false
        num_listings = 5
    end
    1.upto(num_listings) do |i|
        addr = Faker::Address
        listing_photos = photos.sample(2)
        full_address = {
            line_1: addr.street_address,
            line_2: addr.secondary_address,
            city: addr.city,
            state: addr.state,
            zip_code: addr.zip_code,
            country: addr.country
        }
        bedroom = rand(1..4)
        beds = rand(bedroom..2*bedroom)
        bathroom = rand(1..2) + 0.5*(rand < 0.25 ? 1 : 0)
        user = i > num_listings/2 ? confirmed_user : skip_confirm_user
        start_date = Faker::Date.between(from: listings_start_begin, to: listings_start_end)
        end_date = start_date + 20.weeks
        listing = Listing.create!(
            listing_type: listing_type_list.sample,
            bedroom: bedroom,
            beds: beds,
            bathroom: bathroom,
            listing_name: addr.street_address,
            summary: Faker::Lorem.paragraph(sentence_count: 9),
            address: addr.full_address,
            apt_number: rand(2) == 1 ? addr.secondary_address : nil,
            full_address: full_address,
            user_id: user.id,
            price: rand(250..500) * 100,
            longitude: center_point[:lng] + rand(-0.02..0.02),
            latitude: center_point[:lat] + rand(-0.02..0.02),

            start_date: start_date,
            end_date: end_date,

            amenities: amenities.sample(rand(amenities.length) + 1),

            referral_code: affiliate_user.referral_code,

            active: true,
            status: 1
        )
        listing.images.attach(io: File.open(listing_photos[0][:path]), filename: listing_photos[0][:file])
        listing.images.attach(io: File.open(listing_photos[1][:path]), filename: listing_photos[1][:file])
    end
end



listings_confirm = Listing.where(user_id: confirmed_user.id, status: 1).limit(4)
listings_skip = Listing.where(user_id: skip_confirm_user.id, status: 1).limit(3)

puts "Generating reservations"
# fake some reservations with corresponding charges
for i in listings_confirm do
    user = payment_test_user

    reservation_listing_id = i.id
    res_start_date = i.start_date.to_date + 3.weeks
    res_end_date = res_start_date + 8.weeks
    reservation_params = {
        "listing_id": reservation_listing_id,
        "host_id": i.user_id,
        "start_date": res_start_date,
        "end_date": res_end_date
    }
    days = (res_end_date - res_start_date).to_i + 1

    # Make a reservation
    reservation = user.reservations.build(reservation_params)
    reservation.listing = i
    reservation.host = reservation.listing.user
    reservation.expires_at = DateTime.current + RESERVATION_EXPIRY_DAYS.days

    reservation.save

    create_request_notification(reservation)


    # calculate reservation costs
    charge_type=[0,1].sample

    # Create charges objects
    charges = PaymentServices.new(reservation).create_reservation_charges(charge_type)
end

puts "Generating conversations"
# fake some conversations (associated with user1's listings)
listings = Listing.where(user_id: confirmed_user.id).limit(user_list.length)
listings.each do |listing|
    user = user_list.delete_at(rand(user_list.length))
    convo = Conversation.create!(sender_id: user.id, recipient_id: listing.user_id, listing_id: listing.id)
    
    # fake some messages
    convo.messages.create!(context: "Hello from #{convo.sender.first_name}", user_id: convo.sender_id)
    1.upto(4) do |i|
        convo.messages.create!(context: Faker::Lorem.paragraph(sentence_count: i), user_id: [convo.sender_id, convo.recipient_id].sample)
    end
end

puts "Finished seeding!"