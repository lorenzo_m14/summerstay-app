# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_22_235205) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.integer "position"
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "charges", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "status", default: 0
    t.date "due_by"
    t.integer "stripe_fee"
    t.integer "platform_cost"
    t.integer "deposit"
    t.integer "total_cost"
    t.integer "tax"
    t.integer "discount"
    t.integer "charge_type"
    t.integer "charge_num"
    t.string "payment_method_type"
    t.string "payment_method_id"
    t.string "stripe_charge_id"
    t.string "invoice_url"
    t.string "receipt_url"
    t.datetime "paid_at"
    t.uuid "reservation_id"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reservation_id"], name: "index_charges_on_reservation_id"
    t.index ["user_id"], name: "index_charges_on_user_id"
  end

  create_table "claims", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "user_type"
    t.integer "claim_type"
    t.text "content"
    t.uuid "user_id"
    t.uuid "reservation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reservation_id"], name: "index_claims_on_reservation_id"
    t.index ["user_id"], name: "index_claims_on_user_id"
  end

  create_table "conversations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "sender_id"
    t.uuid "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "listing_id"
    t.uuid "reservation_id"
    t.index ["listing_id"], name: "index_conversations_on_listing_id"
    t.index ["reservation_id"], name: "index_conversations_on_reservation_id"
    t.index ["sender_id", "recipient_id"], name: "index_conversations_on_sender_id_and_recipient_id"
  end

  create_table "listings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "status", default: 0
    t.string "listing_type"
    t.integer "bedroom"
    t.integer "beds"
    t.float "bathroom"
    t.string "listing_name"
    t.text "summary"
    t.string "address"
    t.string "amenities", default: [], array: true
    t.date "start_date"
    t.date "end_date"
    t.integer "price"
    t.boolean "active", default: false
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
    t.uuid "referral_code_id"
    t.string "apt_number"
    t.jsonb "full_address", default: {}, null: false
    t.boolean "ready", default: false
    t.boolean "booked", default: false
    t.boolean "bookable", default: true
    t.datetime "first_booked_at"
    t.integer "display_price"
    t.datetime "ready_at"
    t.index ["full_address"], name: "index_listings_on_full_address", using: :gin
    t.index ["referral_code_id"], name: "index_listings_on_referral_code_id"
    t.index ["user_id"], name: "index_listings_on_user_id"
  end

  create_table "messages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "context"
    t.uuid "user_id"
    t.uuid "conversation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "notifications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "notification_type", null: false
    t.string "notifiable_type"
    t.datetime "read_at"
    t.uuid "recipient_id", null: false
    t.uuid "actor_id"
    t.uuid "notifiable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content"
    t.string "notification_url"
    t.index ["recipient_id", "notification_type"], name: "index_notifications_on_recipient_id_and_notification_type"
    t.index ["recipient_id"], name: "index_notifications_on_recipient_id"
  end

  create_table "payouts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "status", default: 0
    t.date "due_by"
    t.integer "payout_amount"
    t.integer "payout_num"
    t.integer "ss_fee"
    t.string "stripe_transfer_id"
    t.string "stripe_payout_id"
    t.string "merchant_id"
    t.uuid "reservation_id"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "jid"
    t.boolean "is_referral", default: false
    t.index ["reservation_id"], name: "index_payouts_on_reservation_id"
    t.index ["user_id"], name: "index_payouts_on_user_id"
  end

  create_table "referral_codes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code"
    t.uuid "user_id"
    t.index ["user_id"], name: "index_referral_codes_on_user_id"
  end

  create_table "reservation_costs", force: :cascade do |t|
    t.integer "price"
    t.integer "renter_service_fee"
    t.integer "total_rent"
    t.integer "num_weeks"
    t.integer "deposit"
    t.integer "stripe_fee"
    t.integer "continuous_fee"
    t.integer "tax"
    t.integer "discount"
    t.integer "total_cost"
    t.integer "destination_amount"
    t.integer "charge_type"
    t.string "payment_method_type"
    t.string "payment_method_id"
    t.integer "deposit_status", default: 0
    t.uuid "reservation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subscription_id"
    t.string "sub_schedule_id"
    t.integer "sub_status"
    t.datetime "sub_start_date"
    t.index ["reservation_id"], name: "index_reservation_costs_on_reservation_id"
  end

  create_table "reservations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "status", default: 0
    t.uuid "user_id"
    t.uuid "listing_id"
    t.uuid "host_id"
    t.uuid "canceller_id"
    t.date "start_date"
    t.date "end_date"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "request_message"
    t.uuid "referral_code_id"
    t.datetime "approved_at"
    t.datetime "declined_at"
    t.index ["listing_id"], name: "index_reservations_on_listing_id"
    t.index ["referral_code_id"], name: "index_reservations_on_referral_code_id"
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "authentication_token"
    t.string "provider"
    t.string "uid"
    t.string "fb_image"
    t.string "phone_number"
    t.text "description"
    t.string "stripe_id"
    t.string "merchant_id"
    t.integer "unread", default: 0
    t.uuid "referral_code_id"
    t.string "referral_url"
    t.integer "connect_account_status", default: 0
    t.string "school_name"
    t.integer "id_verify_status", default: 0
    t.string "user_type"
    t.string "id_verify_token"
    t.boolean "receive_marketing", default: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["referral_code_id"], name: "index_users_on_referral_code_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "charges", "reservations"
  add_foreign_key "charges", "users"
  add_foreign_key "claims", "reservations"
  add_foreign_key "claims", "users"
  add_foreign_key "listings", "referral_codes"
  add_foreign_key "listings", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "payouts", "reservations"
  add_foreign_key "payouts", "users"
  add_foreign_key "referral_codes", "users"
  add_foreign_key "reservation_costs", "reservations"
  add_foreign_key "reservations", "listings"
  add_foreign_key "reservations", "referral_codes"
  add_foreign_key "reservations", "users"
  add_foreign_key "users", "referral_codes"
end
