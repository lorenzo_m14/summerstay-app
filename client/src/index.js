import React from 'react';
//import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'typeface-roboto';

import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { store } from './store/storeConfig.js'


// TODO: Test to see if this works.
// Turn off console logging in production
// function noop() {}
// if (process.env.NODE_ENV === 'production') {
//   console.log = noop;
//   console.warn = noop;
//   console.error = noop;
// }

render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  )
// import * as serviceWorker from './serviceWorker';
//ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
