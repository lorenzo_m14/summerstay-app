import { CLIENT_URL } from '../constants/index.js'
import router from './sitemap-routes'
import Sitemap from 'react-router-sitemap'

const sitemapUrl = CLIENT_URL ? CLIENT_URL : 'https://www.thesummerstay.com'

const sitemap = (
  new Sitemap(router)
    .build(sitemapUrl)
    .save("./public/sitemap.xml")
);