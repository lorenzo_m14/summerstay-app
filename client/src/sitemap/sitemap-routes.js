import React from 'react';
import { Switch, Route} from 'react-router-dom'

export default 
(
  <Route>
    <Route exact path="/" />

    {/* listings pages */}
    <Route exact path="/listings" />
    <Route exact path="/listings/view/:id" />

    {/* static pages */}
    <Route exact path="/host" />
    <Route exact path="/campus-managers" />
    <Route exact path="/how-it-works" />
    <Route exact path="/payment-plans" />

    {/* footer pages */}
    <Route exact path="/terms" />
    <Route exact path="/support" />
    <Route exact path="/privacy" />
    <Route path="/policies" />
    {/* <Route exact path="/community-guidelines" component={CommunityGuidelines} /> */}

    {/* authentication pages */}
    <Route path="/login" />
    <Route path="/signup" />

    <Route path="/dashboard" />

    {/* TODO: view profile */}
    {/* <Route path="/users/:id" component={Profile} /> */}
  </Route>
); 
