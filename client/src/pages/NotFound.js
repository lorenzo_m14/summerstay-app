import React, { Component } from 'react';
import '../CSS/App.css';
import ErrorImage from '../Assets/error.png'
import { Helmet } from 'react-helmet';

class NotFound extends Component {

  render() {
    return (
      <div  style={{marginTop:70}}>
        <Helmet>
            <title>404 Not Found - SummerStay</title>
        </Helmet>
       
       <div style={{display:'flex', position:'relative',flexDirection:'column', alignItems:'center', width:'100%'}}>
            
            <img src={ErrorImage} width='70%' alt="not found"/>
            <div style={{position:'absolute', alignItems:'center', top: 150, margin: 40}}>
              <h1>Sorry, we couldn't find the page you were looking for.</h1>
            </div>
        </div>
      </div>
      
    ); 
  }
}

export default NotFound;