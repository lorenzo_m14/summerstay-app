import React, {Component} from 'react';

import HomeOptionBox from '../components/home/HomeOptionBox';
import Footer from '../components/footer.js'
import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import { Helmet } from 'react-helmet';

import DarkBackground from '../Assets/darkBackground.svg'
import QuestionAnswers from '../components/utilities/questionAnswers.js'

import '../CSS/App.css';
import '../CSS/Headers.css';
import '../CSS/Static.css'
import '../CSS/Buttons.css'

import InfoDisplayBoxes from '../components/utilities/infoDisplayBoxes.js'

import HostGuarenteed from '../Assets/host-icons/IconHostGuaranteedPayouts@2x.png'
import TotalControl from '../Assets/host-icons/IconHostTotalControl@2x.png'
import HostRental from '../Assets/host-icons/IconHostYourRental@2x.png'

import { connect } from 'react-redux'

const sellingPoints = [{title: 'List your place', summary: 'Explain what’s special, show it off with photos, and set your rent price.' },
                      {title: 'Find the perfect match', summary: 'We’ll market your sublet to students and interns looking for housing.'},
                      {title: 'Collect rent payments', summary: 'Through SummerStay, receive guaranteed rent payments directly to your bank account.'}]

const hostFAQ = [{question: <p className="NoMargin PoliciesHeader">Will I know who’s subletting my apartment?</p>, answer: <div className="NoMargin"><p>Of course. SummerStay gives hosts the ability to accept or decline all requests to book. Hosts are able to view the profile of applicants to find more information. We encourage hosts and renters to familiarize themselves using our chat function to make sure all living requirements are met.</p><Button title="Read more about how we verify renter identities" target='_blank' link='/policies/how_does_summerstay_verify_renters' type='PurpleTransparent InlineText' /></div>},
                  {question: <p className="NoMargin PoliciesHeader">How will I get paid?</p>, answer: <div className="NoMargin"><p>You will create a Stripe account in the dashboard where you can connect a bank account or debit card. You will receive bi-weekly payments directly to your account. </p><Button title='Read more about the payout process' target='_blank' link='/policies/how_will_i_get_paid' type='PurpleTransparent InlineText' /></div>},
                  {question: <p className="NoMargin PoliciesHeader">Can I report incidents during the rental period?</p>, answer: <div className="NoMargin"><p>SummerStay uses a Claim Form to report any issues and we are always an email away.</p><Button title='Read more about the claims process' target='_blank' link='/policies/how_can_i_file_a_claim' type='PurpleTransparent InlineText' /></div>},
                  {question: <p className="NoMargin PoliciesHeader">Are there requirements to listing my apartment?</p>, answer: <div className="NoMargin"><p>All of the requirements for listing your place can be found by going through the hosting process. The main requirements will include verifying your identity and listing all of the amenities and details of your place.</p><Button title='Read more about hosting your place on SummerStay' target='_blank' link='/policies/how_can_i_sublet_my_place_what_information_is_required' type='PurpleTransparent InlineText' /></div>},
                  {question: <p className="NoMargin PoliciesHeader">I have a question about hosting that is not addressed here.</p>, answer: <div className="NoMargin"><p>Send us an email at support@thesummerstay.com and our team will get back to you as soon as possible!</p><Button title='Get Support' target='_blank' link='/support' type='PurpleTransparent InlineText' /></div>}]

class HostLanding extends Component {
  constructor(props){
    super(props)
    this.state={

    }
  }

  componentDidMount(){
    window.scrollTo(0, 0);
  }

  pageScroll=()=>{
    window.scrollTo(0, this.myRef.offsetTop)
  }

  render(){
  const isSmallComputer = this.props.appSize.isSmallComputer
  const isLargePhone = this.props.appSize.isLargePhone
  const isMediumPhone = this.props.appSize.isMediumPhone

  const valueStyling = isSmallComputer ?  {display:'flex', marginBottom: 20} : {display:'flex', height: 230}
  const valueHeaderStyling = isSmallComputer ? {display:'flex', flexDirection:'column', marginLeft: 0, marginBottom: 20} : {display:'flex', flexDirection:'column', marginLeft: 40,height: 230}
  return (
    <div className="StaticMain">
      <Helmet>
        <title>List Your Place - SummerStay</title>
      </Helmet>
      <div ref={ (ref) => this.myRef=ref } className='HostHeaderImage'>
        <div className="StandardMargin" style={{flex: 1, marginRight: '10vw',  alignItems:'flex-end', justifyContent:'center'}}>
          <HomeOptionBox />
        </div>
      </div>

      {/* Icon Info Boxes */}
      <div className="StandardMargin" style={{flexDirection: isMediumPhone ? 'column' : 'row', marginTop: 80, marginBottom: 80, justifyContent:'space-between', alignItems: isMediumPhone ? 'center' : 'flex-start'}}>
        <div className='SmallIconTextBox'>
          <div>
            <img src={HostGuarenteed} style={{height: 70}} alt="Security Icon"/>
          </div>
          <h2 className='SoftHeader' style={{marginBottom: 0}}>Guaranteed payouts</h2>
          <p className='GrayText'>We’ll take a security deposit from your renter to give you peace of mind. Even if they miss a payment, we won’t.</p>
        </div>
        <div className='SmallIconTextBox'>
          <div>
            <img src={TotalControl} style={{height: 70}} alt="Money Icon"/>
          </div>
          <h2 className='SoftHeader' style={{marginBottom: 0}}>Total control</h2>
          <p className='GrayText'>Set your rent, dates, rules, and more. The subletting process is controlled by you.</p>
        </div>
        <div className='SmallIconTextBox'>
          <div>
            <img src={HostRental} style={{height: 70}} alt="Home Icon"/>
          </div>
          <h2 className='SoftHeader' style={{marginBottom: 0}}>Your rental, your way</h2>
          <p className='GrayText'>You’ll review all booking details before you decide to accept. We will make the process clear.</p>
        </div>
      </div>

      <div style={{background: '#F6F9FC' , marginTop: 20}}>
        <div className="StandardMargin" style={{marginTop: 80, marginBottom: 50}}>
          <p style={{fontWeight: 900,color: '#6c4ef5'}}>HOW SUBLETTING WORKS</p>
          <div style={{display:'flex', flexDirection: isLargePhone ? 'column' :'row',justifyContent: 'space-between'}}>
            <h1 style={{ width: isLargePhone ? '100%': '30vw', margin: 0}}>It's subletting made simple.</h1>
            <p className="SubTitleText" style={{width: isLargePhone ? '100%': '30vw'}}>Quickly market your apartment, compare booking requests, and collect rent payments sent straight to your bank account.</p>
          </div>
        </div>
      </div>
          
      {/* Host Info Boxes*/}
      <div style={{background: 'linear-gradient(to bottom, #F6F9FC 50%, #fff 50%)'}}>
        <div className="StandardMargin" >
          
          <InfoDisplayBoxes highlightColor='#6c4ef5' details={sellingPoints}/>
        </div>
      </div>
      

      {/* Host Activation*/}
      {/* <div style={{background:`url(${DarkBackground})`}}>
        <div className="StandardMargin" style={{flexWrap:'wrap',paddingTop: 100, paddingBottom: 50}}>

          <div style={{display:'flex', flexDirection:isSmallComputer? 'column' : 'row',
          justifyContent:isSmallComputer ? 'center' :'space-between', alignItems: isSmallComputer?'center':'flex-start' }}>

            <div style={{display:'flex', flexDirection:'column',width: isSmallComputer ?'80%' :'30vw', minWidth:200 }}>
              
              <div style={valueHeaderStyling}>
                <h1 style={{marginTop: 0, color: '#fff'}}>Take control of your sublet experience.</h1>
                <div style={{display:'flex', justifySelf:'flex-end'}}>
                  <NLButton onClick={this.pageScroll} title="LIST YOUR PLACE" type="UnderlinedButton White" />
                </div>
              </div> 
              
              <div style={valueStyling}>
                <Control style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                <div>
                  <h2 style={{marginTop: 10,color: '#fff'}}>Total control</h2>
                  <p className="SubTitleText" style={{color: '#fff'}}>Set your rent, dates, rules, and more. The subletting process is controlled by you.</p>
                </div>
              </div> 
            </div>
          
            <div style={{display:'flex', flexDirection:'column',width:isSmallComputer ?'80%' :'30vw', minWidth:200}}>
              <div style={valueStyling}>
                <Image style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                <div>
                  <h2 style={{marginTop: 10,color: '#fff'}}>Your rental, your way</h2>
                  <p className="SubTitleText"style={{color: '#fff'}}>You’ll review all booking details before you decide to accept. We will make the process clear.</p>
                </div>
              </div> 
              <div style={valueStyling}>
                <Check style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                <div>
                  <h2 style={{marginTop: 10,color: '#fff'}}>Guaranteed payouts</h2>
                  <p className="SubTitleText"style={{color: '#fff'}}>We’ll take a security deposit from your renter to give you peace of mind. Even if they miss a payment, we won’t.</p>
                </div>
              </div> 
            </div>
          </div>
        
        </div> */}
      {/* </div> */}
      <div className="StandardMargin" style={{marginTop: 120, marginBottom: 100}}>
          <QuestionAnswers details={hostFAQ}>
            <div style={{color: '#2E3440', paddingRight: 20 }}>
              <h1 style={{marginTop: 0}}>Frequently Asked Questions</h1>
              <h3 style={{marginBottom: 0, marginLeft: 20}}>More questions about hosting?</h3>
              <p className="SubTitleText" style={{marginTop: 0, marginLeft: 20}}>See our <Button title='policies' link='/policies' type='InlineText LargeText' /> page<br/>Or <Button title='contact us' link='/support' type='InlineText LargeText' /> directly.</p>
            </div>
          </QuestionAnswers>
        </div>

      <Footer />
    </div>
  )}
}
const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(HostLanding)
