import React, { Component } from 'react'
import '../CSS/App.css'
import '../CSS/ListingsMain.css'
import { GET, POST } from '../constants/requests.js'
import { SAVE_ROUTE } from '../constants/functions.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import MessageBox from '../components/messageBox.js'
import BookingBox from '../components/payment/bookingBox.js'
import Footer from '../components/footer.js'
import Info from '../components/listing/info.js'
import { DISPLAY_NUMBER } from '../constants/functions.js'
import ReactGA from 'react-ga'

import ImageCarousel from '../components/utilities/imageCarousel.js'

import NoPhoto from '../Assets/noPhoto.png'
import ModalWrapper from '../components/utilities/Modal.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import Paper from '@material-ui/core/Paper'
import AvatarWrapper from '../components/utilities/avatar.js'
import { Helmet } from 'react-helmet';

import { connect } from 'react-redux'
import { Route } from 'react-router-dom'

// need component did mount that makes a request to the server for the listing
class ListingSub extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedListing: null,
      listingCostParams: [],
      showChat: false,
      selectedConversation: null,
      loading: false,
      initailDates: null,
      showReserve: false,
      width: window.innerWidth
    }
    // this.initailDates = null
  }
  componentWillMount () {
    window.scrollTo(0, 0)
  }
  componentDidMount () {
    this.setState({ loading: true })
    window.addEventListener('resize', this.handleWindowSizeChange)
    let path = this.props.location.pathname.split('/')
    const id = path[path.length - 1]
    // TODO: REROUTE IF LISTING IS INACTIVE
    GET(`/api/v1/listings/${id}`)
      .then(data => {
        if (data.listing.active) {
          this.setState({
            selectedListing: data.listing,
            listingCostParams: data.cost_params // THERE IS SOME ISSUE HERE
          })
          this.setState({ loading: false })
        } else {
          this.props.history.replace(`/listings/edit/${id}`)
        }
      })
      .catch(error => {
        this.setState({ loading: false })
      })
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth })
  }

  createConversation = history => {
    if (this.props.user !== null) {
      ReactGA.event({
        category: 'Booking',
        action: 'Clicked message host', 
        label: 'Message Host'
      })
      this.setState({ loading: true })
      const send_data = {
        listing_id: this.state.selectedListing.id,
        recipient_id: this.state.selectedListing.host.id
      }
      POST('/api/v1/conversations', send_data)
        .then(data => {
          this.setState({
            showChat: true,
            loading: false,
            selectedConversation: data.conversation.id
          })
        })
        .catch(error => {
          this.setState({
            showError: true,
            loading: false
          })
        })
    } else {
      
      SAVE_ROUTE(`/listings/view/${this.state.selectedListing.id}`)
      history.push('/login')
    }
  }

  render () {
    const { classes, appSize } = this.props
    const listing = this.state.selectedListing
    const isMobile = appSize.isSmallComputer
    const image_data =
      listing && listing.images
        ? listing.images.map(image => ({ source: image.url }))
        : [{ source: NoPhoto }]
    const cost_params = this.state.listingCostParams

    const title = listing && listing.listing_name
    const headMetadata = (
      <Helmet>
          <title>{title} - SummerStay</title>
      </Helmet>
    )

    return (
      <div style={{ marginTop: 70 }}>
        {listing && headMetadata}
        <CustomLoading loading={this.state.loading} />
        <ModalWrapper
          open={this.state.showChat}
          onClose={() => this.setState({ showChat: false })}
          style={{ maxWidth: 620 }}
        >
          <MessageBox
            enableChat={this.state.showChat}
            selectedConversation={this.state.selectedConversation}
          />
        </ModalWrapper>
        {
          listing && !isMobile ? 
            <div style={{ display: 'flex', flexDirection: 'column', marginTop: 20}}>
              
              <div className="StandardMargin">
                <ImageCarousel imageData={image_data}/>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center'}} >
                  <Info listing={listing} isMobile={isMobile}/>
                  <div style={{ position: 'sticky', top: 0}}>
                    <BookingBox listing={listing} cost_params={this.state.listingCostParams} dates={this.state.initailDates}/>
                    {
                      listing.host ? 
                        <Paper elevation={1} className='HostBox'>
                          <div style={{ display: 'flex', alignItems:'center' }}>
                            
                            <AvatarWrapper user={listing.host} size='medium'/>
                            <h3 style={{margin: 0, marginLeft: 10}}> {listing.host.first_name} </h3>
                          </div>
                          <p className="SoftText">Lingering questions? Send {listing.host.first_name} a message to learn more.</p>
                          <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
                            <Route render={({ history }) => (
                                <NLButton title='Message' onClick={() => this.createConversation(history)} type='MediumButton' />
                              )}
                            />
                          </div>
                        </Paper>
                      : null
                    }
                  </div>
                </div>
              </div>
              <Footer />
            </div>
          : null
        }
        {
          listing && isMobile && !this.state.showReserve ?
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <ImageCarousel imageData={image_data}/>
            <div className="StandardMargin">
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
                <Info listing={listing} isMobile={isMobile}/>

                <div style={{position: 'fixed', display: 'flex', alignItems: 'center', justifyContent: 'center',  bottom: 0,
                  width: '100vw', zIndex: 1, paddingTop:20, paddingBottom: 20, backgroundColor: 'white' }}>
                  <div style={{ display: 'flex', flex: 1, justifyContent: 'space-between' , alignItems:'center',
                         marginLeft: '10vw', marginRight: '10vw'}}>
                    <div style={{display:'flex', alignItems:'flex-end'}}>
                      <h2 style={{margin:0}}>${DISPLAY_NUMBER(listing.price*(1+cost_params.service_fee))}</h2>
                      <h4 style={{margin:0}}>&nbsp;per week</h4>
                    </div>
                    <div>
                      <NLButton title='Check Booking' type='MediumButton' onClick={() => this.setState({ showReserve: !this.state.showReserve })} />
                    </div>
                  </div>
                </div> 
              </div>
            </div>
            <Footer />
          </div>
          : null
        }
        {
          listing && isMobile && this.state.showReserve ? 
             
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', backgroundColor:'#f9f9fc',
                          height: 'calc(100vh - 150px)', marginBottom: 80, overflow:'auto'}}>
              <BookingBox listing={listing} cost_params={this.state.listingCostParams} dates={this.state.initailDates} isMobile={isMobile} />
              {
                listing.host ? 
                  <Paper elevation={1} className='HostBox'>
                    <div style={{ display: 'flex', alignItems:'center' }}>
                      <AvatarWrapper user={listing.host} size='medium'/>

                      <h3 style={{margin: 0, marginLeft: 10}}> {listing.host.first_name} </h3>
                    </div>
                    <p className="SoftText">Lingering questions? Send the host a message to learn more.</p>
                    <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
                      <Route render={({ history }) => (
                          <NLButton title='Message' onClick={() => this.createConversation(history)} type='MediumButton' />
                        )}
                      />
                    </div>
                  </Paper>
                : null
              }
              <div style={{position: 'fixed', display: 'flex', alignItems: 'center', justifyContent: 'center',  
                      bottom: 0,width: '100vw', zIndex: 1,height: 80, backgroundColor: 'white' }}>
                <div style={{ display: 'flex', flex: 1, justifyContent: 'space-between' , alignItems:'center',
                        marginLeft: '10vw', marginRight: '10vw'}}>
                  <div style={{display:'flex', alignItems:'flex-end'}}>
                    <h2 style={{margin:0}}>${DISPLAY_NUMBER(listing.price*(1+cost_params.service_fee))}</h2>
                    <h4 style={{margin:0}}>&nbsp;per week</h4>
                  </div>
                  <div>
                    <NLButton title='Close' type='MediumButton' onClick={() => this.setState({ showReserve: !this.state.showReserve })} />
                  </div>
                </div>
              </div>
            </div>
          : null
        }
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user,
    appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(ListingSub)
