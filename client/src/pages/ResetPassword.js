import React, { Component } from 'react';
import '../CSS/App.css';
import { PUT, GET } from '../constants/requests.js'
import { PASSWORD_ERROR } from '../constants/index.js'
import { PARSE_QUERY, LOCAL_PASSWORD_CHECK } from '../constants/functions.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import CircleLoad from '../components/utilities/circleLoad.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import Button from '../components/utilities/Button.js'
import { Helmet } from 'react-helmet';

import { CustomTextbox } from '../constants/inputs.js'

import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux'
import { loginUser } from '../store/actions/passThrough.js'

// need component did mount that makes a request to the server for the listing
class ResetPassword extends Component {
    constructor(props){
        super(props);
        this.state = {
            reset_password_token: '',
            password: '',
            passwordConfirmed: '',
            loading: false, 
            resetLoading: false,
            error: false,
            route:'/'
        };
    }

    componentDidMount=()=>{        
        const query = PARSE_QUERY(window.location.search)
        if('reset_password_token' in query){
            this.setState({loading: true})
            const { reset_password_token } = query
            // this request checks if the token is valid
            GET(`/api/v1/users/password/edit?reset_password_token=${reset_password_token}`)
            .then(data => {
                this.setState({ 
                    reset_password_token,
                    loading: false
                 })
            })
            .catch(error => {
                console.log(error)
                this.setState({ loading: false, error: true  })

            })
        }
        else{
            console.log("Reset password token not found")
            this.setState({ loading: false, error: true })

        }
    }

    submitReset = () => {
        // TODO: check if passwords are secure, probably on server side
       let error = false
        if(!LOCAL_PASSWORD_CHECK(this.state.password)){
            error= true
            this.setState({passError: true})
        }else{
            this.setState({passError: false})
        }
        if(this.state.password !== this.state.passwordConfirmed){
            error= true
            this.setState({passConfirmError: true})
        }else{
            this.setState({passConfirmError: false})
        }

        if(!error && !this.state.resetLoading) {
            this.sendResetRequest();
        }
    }

    sendResetRequest = () => {
        this.setState({resetLoading: true})

        const send_data = { 
            reset_password_token: this.state.reset_password_token,
            password: this.state.password,
            password_confirmation: this.state.password_confirmation,
        }

        PUT(`/api/v1/users/password`, send_data)
        .then(data => {
            console.log("Successfully reset password")
            this.props.onLoginUser(data.user);

            // TODO: Success and rerouting logic
            this.props.history.push('/dashboard/listings')
        })
        .catch(error => {
            this.setState({resetLoading: false, error: true})
            // TODO: Handle error, probably would be a server-side password error
        })
    }
    handlePassword=(type, event)=>{
        let val = event.target.val
        if(type === 'password' && this.state.passError && LOCAL_PASSWORD_CHECK(val) ){
            this.setState({passError: false})
        }
        if(type === 'passwordConfirmed' && this.state.passConfirmError && this.state.password === val){
            this.setState({passConfirmError: false})
        }
        this.setState({[type]: event.target.value})
    }
  // TODO: Fix validation error messages, and className prop
  render() {
    const rootStyling = {padding: 20, marginTop: 30,width:'calc(95vw - 40px)',maxWidth: 400}
    return (
        <div style={{display:'flex', justifyContent:'center',marginTop:70, background:'#f9f9fc', minHeight:'calc(100vh - 70px)'}}>
            <Helmet>
                <title>Reset Password - SummerStay</title>
            </Helmet>
            <CustomLoading loading={this.state.loading}/>
            <div>
            {
                !this.state.loading && !this.state.error &&
                <Paper elevation={1} style={rootStyling}>
            
                    <div style={{display:'flex', flexDirection:'column'}}>
                        <h1 style={{margin: 0, marginBottom: 0}}>Reset Password</h1>  
                        {
                            this.state.passError ? <p style={{margin: 0, marginTop:10, color:'#e62929'}}>{PASSWORD_ERROR}</p>: null
                        }
                        {
                            this.state.passConfirmError ? <p style={{margin: 0, marginTop:10, color:'#e62929'}}>Your passwords do not match.</p>: null
                        }
                        <div style={{height: 20}}/>
                        <CustomTextbox
                            error={this.state.passError}
                            required={true}
                            value={this.state.password}
                            id="outlined-search"
                            label="New Password"
                            type='password'
                            full
                            // className={classes.textField}
                            onChange={(event)=>this.handlePassword('password', event)}
                        />
                        <div style={{height:10}}/>
                        <CustomTextbox
                            error={this.state.passConfirmError}
                            required={true}
                            value={this.state.passwordConfirmed}
                            id="outlined-search"
                            label="Confirm Password"
                            type='password'
                            full
                            // className={classes.textField}
                            onChange={(event)=>this.handlePassword('passwordConfirmed', event)}
                        />
                        <div style={{height:20}}/>
                        {
                            !this.state.resetLoading ?
                            <NLButton title ="Reset Password" onClick={this.submitReset} type="LargeButton"/>
                            : <CircleLoad/>
                        }
                        
                    </div>
                    
                
                </Paper>
                
            }
            {
                !this.state.loading && this.state.error &&
                <Paper elevation={1} style={rootStyling}>
            
                    <div style={{display:'flex', flexDirection:'column'}}>
                        <h1 style={{margin: 0}}>Reset Password</h1>  
                        <p>It seems there was a problem with your request. Try restarting the processes <Button title ="here" link="/login" type="InlineText"/> or <Button title ="contact us" link="/support" type="InlineText"/> if the problem persists.</p>
                    
                    </div>
                </Paper>
            }
            </div>
        </div>
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user, 
  }
}
const mapDispatchToProps  = dispatch => {
  return{
      onLoginUser: (user) => dispatch(loginUser(user)),       
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ResetPassword);
