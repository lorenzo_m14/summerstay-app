import React, { Component } from 'react';
import '../CSS/App.css';
import { POST } from '../constants/requests.js'
import { PARSE_QUERY } from '../constants/functions.js'

import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import Paper from '@material-ui/core/Paper';

import Payment from '../Assets/payment.png'

import { connect } from 'react-redux'
import { loginUser } from '../store/actions/passThrough.js'


// need component did mount that makes a request to the server for the listing
class HandlePayment extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            error: false, 
            returnRoute: '/dashboard/payouts',
        };
    }

    componentDidMount(){
        const paymentState = JSON.parse(localStorage.getItem('paymentState'))
        const returnRoute = JSON.parse(localStorage.getItem('returnRoute'))

        if(returnRoute && returnRoute.route){
          this.setState({returnRoute: returnRoute.route})
          localStorage.removeItem('returnRoute')
        }
               
        if(paymentState === null){
          this.setState({
            error: true, 
            loading: false
          })
          return
        }

        const query = PARSE_QUERY(this.props.location.search)

        if(query.state === paymentState){
            // Request to server Stripe connect callback endpoint
            // Returns user with valid merchant_id
            const send_data = { code: query.code }
            POST('/api/v1/payouts/connect_callback', send_data)
            .then(data => {
              localStorage.removeItem('paymentState')
              this.props.onLoginUser(data.user) 
              this.setState({ loading: false })
            })
            .catch(error => {
              this.setState({
                error:true, 
                loading: false
              })
            }); 
        }else{
          this.setState({
            error:true, 
            loading: false
          })
        }
        
    }
    handleFinish=()=>{

      this.props.history.replace(this.state.returnRoute)
    }


  render() {
    const rootStyling = { padding: 20, marginTop: 30,width:'calc(95vw - 40px)',maxWidth: 400 }
    return (
        <div style={{display:'flex', justifyContent:'center',marginTop:70, background:'#f9f9fc', minHeight:'calc(100vh - 70px)'}}>
          <CustomLoading loading={this.state.loading}/>
          <div>
            {
              !this.state.loading && !this.state.error ? 
                <Paper elevation={1} style={rootStyling}>
                  <h1 style={{margin: 0}}>Host Account Confirmed</h1>
                  <p className="GrayText">Thank you for creating a SummerStay Host Account. Continue to return to payouts or click <Button title ="here" link="/dashboard/listings" type="InlineText"/> to create a listing.</p>
                  <div style={{display:'flex', justifyContent:'center'}}>
                    <div>
                      <img src={Payment} width="90%" height="auto" alt='Process'/>
                    </div>
                  </div>
                  <NLButton title='Continue' onClick={this.handleFinish} type="LargeButton"/> 
                </Paper> : null
            }
            {
              !this.state.loading && this.state.error ? 
                <Paper elevation={1} style={rootStyling}>
                  <div style={{display:'flex', flexDirection:'column'}}>
                    <h1 style={{margin: 0}}>Account Setup Error</h1> 
                    <p>It seems there was a problem with setting up your Stripe Connect Account. The information you provided is invalid. Try restarting the process <Button title ="here" link="/dashboard/payouts" type="InlineText"/> or <Button title ="contact us" link="/support" type="InlineText"/> if the problem persists.</p>
                  </div>
                </Paper> : null
            }
          </div>
        </div>
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user, 
  }
}
const mapDispatchToProps  = dispatch => {
  return{
      onLoginUser: (user) => dispatch(loginUser(user)),   
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(HandlePayment)
