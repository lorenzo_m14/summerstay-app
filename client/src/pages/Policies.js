import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'
import Button from '../components/utilities/Button.js'
import { Helmet } from 'react-helmet';

import Footer from '../components/footer'
import ExpansionPanelWrapper from '../components/utilities/expansionPanelWrapper.js'

import Booking from '../Assets/policies/FAQbooking.png'
import Changes from '../Assets/policies/FAQchanges.png'
import Hosting from '../Assets/policies/FAQhosting.png'
import ID from '../Assets/policies/FAQIDverification.png'
import Payment from '../Assets/policies/FAQpayment.png'
import Support from '../Assets/policies/FAQsupport.png'


import '../CSS/App.css';
import '../CSS/Headers.css';
import '../CSS/Static.css'

const policies = {
  Header_1:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={Booking} alt="Booking"/><h2>Renters - Booking</h2></div>,
    questions:[{question: <p className="PoliciesHeader">How do I check if a place is available?</p>, answer: <p className="NoMargin"><ul>
<li>As long as you enter your move-in and move-out dates when searching, all listings that appear will be available during your stay.</li>
<li>Additionally, the calendar on a listing will show you available dates (look out for bolded dates).&nbsp;</li>
<li>If you would like to communicate directly with the host to verify available dates you can send them a message before going through the booking process. You&rsquo;ll find the chat feature directly below the &lsquo;Check Booking&rsquo; box.</li>
</ul></p>, route: 'how_do_i_check_if_a_place_is_available'},
    {question: <p className="PoliciesHeader">What is the minimum sublet duration?</p>, answer: <p className="NoMargin">The minimum required sublet duration is 4 weeks. You can utilize the filters on the Find Your Stay page to search for places that are available for the dates you need.</p>, route: 'what_is_the_minimum_sublet_duration'},
    {question: <p className="PoliciesHeader">How do I book a place? What does check booking mean?</p>, answer: <p className="NoMargin"><ul>
<li>In order to book a place, you will use the booking process on a listing (check booking).&nbsp;</li>
<li>When you check a booking, you are committing to submitting a reservation request. If you have any hesitations, feel free to message the host using the messaging box on the listing.</li>
<li>The booking process will allow you to select your dates, payment plan, payment type and also verify your ID if you haven&rsquo;t already.</li>
</ul></p>, route: 'how_do_i_book_a_place_what_does_check_booking_mean'},
    {question: <p className="PoliciesHeader">How can I contact the host before booking?</p>, answer: <p className="NoMargin">To reach out directly to the host of the listing before sending a booking request, use the chat feature on the listing page. Your last name is always hidden before a reservation is approved.</p>, route: 'how_can_i_contact_the_host_before_booking'},
    {question: <p className="PoliciesHeader">What should I do if something goes wrong during the booking?</p>, answer: <p className="NoMargin">SummerStay uses a Claim Form to report any issues. The form can be found attached to an apartment under the <Button title='Listings tab' link='/dashboard/listings' /> in <Button title='My Dashboard' link='/dashboard' />. Claims are automatically linked to a reservation. Once a claim is submitted, it is reviewed and both parties will be directly contacted.</p>, route: 'what_should_i_do_if_something_goes_wrong_during_the_booking'}
    ]
  },
  Header_2:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={Payment} alt="Payment"/><h2>Renters - Payment</h2></div>,
    questions:[{question: <p className="PoliciesHeader">What payment methods does SummerStay accept?</p>, answer: <p className="NoMargin"><ul>
<li>You may use a Credit/Debit card (which incurs a 3% processing fee) or connect your bank account ($5.25 per transaction processing fee).&nbsp;</li>
<li>You will be prompted to add your preferred payment type when you book a listing. You may update your payment information for an existing reservation by clicking on the <Button title='Stays' link='/dashboard/stays' /> tab of your dashboard.</li>
</ul></p>, route: 'what_payment_methods_does_summerstay_accept'},
    {question: <p className="PoliciesHeader">What are the Pay-As-You-Stay and Standard Payment Programs?
</p>, answer: <p className="NoMargin"><ul>
<li><strong>Pay-As-You-Stay Program</strong>: A plan that allows you to pay during their stay with automatically-charged bi-weekly payments that begin 21 days after move-in date. You are still charged the first payment upon acceptance of a booking request. Charges occur following the &lsquo;Payment Schedule&rsquo; attached to your stay.</li>
<li><strong>Standard Payment Program:</strong> You will pay an initial payment (2 weeks of rent, deposit, and all stripe fees) that is charged automatically when a booking request is accepted. Your second and remaining payment will be due 14 days before move-in. If reservation approval occurs within 14 days of move-in, both payments are processed at approval.&nbsp;</li>
</ul></p>, route: 'what_are_the_payasyoustay_and_standard_payment_programs'},
    {question: <p className="PoliciesHeader">How do we securely process payments?</p>, answer: <p className="NoMargin">We use <a href='https://stripe.com/about' target='_blank'>Stripe</a> for all debit and credit card transactions and <a href="https://plaid.com/company/" target='_blank'>Plaid</a> to connect directly to your bank account.
</p>, route: 'how_do_you_securely_process_payments'},
    {question: <p className="PoliciesHeader">What fees will I be charged?</p>, answer: <p className="NoMargin"><ul>
<li><strong>Pay-As-You-Stay Fee:</strong> We require an additional 9.5% financing fee for this payment program.&nbsp;</li>
<li><strong>Processing Fee:</strong> The Fee is calculated dependent on your payment method. When you use a credit card, Stripe charges a 3% Processing Fee. For bank accounts, the fee is $5.25 per transaction.&nbsp;</li>
</ul></p>, route: 'what_fees_will_i_be_charged'},
    {question: <p className="PoliciesHeader">When am I charged for a reservation?</p>, answer: <p className="NoMargin"><ul>
<li>Regardless of your payment plan you will be charged an initial payment, which includes a refundable security deposit, once the host confirms your booking request.&nbsp;</li>
<li><strong>Pay-As-You-Stay Plan: </strong>Payments being 21 days after move-in date. You will automatically be charged every 14 days via your preferred payment method.</li>
<li><strong>Standard Payment Plan:</strong> The remainder of your rent is due 14 days before your move-in date.</li>
</ul></p>, route: 'when_am_i_charged_for_a_reservation'},
    {question: <p className="PoliciesHeader">Do I need to pay for utilities?</p>, answer: <p className="NoMargin">Unless specifically contradicted by the host as part of a listing, all utilities are included in the rental price. </p>, route: 'do_i_need_to_pay_for_utilities'},
    {question: <p className="PoliciesHeader">When will my Security Deposit be returned?
</p>, answer: <p className="NoMargin">SummerStay will return your security deposit within 30 days of move-out if there have been no damages incurred to your Stay. Deposits are returned to your preferred payment method. If your deposit cannot be returned to your preferred payment method, our team will reach out to you directly.
</p>, route: 'when_will_my_security_deposit_be_returned'},
    {question: <p className="PoliciesHeader">What should I do if someone asks me to pay outside of the SummerStay website?
</p>, answer: <p className="NoMargin">We strongly advise against handling payments outside of our platform as we cannot issue refunds if there is a problem and your host cannot be held responsible in case of fraud or misconduct. </p>, route: 'what_should_i_do_if_someone_asks_me_to_pay_outside_of_the_summerstay_website'}
    ]
  },
    Header_3:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={Hosting} alt="Hosting"/><h2>Hosting and Subletting</h2></div>,
    questions:[{question: <p className="PoliciesHeader">Am I allowed to sublease through SummerStay?</p>, answer: <p className="NoMargin">In the interest of maintaining the integrity of SummerStay listings, we only allow subleasing with the express permission and management by the property owner themselves. If you are considering subleasing your rental property, check your lease and get permission from your host before listing your space on SummerStay.</p>, route: 'am_i_allowed_to_sublease_through_summerstay'},
    {question: <p className="PoliciesHeader">Who can sublet on SummerStay?
</p>, answer: <p className="NoMargin"><ul>
<li>SummerStay targets college students and interns during the summer, but anyone is welcome to sublet their place through SummerStay. Please see our terms and conditions for more detailed information on subletting through SummerStay.&nbsp;</li>
<li><Button title='Click' link='/host' /> to host your place on SummerStay and begin your journey to finding a subletter.&nbsp;</li>
</ul></p>, route: 'who_can_sublet_on_summerstay'},
    {question: <p className="PoliciesHeader">How can I sublet my place? What information is required?
</p>, answer: <p className="NoMargin"><ul>
<li>Visit our <Button title='host page' link='/host' /> and start the listing process by specifying what type of your place you have and entering your address.&nbsp;</li>
<li>You will need to go through the listing process and fill out the request information in order to list your place on SummerStay.&nbsp;</li>
<li>You will need to specify 22 or more days that you are looking to sublet your place for, specify location, amenities, listing title and summary, and upload pictures. For the safety of you and any future renters, ID verification is required in order to host and rent on SummerStay.&nbsp;</li>
</ul></p>, route: 'how_can_i_sublet_my_place_what_information_is_required'},
    {question: <p className="PoliciesHeader">How can I edit my listing details?
</p>, answer: <p className="NoMargin"><ul>
<li>You will be able to find all of your listings in the <Button title='dashboard' to='/dashboard/listings' /> where you can view and edit them.&nbsp;</li>
<li><strong><em>In Progress</em> Listing</strong>- before you&rsquo;ve completed your listing it can only be seen by you in the dashboard. You will be able to edit any and all information before making the listing public.&nbsp;</li>
<li><strong><em>Active</em> Listing</strong> - once your listing is active it can be viewed by potential renters.</li>
</ul></p>, route: 'how_can_i_edit_my_listing_details'},
    {question: <p className="PoliciesHeader">How is my listing’s location shown on the map? Is my address visible?
</p>, answer: <p className="NoMargin"><ul>
<li><strong>Map Location - </strong>In order to maintain your privacy, we show only the general location of your place using a circular marker. This will allow potential renters the ability to see how close your place is to other important places without giving away the exact location.&nbsp;</li>
<li><strong>Address Privacy</strong> - We will only show your address to verified renters and it is up to your discretion to share your address to potential renters through direct messages.</li>
</ul></p>, route: 'how_is_my_listings_location_shown_on_the_map_is_my_address_visible'},
    {question: <p className="PoliciesHeader">How should I price my place?
</p>, answer: <p className="NoMargin"><ul>
<li>Ultimately, pricing is up to you but there are a couple of things you should consider when pricing your place.&nbsp;</li>
<li><strong>Competitive Pricing - </strong>take into consideration the general pricing of your place in comparison to other places with similar features. Your place might have some competitive advantages such as being closer to public transportation or having in-unit washers and dryers. Make sure to elaborate on the competitive features of your place in your listing description. In general, you will increase the likelihood of renting out your place if you charge a price that is reasonable compared to similar places in your area.&nbsp;</li>
<li><strong>Utilities and fees - </strong>utilities and all other secondary fees should be baked into the rental price that you charge for your place. As you will still be responsible for handling rent and utilities/fees payments while your subletter stays at your place, for simplicity's sake, we ask that you take those secondary costs into consideration and include them in your flat rental cost.</li>
</ul></p>, route: 'how_should_i_price_my_place'},
    {question: <p className="PoliciesHeader">What does it cost to list on SummerStay? </p>, answer: <p className="NoMargin">Putting your listing on SummerStay is completely free. If you decide to accept a booking request, SummerStay receives a 6% fee of your listed rental price. This fee includes both the service fee and payment processing fee that is charged by Stripe. This service fee allows us to operate and grow our business and allows you to find a verified renter and recoup the losses you would take otherwise.
</p>, route: 'what_does_it_cost_to_list_on_summerstay'},
    {question: <p className="PoliciesHeader">Why is the price of my listing higher than the amount I put?
</p>, answer: <p className="NoMargin"><ul>
<li>You set your rental price which is shown in search and on your listing. This gives you full control over how your place is shown to potential renters anywhere on our site.&nbsp;</li>
<li>We add an additional 5% base to your listing that is shown to renters. This Service Fee cost to the renter allows us to pay our vendors and guarantee your payouts, even if they miss a payment. On top of the rental price that you list we charge processing fees for different payment options and, in some cases, a Pay-As-You-Stay financing fee for renters who choose that payment method.&nbsp;</li>
</ul></p>, route: 'why_is_the_price_of_my_listing_higher_than_the_amount_i_put'},
    {question: <p className="PoliciesHeader">How will I get paid?</p>, answer: <p className="NoMargin">SummerStay uses <a href='https://stripe.com/about' target='_blank'>Stripe</a> to manage all payment information. Hosts will be prompted to set up a Stripe account before they are able to accept a booking. Hosts will have the option to link Stripe to a Debit Card or Bank Account. Scheduled payments to Hosts begin after Renter move-in day to ensure protection against fraudulent advertising. Rent payments are wired on a bi-weekly schedule. Payment schedules can be found in <Button title='Dashboard payouts' link='/dashboard/payouts' /> or in your Stripe account.</p>, 
    route: 'how_will_i_get_paid'},
    {question: <p className="PoliciesHeader">How do I edit or change my payout method?
</p>, answer: <p className="NoMargin"><ul>
<li>Go to the payouts tab in the dashboard <Button title='payouts' link='/dashboard/payouts' /> where you can edit and add payout methods using the Stripe dashboard.&nbsp;</li>
<li>There are two payout payment types that you can connect: a bank account or debit card. You will not incur any additional charges for payouts.&nbsp;</li>
<li>All payouts will be handled through the stripe dashboard which is a secure 3rd party payment platform.&nbsp;</li>
</ul></p>, route: 'how_do_i_edit_or_change_my_payout_method'},
    {question: <p className="PoliciesHeader">When will I get my payout(s)?
</p>, answer: <p className="NoMargin">The initial payouts will start 14 days after move-in and continue on a bi-weekly schedule until all payments are fulfilled, paid through your Stripe Connect account. Stripe can take up to 7 business days to complete a payment.
</p>, route: 'when_will_i_get_my_payouts'},
    {question: <p className="PoliciesHeader">Who will be responsible for paying my landlord?
</p>, answer: <p className="NoMargin"><ul>
<li>Hosts are directly responsible for paying their landlord as normal.</li>
<li>Using SummerStay has no effect on your existing lease contract with your landlord. You will need to personally handle payments to your landlord which will likely remain the same as when you are occupying your place.</li>
</ul></p>, route: 'who_will_be_responsible_for_paying_my_landlord'},
    {question: <p className="PoliciesHeader">What should I do if I won’t be there on move-in or move-out day?
</p>, answer: <p className="NoMargin"><ul>
<li>Coordinate with your subletter to ensure that they can access the place upon arrival and safely leave anything that you will need from them upon departure.&nbsp;</li>
<li>In most major cities you can find secure key exchange services such as <a href='https://keycafe.com/' target='_blank'>Key Cafe</a> which allows you to leave your key and provide access remotely. Additionally, you can place a secure lockbox with an access code for your future renters.&nbsp;</li>
</ul></p>, route: 'what_should_i_do_if_i_wont_be_there_on_move_in_or_move_out_day'},
    {question: <p className="PoliciesHeader">What should I do if my renter damages my place?</p>, answer: <p className="NoMargin">SummerStay uses a Claim Form to report any issues. The form can be found attached to an apartment under the <Button title='Listings tab' link='/dashboard/listings' /> in <Button title='My Dashboard' link='/dashboard' />. Claims are automatically linked to a reservation. Once a claim is submitted, it is reviewed and both parties will be directly contacted.</p>, route: 'what_should_i_do_if_my_renter_damages_my_place'}
    ]
  },
    Header_4:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={ID} alt="ID"/><h2>ID Verification</h2></div>,
    questions:[{question: <p className="PoliciesHeader">How does SummerStay verify hosts?
</p>, answer: <p className="NoMargin"><ul>
<li>In order to list a place on SummerStay, we require that hosts verify their identity by utilizing <a href='https://passbase.com/' target='_blank'>PassBase</a>, a third-party ID verification service which verifies all hosts using a government-issued ID.&nbsp;</li>
<li>Additionally, our team has additional security protocols to verify the accuracy of all listings on our site.</li>
</ul></p>, route: 'how_does_summerstay_verify_hosts'},
    {question: <p className="PoliciesHeader">How does SummerStay verify renters?
</p>, answer: <p className="NoMargin">In order to complete a booking request, we require that all potential renters verify their identity by utilizing <a href='https://passbase.com/' target='_blank'>PassBase</a>, a third-party ID verification service which verifies all renters using a government-issued ID.
</p>, route: 'how_does_summerstay_verify_renters'},
    {question: <p className="PoliciesHeader">Why do I need to verify my Identity?</p>, answer: <p className="NoMargin">We value your safety and success when using our website. We want both hosts and renters to know that whoever they’re talking to is who they claim to be and is willing to authenticate their identity. This allows us to provide a safe and secure platform and minimize the amount of fake and scam listings on our site.
</p>, route: 'why_do_i_need_to_verify_my_identity'}
    ]
  },
    Header_5:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={Changes} alt="Changes"/><h2>Changes and Cancellations</h2></div>,
    questions:[{question: <p className="PoliciesHeader">Can I cancel a booking request (as a renter)?
</p>, answer: <p className="NoMargin">In order to cancel a booking request, you'll need to send an email to our team at <a href="mailto:support@thesummerstay.com">support@thesummerstay.com</a>. Please use the email you signed up with, and include the listing name and dates requested.</p>, route: 'can_i_cancel_a_booking_request_as_a_renter'},
    {question: <p className="PoliciesHeader">What happens if the renter cancels a reservation?
</p>, answer: <p className="NoMargin">Unless extenuating circumstances exist, a renter who cancels a confirmed booking will forfeit all amounts paid to date, up to the full rental amount; however, the security deposit will remain refundable. The host who suffered the cancellation shall receive a convenience payment of 50% of the amounts collected from a renter who canceled the booking. Notwithstanding the foregoing, a renter may cancel the booking after check-in in the event the host materially misrepresented the nature or condition of the Premises, amenities provided or other information in the Listing. In such cases, the renter will receive a full refund.
</p>, route: 'what_happens_if_the_renter_cancels_a_reservation'},
    {question: <p className="PoliciesHeader">What happens if the host cancels a reservation?
</p>, answer: <p className="NoMargin"><ul>
<li>In the unlikely event that this occurs, renters will receive a full refund via the same payment method that you used to book the place.&nbsp;</li>
<li>Please contact us directly by sending us an email at <a href="mailto:support@thesummerstay.com">support@thesummerstay.com</a> with the subject line &ldquo;my booking was canceled&rdquo; and we will respond immediately to help you out.&nbsp;</li>
<li>Visit our <Button title='support page' link='/support' /> for more. 
</li>
</ul></p>, route: 'what_happens_if_the_host_cancels_a_reservation'},
    {question: <p className="PoliciesHeader">Can I extend my reservation?</p>, answer: <p className="NoMargin">You may extend a reservation by creating a new booking request to the same place that you are currently staying in. We don’t yet have a feature that allows you to extend an existing reservation but you may book a new reservation as long as those dates are open and the current host approves. Be sure to note that you cannot book a place with a start date that is the same day as when you are making the booking. Be sure to book at least a day in advance.
</p>, route: 'can_i_extend_my_reservation'}
    ]
  },
  Header_6:{
    title: <div style={{display:'flex', alignItems:'center'}}><img style={{height: 50, marginRight:10}} src={Support} alt="Support"/><h2>Support</h2></div>,
    questions: [
    {question: <p className="PoliciesHeader">How can I get help?
</p>, answer: <p className="NoMargin">Please visit our <Button title='support page' link='/support' /> to find all of our contact info.</p>, route: 'how_can_i_get_help'},
    {question: <p className="PoliciesHeader">How can I file a claim?
</p>, answer: <p className="NoMargin">SummerStay uses a Claim Form to report any issues. The form can be found attached to an apartment under the <Button title='Listings tab' link='/dashboard/listings' /> in <Button title='My Dashboard' link='/dashboard' />. Claims are automatically linked to a reservation. Once a claim is submitted, it is reviewed and both parties will be directly contacted.</p>, route: 'how_can_i_file_a_claim'}
    ]
  }
  
}

class Policies extends Component {
  constructor(props) {
    super(props)
    
    // Creating an object to contain the refs for each qa element
    // these refs are used for scrolling to the component on did mount for easy question finding
    this.references = Object.values(policies).reduce((polArray, p)=>polArray.concat(p.questions),[])
    .reduce((polRefs, p) => {
      polRefs[p.route] = React.createRef()  
      return polRefs;
    }, {});
    
  }
  componentDidMount() {
    window.scrollTo(0, 0)
    const path = this.props.location.pathname.split('/')

    if(path.length >= 2){
      const scrollLoc = this.references[path[2]]
      if (scrollLoc) {
        window.scrollTo(0, scrollLoc.current.offsetTop)
      } else {
        this.props.history.push('/policies')
      }
    }
    
  }

  render() {

    return (
      <div className='StaticMain'>
        <Helmet>
          <title>Policies - SummerStay</title>
        </Helmet>
        <div className='StandardMargin'>
          <h1 style={{textAlign:'center'}}>SummerStay's Policies</h1>
          
            {
              Object.keys(policies).map(key =>
                <div style={{marginBottom: 50}}>
                  
                    {policies[key].title}
                  
                  {
                    policies[key].questions.map(p=>
                      <div key={p.route} ref={this.references[p.route]}>
                        <ExpansionPanelWrapper
                          // onChange={()=>this.props.history.replace(`policies/${p.route}`)}
                          expanded={this.props.location.pathname === `/policies/${p.route}`}
                          link={`/policies/${p.route}`}
                          question={p.question}
                          answer={
                            <Route exact path={`/policies/${p.route}`}  
                              render={(props) => p.answer}
                            />
                          }
                        />
                      </div>
                    )
                  }
                </div>

                
              )
            }
           
            {
            
                
                    // <ExpansionPanelWrapper
                    //   {...props}
                    //   onChange={()=>this.props.history.replace(`policies/${p.route}`)}
                    //   expanded={this.props.location.pathname === p.route}
                    //   question={p.question}
                    //   answer={p.answer}
                    // />
                 
            }
          
        </div>
        <Footer />
      </div>

    )
  }
}

const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(Policies)
