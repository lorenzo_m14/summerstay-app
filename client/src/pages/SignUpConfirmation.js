import React, { Component } from 'react';
import '../CSS/App.css';
import { GET, POST } from '../constants/requests.js'
import { PARSE_QUERY } from '../constants/functions.js'
import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import Payment from '../Assets/payment.png'
import { Helmet } from 'react-helmet';

import Paper from '@material-ui/core/Paper';

import CustomLoading from '../components/utilities/loadingBar.js'

import { connect } from 'react-redux'
import { loginUser } from '../store/actions/passThrough.js'

// need component did mount that makes a request to the server for the listing
class SignUpConfirmation extends Component {
    constructor(props){
        super(props);
        this.state = {
            confirmed: false, 
            loading: true, 
            error: null,
            errorMessage: '',
            route:'/listings'
        }
    }

    componentDidMount=()=>{
        const query = PARSE_QUERY(window.location.search)
        const rr = JSON.parse(localStorage.getItem('returnRoute'))

        if('confirmation_token' in query){
            
            GET(`/api/v1/users/confirmation?confirmation_token=${query.confirmation_token}`)
            .then(data => {
                this.props.onLoginUser(data.user);

                if(rr !== null && rr.route !== "TEMP"){
                    localStorage.removeItem('returnRoute');
                    
                    this.setState({
                        loading: false,
                        route: rr.route
                    })
                }else if(rr !== null && rr.route === "TEMP"){ // add return toute logic    
                    const tempData = JSON.parse(localStorage.getItem('tempData'))
                    const send_data = {
                        listing: {
                            listing_type: tempData.roomType,
                            address: tempData.address.location,
                            latitude: tempData.address.lat,
                            longitude: tempData.address.lng,
                            full_address: tempData.address.fullAddress,
                        }
                    }

                    POST('/api/v1/listings', send_data)
                    .then(data => {
                        this.setState({
                            loading: false, 
                            route: `/listings/edit/${data.listing.id}`
                        })
                    })
                    .catch(error => {
                        this.setState({
                            error: 'listing',
                            loading: false, 
                            route: '/dashboard/listings'
                        })
                    })

                }else{
                    this.setState({ 
                        loading: false, 
                        route: '/dashboard/listings'
                    })
                }
                
            })
            .catch(error => {
                const errorKeys = Object.keys(error.error)
                const formattedErrorKeys = errorKeys.map(k => k.replace('_', ' '))
                const firstErrorMessage = error.error[errorKeys[0]]

                this.setState({ 
                    loading: false, 
                    error: 'account',
                    errorMessage: firstErrorMessage
                })
            })

        }else{
            this.setState({ 
                loading: false, 
                error: 'code'
            })
        }
    }

    handleFinish=()=>{
      this.props.history.replace(this.state.route)
    }

  render() {
      const rootStyling = { padding: 20, marginTop: 30,width:'calc(95vw - 40px)',maxWidth: 400 }
    return (
        <div style={{display:'flex', justifyContent:'center',marginTop:70, background:'#f9f9fc', minHeight:'calc(100vh - 70px)'}}>
            <Helmet>
                <title>Sign Up Confirmation - SummerStay</title>
            </Helmet>
            <CustomLoading loading={this.state.loading}/>
            <div>
                {
                    !this.state.loading && !this.state.error ? 
                        <Paper elevation={1} style={rootStyling}>
                            <h1 style={{margin: 0}}>Account Confirmed</h1>
                            <p className="GrayText">Thank you for creating a SummerStay account. Continue to make a listing or click <Button title ="here" link="/listings" type="InlineText"/> to find a SummerStay.</p>
                            <div style={{display: 'flex', justifyContent: 'center'}}>
                                <div>
                                    <img src={Payment} width="90%" height="auto" alt='Process'/>
                                </div>
                            </div>
                            <NLButton title='Continue' onClick={this.handleFinish} type="LargeButton"/> 
                        </Paper> : null
                }
                {
                    !this.state.loading && this.state.error ? 
                        <Paper elevation={1} style={rootStyling}>
                            <div style={{display:'flex', flexDirection:'column'}}>
                                <h1 style={{margin: 0}}>Sign Up Confirmation</h1> 
                                {
                                    this.state.error !== 'listing' &&
                                    <div style={{marginTop: 15}}>
                                        <p>{this.state.errorMessage}</p>
                                    </div>
                                } 
                                {
                                    this.state.error === 'listing' &&
                                    <div>
                                        <p>We were able to confirm your account but unable to create the listing you started. Sorry about that. You can quickly create your listing in the Dashboard.</p>
                                        <NLButton title='Dashboard' onClick={this.handleFinish}type="LargeButton"/> 
                                    </div>
                                }
                            </div>
                        </Paper> : null
                }
            </div>
        </div>
    )}
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user, 
  }
}
const mapDispatchToProps  = dispatch => {
  return{
      onLoginUser: (user) => dispatch(loginUser(user)),       
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(SignUpConfirmation);
