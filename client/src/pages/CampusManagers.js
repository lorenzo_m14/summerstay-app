import React, { Component } from 'react'
import '../CSS/App.css'
import '../CSS/Static.css'

import { connect } from 'react-redux'

import Footer from '../components/footer.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import ModalWrapper from '../components/utilities/Modal.js'
import TypeForm from '../components/utilities/typeform.js'
import { Helmet } from 'react-helmet';

import ManagerGirl from '../Assets/campus-managers-boys-laptop.jpg'

class CampusManagers extends Component {
  constructor(props){
    super(props)
    this.state={
      show: false
    }
  }
  componentDidMount(){
    window.scrollTo(0, 0);
  }
  render() {
    const isLargePhone = this.props.appSize.isLargePhone
    const isMediumPhone = this.props.appSize.isMediumPhone

    return (
      <div className="StaticMain" style={{display: 'flex'}}>
        <Helmet>
          <title>Campus Managers - SummerStay</title>
        </Helmet>
        <ModalWrapper
          open={this.state.show}
          onClose={()=>this.setState({show: false})}
          style={{padding: 0, maxWidth: 600}}
        >
          <TypeForm formUrl='https://thesummerstay.typeform.com/to/RS946H'/>
        </ModalWrapper>
        
        <div className='LandingMain'>
          <div className='LandingHeader'>
            {
              !isLargePhone ? 
              <div className='GradientCut'/>
              : null
            }
            <div style={{ zIndex: 2 }}>
              <h1 className="TitleText">Become a Campus Manager today</h1>
              <p className="SubTitleText">SummerStay’s Campus Manager position is a unique opportunity for students looking to gain employment experience at a startup.</p>
            </div>
          </div>
          <img src={ManagerGirl} className='LandingImage' />
        </div>

        <div className="TighterMargin" ref={this.myRef} style={{ marginTop: 50, marginBottom: 50, alignItems: 'center'}}>
          <div style={{maxWidth: 700, display: 'flex', flexDirection: 'column'}}>
            <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginBottom: 70, marginTop: 70}}>
              <div style={{display:'flex', flex: 1, backgroundColor:'#21222f', height:'1px',  marginRight: 20}}/>
              <h1 style={{margin: 0}}>Vision</h1>
              <div style={{display:'flex', flex: 1,backgroundColor:'#21222f', height:'1px',  marginLeft: 20}}/>
            </div>
            

            <p className="SubTitleText">SummerStay was founded with the purpose of creating a student-centered housing marketplace. Through our platform students market their sublets, and renters apply to and book those listings and pay deposits and weekly rent. Meanwhile, our technology requires ID-Verification, protects against fraud, and guarantees rent payment. Whether it’s for a month, a year or sometime in between, students rely on SummerStay for safe and reliable subletting experiences.</p>
            <p className="SubTitleText">We’re hiring Campus Managers to drive product and brand growth. You’ll be working directly with our founding team and building out your network with Campus Managers across the country.</p>
            {/* <Divider/> */}

            <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginBottom: 70, marginTop: 70}}>
              <div style={{display:'flex', flex: 1, backgroundColor:'#21222f', height:'1px',  marginRight: 20}}/>
              <h1 style={{margin: 0}}>What to expect when you join us</h1>
              <div style={{display:'flex', flex: 1,backgroundColor:'#21222f', height:'1px',  marginLeft: 20}}/>
            </div>
            {/* <h3 style={{fontSize: 30, color:'#333333'}}>What You’ll Do</h3> */}
            <p className="SubTitleText">You will spend your semester ideating and executing growth and marketing strategies. Senior members of our leadership and product teams will work alongside you to support and fund your ideas.<br></br><br></br>You’ll have complete freedom to define your own growth and management strategies on-campus. Whether it be primarily social-media, event organization and promotion, guerrilla marketing, or a different path entirely. The Campus Manager position is openly defined to encourage strategies tailored to your strengths. There’s also no set time commitment; work and earn on your schedule.</p>
            <h2>Core Responsibilities</h2>
            <ul>
              <li className="SubTitleText">Drive on-campus user growth</li>
              <li className="SubTitleText">Own marketing and growth strategies from ideation to execution</li>
              <li className="SubTitleText">Work alongside the founding team of a growth-driven startup</li>
              <li className="SubTitleText">Learn and grow as you gain experience</li>
              <li className="SubTitleText">Be the face of SummerStay on-campus</li>
            </ul>
            <h2>Qualifications</h2>
            <ul>
              <li className="SubTitleText">Excellent communication skills, with a natural ability to present, articulate and effectively communicate your message</li>
              <li className="SubTitleText">Creative thinker that can drive a message through social media platforms, event coordination, guerrilla marketing, and in-person conversation</li>
              <li className="SubTitleText">Ambitious, organized, and design-oriented</li>
              <li className="SubTitleText">Proven ability to excite and engage your peers</li>
              <li className="SubTitleText">History of taking leadership roles on-campus</li>
            </ul>
            <h2>Benefits</h2>
            <p className="SubTitleText">We greatly reward Campus Managers who show strong initiative and great engagement. Therefore we offer a result-based pay scale that:</p>
            <ul>
              <li className="SubTitleText">Referral bonus on every successful listing on-campus</li>
              <li className="SubTitleText">Referral bonus on every successful user on-campus</li>
              <li className="SubTitleText">Additional bonuses are paid on a case-by-case basis for Campus Managers that show exceptional leadership and exceed expectations</li>
            </ul>
            <p className="SubTitleText">We also want to make sure our people are taken care of. You’ll receive:</p>
            <ul>
              <li className="SubTitleText">Resume-building marketing, design, coordination, and communication experience</li>
              <li className="SubTitleText">Experience growing a startup and building a brand on day one</li>
              <li className="SubTitleText">Letters of recommendation for future internships & employment opportunities</li>
              <li className="SubTitleText">Access to national network of Campus Managers</li>
              <li className="SubTitleText">Free swag!</li>
            </ul>
            {/* <Divider/> */}
            <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginBottom: 20, marginTop: 70}}>
              <div style={{display:'flex', flex: 1, backgroundColor:'#21222f', height:'1px',  marginRight: 20}}/>
              <h1 style={{margin: 0}}>Apply today</h1>
              <div style={{display:'flex', flex: 1,backgroundColor:'#21222f', height:'1px',  marginLeft: 20}}/>
            </div>
            {/* <h3 style={{fontSize: 30, color:'#333333'}}>Application Information</h3> */}
            <p className="SubTitleText">Once we have identified that you have the qualifications, expect a few messages followed by a video call or an in-person interview. Click below to start your application, good luck!</p>
            <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginBottom: 20}}></div>
            <NLButton title="Campus Manager Application" onClick={()=>this.setState({show: true})} type="LargeButton"/>
            <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginTop: 50}}></div>
            <p className="SubTitleText">If you’d like to personally reach out with questions, please email our Head of Recruiting at alex@thesummerstay.com.</p>
          </div>
        </div>
        <div style={{width:'100%', marginTop: 50}}>
          <Footer/>
          </div>
      </div>      
    ); 
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user,
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(CampusManagers)