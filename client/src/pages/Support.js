import React from 'react'
import Footer from '../components/footer'
import Button from '../components/utilities/Button.js'
import { Helmet } from 'react-helmet';

import '../CSS/App.css';
import '../CSS/Headers.css';
import '../CSS/Static.css'

export default function Support(props) {

  window.scrollTo(0, 0)

  return (
    <div className='StaticMain'>
      <Helmet>
          <title>Support - SummerStay</title>
      </Helmet>
      <div className='StandardMargin'>
          <h1>Support</h1>
          <div>
            <h3>Contact Us</h3>
            <p>Please email us with any questions or concerns - <a href='mailto:support@thesummerstay.com' className='Purple'>support@thesummerstay.com</a>.</p>
            <h3>Policies and FAQ</h3>
            <p>Have a question? Check out our <Button title='policies page' link='/policies' type='Purple InlineText' /> where you might find an answer.</p>
          </div>
      </div>

      <div style={{width:'100%', marginTop: 50}}>
        <Footer/>
      </div>
    </div>
  );
}