import React, { Component } from 'react';
import '../CSS/App.css';
import ErrorImage from '../Assets/error.png'

class NoMatch extends Component {

  render() {
    return (
      <div  style={{marginTop:70}}>
       
       <div style={{display:'flex', position:'relative',flexDirection:'column', alignItems:'center', width:'100%'}}>
            
            <img src={ErrorImage} width='70%' alt="no listings"/>
            <div style={{position:'absolute', top: 150, margin: 40}}>
              <h1>Something bad happened, we're sorry...</h1>

            </div>
            
        </div>
      </div>
      
    ); 
  }
}

export default NoMatch;