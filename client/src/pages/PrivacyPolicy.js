import React from 'react'
import Footer from '../components/footer'
import { Helmet } from 'react-helmet';

import '../CSS/App.css'
import '../CSS/Headers.css'
import '../CSS/Static.css'


export default function PrivacyPolicy(props) {

  window.scrollTo(0, 0)
  
  return (
    <div className='StaticMain'>
      <Helmet>
          <title>Privacy Policy - SummerStay</title>
      </Helmet>
      <div className='StandardMargin'>
        <h1>Privacy Policy</h1>

        <h2>PRIVACY POLICY OF SUMMERSTAY, LLC,</h2>
        <h3>a Delaware limited liability company</h3>
        <p>Last updated: October 1th, 2019</p>
        <p>THIS PRIVACY POLICY (these “Terms”) govern access to and use of the Platform, and constitute a legally binding agreement between SummerStay, LLC, a Delaware limited liability company (the “Company”, “we”, “us”, or “our”), and parties who interact with the Platform. Capitalized terms used herein have the meanings set forth in Section 2 hereof.</p>

        <h3>1. Privacy Policy.</h3>
        <p>
        1.1.   Introduction.
        </p>
        <p>
        1.1.1. The Company respects Users’ privacy and is committed to protecting it through compliance with this Privacy Policy. This Privacy Policy describes the types of information the Company may collect from Users or that Users may provide when
        visiting the Platform, and the Company’s practices for collecting, using, maintaining, protecting, and disclosing that information.
        </p>
        <p>
        1.1.2. This Privacy Policy applies to information the Company collects: (i) on its Website;
        </p>
        <p>
        (ii)    in email, text, and other electronic messages between Users and this Website;
        </p>
        <p>
        (iii)   through mobile and desktop applications Users download from this Website, which provide dedicated non-browser-based interaction between Users and this Website; (iv) when Users interact with our advertising and applications on third-party websites and services, if those applications or advertising include links to this policy; and (v) other reasonable sources.
        </p>
        <p>
        1.1.3. This Policy does not apply to information collected by: (i) the Company offline or through any other means, including (without limitation) on any other website operated by the Company or any third party; or (ii) any third party, including (without limitation) through any application or content (such as advertising) that may link to or be accessible from or on the Website
        </p>
        <p>
        1.1.4. A User that does not agree with this Privacy Policy may choose to not use the Website or otherwise access the Platform. By accessing or using this Website or the Platform, Users agree to this privacy policy.
        </p>
        <p>
        1.2.   Children Under the Age of 2. The Website is not intended for children under 2 years of age. No one under age 2 may provide any information to or on the Website. The Company does not knowingly collect personal information from children under 2. If a User is under 2, do not use or provide any information on this Website or through any of its features, register on the Website, engage in transactions through the Website, use any of the interactive or public comment features of this Website, or provide any information about oneself to us, including one’s name, address, telephone number, email address, or any screen name or user name one may use. If the Company learns that it has collected or received personal information from a child under 2 without verification of parental consent, it will delete that information. If a User believes the Company might have any information from or about a child under 2, please send an email to support@thesummerstay.com.
        </p>
        <p>
        1.3.   Information Collected by the Company. The Company may collect several types of information from and about Users of the Website, including information: (i) by which a User may be personally identified, such as name, postal address, e-mail address, telephone number (“personal information”); (ii) that is about a User but individually does not identify such User; and (iii) about a User’s internet connection, the equipment used to access our Website, and usage details. The Company may collect this information: (x) directly from a User when such User provides it to the Company; (y) automatically as a User navigates through the Website (such as usage details, IP addresses, and information collected through cookies, web beacons, and other tracking technologies); and (z) from third parties (such as business partners).
        </p>
        <p>
        1.4.   Information Provided to the Company. Information the Company may collects on or through the Website may include: (i) information that Users provide by filling in forms on the Website, including (without limitation) information provided at the time of registering to use our Website, posting material, or requesting further services; (ii) records and copies of correspondence between Users and the Company, including (without limitation) email addresses; (iii) Users’ responses to surveys that the Company might ask Users to
        complete for research purposes; (iv) details of transactions Users carry out through the Website, including (without limitation) financial information provided through the Website; and (v) Users’ search queries on the Website. Users also may provide information to be published or displayed (“posted”) on public areas of the Website, or transmitted to other Users or third parties. Such User Content posted on and transmitted to others is done at a User’s own risk. Although the Company may limit access to certain pages, no security measures are perfect or impenetrable. Additionally, the Company cannot control the actions of other Users of the Website with whom a User may choose to share User Content. Therefore, the Company cannot and does not guarantee that User Content will not be viewed by unauthorized persons.
        </p>
        <p>
        1.5.   Automatic Data Collection. As a User navigates through and interacts with the Website, the Company may use automatic data collection technologies to collect certain information about the User’s equipment, browsing actions, and patterns, including (without limitation) details of visits to the Website, and information about a User’s computer and internet connection (such as the IP address, operating system, and browser type). The Company also may use these technologies to collect information about the User’s online activities over time and across third-party websites or other online services (behavioral tracking). The information collected automatically may only include statistical data (and not personal information) but the Company may maintain it or associate it with personal information collected in other ways or received from third parties. Doing so helps the Company improve the Website, and deliver a better and more personalized service, including by enabling us to: (i) estimate audience size and usage patterns; (ii) store information about Users’ preferences, allowing customization of the Website according to individual interests; (iii) speed up searches; and (iv) recognize a User when such User returns to the Website. Technologies used for such automatic data collection may include (without limitation): (x) cookies (or browser cookies); (y) flash cookies; and (z) web beacons.
        </p>
        <p>
        1.6.   Third Parties. Some content or applications (such as advertisements) on the Website may be served by third-parties using cookies or other technologies to collect information about Users using the Website. The information collected by such third parties may be used to provide Users with interest-based (behavioral) advertising or other targeted content. The Company does not control these third parties’ tracking technologies or how they may be used. Users with questions about an advertisement or other targeted content should contact the responsible provider directly.
        </p>
        <p>
        1.7.   How Information is Used. The Company may use information collected about Users or that Users provide to the Company, including any personal information: (i) to present the Website and its contents to Users; (ii) to provide Users with information, products, or services that requested by Users from us; (iii) to fulfill any other purpose for which Users provide it; (iv) to provide Users with notices about their accounts; (v) to carry out the Company’s obligations, and enforce its rights arising from any contracts entered into between Users and the Company (such as for billing and collection); (vi) to notify Users about changes to the Website or any products or services offered or provided though it;
        (vii)   in any other way the Company may describe when Users provide the information; and (viii) for any other purpose with a User’s consent.
        </p>
        <p>
        1.8.   Disclosure of Information. The Company may disclose aggregated information about Users without restriction, and personal information collected or provided as described in this Privacy Policy: (i) to our subsidiaries and affiliates; (ii) to contractors, service
        providers, and other third parties we use to support our business; (iii) to a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of the Company’s assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information held by the Company about Users is among the assets transferred; (iv) to fulfill the purpose for which such information is provided; (v) for any other purpose disclosed by the Company when a User provides the information; and (vi) with a User’s consent. The Company may also disclose Users’ personal information: (x) to comply with any court order, law, or legal process, including to respond to any government or regulatory request; (y) to enforce or apply these Terms and relating agreements (such as for billing and collection purposes); and (z) if the Company believes such disclosure is necessary or appropriate to protect the rights, property, or safety of the Company, Users, or others.
        </p>
        <p>
        1.9.   California Residents. California law may provide California residents with additional rights regarding our use of personal information. To learn more about such California privacy rights, visit https://oag.ca.gov/privacy/ccpa. In addition, California’s “Shine the Light” law (Civil Code § 1798.83) permits Users of the Application that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an email to personalinformation@summerstay.com. .
        </p>

        <h3>2. Definitions.</h3>
        <p>
        2.1.   Certain Definitions. For the purposes of these Terms, the following terms shall have the meanings set forth below:
        </p>
        <p>
        “Account” means a account on the Platform registered by a User.
        </p>
        <p>
        “Agreement” means the legally binding agreement between a User and the Company by such User’s acceptance of these Terms, creation of an Account, and/or use of the Platform.
        </p>
        <p>
        “Application” means the mobile application(s) through which the Platform is accessible.
        </p>
        <p>
        “Company Content” means any proprietary content of the Company, and any content licensed or authorized for use by or through the Company from a third party.
        </p>
        <p>
        “Content” means Company Content, User Content, and any other content uploaded or published on the Platform.
        </p>
        <p>
        “Damage Claim” means any claim initiated by a Sublessor against a Sublessee alleging damage to the Premises, or to any personal property on or about the Premises.
        </p>
        <p>
        “Law” means any applicable federal, state, or local law or regulation, including (without limitation) zoning laws.
        </p>
        <p>
        “Lease Agreement” means a lease agreement for a Premises entered into by and between an Owner and a Sublessor.
        </p>
        <p>
        “Listing” means a Premises published by a Sublessor on the Platform.
        </p>
        <p>
        “Owner” means the legal owner or lessor of a Premises being offered for sublease by a Sublessor to a Sublessee through the Platform.
        </p>
        <p>
        “Payment Method” means a financial instrument that you have added to your Account, such as a credit card, debit card, or PayPal account.
        </p>
        <p>
        “Payout” means a payment initiated by the Company to a Member for services performed in connection with the Platform.
        </p>
        <p>
        “Payout Method” means a financial instrument that you have added to your Account, such as a PayPal account, direct deposit, a prepaid card, or a debit card.
        </p>
        <p>
        “Platform” means the Company’s online marketplace that enables Sublessors to publish Listings, and to communicate and transact directly with Sublessees. The Platform includes the Application, the Website, and any other websites, mobile applications, or other platforms through which the Company makes such services available.
        </p>
        <p>
        “Premises” means a property owned by an Owner, leased by a Sublessor, and offered by a Sublessor to a Sublessee to sublease through the Platform.
        </p>
        <p>
        “Privacy Policy” means the policy set forth in Section 1 hereof, which policy Users are deemed to have agreed to by use of the Platform.
        </p>
        <p>
        “Service Fees” means any Sublessee Fee and/or any Sublessor Fee.
        </p>
        <p>
        “Sublease Agreement” means a sublease agreement for a Premises entered into by and between a Sublessor and a Sublessee that results from the Platform.
        </p>
        <p>
        “Sublessee” means a User that subleases a Premises from a Sublessor through the Platform.
        </p>
        <p>
        “Sublessee Fees” means any fees the Company charges a Sublessee for the Company’s services, which fees are due and owing to the Company upon the provision of such services.
        </p>
        <p>
        “Sublessor” means a lessor of a Premises pursuant to a lease agreement with the Owner of such Premises, who subleases such Premises to a Sublessee through the Platform.
        </p>
        <p>
        “Sublessor Fees” means any fees the Company charges a Sublessor for the Company’s services, which fees are due and owing to the Company upon the provision of such services.
        </p>
        <p>
        “Terms” means these terms and conditions, as updated by the Company from time to time, and any other terms or policies enacted by the Company from time to time.
        </p>
        <p>
        “User” means a registered user of the Platform, including a Sublessor or Sublessee.
        </p>
        <p>
        “User Content” means any text, photos, audio, video, or other content owned or licensed by a User, and uploaded by a User onto Platform.
        </p>
        <p>
        “Website” means the Company’s website through which the Platform available, www.thesummerstay.com, including any subdomains thereof.
        </p>
        <p>
        2.2.   Other Definitions. Other defined terms have the meanings given to such terms in other Sections these Terms.
        </p>

        <h3>3. Contact Information.</h3>
        <p>To ask questions or comment about this Privacy Policy, contact us at: <a href='mailto:support@thesummerstay.com' className='SimpleButton '>support@thesummerstay.com</a></p>
      </div>

      <Footer />
    </div>

  );
}