import React, { Component } from 'react';
import '../CSS/App.css';

import { PARSE_QUERY } from '../constants/functions.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import RotatingInfo from '../components/utilities/rotatingInfo.js'
import QuestionAnswers from '../components/utilities/questionAnswers.js'
import Divider from '@material-ui/core/Divider';
import Footer from '../components/footer.js'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet';

import ChattingPeople from '../Assets/how-it-works/2-people.jpg'
import AcceptBooking from '../Assets/how-it-works/accept-booking.jpg'
import CreateListing from '../Assets/how-it-works/create-listing.jpg'
import FindPlace from '../Assets/how-it-works/find-place.jpg'
import HandKeys from '../Assets/how-it-works/hand-keys.jpg'
import ReceiveOffers from '../Assets/how-it-works/receive-bookings.jpg'
import Button from '../components/utilities/Button.js' 

const hostDetails = [{title: 'List your place', content:'List your home on SummerStay to begin receiving offers. Listings are noncommittal - no financial information is required, and hosts will review all requests to book.', img: CreateListing},
                    {title: 'Receive offers', content:'Review booking dates, your payment details, and chat with potential renters to find a sublet that works for you.', img: ReceiveOffers},
                    {title: 'Accept SummerStay', content:'When you\'re ready, accept a SummerStay. We’ll take a security deposit from the renter to help give you peace of mind.', img: ChattingPeople},
                    {title: 'Hand over keys', content:'You’ll need to hand over the keys - and you’re done. Enjoy bi-weekly rent payments straight to your bank account.', img: AcceptBooking}]

const renterDetails = [{title: 'Search', content:'Find a student apartment that you’ll love.', img: FindPlace},
                      {title: 'Request to book', content:'Choose the dates you want and send your request. You’ll only be charged your initial payment if the host accepts within five days.', img: ChattingPeople},
                      {title: 'Grab the keys', content:'Finalize move-in details, and your summer housing search is done.', img: HandKeys}] 

      
const hostFAQ = [{question: <p className="NoMargin">Will I know who’s subletting my apartment?</p>, answer:<p className="NoMargin">Of course. SummerStay gives hosts the ability to accept or decline all requests to book. Hosts are able to view the profile of applicants to find more information. We encourage hosts and renters to familiarize themselves using our chat function to make sure all living requirements are met.</p>},
                  {question: <p className="NoMargin">How will I get paid?</p>, answer:<p className="NoMargin">SummerStay uses Stripe to manage all payment information. Hosts will be prompted to set up a Stripe account before they are able to accept a booking. Hosts will have the option to link Stripe to a Debit Card or Bank Account. Scheduled payments to Hosts begin after Renter move-in day to ensure protection against fraudulent advertising. Rent payments are wired on a bi-weekly schedule. Payment schedules can be found in the My Dashboard page or Stripe account.</p>},
                  {question: <p className="NoMargin">Can I report incidents during the rental period?</p>, answer:<p className="NoMargin">SummerStay uses a Claim Form to report any issues. The form can be found attached to an apartment under the ‘Listings’ tab of the ‘My Dashboard’ section. Claims are automatically linked to a reservation. Once a claim is submitted, it is reviewed and both parties will be directly contacted.</p>},
                  {question: <p className="NoMargin">Are there requirements to listing my apartment?</p>, answer:<p className="NoMargin">Here’s a link to our Terms and Conditions.</p>},
                  {question: <p className="NoMargin">What’s the difference between payment plans?</p>, answer:<p className="NoMargin">SummerStay offers two options for payment: the Standard Plan and the Pay-As-You-Stay Plan. Both options require an Initial Payment that is withdrawn when a booking is accepted. The Standard Plan involves a singular Final Payment that is due before move-in and is paid at the Renter’s convenience. This plan is ideal for Renters who prefer to pay the entirety of their rent upfront. The second option is our Pay-As-You-Stay Plan. This plan allows Renters to pay the majority of their rent during the summer, and is ideal for students or interns who are employed during their Stay. Starting 21 days after move-in, rent payments are automatically charged from the renter’s chosen payment option until the total balance is paid. Because up-front financing is expensive, this plan involves a small additional fee that allows us to keep it offered. Additional information about this plan can be found on the Pay-As-You-Stay page.</p>},
                  {question: <p className="NoMargin">I have a question about hosting that is not addressed here.</p>, answer:<p className="NoMargin">Send us an email at support@thesummerstay.com and our team will get back to you as soon as possible!</p>}]
    
const renterFAQ = [{question: 'How does SummerStay verify hosts/renters?', answer: <p>In order to list a place on SummerStay, we require that hosts and renters verify their identity by utilizing <a href='https://passbase.com/' target='_blank'>PassBase</a>. The integration authenticates users by requiring a Government-Issued ID. </p>},
{question: 'How can I pay for my booking or get paid?', answer:<p><ul>
<li><strong>Renter - </strong>You have two options to pay for a booking - a bank account ($5.25 fee per transaction) or a credit card (3% transaction fee).&nbsp;</li>
<li><strong>Host </strong>- You will be able to add a bank account or debit card in the dashboard (you will not incur a fee for a payout).&nbsp;</li>
<li>We utilize <a href="https://stripe.com/about">Stripe</a>, a secure 3rd party payment service, which allows us to keep your information secure while processing payments quickly.&nbsp;</li>
</ul></p>},
{question: 'What if I couldn’t find the answer to my question?', answer:<p>Send our team an email at <a href='mailto:support@thesummerstay.com' targe='_blank'>support@thesummerstay.com</a>!</p>},
{question: 'Is SummerStay in my city?', answer:<p><ul>
<li>We are located in most major cities and are growing quickly as more places are listed!&nbsp;</li>
<li><Button title='Search' link='/listings' /> for your city to see if there are any active listings.</li>
</ul></p>}
]

class HowItWorks extends Component {
  constructor(props){
    super(props)
    this.state={
      display: 'hosts'
    }
  }
  
  componentDidMount(){
    window.scrollTo(0, 0)
    const currUrl = PARSE_QUERY(this.props.location.search)
    if ('FAQ' in currUrl) this.pageScroll()
  }

  pageScroll=()=>{
    window.scrollTo(0, this.myRef.offsetTop)
  }

  handleDisplay=(type)=>{
    this.setState({
      display: type
    })
  }
  render() {
    const isLargePhone = this.props.appSize.isLargePhone


    return (
      <div style={{marginTop:70, display:'flex',flexDirection: 'column',  background:'#fff'}}>
        <Helmet>
          <title>How It Works - SummerStay</title>
        </Helmet>
        <div className="StandardMargin">
          <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginBottom: 50, marginTop: 20}}>
            <div style={{display:'flex', flex: 1, backgroundColor:'#21222f', height:'1px',  marginRight: 50}}/>
            <div style={{display:'flex', justifyContent:'center'}}>
              <NLButton title='For Hosts' onClick={()=>this.handleDisplay('hosts')} 
                        type={this.state.display === 'hosts' ? 'MediumButton SolidPurple' : 'MediumButton'}/>
              <div style={{width: 50}}/>
              <NLButton title='For Renters' onClick={()=>this.handleDisplay('renters')} 
                        type={this.state.display === 'renters' ? 'MediumButton SolidPurple' : 'MediumButton'}/>
            </div>
            <div style={{display:'flex', flex: 1, backgroundColor:'#21222f', height:'1px',  marginLeft: 50}}/>
          </div>
        </div>
        <div className="StandardMargin" style={{ minHeight: 'calc(100vh - 181px)'}}>
          
            {
              this.state.display === 'hosts' &&
                <RotatingInfo details={hostDetails}>
                  <div>
                    <h1 style={{marginTop: 0}}>Hosting with SummerStay</h1>
                    <p className="SubTitleText" style={{ marginTop: 0, marginBottom: 60 }}>Learn how listing your apartment, accepting  <br/>a sublettor, and collecting rent works.</p>
                  </div>
                </RotatingInfo> 
            }
            {
              this.state.display === 'renters' && 
              <RotatingInfo details={renterDetails}>
                  <div>
                    <h1 style={{marginTop: 0}}>Renting with SummerStay</h1>
                    <p className="SubTitleText" style={{ marginTop: 0, marginBottom: 60 }}>Learn how finding your stay, requesting <br/>a sublet, and paying rent works.</p>
                  </div>
              </RotatingInfo> 
            }
          
        </div>
        <div ref={ (ref) => this.myRef=ref } className="StandardMargin" style={{ marginBottom: 200}}>
          <Divider style={{marginBottom: 100}}/>
          
          <QuestionAnswers details={renterFAQ}>
            <div style={{color: '#2E3440', paddingRight: 20 }}>
              <h1 style={{marginTop: 0}}>Frequently Asked Questions</h1>
              <h3 style={{marginBottom: 0, marginLeft: 20}}>More questions about our process?</h3>
              <p className="SubTitleText" style={{marginTop: 0, marginLeft: 20}}>See our <Button title='policies' link='/policies' type='InlineText LargeText' /> page<br/>Or <Button title='contact us' link='/support' type='InlineText LargeText' /> directly.</p>
            </div>
          </QuestionAnswers>
        </div>
        <Footer pageScroll={this.pageScroll} />
      </div>
      
    )
  }
}
const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(HowItWorks)
