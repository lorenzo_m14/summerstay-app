import React, { Component } from 'react'
import '../CSS/App.css'
import '../CSS/Static.css'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet';

import Image from '@material-ui/icons/CropOriginal'
import Check from '@material-ui/icons/CheckCircleOutline'
import Control from '@material-ui/icons/ControlCamera'
import InfoDisplayBoxes from '../components/utilities/infoDisplayBoxes.js'

import Footer from '../components/footer.js'
import Button from '../components/utilities/Button.js'
import GrayDots from'../Assets/grayDots.png'
import PurpleBackground from '../Assets/purpleBackground.svg'

import SmilingBoy from '../Assets/payment-pics/smiling-boy.jpg'
import CoupleLaptop from '../Assets/payment-pics/couple-laptop.jpg'

import Standard1 from '../Assets/payment-pics/IconStandard1.png'
import Standard2 from '../Assets/payment-pics/IconStandard2.png'
import Standard3 from '../Assets/payment-pics/IconStandard3.png'
import Room1 from '../Assets/payment-pics/room1.jpg'

import Pay1 from '../Assets/payment-pics/IconPays1.png'
import Pay2 from '../Assets/payment-pics/IconPays2.png'
import Pay3 from '../Assets/payment-pics/IconPays3.png'
import Room2 from '../Assets/payment-pics/room2.jpg'

import Cal from '@material-ui/icons/DateRange'
import ClearAll from '@material-ui/icons/ClearAll'
import CenterFocus from '@material-ui/icons/CenterFocusWeak'

import NLButton from '../components/utilities/NoLinkButton.js'


const sellingPoints = [{icon: <Cal style={{ fontSize:35,color:'#fff'}}/>, title: 'Initial deposit', summary: 'Renters are charged a security deposit and 2 weeks rent up front when a booking is accepted.' },
                      {icon: <ClearAll style={{ fontSize:35,color:'#d5d9e0'}}/>, title: 'Biweekly rent', summary: "Rent payments don't begin until 21 days after move-in."},
                      {icon: <CenterFocus style={{ fontSize:35,color:'#d5d9e0'}}/>, title: 'Deposit returned', summary: 'Security deposits are returned within 30 days of a successful stay.'}]



class PayAsYouStay extends Component {
  constructor(props){
    super(props);
    this.section1 = React.createRef() 
    this.section2 = React.createRef()
    this.section3 = React.createRef() 
    this.state = {
      selected: null
    }
  }
  
  componentDidMount(){
    window.scrollTo(0, 0);
  }
  
  movePage=(ref, index)=>{
    this.setState({selected: index})
    window.scrollTo(0, ref.current.offsetTop)
  }

  pageScroll=()=>{
    window.scrollTo(0, this.myRef.offsetTop)
  }
  
  render() {
    const navButtons =[{title:'Payments', ref:this.section1, icon: <Cal style={{ fontSize:20,color:'#d5d9e0'}}/>},
                        {title:'Financing', ref:this.section2, icon: <ClearAll style={{ fontSize:20,color:'#d5d9e0'}}/>},
                        {title:'Security', ref:this.section1, icon: <CenterFocus style={{ fontSize:20,color:'#d5d9e0'}}/>}]
    const isMediumPhone = this.props.appSize.isMediumPhone
    const isLargePhone = this.props.appSize.isLargePhone
    const isSmallComputer = this.props.appSize.isSmallComputer

    // console.log('height: ', document.getElementById('landing-photo').offsetHeight)

    return (
      <div className='StaticMain'>
        <Helmet>
          <title>Payment Plans - SummerStay</title>
        </Helmet>
        <div className='LandingMain'>
          <div className='LandingHeader' >
            { !isLargePhone ? 
             <div className='GradientCut' /> 
            : null}
            <div style={{ zIndex: 2}}> 
              <h1 className='TitleText'>Payment programs built for students</h1>
              <p className='SubTitleText'>Extended bookings can be expensive. SummerStay's Pay-As-You-Stay program makes it simple.</p>
            </div>
          </div>

          <img className='LandingImage' src={CoupleLaptop} />
        </div>
      
      {/* <div className="StaticGradient" >
          <div className="GradientCut"/>
        </div>
        <div className="StaticHeader">
          <div style={{color: '#21222f', width: '30vw'}}>
            <h1 className="TitleText">Payment plans designed for you</h1>
            <p className="SubTitleText">Extended bookings can be expensive, especially for full-time students. SummerStay makes it easy.</p>
            <div style={{width: '60%'}}>
              <NLButton onCLick={()=>this.pageScroll} title='LEARN MORE' type='UnderlinedButton Purple'/>
            </div>
          </div>

          <div style={{position: 'relative'}}>
            <img src={PaymentDiscussion} style={{height: 'auto', width: '40vw', minWidth: 200}} alt='building'/>
            <img src={Dots} style={{position:'absolute', top: -30, right: -30, zIndex: -2}} alt='building'/>
          </div>
        </div> */}

        
        <div className="StandardMargin" style={{marginTop: 50}}>
          {/* <div style={{textAlign:'center'}}>
            <p className="CapsText">PAY-AS-YOU-STAY</p>
            <h1 style={{ marginTop: 0}}>Start paying rent three weeks after move-in</h1>
          </div>
          <div style={{display:'flex',justifyContent:'space-between',flexWrap:'wrap',
                      marginTop: 50, marginBottom: 50}}>
            <div style={{position:'relative'}}>
              <img src={SmilingBoy} style={{height: 'auto', width: isLargePhone ? '80vw' : 300}} alt='building'/>
              {
                !isSmallComputer ? 
                  <img src={GrayDots} style={{position:'absolute', bottom: -40, right: -50, zIndex: -1}} alt='dots'/>
                : 
                null
              }

            </div>
    
            <div style={{ width: isLargePhone ? '80vw' : '30vw'}}>
              <p className="SubTitleText">Book your place with a small deposit, move-in, and wait three weeks. Then start paying rent. <br></br><br></br> SummerStay allows renters to cover the cost of bookings through a recurring payment plan. The program allows customers to spread out rent payments on a bi-weekly schedule, replacing the traditional lump sum payment. Recurring payments can be set up with credit cards, debit cards, or bank accounts at checkout. </p>
            </div>
          </div> */}
          {
          isSmallComputer ?
            <div>
              <h2>Standard Payment Program</h2>
              <img style={{width:"100%", height:'auto'}} src={Room1} />
            <div style={{marginTop: 20}}>
              
              <p style={{color:'#5E6C77'}}>Pay for your reservation before you move-in with two payments. Choose this program to pay a lower cost and get rent costs out of the way. The first payment is automatically charged when the host accepts your request to book. The last payment is due 14 days before your move-in date.</p>
              <div style={{display:'flex', justifyContent:'space-between'}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard1} />
                  <p className="ExtraSmallText">UPFRONT RENT</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard2} />
                  <p className="ExtraSmallText">PAY ONLINE</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard3} />
                  <p className="ExtraSmallText">LESS THAN A DORM</p>
                </div>
              </div>
            </div>
            <h2 style={{marginTop: 50}}>Pay-As-You-Stay Program</h2>
            <img style={{width:"100%", height:'auto'}} src={Room2} />
            <div >
              <p style={{color:'#5E6C77'}}>Pay the majority of your rent after move-in. This program is perfect for interns looking to start work before worrying about rent costs. An initial payment is automatically charged when your booking request is accepted. Subsequent rent begins 21 days after move-in and is automatically withdrawn on a biweekly schedule.</p>
              <div style={{display:'flex', justifyContent:'space-between'}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay1} />
                  <p className="ExtraSmallText">MOVE-IN, THEN PAY</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay2} />
                  <p className="ExtraSmallText">BUILT FOR INTERNS</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay3} />
                  <p className="ExtraSmallText">AUTOMATICALLY CHARGED</p>
                </div>
              </div>
            </div>
          </div>
          :
          <div>
          <div style={{display:'flex'}}>
            <div style={{width: '50%', margin: 20}}>
              <h2>Standard Payment Program</h2>
              <p style={{color:'#5E6C77'}}>Pay for your reservation before you move-in with two payments. Choose this program to pay a lower cost and get rent costs out of the way. The first payment is automatically charged when the host accepts your request to book. The last payment is due 14 days before your move-in date.</p>
              <div style={{display:'flex', justifyContent:'space-between', marginTop:50}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard1} />
                  <p className="ExtraSmallText">UPFRONT RENT</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard2} />
                  <p className="ExtraSmallText">PAY ONLINE</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Standard3} />
                  <p className="ExtraSmallText">LESS THAN A DORM</p>
                </div>
              </div>
            </div>
              <img style={{width:'calc(50% - 40px)', objectFit: 'cover', height:'auto', margin: 20}} src={Room1} />
          </div>




          <div style={{display:'flex'}}>
            <img style={{width:"calc(50% - 40px)", objectFit: 'cover', height:'auto', margin: 20}} src={Room2} />
            <div style={{ width: '50%', margin: 20}}>
              <h2>Pay-As-You-Stay Program</h2>
              <p style={{color:'#5E6C77'}}>Pay the majority of your rent after move-in. This program is perfect for interns looking to start work before worrying about rent costs. An initial payment is automatically charged when your booking request is accepted. Subsequent rent begins 21 days after move-in and is automatically withdrawn on a biweekly schedule.</p>
              <div style={{display:'flex', justifyContent:'space-between', marginTop:50}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay1} />
                  <p className="ExtraSmallText">MOVE-IN, THEN PAY</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay2} />
                  <p className="ExtraSmallText">FOR INTERNS</p>
                </div>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                  <img style={{width:50}} src={Pay3} />
                  <p className="ExtraSmallText">AUTO CHARGED</p>
                </div>
              </div>
            </div>
            
          </div>
          </div>
          }
        </div>
        
        <div style={{background:'#F6F9FC',marginTop: 70}}>
        <div ref={ (ref) => this.myRef=ref } className="StandardMargin" style={{marginTop: 70}}>
          <p className="CapsText">HOW IT WORKS</p>
          <div style={{display:'flex', justifyContent: 'space-between', flexDirection: isLargePhone ? 'column' : 'row' }}>
            <h1 style={{ width: isLargePhone ? '100%' : '30vw', margin: 0}}>Choose Pay-As-You-Stay and rent with confidence.</h1>
            <p className="SubTitleText" style={{width: isLargePhone ? '100%' : '30vw'}}>Save your money for what matters. Pay-As-You-Stay offers a low-cost housing alternative to fit an intern’s schedule.</p>
          </div>
        </div>  
        </div>
        <div style={{paddingTop: 50,background: 'linear-gradient(to bottom, #F6F9FC 50%, #fff 50%)'}} ref={this.section2}>
          <div className="StandardMargin">
            <InfoDisplayBoxes highlightColor='#282c3b' details={sellingPoints} />
          </div>
        </div>


          {/* <div style={{background:`url(${PurpleBackground})`}}>
            <div className="StandardMargin" style={{flexWrap:'wrap',paddingTop: 150, paddingBottom: 50}}>
              <div style={{display:'flex', justifyContent:'space-between', flexWrap:'wrap' }}>

                <div style={{display:'flex', flexDirection:'column' }}>
                
                  <div style={{display:'flex', flexDirection:'column', width: isLargePhone ? '70vw' : 400, height: 250, marginLeft: 40}}>
                    <h1 style={{marginTop: 0, color: '#fff'}}>Take control of your sublet experience.</h1>
                    <div style={{display:'flex', justifySelf:'flex-end'}}>
                      <Button title="FIND YOUR PLACE" type="UnderlinedButton White" link='/listings'/>
                    </div>
                  </div> 
                  
                  <div style={{display:'flex', width: isLargePhone ? '70vw' : 400, height: 250}}>
                    <Control style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                    <div>
                      <h2 style={{marginTop: 10,color: '#fff'}}>Peace of Mind</h2>
                      <p className="SubTitleText" style={{color: '#fff'}}>Rent payments are withheld until move-in is complete. We hold hosts accountable for accurately portraying their space; if you're not satisfied, we'll refund your payment in full.</p>
                    </div>
                  </div> 
                </div>
              
                <div style={{display:'flex', flexDirection:'column'}}>
                  <div style={{display:'flex',width: isLargePhone ? '70vw' : 400, height: 250}}>
                    <Image style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                    <div>
                      <h2 style={{marginTop: 10,color: '#fff'}}>Pay Online</h2>
                      <p className="SubTitleText"style={{color: '#fff'}}>Pay rent using a credit card or bank account. We use Stripe Payments, a secure payment processing platform, to keep your money and information safe.</p>
                    </div>
                  </div> 
                  <div style={{display:'flex',width: isLargePhone ? '70vw' : 400, height: 250}}>
                    <Check style={{fontSize: 30, color: '#fff', marginTop: 10, marginRight: 10}}/>
                    <div>
                      <h2 style={{marginTop: 10,color: '#fff'}}>Here For You</h2>
                      <p className="SubTitleText"style={{color: '#fff'}}>Our team is here to answer your questions and support you during your stay. We want to take the uncertainty our of subletting. </p>
                    </div>
                  </div> 
                </div>
              </div>
            
            </div>
          </div> */}



        



          <div style={{width:'100%', marginTop: 50}}>
              <Footer/>
          </div>
          
        
        
        

        {/* <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>     
          <img style={{marginLeft: 'auto',marginRight: 'auto',width:'100%',display: 'block'}} src={LOL} alt='Payment'/>
          
          <div style={{maxWidth:800, margin:20}}>
            <h1 style={{fontSize: 50, color:'#333333', textAlign:'center'}}>Introducing <i>Pay-As-You-Stay</i></h1>
            <p style={{fontSize: 20, color:'#333333'}}>SummerStay is reinventing how students find housing. We’re looking to provide summer housing that works for students and interns. That means offering housing at costs lower than traditional options. It also means allowing students to pay when they are able to.</p>
            <Divider/>
            <h3 style={{fontSize: 30,color:'#333333',marginBottom: 40}}>Flexible Payment Options</h3>
          
            <p style={{fontSize: 20, color:'#333333'}}>SummerStay is launching a new program, <i>Pay-As-You-Stay</i>, allowing students to pay for their summer housing while they work.</p>
            <p style={{fontSize: 20, color:'#333333', marginBottom: 40}}>Our continuous pricing option allows students to secure affordable, reliable housing without a large upfront cost. Unlike our competitors, SummerStay only requires an initial payment and security deposit to move in. We understand many students are unable to pay for their housing until they begin summer work. That is why we’ve introduced our <i>Pay-As-You-Stay</i> program, designed to allow students to pay for their housing over the course of the summer.</p>
            <Divider/>
            <h3 style={{fontSize: 30, color:'#333333'}}><i>Pay-As-You-Stay</i> Terms</h3>
            <p style={{fontSize: 20, color:'#333333'}}>By making a booking and choosing to <i>Pay-As-You-Stay</i>, you agree to these Terms and Conditions. The Pay As You Stay feature allows Renters to pay a portion of a booking’s Total Fees at the time of booking and pay the remainder of the Total Fees during their Stay. If you choose to book with the Pay as you Stay feature, the SummerStay Platform will notify you of the amount and schedule of each payment due. On the remaining payment due dates, SummerStay will automatically charge the original Payment Method you used to make the booking. </p>
          </div>
          
        </div>
        <div style={{width:'100%', marginTop: 50}}>
          <div style={{display: 'flex', justifyContent: 'space-around', alignItems: 'center',height:300, backgroundColor:'#6c4ef5', flexWrap:'wrap',marginTop: 50}}>
            <p style={{fontSize:60, fontWeight:600, padding: 10,color: '#ffffff', margin:0}}>Take advantage of flexible<br/>payment</p>
            <Button title ="Find a Place" link="/listings" type="LargeButton White"/>
          </div>
          <Footer/>
        </div> */}
      
      </div>
      
    ); 
  }
}

const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(PayAsYouStay)