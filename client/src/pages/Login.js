import React, {Component} from 'react'; 
import { CLIENT_URL, FB_APP_ID, SHOULD_DEBUG_FB } from '../constants/index.js';
import { POST} from '../constants/requests.js'
import ModalWrapper from '../components/utilities/Modal.js'
import ForgotPassword from '../components/forgotPassword.js'
import { Helmet } from 'react-helmet';

import FacebookLogin from 'react-facebook-login';
import { IS_FB_APP } from '../constants/functions.js';

import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import ErrorModal from '../components/utilities/errorModal.js'

import Paper from '@material-ui/core/Paper';
import { CustomTextbox } from '../constants/inputs.js'
import Facebook from '@material-ui/icons/Facebook'

import CustomLoading from '../components/utilities/loadingBar.js'

import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import '../CSS/ListingsMain.css';

import { connect } from 'react-redux'
import { loginUser } from '../store/actions/passThrough.js'

const styles={
    root: {
        padding: '4px 8px',
        display: 'flex',
        
        width: 400,
      },textField:{
        margin: 0,
        marginTop:10,
        '& label.Mui-focused': {
            color: '#0a3f77',
          },
        '& .MuiOutlinedInput-root': {
            // '& fieldset': {
            //   borderColor: 'red',
            // },
            '&:hover fieldset': {
              borderColor: 'black',
            },
            '&.Mui-focused fieldset': {
              borderColor: '#0a3f77',
            }
          }
      },
     
}
class Login extends Component {
    constructor(props){
      super(props);
      this.state = {
        email: '', 
        password:'',
        loading: false, 
        passError: false, 
        emailError: false,
        requestError:false,
        requestErrorMessage: '',

        responseDebug: 'nothing',
      };
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    handleLogin=(data)=>{
        const temp = JSON.parse(localStorage.getItem('tempData'))
        this.props.onLoginUser(data.user); // put the user object in redux
        const rr = JSON.parse(localStorage.getItem('returnRoute'))
        if(temp != null){// once I have the user object i need to make a listing if the user started the proccess
            const send_data = {
                listing: {
                    listing_type: temp.roomType,
                    address: temp.address.location,
                    latitude: temp.address.lat,
                    longitude: temp.address.lng,
                    full_address: temp.address.fullAddress
                },
            }
            POST('/api/v1/listings', send_data)
            .then(data => {
                localStorage.removeItem('tempData')
                this.props.history.push(`/listings/edit/${data.listing.id}`)
            })
            .catch(error => {
                this.setState({ // reset the state nomatter what
                    email: '', 
                    password: '',
                    loading: false,
                })
                localStorage.removeItem('tempData')
                this.props.history.push('/dashboard')
            }); 

        }else if(rr !== null){
            localStorage.removeItem('returnRoute');
            
            if('route' in rr){
                console.log('Routing to '+ rr.route)
                this.props.history.push(rr.route)
            }
            
        }else{ // add return toute logic 
            this.props.history.push('/dashboard/listings')
        }
    }

    loginRequest= () =>{
        // run fetch and clear data

        let error = false
        if(this.state.email.length === 0){
            error = true
            this.setState({emailError: true})
        }else{
            this.setState({emailError: false})
        }
        if(!error && !this.state.loading){
            this.setState({loading: true}) // give the user a visual que thier request was made

            const send_data = {
                user: {
                    email: this.state.email,
                    password: this.state.password
                }
            }

            POST('/api/v1/users/sign_in', send_data)
            .then(data => {
                this.handleLogin(data)
            })
            .catch(error => {
                this.setState({ // reset the state nomatter what
                    loading: false,
                    requestError: true,
                    requestErrorMessage: error.error,
                })
            });

        }
        
    }

    loginFacebook = (response) => {
        if (response.accessToken) {
            this.setState({loading: true})
            const send_data = {
                facebook_access_token: response.accessToken,
            }
            POST('/api/v1/users/facebook_login', send_data)
            .then(data => {
                this.handleLogin(data)
            })
            .catch(error => {
                this.setState({ // reset the state nomatter what
                    loading: false,
                    requestError: true,
                    requestErrorMessage: error.error ? error.error : error,
                })
            });
        }
    }
      
    render(){
        const { classes } = this.props;
        const redirectUri = CLIENT_URL + '/login'
        return(
            <div style={{marginTop:70, background:'#f9f9fc', minHeight:'calc(100vh - 70px)'}}>
            <Helmet>
                <title>Login - SummerStay</title>
            </Helmet>
            <CustomLoading loading={this.state.loading}/>
            
            <div style={{display:"flex",justifyContent:"center", marginTop: 30, marginBottom: 20}}>
                <ModalWrapper open={this.state.openModal} onClose={()=>this.setState({openModal: false})}>
                    <ForgotPassword/>
                </ModalWrapper>
                <ErrorModal message={this.state.requestErrorMessage} show={this.state.requestError} close={()=> this.setState({requestError:false})}/>
                <Paper className={classes.root} elevation={1}>
                    <div style={{display:'flex', flex: 1,flexDirection:'column', padding: 20}}>
                        <h1 style={{textAlign:'center', marginTop: 0}}>Log in</h1>
                        
                        {
                            IS_FB_APP ?

                            <FacebookLogin
                                appId={FB_APP_ID}
                                fields="first_name,last_name,email,id,picture"
                                autoLoad={false}
                                cookie={true}
                                xfbml={true}
                                redirectUri={redirectUri}
                                isMobile={true}
                                callback={this.loginFacebook}
                                cssClass="LargeButton FaceBookColor"
                                textButton="Log in with Facebook"
                                icon={<Facebook style={{marginRight: 10}}/>}
                            />

                            :

                            <FacebookLogin
                                appId={FB_APP_ID}
                                fields="first_name,last_name,email,id,picture"
                                autoLoad={false}
                                redirectUri={redirectUri}
                                isMobile={false}
                                callback={this.loginFacebook}
                                disableMobileRedirect={true}
                                cssClass="LargeButton FaceBookColor"
                                textButton="Log in with Facebook"
                                icon={<Facebook style={{marginRight: 10}}/>}
                            />                            
                        }
                        
                        <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginTop: 20, marginBottom: 10}}>
                            <div style={{display:'flex', flex: 1, backgroundColor:'#a6a6a6', height:'1px',  marginRight: 20}}/>
                            <p className="GrayText" style={{margin: 0}}>OR</p>
                            <div style={{display:'flex', flex: 1, backgroundColor:'#a6a6a6', height:'1px',  marginLeft: 20}}/>
                        </div>
                        <div style={{ display:'flex', flexDirection:'column', alignItems:'center', marginBottom: 5 }}>
                            {
                                this.state.emailError ? <p style={{margin:0, color:'red'}}>Make sure to provide an email</p>: null
                            }
                        </div>
                            <div style={{height: 10}}/>
                            <CustomTextbox
                                error={this.state.emailError}
                                value={this.state.email}
                                id="outlined-search"
                                label="Email"
                                type="email"
                                full
                                // className={classes.textField}
                                onChange={(event)=>{ this.setState({email: event.target.value})}}
                            />

                            <div style={{height: 20}}/>
                            <CustomTextbox
                                full
                                autoComplete='current-password'
                                error={this.state.passError}
                                value={this.state.password}
                                label="Password"
                                type="password"
                                
                                onChange={(event)=>{ this.setState({password: event.target.value})}}
                                onKeyPress={(ev) => {
                                    if (ev.key === 'Enter') {
                                      this.loginRequest()
                                      ev.preventDefault();
                                    }
                                  }}
                            />
                      
                        <div style={{height:20}}/>
                        <NLButton title ="Log in with Email" onClick={this.loginRequest} type="LargeButton SolidPurple"/>

                        <div style={{display:'flex', flexDirection:'column',alignItems:'center', marginTop: 10}}>
                            {/* Note: Added a new endpoint /users/facebook_login that throws error if not logged in */}
                            {/* <p className="GrayText SmallText" style={{textAlign:'center'}}>By logging in, you agree to SummerStay's <Button title ="Privacy Policy" link="/privacy" type="InlineText Purple SmallText"/> and <Button title ="Terms & Conditions" link="/terms" type="InlineText Purple SmallText"/></p>
                            <div style={{width:'100%', backgroundColor:'#a6a6a6', height:'1px', marginBottom: 14}}/> */}
                            <NLButton title ="Forgot Password?" onClick={()=>this.setState({openModal: true})} type="InlineText Purple"/>
                            <p style={{marginTop: 14, marginBottom:0}}>Don't have an account? <Button title ="Sign up" link="/signup" type="InlineText Purple"/></p>
                        </div>
                        
                        
                    </div>
                </Paper>
            </div>
            </div>
            
        )
    }
}
    
Login.propTypes = {
    classes: PropTypes.object.isRequired,
  };
const mapStateToProps = state => {
    return {
        user: state.housingApp.user, 
    }
  }
  const mapDispatchToProps  = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user))
    }
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(Login));