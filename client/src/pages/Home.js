import React, { Component } from 'react';

import '../CSS/App.css';
import '../CSS/Headers.css';
import '../CSS/Static.css'
import InfoDisplayBoxes from '../components/utilities/infoDisplayBoxes.js'
import DarkBackground from '../Assets/darkBackground.svg'
import { connect } from 'react-redux'
import { PARSE_QUERY } from '../constants/functions.js'

import CardCarousel from '../components/utilities/cardCarousel.js'
import FeaturedListings from '../components/home/featuredListings.js'
import ListingSearchBar from '../components/utilities/searchBar.js'
import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import Footer from '../components/footer.js'
import FavoriteBorder from '@material-ui/icons/FavoriteBorder'
import WorkOutline from '@material-ui/icons/WorkOutline'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import Peace from '@material-ui/icons/Brightness5Outlined';
import Control from '@material-ui/icons/ControlCamera'
import Real from '@material-ui/icons/HomeOutlined'

import SecurtityIcon from '../Assets/SecurityIcon.png'
import MoneyIcon from '../Assets/moneyIcon.png'
import HomeIcon from '../Assets/houseIcon.png'

import Boston from '../Assets/featured-cities/Boston.png'
import Chicago from '../Assets/featured-cities/Chicago.png'
import LA from '../Assets/featured-cities/Los-Angeles.png'
import NYC from '../Assets/featured-cities/New-York.png'
import Seattle from '../Assets/featured-cities/Seattle.png'
import SanFran from '../Assets/featured-cities/San-Francisco.png'
import Washington from '../Assets/featured-cities/Washington.png'
import Girls from '../Assets/girls.jpg'
import PurpleDots from '../Assets/purpleDots.svg'
import Dots from'../Assets/dots.svg'
import LookingAtComputer from '../Assets/how-it-works/looking-at-computer.jpg'

import CollegeGirl from '../Assets/home-pics/college-girl.jpg'
import HeadphoneGirl from '../Assets/home-pics/headphone-girl.jpg'


const PayAsYouStayPoints = [{icon: <FavoriteBorder style={{ fontSize:35,color:'#fff'}}/>, title: 'Guaranteed Quality', summary: 'Online sublets can be unreliable. We’ll verify your host to ensure quality listings.' },
                            {icon: <WorkOutline style={{ fontSize:35,color:'#a6a6a6'}}/>, title: 'Perfect for Interns', summary: 'Flexible dates, immediate availability, and sublets to meet your needs make SummerStay the preferred housing option for interns.'},
                            {icon: <AttachMoneyIcon style={{ fontSize:35,color:'#a6a6a6'}}/>, title: 'Make Life Simple', summary: 'Select your apartment, request to book, and pay online. Move in and out on your schedule.'}]

const places = [{name: 'Seattle', state: 'Washington, USA', component: Seattle, loc: 'SeattleWa'}, {name: 'New York City', state: 'New York, USA', component: NYC, loc: 'NewYorkCity'},
                            {name: 'San Francisco', state: 'California, USA', component: SanFran, loc: 'SanFrancisco'}, {name: 'Chicago', state: 'Illinois, USA', component: Chicago, loc: 'Chicago'},  
                            {name: 'Boston', state: 'Massachusetts, USA', component: Boston, loc: 'Boston'}, {name: 'Washington D.C.', state: 'District of Columbia, USA', component: Washington, loc: 'WashingtonDC'},
                            {name: 'Los Angeles', state: 'California, USA', component: LA,loc: 'LosAngeles'}]
                            
class Home extends Component {
  constructor(props){
    super(props);
    this.state = {
      slide1:false,
      slide2:false,
      slide3:false,
      width: window.innerWidth, 
      featuredListings: [],
      locationError: false,
    };
    
  }

  componentDidMount(){
    window.scrollTo(0, 0);
    document.addEventListener('scroll', this.trackScrolling);
    window.addEventListener('resize', this.handleWindowSizeChange);
    const currUrl = PARSE_QUERY(this.props.location.search)
    if ('about' in currUrl) this.pageScroll()
    if ('privacy' in currUrl) {
      this.props.history.push('/privacy')
    }
  }

  isBottom(el) {
    return el.getBoundingClientRect().bottom <= window.innerHeight;
  }

  searchLocation = (address) => {
    this.props.history.push(`/listings?loc=${address.replace(/\s/g,'')}`)
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
    window.removeEventListener('resize', this.handleWindowSizeChange);

  }

  clear = () => {

  }
  pageScroll=()=>{
    window.scrollTo(0, this.myRef.offsetTop)
  }
  trackScrolling = () => {
    // const wrappedElement1 = document.getElementById('pass1');
    // if (this.isBottom(wrappedElement1)) {
    //   this.setState({slide1:true})
    // }
    // const wrappedElement2 = document.getElementById('pass2');
    // if (this.isBottom(wrappedElement2)) {
    //   this.setState({slide2:true})
    // }
    // const wrappedElement3 = document.getElementById('pass3');
    // if (this.isBottom(wrappedElement3)) {
    //   this.setState({slide3:true})
    //   document.removeEventListener('scroll', this.trackScrolling);
    // }
  };

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };



  render() {

    const {isSmallPhone,isMediumPhone,isLargePhone, isSmallComputer } = this.props.appSize
    const investorStyling = isMediumPhone ? {fontSize:40, marginTop: 0, marginBottom: 10} : {fontSize:90, marginTop: 0, marginBottom: 10}
    const investorStylingSub = isMediumPhone ? {fontSize:20, color:'#6c4ef5',verticalAlign:'64%'} : {fontSize:30, color:'#6c4ef5',verticalAlign:'115%'}
    const investorSize = isMediumPhone ? {width:'100%', margin: 10, textAlign:'center'} : {width:250, margin: 10, textAlign:'center'}
    const headerStyling = isMediumPhone ? {display:'flex', flexDirection:'column', width:'calc(100% - 40px)', margin: 20}: {display:'flex', flexDirection:'column', width:'35vw', marginLeft: '15vw', marginTop: '10vh'}
    const slides = places.map(p => <FeaturedListings image={p.component} name={p.name} state={p.state} body={p.loc} 
      onClick={()=>this.props.history.push(`/listings?loc=${p.loc}`)} />)

    return (
      <div className='StaticMain'>
        <div className='HomeLandingMain'>
        
          <img src={CollegeGirl} className='LandingImage' height='auto' alt="college girl"/>

          <div className='LandingHeader' style={{ paddingTop: isLargePhone ? 50 : 110 }} >
            { !isLargePhone ? 
             <div className='OppositeGradientCut' /> 
            : null}
            <div style={{ zIndex: 2 }}>
              <h1 className='TitleText'>Find student rooms & apartments for rent</h1>
              <p className='SubTitleText'>SummerStay is the safest and easiest place for students to list housing sublets and find renters for 4+ weeks.</p>
              <div style={{ width: '60%', marginTop: 30, minWidth: 225}}>
                <ListingSearchBar alt initialVal='' placeholder='Try "Seattle"' error={this.state.locationError} main={false} updateLocation={this.searchLocation} 
                  problem={(val) => this.setState({ locationError: val })} clear={this.clear} />
              </div>
            </div>
          </div>
          

        </div>

        {/* Featured Cities */}
        <div style={{backgroundColor:'#fff'}}>
          <div className="StandardMargin">
            <div style={{display:'flex', flexDirection:'column', marginTop: 50, marginBottom:50}}>
              <div style={{display:'flex', flexDirection:isMediumPhone? 'column':'row',justifyContent: 'space-between'}}>
                <div>
                  <h2 style={{margin: 0, paddingLeft: 15}}>Featured cities for summer internships</h2>
                </div>
              </div>
              
              <div style={{ paddingTop: 5 }}>
                <CardCarousel slides={ slides } />
              </div>
              
            </div>
          </div>
        </div>

        <div className="StandardMargin" style={{flexDirection: isMediumPhone ? 'column' : 'row', justifyContent:'space-between', alignItems: isMediumPhone ? 'center' : 'flex-start', marginBottom: 90, marginTop: 40}}>
          <div className='SmallIconTextBox'>
            <div>
              <img src={SecurtityIcon} style={{height: 70}} alt="Security Icon"/>
            </div>
            <h2 className='SoftHeader' style={{marginBottom: 0}}>Your housing is safe</h2>
            <p className='GrayText'>Secure payments, hosts and renters are ID verified, and around-the-clock support</p>
          </div>
          <div className='SmallIconTextBox'>
            <div>
              <img src={MoneyIcon} style={{height: 70}} alt="Money Icon"/>
            </div>
            <h2 className='SoftHeader' style={{marginBottom: 0}}>Perfect for interns</h2>
            <p className='GrayText'>Flexible dates, immediate availability, and sublets posted by students</p>
          </div>
          <div className='SmallIconTextBox'>
            <div>
              <img src={HomeIcon} style={{height: 70}} alt="Home Icon"/>
            </div>
            <h2 className='SoftHeader' style={{marginBottom: 0}}>More for less</h2>
            <p className='GrayText'>More space, real apartments, bookable on a weekly schedule</p>
          </div>
        </div>

        {/* Pay-As-You-Stay Info*/}
        {/* <div style={{background:'linear-gradient(to bottom, #fff 50%, #282c3b 50%)'}}>
          <div className="StandardMargin">
            <InfoDisplayBoxes highlightColor='#6c4ef5' details={PayAsYouStayPoints} />
            
          </div>
        </div> */}
        <div style={{background:`url(${DarkBackground})` }}>
            <div className="StandardMargin">
            <div style={{display:'flex', flexDirection: isMediumPhone ? 'column' :'row',justifyContent: 'space-between', paddingTop: 100, paddingBottom: 100}}>
                {/* <div style={{position: 'relative'}}>
                  <img src={Bed} style={{width: '30vw'}} alt='building'/>
                  
                </div> */}
                <div style={{width: isMediumPhone ? '100%':'50%', paddingRight: isMediumPhone ? 0 : 25, marginTop: 10, marginBottom: isMediumPhone ? 20 : 0}}>
                  <img src={HeadphoneGirl} style={{width: '100%' ,zIndex:6}} alt="friends"/>
                  {/* <img src={PurpleDots} style={{position:'absolute', zIndex: 5,top: -30, right: -30}} alt='building'/> */}
                </div>
                <div style={{width: isMediumPhone?'100%':'40%'}}>
                  <h1 style={{color:'#fff', marginTop: 0}}>Pay upfront or during your stay</h1>
                  <p className="SubTitleText" style={{color:'#fff'}}>Seamless booking for you. Flexible payment plans for your bank account.  We make mid-to-long term bookings easy.</p>
                  <div style={{ marginTop: 40}}>
                    <Button title='Learn More' link='/payment-plans' type='MediumButton SolidWhite'/>
                  </div>
                </div>
                
              </div>
              
            </div>
          </div>

          

         
        {/* Host*/}
        <div ref={ (ref) => this.myRef=ref } style={{display:'flex', flexDirection:'column'}}>
          
          {/* Host Header*/}
          <div className="StandardMargin">
          <div style={{display:'flex', flexDirection:isMediumPhone? 'column': 'row', justifyContent: 'space-between', marginTop: 150}}>
            {
              isMediumPhone ? 
                <div style={{display:'flex', position: 'relative',marginBottom: 20}}>
                  <img src={Girls} style={{width:'100%', height:'40vh', objectFit: 'cover'}} alt="friends"/>
                  <img src={PurpleDots} style={{position:'absolute', top: -30, right: isMediumPhone ?0 :-30, zIndex: -1}} alt='building'/>
                </div>
              : null
            }
            <div style={{width: isMediumPhone ? '100%':'30vw'}}>
              <h1 style={{marginTop: 0}}>Find subletters easily and quickly.</h1>
              <p className="SubTitleText">Students who sublet with SummerStay save more and do less.</p>
                
              <div style={{display:'flex', alignItems:'center'}}>
                <Peace className="GrayText" style={{fontSize: 25}}/>
                <div style={{marginLeft: 10}}>
                  <h3 style={{marginBottom: 0, marginTop: 10}}>Peace of Mind</h3>
                  <p style={{marginTop: 0, color: '#5E6C77'}}>Guaranteed rent: on time, bi-weekly.</p>
                </div>
              </div>
              <div style={{display:'flex', alignItems:'center'}}>
                <Control className="GrayText" style={{fontSize: 25}}/>
                <div style={{marginLeft: 10}}>
                  <h3 style={{marginBottom: 0, marginTop: 10}}>Total Control</h3>
                  <p style={{marginTop: 0, color: '#5E6C77' }}>You define the sublet and pick your renter.</p>
                </div>
              </div>
              <div style={{display:'flex', alignItems:'center'}}>
                <Real className="GrayText" style={{fontSize: 25}}/>
                <div style={{marginLeft: 10}}>
                  <h3 style={{marginBottom: 0, marginTop: 10}}>Real Sublets</h3>
                  <p style={{marginTop: 0, color: '#5E6C77'}}>Renters are required to book for at least 4 weeks.</p>
                </div>
              </div>

              <div style={{ marginTop: 20, marginLeft: 35}}>
                <Button title="Explore Subletting" link="/host" type="MediumButton SolidPurple"/>
              </div>
            </div>
            {
              !isMediumPhone ? 
                <div style={{display:'flex', position: 'relative',minWidth: 300}}>
                  <img src={Girls} style={{width:'30vw', height:'40vh', objectFit: 'cover'}} alt="friends"/>
                  <img src={PurpleDots} style={{position:'absolute', top: -30, right: -30, zIndex: -1}} alt='building'/>
                </div>
              : null
            }
            
          </div>
          </div>
          {/* <div className="StandardMargin" style={{backgroundImage:`url(${Friends})`,backgroundSize: 'cover', borderRadius: 5,
          backgroundRepeat: 'no-repeat',backgroundPosition: 'center', marginTop: 100, height: '50vh', padding: 50, 
          alignItems:'flex-end'}}> */}
            

    
          {/* Investor Selling Points*/}
          <div  style={{backgroundColor: '#F6F9FC', marginTop: 150}}>
            <div className="StandardMargin" style={{marginTop: 40, marginBottom: 40}}>
              <div style={{textAlign:'center'}}>
                <p style={{color:'#6c4ef5', fontWeight: 900}}>INTRODUCING SUMMERSTAY</p>
                <h1 style={{margin: 0}}>Created to fix the university sublet market.</h1>
              </div>
              
              <div style={{display:'flex', flexDirection:isMediumPhone? 'column': 'row',justifyContent:'space-between', marginTop: 40}}>
                
                <div style={investorSize}>
                  <h1 className="altFont" style={investorStyling}>87<span style={investorStylingSub}>%</span></h1>
                  <p className="ExtraSmallText" style={{color:'#5E6C77'}}>Percentage of US students living off-campus. </p>
                </div>
                <div style={investorSize}>
                  <h1 className="altFont" style={investorStyling}>1.5<span style={investorStylingSub}>M</span></h1>
                  <p className="ExtraSmallText" style={{color:'#5E6C77'}}>Number of internships filled yearly in the United States. </p>
                </div>
              

                <div style={investorSize}>
                  <h1 className="altFont" style={investorStyling}><span style={investorStylingSub}>$</span>1,640</h1>
                  <p className="ExtraSmallText" style={{color:'#5E6C77'}}>Average monthly price for a private bedroom in summer dorm programs. </p>
                </div>
              </div>

              <div style={{display:'flex', flexDirection:isMediumPhone ? 'column': 'row', justifyContent: 'space-between', marginTop: 50}}>
                {
                  isMediumPhone ? 
                    <div style={{position: 'relative', marginBottom: 20}}>
                      <img src={LookingAtComputer} style={{width:'100%'}} alt='building'/>
                      <img src={Dots} style={{position:'absolute', top: -30, right: -30,zIndex:-1}} alt='building'/>
                    </div> : null
                }

                <div style={{width: isMediumPhone ? '100%':'30vw'}}>
                  <h1 style={{ marginTop: 0}}>Reimagining student housing</h1>
                  <p className="SubTitleText" style={{color:'#5E6C77'}}>We built SummerStay to reimagine the student housing market. While the majority of students prefer to live off-campus, few occupy their apartments for the duration of a year-long lease. Subletting during the summer and semesters spent abroad become commonplace across university campuses. Meanwhile, the amount of students temporarily relocating for internships and career opportunities is on the rise. Interns have historically been forced into choosing between expensive AirBnb’s, unattractive school dorms, and unverified sublets. We believe there is a better way. Realizing the value a housing platform for students would provide, SummerStay was born.</p>
                </div>
                {
                  !isMediumPhone ? 
                    <div style={{position: 'relative', }}>
                      <img src={LookingAtComputer} style={{width:'30vw'}} alt='building'/>
                      <img src={Dots} style={{position:'absolute', top: -30, right: -30,zIndex:-1}} alt='building'/>
                    </div> : null
                }
                
                
              </div>
              
            </div>
          </div>

        </div>
        
        <Footer pageScroll={this.pageScroll} />
      </div>
    ); 
  }
}
const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(Home)
