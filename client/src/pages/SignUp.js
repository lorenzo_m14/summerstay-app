import React, {Component} from 'react'; 
import { CLIENT_URL, FB_APP_ID, PASSWORD_ERROR } from '../constants/index.js'
import { POST } from '../constants/requests.js'
import { CustomTextbox } from '../constants/inputs.js'
import { LOCAL_EMAIL_CHECK, SAVE_ROUTE, LOCAL_PASSWORD_CHECK } from '../constants/functions.js'
import FacebookLogin from 'react-facebook-login'
import { IS_FB_APP } from '../constants/functions.js';
import { Helmet } from 'react-helmet';

import Button from '../components/utilities/Button.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import ModalWrapper from '../components/utilities/Modal.js'
import Facebook from '@material-ui/icons/Facebook'

import Paper  from '@material-ui/core/Paper'

import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { loginUser } from '../store/actions/passThrough.js'
import '../CSS/ListingsMain.css'

const styles={
    root: {
        padding: '4px 8px',
        display: 'flex',
        
        width: 400,
    }
}
class SignUp extends Component {
    constructor(props){
      super(props);
      this.state = {
        firstName:'', 
        lastName:'', 
        email:'',
        password:'', 
        passError:false,
        fNameError:false,
        lNameError: false, 
        emailError:false, 
        loading: false,

        openConfirm: false, 
        signUpError: false
      };
    }
    
    componentDidMount() {
        window.scrollTo(0, 0)
    }

    handleSignUp=(data, facebook)=>{
        if(facebook){
            this.props.onLoginUser(data.user); // put the user object in redux
            const temp = JSON.parse(localStorage.getItem('tempData'))
            const rr = JSON.parse(localStorage.getItem('returnRoute'))
            
            if(temp != null){// once I have the user object i need to make a listing if the user started the proccess
                const send_data = {
                    listing: {
                        listing_type: temp.roomType,
                        address: temp.address.location,
                        latitude: temp.address.lat,
                        longitude: temp.address.lng,
                        full_address: temp.address.fullAddress,
                    },
                }
                POST('/api/v1/listings', send_data)
                .then(data => {
                    localStorage.removeItem('tempData')
                    this.props.history.push(`/listings/edit/${data.listing.id}`)
                })
                .catch(error => {
                    localStorage.removeItem('tempData')
                    this.props.history.push('/dashboard')
                });
    
            }else if(rr !== null){
                localStorage.removeItem('returnRoute');
                if('route' in rr){
                    console.log('Routing to '+ rr.route)
                    this.props.history.push(rr.route)
                }
            }else{ // add return toute logic    
                this.props.history.push('/dashboard')
            }
        
        }else if(this.props.temp !== null){// once I have the user object i need to make a listing if the user started the proccess
            SAVE_ROUTE("TEMP")

            this.resetInfo()
        }else{
            this.resetInfo()
        }
    }

    signUpRequest=() =>{
       // need to check to make sure it is a valid email
       //check password length
       
       let error = false
        if(!LOCAL_PASSWORD_CHECK(this.state.password)){
            error = true
            this.setState({passError: true})
        }else{
            this.setState({passError: false})
        }
        if(this.state.firstName.length===0){
            error = true
            this.setState({ fNameError: true })
        }else{
            this.setState({fNameError: false})
        }
        if(this.state.lastName.length===0){
            error= true
            this.setState({ lNameError: true })
        }else{
            this.setState({lNameError: false})
        }
        if(!LOCAL_EMAIL_CHECK(this.state.email)){
            error = true
            this.setState({ emailError: true })
        }else{
            this.setState({emailError: false})
        }
        if(!error){
            this.setState({loading:true})
            const send_data = {
                user: {
                    email: this.state.email,
                    password: this.state.password,
                    first_name: this.state.firstName,
                    last_name: this.state.lastName
                }
            }
            POST('/api/v1/users/', send_data)
            .then(data => {
                this.handleSignUp(data, false)
            })
            .catch(error => {
                // TODO: needs some error handling here
                this.setState({
                    signUpError: true,
                    loading:false
                })
            });

        }
    }

    // on callback from Facebook OAuth request, 
    signUpFacebook = (response) => {
        if (response.accessToken) {
            this.setState({ loading: true })
            const send_data = {
                facebook_access_token: response.accessToken,
            }
            
            POST('/api/v1/users/facebook', send_data)
            .then(data => {
                this.handleSignUp(data, true)
            })
            .catch(error => {
                // TODO: needs some error handling here
                this.setState({
                    loading:false
                })
            });
        }

    }
    updateName=(type, name)=>{
        this.setState({
            [type]: name.charAt(0).toUpperCase() + name.slice(1)
        })
    }
    resetInfo=()=>{
        this.setState({
            openConfirm: true,
            loading:false,
            firstName:'', 
            lastName:'', 
            email:'',
            password:'', 
        }) 
    }

    render(){
        const { classes } = this.props;
        const redirectUri = CLIENT_URL + '/signup'

        return(
            <div style={{marginTop:70, background:'#f9f9fc', minHeight:'calc(100vh - 70px)'}}>
                <Helmet>
                    <title>Sign Up - SummerStay</title>
                </Helmet>
                <ModalWrapper open={this.state.openConfirm} onClose={()=>this.setState({openConfirm: false})}>
                    <h2 style={{marginTop:0}}>Confirm your account</h2>
                    <p className="GrayText" style={{margin:0, marginBottom: 20}}>Check your email for a confirmation link to get started. Look in your <span className="LightBold">spam or promotions folder</span> just in case it got sent there!</p>
                    <NLButton title="Close" onClick={()=>this.setState({openConfirm: false})} type="LargeButton"/>
                </ModalWrapper>
                <ModalWrapper open={this.state.signUpError} onClose={()=>this.setState({signUpError: false})}>
                    <h2 style={{marginTop:0}}>Error</h2>
                    <p className="GrayText" style={{margin:0, marginBottom: 20}}>It looks like there was a problem with your request. Have you already made an account? If your problem persists <Button title ="contact us" link="/support" type="InlineText"/>.</p>
                    <NLButton title="Close" onClick={()=>this.setState({signUpError: false})} type="LargeButton"/>
                </ModalWrapper>
                <CustomLoading loading={this.state.loading}/> 
                <div style={{display:"flex",justifyContent:"center", marginTop: 30, marginBottom: 20}}>
                <Paper className={classes.root} elevation={1}>
                    <div style={{display:'flex', flex: 1, flexDirection:'column', padding: 20}}>
                        <h1 style={{textAlign:'center', marginTop: 0}}>Sign Up</h1>
                        {
                            IS_FB_APP ?

                            <FacebookLogin
                                appId={FB_APP_ID}
                                fields="first_name,last_name,email,id,picture"
                                autoLoad={false}
                                cookie={true}
                                xfbml={true}
                                redirectUri={redirectUri}
                                isMobile={true}
                                callback={this.signUpFacebook}
                                cssClass="LargeButton FaceBookColor"
                                textButton="Sign up with Facebook"
                                icon={<Facebook style={{marginRight: 10}}/>}
                            />

                            :

                            <FacebookLogin
                                appId={FB_APP_ID}
                                fields="first_name,last_name,email,id,picture"
                                autoLoad={false}
                                redirectUri={redirectUri}
                                isMobile={false}
                                callback={this.signUpFacebook}
                                disableMobileRedirect={true}
                                cssClass="LargeButton FaceBookColor"
                                textButton="Sign up with Facebook"
                                icon={<Facebook style={{marginRight: 10}}/>}
                            />                            
                        }

                        <div style={{display:'flex', justifyContent:'center', alignItems:'center', marginTop: 20, marginBottom: 10}}>
                            <div style={{display:'flex', flex: 1, backgroundColor:'#a6a6a6', height:'1px',  marginRight: 20}}/>
                            <p className="GrayText" style={{margin: 0}}>OR</p>
                            <div style={{display:'flex', flex: 1, backgroundColor:'#a6a6a6', height:'1px',  marginLeft: 20}}/>
                        </div>
                        <div style={{display:'flex', flexDirection:'column',alignItems:'center', marginBottom: 10, textAlign:'center'}}>
                            {
                                this.state.fNameError || this.state.lNameError ? <p style={{margin:0, color:'red'}}>Please provide your first and last name.</p>: null
                            }
                            {
                                this.state.emailError ? <p style={{margin:0, color:'red'}}>Please provide a valid email.</p>: null
                            }
                            {
                                this.state.passError ? <p style={{margin:0, color:'red'}}>{PASSWORD_ERROR}</p>: null
                            }
                        </div>
                        <CustomTextbox
                            autoComplete='given-name'
                            error={this.state.fNameError}
                            value={this.state.firstName}
                            id="outlined-search"
                            label="First Name"
                            type="text"
                            full
                            onChange={ (event)=>this.updateName('firstName', event.target.value) }
                        />
                        <div style={{height:20}}/>

                        <CustomTextbox
                            autoComplete='family-name'
                            error={this.state.lNameError}
                            value={this.state.lastName}
                            id="outlined-search"
                            label="Last Name"
                            full
                            onChange={(event)=>this.updateName('lastName', event.target.value)}
                        />
                        <div style={{height:20}}/>

                        <CustomTextbox
                            error={this.state.emailError}
                            value={this.state.email}
                            id="outlined-search"
                            label="Email (Confirmation Required)"
                            type="email"
                            full
                            onChange={(event)=>{ this.setState({email: event.target.value})}}
                        />
                        <div style={{height:20}}/>
  
                        <CustomTextbox
                            error={this.state.passError}
                            value={this.state.password}
                            id="outlined-search"
                            label="Password"
                            type='password'
                            full
                            onChange={(event)=>{ this.setState({password: event.target.value})}}
                        />
                        <div style={{height:20}}/>

                        <NLButton title ="Sign up with Email" onClick={this.signUpRequest} type="LargeButton SolidPurple"/>
                        <div style={{display:'flex', flexDirection:'column',alignItems:'center', marginTop: 10}}>
                            <p className="GrayText SmallText" style={{textAlign:'center'}}>By signing up, you agree to SummerStay's <Button title ="Privacy Policy" link="/privacy" type="InlineText Purple SmallText"/> and <Button title ="Terms & Conditions" link="/terms" type="InlineText Purple SmallText"/></p>
                            <div style={{width:'100%', backgroundColor:'#a6a6a6', height:'1px'}}/>
                            <p style={{marginTop: 14, marginBottom:0}}>Already have an account? <Button title ="Log in" link="/login" type="InlineText Purple"/></p>
                        </div>

                    </div>
                </Paper>
            </div>
            </div>
            
        )
    }
}
    
SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  const mapStateToProps = state => {
    return {
        user: state.housingApp.user
    }
  }
  const mapDispatchToProps  = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user))
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(SignUp));
