import React, { Component } from 'react'
import { connect } from 'react-redux'

import '../CSS/App.css';
import '../CSS/ListingsMain.css'
import { STRIPE_API_KEY, GENERAL_ERROR, DISABLE_ID_VERIFICATION } from '../constants/index.js'
import { GET, POST } from '../constants/requests.js'

import {Elements, StripeProvider} from 'react-stripe-elements'
import PassbaseWrapper from '../components/utilities/passbaseWrapper.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import Footer from '../components/footer.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import PaymentTypeSelector from '../components/payment/paymentTypeSelector.js'
import ConfirmPayment from '../components/payment/confirmPayment.js'
import CheckoutForm from '../components/payment/checkoutForm.js'
import ErrorModal from '../components/utilities/errorModal.js'
import SuccessModal from '../components/utilities/successModal.js'
import PaymentTypeBox from '../components/payment/paymentTypeBox.js'
import StartConversationBox from '../components/payment/startConversationBox.js'
import StepNavigationButtons from '../components/utilities/stepNavigationButtons.js'
import { Helmet } from 'react-helmet';

import PropTypes from 'prop-types'
import clsx from 'clsx'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import StepConnector from '@material-ui/core/StepConnector'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Paper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'

import Check from '@material-ui/icons/Check'

import format from "date-fns/format"

const styles= theme =>({
  
  avatar: {
    margin: 10,
    color: '#fff',
    backgroundColor:'#6c4ef5',
  },

  root:{
    display:'flex', 
    flexDirection:'column',
    justifyContent:'space-between',
    width:330, 
    margin:10, 
    padding: 20
  },
  verify:{
    display:'flex', 
    flexDirection:'column',
    justifyContent:'space-between',
    width:'60%',
    maxWidth:800, 
    padding: 20, 
    margin:30
  }

})

const QontoConnector = withStyles({
  alternativeLabel: {
    top: 10,
    left: 'calc(-50% + 16px)',
    right: 'calc(50% + 16px)',
  },
  active: {
    '& $line': {
      borderColor: '#6c4ef5',
    },
  },
  completed: {
    '& $line': {
      borderColor: '#6c4ef5',
    },
  },
  line: {
    borderColor: '#eaeaf0',
    borderTopWidth: 3,
    borderRadius: 1,
  },
})(StepConnector);

const useQontoStepIconStyles = makeStyles({
  root: {
    color: '#eaeaf0',
    display: 'flex',
    height: 22,
    alignItems: 'center',
  },
  active: {
    color: '#6c4ef5',
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: '50%',
    backgroundColor: 'currentColor',
  },
  completed: {
    color: '#6c4ef5',
    fontSize: 20,
  },
});

function QontoStepIcon(props) {
  const classes = useQontoStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
    </div>
  );
}

QontoStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
};

class BookListing extends Component {
  constructor(props){
    super(props);
    this.state = {
      error: false,
      errorMessage:'',
      success: false, 
      successMessage: '',
      loading: false, 
      stage: 0, 
      paymentType: null,
      paymentMethodID: null, 
      continuous: null, 
      standard: null, 
      hostMessage:'',
      // referralCode: null,
      // verifiedReferralCode: null, // verifiedReferralCode sent to POST /reservations request
      // referralCodeError: null,
      // isReferralVerified: false,
    };
    this.bookingSteps = !DISABLE_ID_VERIFICATION ? ['Select Payment Program', 'Payment Information', 'Verify Identity', 'Confirm Booking']
                          : ['Select Payment Program', 'Payment Information', 'Confirm Booking']

  }

  componentWillMount(){
    window.scrollTo(0, 0);
  }

  componentDidMount(){
    if(this.checkProps()) {
      this.getPaymentInfo('card')
    }

    // this.loadReferralCodeFromCookies()
  }

  // if no date props passed through router, reroute to view listing
  checkProps = () =>{
    const { props } = this
    if(props.location.state === undefined){
      let path = props.location.pathname.split('/')
      let id = path[path.length-1]
      props.history.push(`/listings/view/${id}`)
      return false
    }
    return true
  }

  getPaymentInfo=(type)=>{ 
    this.setState({loading: true})
    const details = this.props.location.state.details
    const start = format((details.dates.start), "yyyy-MM-dd")
    const end = format((details.dates.end), "yyyy-MM-dd")

    const queryParams = `start_date=${start}&end_date=${end}&payment_method_type=${type}`
    const requestURL = `/api/v1/listings/${details.listing.id}/reservation_cost?${queryParams}`

    GET(`${requestURL}&charge_type=upfront`)
    .then(data => {
      this.setState({standard: data, loading: false})
    })
    .catch(error => {
      this.setState({loading: false})
    }); 
  
    GET(`${requestURL}&charge_type=continuous`)
    .then(data => {
      this.setState({continuous: data, loading: false})
    })
    .catch(error => {
      this.setState({loading: false})
    }); 
  }

  // TODO: Remove this and display separate error
  successResButFailMessage=()=>{
    this.setState({
      success: true, 
      successMessage: 'You have just successfully made a reservation request for your SummerStay. Cross your fingers and we\'ll keep you updated. We were unable to send you host a message. Try messaging them again on the dashboard.',
      loading: false
    })
  }
  successRes=()=>{
    this.setState({
      success: true, 
      successMessage: 'You have just successfully made a reservation request for your SummerStay. Cross your fingers and we\'ll keep you updated.',
      loading: false
    })
  }
  
  // Creates conversation + message on booking request
  startConversation=(details)=>{
    const send_data = {
      listing_id: details.listing.id,
      recipient_id: details.listing.host.id
    }

    POST('/api/v1/conversations', send_data)
    .then(data => {
      const conv_send_data = {
        context: this.state.hostMessage,
        user_id: this.props.user.id,
      }
      POST(`/api/v1/conversations/${data.conversation.id}/messages`, conv_send_data)
      .then(data => {
        this.successRes()
      })
      .catch(error => {
        // TODO: Refactor this
        this.successResButFailMessage()
      });

    })
    .catch(error => {
      // TODO: Refactor this
      this.successResButFailMessage()
    }); 
  }
  
  makeReservation=()=>{
    const { paymentType, paymentMethodID } = this.state

    let hostMessage = null
    if(this.state.hostMessage){
      hostMessage = this.state.hostMessage
    }

    if(paymentType === null || paymentMethodID === null){
      this.setState({
        error: true, 
        errorMessage: GENERAL_ERROR,
        loading:false
      })
      return
    }

    let details = this.props.location.state.details
    let s = `${format((details.dates.start), "yyyy-MM-dd")}`
    let e = `${format((details.dates.end), "yyyy-MM-dd")}`
    
    const send_data = {
      listing_id: details.listing.id,
      start_date: s,
      end_date: e,
      charge_type: paymentType, 
      payment_method_id: paymentMethodID,
      request_message: hostMessage,
      // referral_code: verifiedReferralCode
    }

    if (!this.state.loading) {
      this.setState({ loading: true })
      POST('/api/v1/reservations', send_data)
        .then(data => {
          if(this.state.hostMessage){
            this.startConversation(details)
          }else{
            this.successRes()
          }
        })
        .catch(error => {
          this.setState({
            error: true, 
            errorMessage: error.error,
            loading: false
          })
        });
    }

  }

  errorHandler=(message)=>{
    this.setState({
      error: true, 
      errorMessage: message
    })
  }
  navigateHandler=()=>{
    this.props.history.push({
      pathname: '/dashboard/stays'
    })
  }
  loadHandler=()=>{
    this.setState(prevState=>({
      loading: !prevState.loading
    }))
  }

  changeStage=(type, newStage)=>{
    window.scrollTo(0, 0);
    this.setState({
      stage: newStage
    })
  }

  updatePaymentID=(id)=>{
    this.setState({paymentMethodID: id})
  }

  selectPaymentType=(type)=>{
    this.setState({
      paymentType: type, 
    })
  }

  // NOTE: Currently not using referral code in booking flow
  // loadReferralCodeFromCookies = () => {
  //   // check if there's a LeadDyno affiliate code for the session
  //   // if it exists, set it as the default referral code
  //   const referralCode = window.LeadDyno.devTools.getAffiliateCode()
  //   console.log("Referral code from cookies: " + referralCode)
  //   if (referralCode) {
  //     this.setState({ 
  //       referralCode,
  //       verifiedReferralCode: referralCode,
  //     })
  //   }
  // }

  // updateReferralCode = (e) => {
  //   this.setState({ 
  //     referralCode: e.target.value,
  //     referralCodeError: null
  //   })
  // }
    
  // verifyReferralCode = () => {
  //   const { referralCode } = this.state
  //   console.log("Verifying referral code")
  //   if (referralCode) {
  //     GET(`/api/v1/verify_referral_code?referral_code=${referralCode}`)
  //       .then(data => {
  //         this.setState({
  //           verifiedReferralCode: referralCode,
  //           isReferralVerified: true,
  //         })
  //       })
  //       .catch(error => {
  //         this.setState({
  //           // NOTE: can choose to do normal error display with error/erroMessage
  //           // error: true, 
  //           // errorMessage: error.error,
  //           referralCodeError: error.error,
  //           isReferralVerified: false
  //         })
  //       });
  //   }
  // }

  render() {
    const { classes, location, user } = this.props
    const details = location.state ? location.state.details : null
    const isMobile = this.props.appSize.isSmallComputer
    const isLargePhone = this.props.appSize.isLargePhone
    const isMediumPhone = this.props.appSize.isMediumPhone
    const isSmallPhone = this.props.appSize.isSmallPhone
    const canMoveOn = (this.state.paymentType !== null && this.state.stage === 0) ||
                      (this.state.paymentMethodID !== null && this.state.stage === 1) ||
                      (user && (user.id_verify_status==='InReview' || user.id_verify_status==='Approved') && this.state.stage === 2 && !DISABLE_ID_VERIFICATION) 

    return (
      <div  style={{display: 'flex',marginTop:70, backgroundColor:'#f9f9fc'}}>
        <Helmet>
          <title>Book Your Stay - SummerStay</title>
        </Helmet>
        <CustomLoading loading={this.state.loading}/> 
        <ErrorModal show={this.state.error} message={this.state.errorMessage} close={()=>this.setState({error: false})}/>
        <SuccessModal show={this.state.success} message={this.state.successMessage} close={this.navigateHandler}/>
        {
          details ?
          <div style={{display:'flex',flex: 1,flexDirection:'column', minHeight:'calc(100vh - 70px)' }}> 
            <div>
              <Stepper alternativeLabel activeStep={this.state.stage} connector={<QontoConnector />}>
                {this.bookingSteps.map(label => (
                  <Step key={label}>
                    <StepLabel StepIconComponent={QontoStepIcon}>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
              <Divider/>
            </div>
            {
              this.props.user ? 
                <div style={{display:'flex',flexDirection:'row', justifyContent: 'center', marginLeft: isMediumPhone ? 5 : 30, marginRight: isMediumPhone ? 5 : 30,}}>
                  {
                    this.state.stage === 0 ? 
                      <PaymentTypeSelector details={details} continuous={this.state.continuous} standard={this.state.standard}
                        selectType={this.selectPaymentType} currentType={this.state.paymentType}/>
                    : null 
                  }
                  {
                    this.state.stage === 1 ? 
                      <div style={{display:'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
                        <div>
                          <StripeProvider apiKey={`${STRIPE_API_KEY}`}>
                            <Elements>
                              <CheckoutForm updatePaymentID={this.updatePaymentID}
                                error={this.errorHandler} recalculate={this.getPaymentInfo}/>
                            </Elements>
                          </StripeProvider>
                        </div>
                        <PaymentTypeBox details={details} type={this.state.paymentType} 
                          typeDetails={this.state.paymentType === 0 ? this.state.standard : this.state.continuous}
                          selected={false}/>
                      </div>
                    : null 
                  }
                  {
                    !DISABLE_ID_VERIFICATION && this.state.stage === 2 ? 
                      
                      <Paper elevation={1} className={classes.verify}>
                        <div style={{display:'flex', marginBottom:20}}>
                          <div>
                            <h2 style={{margin:0}}>Verify Identity</h2>
                            <p style={{margin:0, marginTop:10}}>
                            SummerStay requires that all renters verify their identity. This process is quick and improves the quality of bookings.
                            </p>
                          </div>
                        </div>
                        <div style={{display:'flex', justifyContent:'center', marginBottom: 20}}>
                          <PassbaseWrapper/>
                        </div>

                      </Paper>
                     
                    : null
                  }
                  {
                    (!DISABLE_ID_VERIFICATION && this.state.stage === 3) || (DISABLE_ID_VERIFICATION && this.state.stage === 2)? 
                    <div style={{display:'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
                      <div>
                      <ConfirmPayment details={details} message={this.state.hostMessage} updateMessage={(m)=>this.setState({hostMessage:m})}
                        paymentType={this.state.paymentType} referralError={this.state.referralError} submit={this.makeReservation} isMobile={isMobile}/>
                      </div>
                      <div style={{display:'flex', flexDirection:'column'}}>
                        <PaymentTypeBox details={details} type={this.state.paymentType} 
                          typeDetails={this.state.paymentType === 0 ? this.state.standard : this.state.continuous}
                          selected={false} />
                        {/* <StartConversationBox message={this.state.hostMessage} 
                          updateMessage={(m)=>this.setState({hostMessage:m})}/> */}
                        
                        {/* NOTE: Needs formatting! School name field populates if has not appeared yet */}
                        {
                          // !this.props.user.school_name ?
                          //   <Paper elevation={1} className={classes.root} style={{border:'2px solid #fff'}}>
                          //   <h4>School Name</h4>
                          //   <SchoolInfo
                          //       user={this.props.user}
                          //       updateSchoolName={this.updateSchoolName}
                          //   /> 
                          //   </Paper>
                          //   :
                          //   null
                        }

                        {/* NOTE: Currently not using referral codes on booking */}
                        {/* <Paper elevation={1} className={classes.root} style={{border:'2px solid #fff'}}>
                          <h4 style={{marginBottom:5, marginTop: 0}}>Referral Code (optional)</h4>
                          <p style={{marginBottom: 10, marginTop: 5}}>Enter a referral code here</p>
                            <CustomTextbox
                              placeholder='Optional Referral Code'
                              full
                              value={this.state.referralCode}
                              error={this.state.referralCodeError}
                              helperText={this.state.referralCodeError}
                              onChange={this.updateReferralCode}
                            />
                            <Divider />
                            <NLButton
                              title ={this.state.isReferralVerified ? "Verified!" : 'Verify'}
                              type={this.state.isReferralVerified ? "MediumButton SolidGreen" : "MediumButton"}
                              onClick={this.state.isReferralVerified ? undefined : this.verifyReferralCode} 
                            /> 
                        </Paper> */}
                      </div>
                    </div>
                    : null 
                  }
                </div>
              : null
            }
            <div style={{display:'flex', flexDirection:'column'}}>

              {
                isMediumPhone ? 
                  <div style={{display: 'flex', position: 'fixed', justifyContent: 'flex-end', alignItems: 'center', left: 0, bottom: 0, 
                    width: '100vw', height: 60, backgroundColor: 'white'}}>
                    <div style={{paddingRight: 50}}>
                      <StepNavigationButtons canMoveOn={canMoveOn} onChange={this.changeStage} 
                        stage={this.state.stage} totalStages={this.bookingSteps.length}/>
                    </div>
                  </div>
                : 
                  <div style={{display:'flex', alignSelf:'flex-end', marginBottom: 30, marginTop: 20, marginRight: '20%'}}>
                    <StepNavigationButtons canMoveOn={canMoveOn} onChange={this.changeStage} 
                      stage={this.state.stage} totalStages={this.bookingSteps.length}/>
                  </div> 
              }
              
              
            </div>
            <Footer/>
          </div> : null
        }
      </div>
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user, 
      appSize: state.housingApp.appSize
  }
}

BookListing.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default connect(mapStateToProps)(withStyles(styles)(BookListing));