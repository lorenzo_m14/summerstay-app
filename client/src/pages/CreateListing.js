import React, { Component } from 'react'
import '../CSS/App.css'
import { GET, PUT } from '../constants/requests.js'
import { DISABLE_ID_VERIFICATION } from '../constants/index.js'

import Paper from '@material-ui/core/Paper'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Button from '../components/utilities/Button.js'
import { Helmet } from 'react-helmet';

import LocationIcon from '@material-ui/icons/Room'
import RoomIcon from '@material-ui/icons/MeetingRoom'
import AmenitiesIcon from '@material-ui/icons/Deck'
import DetailsIcon from '@material-ui/icons/Create'
import PhotoIcon from '@material-ui/icons/PartyMode'
import PaymentIcon from '@material-ui/icons/CalendarToday'
import CheckIcon from '@material-ui/icons/DoneOutline'
import SecurityIcon from '@material-ui/icons/Security'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepButton from '@material-ui/core/StepButton'
import StepConnector from '@material-ui/core/StepConnector'
import StepLabel from '@material-ui/core/StepLabel'
import clsx from 'clsx'

import Switch from '@material-ui/core/Switch'

import ModalWrapper from '../components/utilities/Modal.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import CustomLoading from '../components/utilities/loadingBar.js'
import Location from '../components/editListing/location.js'
import Room from '../components/editListing/room.js'
import Details from '../components/editListing/details.js'
import Amenities, {CheckAmenities} from '../components/editListing/amenities.js'
import Photos from '../components/editListing/photos.js'
import Payment from '../components/editListing/payment.js'
import Verification from '../components/editListing/verification.js'
import ErrorModal from '../components/utilities/errorModal.js'
import { connect } from 'react-redux'

const styles= theme =>({
  root: {
    position:'relative',
    display:"flex",
    justifyContent:"center",
    margin:40,
    marginBottom: 20,
    height: 1,
    minHeight: 660,
  }

})


const BlueSwitch = withStyles({
  switchBase: {
    color: '#6c4ef5',
    
    '&$checked': {
      color: '#6c4ef5',
    },
    '&$checked + $track': {
      backgroundColor: '#6c4ef5',
    },
  },
  checked: {},
  track: {},
})(Switch);


const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 0,
    color: '#fff',
    width: 30,
    height: 30,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor: '#282c3b',
    // backgroundImage:'linear-gradient(135deg,  #1A273E, #6c4ef5  )',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',

  },
  completed: {
    backgroundColor: '#41d09c'
    // backgroundImage:'linear-gradient(135deg, #1A273E, #6c4ef5)',
  },
});

const ColorlibConnector = withStyles({
  alternativeLabel: {
    
  },
  active: {
    '& $line': {
      backgroundColor: '#fff'
      // backgroundImage: 'linear-gradient(95deg, #1A273E, #6c4ef5)',
    },
  },
  completed: {
    '& $line': {
      // backgroundImage: 'linear-gradient(95deg, #1A273E, #6c4ef5)',
      backgroundColor: '#fff'
    },
  },
  line: {
    border: 0,
    width:1,
    paddingRight:4,
    backgroundColor: '#fff',
    borderRadius: 1,
  },
})(StepConnector);

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;
  //3: <AvailabilityIcon style={{fontSize: 16}}/>,
  const icons = {
    1: <LocationIcon style={{fontSize: 16}}/>,
    2: <RoomIcon style={{fontSize: 16}}/>,
    3: <AmenitiesIcon style={{fontSize:16}}/>,
    4: <DetailsIcon style={{fontSize: 16}}/>,
    5: <PhotoIcon style={{fontSize: 16}}/>,
    6: <PaymentIcon style={{fontSize: 16}}/>,
    7: <SecurityIcon style={{fontSize: 16}}/>,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {completed ? <CheckIcon style={{fontSize: 16}}/> : icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
};


class CreateListing extends Component {
  constructor(props){
    super(props); 
    this.state={
      listing: null,
      masterListing: null, 

      loading: false,
      failed: false,
      expireMessage: null,
      error: false,
      errorMessage: null,
      show: null,
      stage: null,
      updatedFields: [],
      FINISHED: false
    };
    this.steps = !DISABLE_ID_VERIFICATION ? ['Location', 'Rooms','Amenities','Summary','Photos','Details', 'Verification'] 
                    : ['Location', 'Rooms','Amenities','Summary','Photos','Details']//'Availability','Details'
  }

  componentDidMount=()=>{
    
    this.setState({
      show: 'Location', 
      stage: 0
    })

    this.refreshListing()
  }

  refreshListing=()=>{

    this.setState({ loading: true })
    let path = this.props.location.pathname.split('/')
    let id = path[path.length-1]

    GET(`/api/v1/my_listings/${id}`)
    .then(data => {
      this.setState({
        listing: data.listing,
        masterListing: data.listing,
        loading: false
      })
    })
    .catch(error => {
      this.setState({ loading: false })
    }) 

  }

  saveChange = (finished) => {
    
    if (!this.state.loading) {
      this.setState({ loading: true })
      let updatedData = {}
      let type
      for(type in this.state.updatedFields){
        updatedData[this.state.updatedFields[type]] = this.state.listing[this.state.updatedFields[type]]
      }
      if(finished){
        updatedData['completed'] = true
      }
      const send_data = { listing: updatedData }
      return PUT(`/api/v1/listings/${this.state.listing.id}`, send_data)
        .then(data => { 
          this.handleSuccessfulUpdate(data.listing)
          return true
        })
        .catch(error => { // THIS MAY NEED SOME UPDATES
          const errorKeys = Object.keys(error.error)
          const formattedErrorKeys = errorKeys.map(k => k.replace('_', ' '))
          const firstErrorMessage = error.error[errorKeys[0]]

          this.setState(prevState=>({
            listing: prevState.masterListing,
            loading: false,
            failed: true,
            error: true,
            errorMessage: firstErrorMessage,
          }))
          return false
        });
    }
  }

  checkDone=(type, obj)=>{ // this handles the check marks
    if(type === "Location") return (obj.address === null || obj.address === '') ? false : true
    if(type === "Rooms") return (obj.bedroom===null || obj.beds===null ||  obj.bathroom===null || 
      obj.bedroom === 0 || obj.beds === 0 || obj.bathroom === 0) ? false : true
    if(type === "Amenities") return CheckAmenities(obj)
    if(type === "Summary") return (obj.summary === null || obj.listing_name === null || 
      obj.summary.length<50 || obj.listing_name.length<8) ? false : true
    if(type === "Photos") return (obj.images === null || obj.images.length < 2) ? false : true
    if(type === "Details") return (obj.price === null || obj.price === 0|| obj.start_date === null || obj.end_date === null) ? false : true
    if(type === "Verification" && !DISABLE_ID_VERIFICATION) return this.props.user && (this.props.user.id_verify_status==='InReview' || this.props.user.id_verify_status==='Approved')
    else return true
  }

  updateActive=(event)=>{
    // THIS NEEDS TO BE STREAMLINED
    if (!this.state.loading) {
      this.setState({ loading: true })
      const send_data = { listing: {active: event.target.checked} }
      PUT(`/api/v1/listings/${this.state.listing.id}`, send_data)
      .then(data => {
        this.handleSuccessfulUpdate(data.listing)
      })
      .catch(error => {
        const errorKeys = Object.keys(error.error)
        const formattedErrorKeys = errorKeys.map(k => k.replace('_', ' '))
        const firstErrorMessage = error.error[errorKeys[0]]

        this.setState(prevState=>({
          listing: prevState.masterListing,
          loading: false,
          failed: true,
          error: true,
          errorMessage: firstErrorMessage,
        }))
      });
    }
  }

  // handles callback for update listing, and updates state
  handleSuccessfulUpdate = (updatedListing) => {
    this.setState({
      listing: updatedListing,
      masterListing: updatedListing,
      loading: false,
      failed: false,
      updatedFields: []
    })
  }

  updateListing=(type, val)=>{

    let added = this.state.updatedFields.includes(type)

    this.setState(prevState=>({
      listing: {
        ...prevState.listing, 
        [type]: val
      }, 
      updatedFields: !added ? [...prevState.updatedFields, type] : [...prevState.updatedFields]
    }))
  }
  findMove=(move)=>{
    this.setState(prevState=>({
      stage: (prevState.stage+move)%this.steps.length, 
      show: this.steps[(prevState.stage+move)%this.steps.length]
    }))
  }
  buttonNav=(type,finished)=>{
    if(type === 'skip' || type === 'back'){
      let move = type === 'skip' ? 1 : -1
      this.findMove(move)
      return
    }
    if(this.state.updatedFields.length>0 || finished){
      const hold = this.state.masterListing.active
      this.saveChange(finished).then(success=>{
        if(success && !hold){
          const temp = this.state.listing
          let i = this.steps.map(s=>this.checkDone(s, temp)).indexOf(false)
          if(i===-1){
            this.setState({FINISHED: true})
            return
          }
          this.handleNav(i, this.steps[i])
        } 
      })
    }else if(type === 'forward'){
      this.findMove(1)
    }
  }

  handleNav=(i, type)=>{
    // this.saveChange(finished)
    this.setState({
      stage: i, 
      show: type
    })
  }

  render() {
    const { classes, appSize } = this.props;
    const { show, listing, masterListing } = this.state
    const ready = listing ? this.steps.map(s=>this.checkDone(s, masterListing)).filter(i => i === false).length === 1  : []
    const sectionsStatus = listing ? this.steps.map(s=>{return {step: s, isFinished: this.checkDone(s, masterListing)}}) : []

    const isMobile = appSize.isSmallComputer
    const isLargePhone = appSize.isLargePhone
    const isSmallPhone = appSize.isSmallPhone

    return (
      <div  style={{marginTop:70, backgroundColor:'#f9f9fc',minHeight:'calc(100vh - 70px)',width:'100%',
                display:"flex", flexDirection:'column',alignItems:"center", marginBottom: isLargePhone ? 60 : 0}}>
        <Helmet>
          <title>Edit Your Listing - SummerStay</title>
        </Helmet>
        <ErrorModal show={this.state.error} message={this.state.errorMessage} close={()=>this.setState({error: false})}/>
        <ModalWrapper
          open={this.state.FINISHED}
          onClose={()=>this.props.history.push('/dashboard/listings')}
        >
          <h2 style={{marginTop: 0}}>Congratulations!</h2>
          <p>Your listing is now active and can be edited in the dashboard. Look out for booking requests sent to your email.</p>
          <p>Need a SummerStay? <Button title ="Explore SummerStays" link="/listings" type="InlineText"/></p>
          
          <NLButton title ="Close" onClick={()=>this.props.history.push('/dashboard/listings')}  type="LargeButton Gray"/>
          
        </ModalWrapper>
        <CustomLoading loading={this.state.loading}/> 
        {
          listing ?
          
            <Paper className={classes.root} elevation={1}>
              {
                !isLargePhone &&
                <div style={{display:'flex', flexDirection:'column', alignItems:'flex-end', 
                          position:'absolute', top: 10, right: 10, zIndex: 9}}>
                  <Button title ="Save & Exit" link='/dashboard' arrow={true}
                      type="BackButton"/>
                  <FormControlLabel
                    control={
                      <BlueSwitch
                        disabled={listing.status !== 'Ready'}
                        checked={listing.active}
                        onChange={this.updateActive}
                        value={listing.active}
                      />
                    }
                    label="Active"
                  />
                </div>
              }

              <div style={{marginTop:5, marginBottom: 5, width: (isLargePhone ? 70 : 200)}}>
                <Stepper activeStep={this.state.stage} nonLinear orientation="vertical" connector={<ColorlibConnector />}>
                  {this.steps.map((label, index) => 
                  
                  <Step key={label}>
                    {/* <StepLabel StepIconComponent={QontoStepIcon}>{label}</StepLabel> */}
                    {
                      isLargePhone ? 
                        <StepButton onClick={()=>this.handleNav(index, label)} completed={this.checkDone(label, masterListing)}>
                          <StepLabel StepIconComponent={ColorlibStepIcon}></StepLabel>
                        </StepButton>
                    : 
                      <StepButton onClick={()=>this.handleNav(index, label)} completed={this.checkDone(label, masterListing)}>
                        <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                      </StepButton>
                    }

                  </Step>
                  )}
                </Stepper>
              </div>
              <div style={{backgroundColor:'#e0e0e0', height: '100%', width: 1}}/>
              
              {
                show === 'Location' ? 
                  <Location listing={listing} onUpdateListing={this.updateListing} ready={ready}
                    handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              {
                show === 'Rooms' ? 
                  <Room listing={listing} onUpdateListing={this.updateListing} ready={ready}
                    handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} isLargePhone={isLargePhone} />
                : null
              }
              {
                show === 'Amenities' ? 
                  <Amenities listing={listing} onUpdateListing={this.updateListing} ready={ready}
                  handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              {
                show === 'Summary' ? 
                  <Details listing={listing} onUpdateListing={this.updateListing} ready={ready} 
                    handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              {
                show === 'Photos' ? 
                  <Photos listing={listing} onUpdateListing={this.updateListing} ready={ready}
                    handleNav={this.buttonNav} handleSuccessfulUpdate={this.handleSuccessfulUpdate} 
                    sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              {
                show === 'Details' ? 
                  <Payment listing={listing} onUpdateListing={this.updateListing} ready={ready}
                    handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              {
                show === 'Verification'  && !DISABLE_ID_VERIFICATION? 
                  <Verification listing={listing} onUpdateListing={this.updateListing} ready={ready} userVerified={this.refreshListing}
                    handleNav={this.buttonNav} sectionsStatus={sectionsStatus} currStep={show} isMobile={isMobile} />
                : null
              }
              </Paper>

          
            : null
        }
        
      </div>
      
    ); 
  }
}

CreateListing.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
      user: state.housingApp.user,
      temp: state.housingApp.temp,
      appSize: state.housingApp.appSize 
  }
}

export default  connect(mapStateToProps)(withStyles(styles)(CreateListing));