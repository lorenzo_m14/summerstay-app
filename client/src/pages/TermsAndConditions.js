import React from 'react'
import Footer from '../components/footer'
import { Helmet } from 'react-helmet';

import '../CSS/App.css';
import '../CSS/Headers.css';
import '../CSS/Static.css'
import { Paper } from '@material-ui/core';


export default function TermsAndConditions(props) {
  
  window.scrollTo(0, 0)

  return (
    <div className='StaticMain'>
      <Helmet>
          <title>Terms and Conditions - SummerStay</title>
      </Helmet>
      <div className='StandardMargin'>
        <h1>Terms and Conditions</h1>
        <h2>TERMS AND CONDITIONS OF SUMMERSTAY, LLC,</h2>
        <h3>a Delaware limited liability company</h3>
        <p>Last updated: October 15th, 2019</p>
        <p>THESE TERMS AND CONDITIONS (these “Terms”) govern access to and use of the Platform, and constitute a legally binding agreement between SummerStay, LLC, a Delaware limited liability company (the “Company”, “we”, “us”, or “our”), and parties who interact with the Platform. Capitalized terms used herein have the meanings set forth in Section 16 hereof.</p>

        <h3>1.  Scope of the Company’s Services.</h3>

        <p>
        1.1.    The Company does not own, create, sell, resell, provide, control, manage, offer, deliver, or supply any Listings or Premises by nature of providing the Platform. Sublessors alone are responsible for their Listings. When Users make or accept a Sublease Agreement, they are entering into a contract directly with each other. Users understand and agree that the Company is not and does not become a party to or other participant in any contractual relationship between Users, Owners, or any combination thereof (including, without limitation, any subletting agreement), nor is the Company a real estate broker or insurer. The Company is not acting as an agent in any capacity for any User, and does not endorse any User or Listing.</p>

        <p>
        1.2.    The Company has no control over and does not guarantee (i) the existence, quality, safety, suitability, or legality of any Listings; (ii) the truth or accuracy of any Listing descriptions, ratings, reviews, or other Content; or (iii) the performance or conduct of any User or third party. Users should always exercise due diligence and care when deciding whether to stay at or sublease a Premises.
        </p>
        <p>
        1.3.    A Sublessor’s relationship with the Company is limited to being an independent, third- party contractor, and not an employee, agent, joint venturer or partner of the Company for any reason. Sublessors understand and agree that they are acting exclusively on their own behalf and for their own benefit, and not on behalf or for the benefit of the Company.
        </p>
        <p>
        1.4.    The Company may restrict the availability of the Platform or certain areas or features thereof, as deemed necessary in the Company’s discretion.
        </p>
        <p>
        1.5.    The Company reserves the right to modify these Terms at any time. If a User disagrees with the revised Terms, such User may terminate its Account and Agreement with the Company. If a User does not terminate its Account and the Agreement before the date the revised Terms become effective, such User’s continued access to or use of the Platform will constitute acceptance of the revised Terms.
        </p>


        <h3>2.  Eligibility; Using the Platform; Service Fees.</h3>

        <p>
        2.1.    In order to access and use the Platform or register an Account, a User must be an individual at least 18 years old or a duly organized, validly existing business, organization, or other legal entity in good standing under the laws of the country in which established, and able to enter into legally binding contracts.
        </p>

        <p>
        2.2.    No Sublessor may make any property available to Users through the Platform without the express, written, legal consent of the Owner of the subject Premises, which consent must be made available by the Sublessor to the Company through evidence reasonably required by the Company; provided, however, such consent need not be obtained in the event that the same is not required by the Owner pursuant to the Lease Agreement (or other binding agreement) between the Owner and Lessor.
        </p>

        <p>
        2.3.    Sublessors alone are responsible for identifying, understanding, and complying with all Laws that apply to their Listings, and for identifying and obtaining any required license, permit, or registration associated with a Sublessee’s use of the Premises.
        </p>

        <p>
        2.4.    Sublessees will be charged a first month down payment when the reservation is confirmed. Then, the rest of the nights will be charged on a monthly basis. Sublessees are required to pay all amounts due.
        </p>

        <p>
        2.5.    The Company may, through third party service providers, collect Service Fees and other related transaction amounts from Sublessees and/or Sublessors in consideration for the use of the Platform, and reserves the right to change its Service Fees at any time. Users are responsible for paying all Service Fees charged by Company. Service Fees will only be refunded in the event a Sublessor accepts cancellation of the Sublessee’s reservation and refunds the entire rental amount.
        </p>

        <h3>3.  Account.</h3>
        <p>
        3.1.    Persons seeking to engage with the Platform may be required to create an account and become a User in order to access certain features of the Platform. Prospective Users may register by logging in through a third party social networking platform (Facebook), or directly through the Website or Application using an email address, as described in this Section 3.
        </p>
        <p>
        3.2.    The Platform may allow Users to link their Accounts with third party accounts by either (i) submitting the requested information about such third party account to the Company through the Platform, or (ii) allowing the Company to access the third party account pursuant to the terms and conditions of such third party account.
        </p>
        <p>
        3.3.    By submitting a third party account information to the Company, Users represents that they are entitled to disclose such information to the Company, and/or grant the Company access to the third party account, including (but not limited to) for use for the purposes described herein, without breach of any obligations to the Company or such third party, and without obligating the Company to pay any fees or making the Company subject to any usage limitations imposed by such third-party service providers. By granting the Company access to a third party account, Users understand that the Company may access, make available, and/or store content that they have provided to and made publicly accessible through such third party account so that it is available on and/or through the Platform, and such content will be considered User Content for the purposes of these Terms. Depending on and subject to the privacy setting governing the third party account, personally identifiable information posted may be available on and through a User’s Account on the Platform.
        </p>
        <p>
        3.4.    The Company is not responsible for any User Content. Under most circumstances, Users have the ability to disable the connection between an Account and any third party account that employed on the Platform by accessing the “Settings” section of the third party’s website. PLEASE NOTE THAT A USER’S RELATIONSHIP WITH A THIRD PARTY SERVICE PROVIDER ASSOCIATED WITH SUCH A THIRD PARTY ACCOUNT IS GOVERNED SOLELY BY SUCH USER’S AGREEMENT(S) WITH THE THIRD PARTY SERVICE PROVIDER. The Company makes no effort to review any User Content for any purpose, including (but not limited to) for accuracy, legality, or non-infringement.
        </p>
        <p>
        3.5.    The Company will create an Account for a User’s use of the Platform based upon the personal information such User provides to us or that we obtain via a third party account (as described in this Section 3). Users agree to provide accurate, current, and complete information about themselves and the Premises (as applicable) during the registration process, and to update such information to keep it accurate, current, and complete. The Company reserves the right to suspend or terminate Users’ Accounts and/or access to the Platform at any time in its discretion.
        </p>
        <p>
        3.6.    Users are responsible for safeguarding their password. Users agree that they will not disclose their password to any third party, and agree to take sole responsibility for any activities or actions under their Accounts, whether or not they have authorized such activities or actions. Users must immediately notify the Company of any unauthorized use of their Account.
        </p>
        <p>
        3.7.    Each User is only permitted to create one Account. Multiple Accounts created by the same User may be deactivated in the Company’s sole discretion.
        </p>
        <h3>4.  Payment Terms.</h3>
        <p>
        4.1.    The Company provides payments services to Members, including payment collection services, payments and payouts, in connection with and through the Platform (“Payment Services”). The Company may temporarily and under consideration of the Members’ legitimate interests (e.g., by providing prior notice) restrict the availability of the Payment Services, or certain services or features thereof, to carry out maintenance measures that ensure the proper or improved functioning of the Payment Services. SummerStay Payments may improve, enhance, and modify the Payment Services, and introduce new Payment Services from time to time. The Company will provide notice to Members of any changes to the Payment Services, unless such changes do not materially increase the Members’ contractual obligations or decrease the Members’ rights under these Terms.
        </p>
        <p>
        4.2.    The Payment Services may contain links to third-party websites or resources (“Third- Party Services”). Such Third-Party Services are subject to different terms and conditions and privacy practices and Members should review them independently. The Company is not responsible or liable for the availability or accuracy of such Third-Party Services, or the content, products, or services available from such Third-Party Services. Links to such Third-Party Services are not an endorsement by the Company of such Third-Party Services. The Company is not responsible for any fees imposed by the Third Party Services and disclaims all liability in this regard.
        </p>
        <p>
        4.3.    When you add a Payment Method or Payout Method to your Account, you will be asked to provide customary billing information such as name, billing address, and financial
        instrument information either to the Company and/or its third-party payment processor(s). You must provide accurate, current, and complete information when adding a Payment Method or Payout Method, and it is your obligation to keep your Payment Method and Payout Method up-to-date at all times.
        </p>
        <p>
        4.4.    BY CHOOSING TO USE THE PLATFORM, YOU AUTHORIZE THE COMPANY TO CHARGE YOUR PAYMENT METHOD FOR ANY BOOKINGS, SERVICE FEES, OR OTHER AMOUNTS DUE IN CONNECTION WITH YOUR ACCOUNT. IF YOU AGREE TO MAKE RECURRING PAYMENTS FOR USE OF CERTAIN SERVICES, YOU ACKNOWLEDGE AND AGREE THAT THE COMPANY WILL NOT PROVIDE ADVANCE NOTICE TO YOU PRIOR TO EACH SUCH PAYMENT.
        </p>
        <h3>5.  Booking Modifications; Cancellations and Refunds.</h3>

        <p>
        5.1.    Sublessees and Sublessors are responsible for any modifications to a booking that they make via the Platform, and agree to pay any fees and/or taxes associated with such modifications.
        </p>
        <p>
        5.2.    Unless extenuating circumstances exist, a Sublessee who cancels a confirmed booking will forfeit all amounts paid to date, up to the full rental amount; however, the security deposit will remain refundable. The Sublessor who suffered the cancellation shall receive a convenience payment of 50% of the amounts collected from a Sublessee who cancelled the booking. Notwithstanding the foregoing, a Sublessee may cancel the booking after check-in in the event the Sublessor materially misrepresented the nature or condition of the Premises, amenities provided, or other information in the Listing.
        </p>
        <h3>6.  Content; Restrictions.</h3>
        <p>
        6.1.    The Company may, in its sole discretion, enable Users to (i) create, upload, post, send, receive, and store Content; and (ii) access and view the Content of another User.
        </p>
        <p>
        6.2.    Users acknowledge and agree that the Platform and the Company Content, including all intellectual property rights associated therewith, are the exclusive property of the Company and/or its licensors or authorizing third-parties. Users shall not remove, alter, or obscure any copyright, trademark, service mark or other proprietary rights or notices incorporated in or accompanying the Platform or the Company Content.
        </p>
        <p>
        6.3.    Users shall not use, copy, adapt, modify, prepare derivative works of, distribute, license, sell, transfer, publicly display, publicly perform, transmit, broadcast or otherwise exploit the Platform or any Content except to the extent any such User is the legal owner thereof, or as expressly permitted by these Terms. Except for the licenses and rights expressly granted in these Terms, no license or right to any Content is granted to any User or third party by nature of a User’s use of the Platform.
        </p>
        <p>
        6.4.    Subject to compliance with these Terms, the Company grants each User, solely for such User’s personal and non-commercial use, a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to download and use the Platform, and to access and view any Content that is made available or accessible to the User through the Platform.
        </p>
        <p>
        6.5.    By creating, uploading, posting, sending, receiving, storing, or otherwise making available any Content on or through the Platform, a User thereby grants to the Company a non-
        exclusive, worldwide, royalty-free, irrevocable, perpetual, sub-licensable, and transferable license to access, use, store, copy, modify, prepare derivative works of, distribute, publish, transmit, stream, broadcast, and otherwise exploit such Content in any manner to provide and/or promote the Platform in any manner deemed reasonable by the Company.
        </p>
        <p>
        6.6.    Each User is solely responsible for all Content made available by such User on or through the Platform. Accordingly, each User represents and warrants that: (i) they are either the sole and exclusive owner of such Content, or they have all rights, licenses, consents, and releases necessary to grant the rights in and to such Content to the Company as contemplated under these Terms; and (ii) neither the Content nor the User’s posting, uploading, publication, submission, or transmittal thereof, nor the Company’s use of the Content as contemplated under these Terms, will infringe, misappropriate, or violate a third party’s rights or result in the violation of any Law.
        </p>
        <p>
        6.7.    By creating an Account or otherwise using the Platform, each User agrees not to (i) act dishonestly or unprofessionally, including (without limitation) by posting Content that is inappropriate, inaccurate, deceitful, or objectionable, as determined by the Company in its discretion; (ii) add Content that is not intended or inaccurate for a designated field; (iii) create a false identity on the Platform; (iv) create an Account for any real person other than themselves, or for an entity for which such User has legal authorization to act on such entity’s behalf; (v) use or attempt to use another User’s Account; (vi) harass, abuse, or harm another person; (vii) send spam or other unwelcome communications to another person; (viii) act in an unlawful, libelous, abusive, obscene, discriminatory, or otherwise objectionable manner; (ix) post anything that contains software viruses, worms, or any other harmful code; or (x) copy or use the information, Content, or data on the Platform in connection with a competitive service, as determined by the Company in its discretion.
        </p>
        <p>
        6.8.    The Company reserves the right to limit Users’ use of the Platform, including the ability to contact other Users. The Company further reserves the right to restrict, suspend, or terminate a User’s Account if the Company believes, in its discretion, that such User may be in breach of this Agreement or the Law, or that such User is otherwise misusing the Platform.
        </p>
        
        <h3>7.  Terms Specific for Sublessors.</h3>
        <p>
        7.1.    Sublessors may not (i) decline a guest based on race, color, ethnicity, national origin, religion, sexual orientation, gender identity, or marital status; (ii) impose any different terms or conditions based on race, color, ethnicity, national origin, religion, sexual orientation, gender identity, or marital status; (iii) post any listing or make any statement that discourages or indicates a preference for or against any guest on account of race, color, ethnicity, national origin, religion, sexual orientation, gender identity, or marital status.
        </p>
        <p>
        7.2.    To create a Listing, Sublessors will be prompted to input certain information about the Premises being listed, including (without limitation) the location, size, features, lease term, and leasing costs thereof. Each Sublessor understands that Listings submitted by such Sublessor and authorized by the Company will be made publicly available via the Platform.
        </p>
        7.3.    Each Sublessor understands and agrees that in order to lawfully sublease the Premises to a Sublessee, such Sublessor is solely responsible for obtaining all approvals and consents as may be required by the Owner of the Premises, the Lease Agreement, or otherwise.
        <p>
        7.4.    Sublessors and Sublessees are solely responsible for ensuring the maintenance of any and all insurance required by the Owner or the Lease Agreement, or that is otherwise reasonably appropriate, including (without limitation) renter’s insurance. The Company shall not be responsible for any failure to obtain or maintain sufficient insurance.
        </p>
        <p>
        7.5.    The Company assumes no responsibility for any Listing’s compliance with any agreements with or duties to third parties or the Law. Sublessors acknowledge and agree that they are responsible for any and all Listings they post. Accordingly, each Sublessor represents and warrants that any Listing they post will not breach any agreements such Sublessor has entered into with any third parties; and will comply with all Laws and/or tax requirements that apply to any Premises listed.
        </p>
        <p>
        7.6.    The Company reserves the right, at any time and without prior notice, to remove or disable access to any Listing for any reason, including Listings that the Company in its sole discretion considers to be objectionable for any reason.
        </p>

        <h3>8.  Terms Specific for Sublessees.</h3>
        <p>
        8.1.    Subject to meeting any requirements set by the Company, Sublessees may book a Listing by following the respective booking process. Sublessees agree to pay all fees (including any security deposit required by the Sublessor) and taxes required for any booking requested.
        </p>
        <p>
        8.2.    Upon receipt of a booking confirmation from the Company, a legally binding agreement is formed between the Sublessee and Sublessor, subject to any additional terms and conditions of the Sublessor that apply, including (without limitation) any cancellation policies, rules, or restrictions specified in the Listing.
        </p>
        <p>
        8.3.    If a Sublessee book a Premises on behalf of additional Sublessees, the booking Sublessee is required to ensure that each additional Sublessee meets the requirements of, is made aware of, and agrees to these Terms, and any terms, conditions, rules, or restrictions set forth by the Sublessor. If the booking Sublessee is booking for an additional Sublessee who is a minor, such booking Sublessee represents and warrants that they are legally authorized to act on behalf of such minor.
        </p>
        <p>
        8.4.    Each Sublessee understands that a confirmed booking of a Premises is a limited license granted to the Sublessee by the Sublessor to enter, occupy, and use the Premises for the duration of the term set forth in the Sublease Agreement, during which time the Owner and/or Sublessor retain the right to re-enter the Premises in accordance with the Lease Agreement, the Sublease Agreement, and/or the Law.
        </p>
        <p>
        8.5.    Sublessees and Sublessors are solely responsible for ensuring the maintenance of any and all insurance required by the Owner or the Lease Agreement, or that is otherwise reasonably appropriate, including (without limitation) renter’s insurance. The Company shall not be responsible for any failure to obtain or maintain sufficient insurance.
        </p>

        <h3>9.  Damage to Premises; Disputes Among Users.</h3>
        <p>
        9.1.    Under no circumstances shall the Company be liable to any User or other third party for any damage to a Premises. Unless otherwise provided in the Sublease Agreement, a Sublessee is responsible for leaving the Premises (including any property located thereon or therein) in the same condition as such Premises was in when the Sublessee arrived, and understands and agrees that any failure to do so automatically forfeits any right to the return of a security deposit associated with the booking of the relevant Premises. Moreover, a Sublessee is responsible for such Sublessee’s own acts and omissions, and also for the acts and omissions of any individual to whom such Sublessee invites or otherwise provides access to the Premises (excluding the Sublessor and/or Owner of the Premises, and their invitees).
        </p>
        <p>
        9.2.    If a Sublessor believes that a Sublessee damaged the Premises, or any personal property located at the Premises, and the Users cannot resolve the matter amongst themselves, the Sublessor may escalate the matter to the Company via a Damage Claim. A Damage Claim must be submitted to the Company at any time during the Sublessee’s stay, or within two (2) weeks after moving out. A Sublessor’s Damage Claim shall include a detailed description of the alleged damage, picture evidence of such damage, an estimate as to the monetary harm resulting from the damage, and any other information or evidence reasonably required by the Company to facilitate the Company’s assessment thereof. As additional evidence of the damage, a Sublessor may submit a bill from the Owner accounting for the damage. If the amount of the damage is equal to or less than the amount of the security deposit, and the Damage Claim is timely submitted, then the Company shall review the evidence and make a determination in its sole discretion as to the merit of the Damage Claim, if appropriate under the circumstances, shall have the right to collect any such sums required to cover the damage and remit the same to the Sublessor. However, the Company will not be involved in resolution of the dispute in the event the amount of the damage is greater than the amount of the security deposit, or the Damage Claim is not timely submitted. Any decision made by the Company in relation to a Damage Claim does not affect the parties’ contractual or statutory rights, and the parties each retain the right to take legal action before a court of law.
        </p>
        <p>
        9.3.    The Sublessee will be given an opportunity to respond and provide its own evidence, and if the Company determines in its sole discretion that the Sublessee is responsible for the damage, then: (i) The Company also reserves the right to otherwise collect payment from Sublessees and pursue any remedies available to the Company in this regard if a Sublessee is responsible for damage.
        </p>
        <p>
        9.4.    Users agree to cooperate with and assist the Company in good faith, and to provide the Company with such information and take such actions as may be reasonably requested by the Company in connection with any Damage Claims, or other complaints or claims made by Users relating to the Premises or any property located thereon or therein. Users shall, upon the Company’s reasonable request and at no cost to such User, participate in mediation or a similar resolution process with another User, which process will be conducted by the Company or a third party selected by the Company or its insurer, with respect to losses for which a User is requesting payment from the Company.
        </p>
        <p>
        9.5.    Sublessors understand and agree that the Company may make a claim under any insurance policy that a Sublessor or Sublessee maintains for a Premises in the event of any damage or loss to the Premises (or personal property located therein or thereon) that
        may have been caused by, or be the responsibility of, such Sublessor or its invitees. Sublessors agree to cooperate with and assist the Company in good faith, and to provide the Company with such information as may be reasonably requested by the Company, to make a claim under any such insurance policy, including (but not limited to) executing documents and taking such further acts as the Company may reasonably request to assist the Company in accomplishing the foregoing.
        </p>

        <h3>10. Prohibited Activities.</h3>
        <p>
        10.1.   Each User is solely responsible for compliance with all Laws and tax obligations that may apply to such User’s use of the Platform. By creating an Account or otherwise using the Platform, every User agrees that it will not, directly or indirectly:
        </p>
        <p>
        10.1.1. Breach or circumvent any Law, third-party rights, or these Terms, including (without limitation) by engaging in illegal activity, including drug-related illegal activity, on or near a Premises offered through the Platform;
        </p>
        <p>
        10.1.2. Use the Platform or any Content thereon for any commercial or other purpose not expressly permitted by these Terms; in a manner that falsely implies the Company’s endorsement or partnership with any User; or in any other way that may reasonably mislead others to perceive any false affiliations between the Company and any User or other third party;
        </p>
        <p>
        10.1.3. Copy, store, access, or use any information about another User in any way that is inconsistent with these Terms, or that otherwise violates the privacy rights of another User or third party;
        </p>
        <p>
        10.1.4. Offer or otherwise suggest the availability of any Premises through the Platform without the Owner’s express written consent;
        </p>
        <p>
        10.1.5. Contact another User for any purpose not associated with a Listing or other associated purpose;
        </p>
        <p>
        10.1.6. Use the Platform to request, make, or accept a booking outside of the Platform, or to otherwise circumvent any Service Fees;
        </p>
        <p>
        10.1.7. Discriminate against or harass anyone on the basis of race, national origin, religion, gender, gender identity, physical or mental disability, medical condition, marital status, age, or sexual orientation; otherwise engage in any violent, harmful, abusive, or disruptive behavior; or otherwise engage in any behavior that violates any Law;
        </p>
        <p>
        10.1.8. Use, display, mirror, or frame the Platform or Content, or any individual element within the Platform, the Company’s name, any Company trademark, logo ,or other proprietary information, or the layout and design of any page or form contained on a page in the Platform, without the Company’s express written consent;
        </p>
        <p>
        10.1.9. Dilute, tarnish, or otherwise harm the Company’s brand in any way, including through unauthorized use of Content, registering and/or using the Company or derivative terms in domain names, trade names, trademarks, or other source identifiers, or registering and/or using domains names, trade names, trademarks or other source
        identifiers that closely imitate or are confusingly similar to domains, trademarks, taglines, promotional campaigns or Content associated with the Company;
        </p>
        <p>
        10.1.10.    Use any robot, spider, crawler, scraper, or other automated means or processes to access, collect data or other content from, or otherwise interact with the Platform for any purpose;
        </p>
        <p>
        10.1.11.    Avoid, bypass, remove, deactivate, impair, descramble, or otherwise attempt to circumvent any technological measure implemented by the Company or any of the Company’s providers;
        </p>
        <p>
        10.1.12.    Attempt to decipher, decompile, disassemble, or reverse engineer any of the software used to provide the Platform;
        </p>
        <p>
        10.1.13.    Take any action that damages or adversely affects the performance or proper functioning of the Platform, or take any action that reasonably has potential to do so;
        </p>
        <p>
        10.1.14.    Export, re-export, import, or transfer the Application; or
        </p>
        <p>
        10.1.15.    Violate or infringe anyone else’s rights, or otherwise cause harm to anyone.
        </p>
        <p>
        10.2.   Sublessors acknowledge that the Company has no obligation to monitor the access to or use of the Platform by any User, or to review, disable access to, or edit any User Content, but has the right to do so to (i) operate, secure and improve the Platform; (ii) ensure Users’ compliance with these Terms; (iii) comply with the Law or any order of a court, law enforcement, or other administrative agency or governmental body; (iv) respond to User Content that it determines in its discretion is harmful or objectionable; or (v) as otherwise set forth in these Terms. Users agree to cooperate with and assist the Company in good faith, and to provide the Company with such information and take such actions as may be reasonably requested by the Company with respect to any investigation undertaken by the Company (or its representatives) regarding the use or abuse of the Platform.
        </p>
        <p>
        10.3.   Users agree to immediately report any person that acts inappropriately (whether online or in person) to appropriate authorities, and then to the Company. Users agree that such reports will not obligate the Company to take any action beyond that required by Law.
        </p>
        
        <h3>11. Term and Termination, Suspension and other Measures.</h3>
        
        <p>
        11.1.   This Agreement between the Company and each User shall be effective for a 30-day term, at the end of which it will automatically and continuously renew for subsequent 30- day terms until such time when the Company or User terminates the Agreement in accordance with this Section 11.
        </p>
        <p>
        11.2.   Users may terminate this Agreement at any time via the Website. If a User cancels their Account as Sublessor, any confirmed booking(s) will be automatically cancelled and the relevant Sublessees will receive a full refund. If a User cancels their Account as a Sublessee, any confirmed booking(s) will be automatically cancelled and any refund will depend upon the terms of the Listing’s cancellation policy.
        </p>
        <p>
        11.3.   Without limiting any rights otherwise specified in these Terms, the Company may terminate this Agreement for convenience at any time by giving a User 30 days’ notice via email to such User’s registered email address.
        </p>
        <p>
        11.4.   The Company may immediately and without notice terminate this Agreement and/or a User’s access to the Platform, and/or cancel any existing or pending bookings, if (i) the Company deems doing so is necessary to comply with the Law or any order of a law enforcement agency; (ii) the User has breached these Terms, the Law, or third party rights; (iii) the User has provided inaccurate, fraudulent, outdated, or incomplete information during the Account registration or Listing process, or thereafter; (iv) the User and/or their Listings at any time fail to meet any applicable quality or eligibility criteria; (v) the User has repeatedly received poor ratings or reviews, or the Company otherwise becomes aware of or has received complaints about such User’s performance or conduct;
        (vi)    the User has repeatedly cancelled confirmed bookings or failed to respond to booking requests without a valid reason; or (vii) the Company believes in good faith that such action is reasonably necessary to protect the personal safety or property of the Company, its Users, or third parties, or to prevent fraud or other illegal activity.
        </p>
        <p>
        11.5.   Upon termination of a User’s Account for any reason set forth in Section 11.4 above, (a) if the User is a Sublessor, the Company may refund any Sublessees in full for any and all confirmed bookings that have been cancelled, irrespective of preexisting cancellation policies, and the User will not be entitled to any compensation for any such cancelled bookings; (b) if the User is a Sublessee, such User will not be entitled to any refunds (including any security deposits) for any such cancelled bookings; and (c) the User shall not be entitled to any restoration of their Account or User Content, to register a new Account, or to otherwise access the Platform.
        </p>
        
        <h3>12. Disclaimers; Liability; Indemnification.</h3>
        <p>
        12.1.   By choosing to use the Platform or Content, Users do so voluntarily and at their sole risk. The Platform and Content thereon are provided “AS IS,” without any express or implied warranty of any kind. Each User agrees that they have had whatever opportunity deemed by them to be necessary to investigate the Company’s services, the Laws, and any potential tax obligations that may be applicable to such User’s Listings and/or decision to stay at a Premises, and that the User is not relying upon any statement of Law or fact made by the Company relating to a Listing.
        </p>
        <p>
        12.2.   Each User acknowledges and agrees that, to the maximum extent permitted by Law, the entire risk arising out of access to and use of the Platform and Content, the publishing or booking of any Listing via the Platform, a stay at any Premises, or any other interaction a User has with other Users, whether in person or online, remains with such User. Neither the Company nor any other party involved in creating, producing, or delivering the Platform or Content will be liable for any incidental, special, exemplary, or consequential damages, including (without limitation) lost profits, loss of data or goodwill, service interruption, computer damage or system failure, or the cost of substitute products or services, or for any damages for personal or bodily injury or emotional distress arising out of or in connection with (i) these Terms, (ii) from the use of or inability to use the Platform or Content, (iii) from any communications, interactions, or meetings with other Users or other persons with whom a User communicates, interacts, or meets with as a result of use of the Platform, or (iv) from publishing or booking of a Listing, whether based on warranty, contract, tort (including negligence), product liability, or any other legal theory,
        and whether or not the Company has been informed of the possibility of such damage, even if a limited remedy set forth herein is found to have failed of its essential purpose. Except as otherwise expressly provided in these Terms, in no event will the Company’s aggregate liability arising out of or in connection with these Terms and use of the Platform including (but not limited to) from publishing or booking any Listings via the Platform, or from the use of or inability to use the Platform or Content and in connection with any Premises or interactions with other Users, exceed the amounts a User has paid or owes for bookings via the Platform as a Sublessee in the twelve (12) month period prior to the event giving rise to the liability, or as a Sublessor, the amounts paid by the Company to such Sublessor in the twelve (12) month period prior to the event giving rise to the liability, or one hundred U.S. dollars ($100.00 USD) if no such payments have been made, as applicable. The limitations of damages set forth above are fundamental elements of the basis of the bargain between the Company and each User. Some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above limitation may not apply to such User. If a User resides outside of the United States, this does not affect the Company’s liability for death or personal injury arising from its negligence, nor for fraudulent misrepresentation, misrepresentation as to a fundamental matter or any other liability which cannot be excluded or limited under the Law.
        </p>
        <p>
        12.3.   To the maximum extent permitted by Law, each User agrees to release, defend (at the Company’s option), indemnify, and hold the Company, its affiliates and subsidiaries, and their officers, directors, employees and agents, harmless from and against any claims, liabilities, damages, losses, and expenses, including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with such User’s (i) breach of these Terms; (ii) improper use of the Platform, or any rights or obligations arising thereunder; (iii) interaction with any User, stay at a Premises, or other participation in activities relating to use of the Platform, including (without limitation) any injuries, losses or damages (whether compensatory, direct, incidental, consequential, or otherwise) of any kind arising in connection with or as a result of such interaction, stay, participation or use; or (iv) breach of any Law or third party rights.
        </p>
    
        <h3>13. Dispute Resolution; Applicable Law and Jurisdiction.</h3>
        <p>
        13.1.   By registering an Account and using the Platform, each User and the Company agree that any dispute, claim, or controversy arising out of or relating to these Terms or the applicability, breach, termination, validity, enforcement, or interpretation thereof, or to the use of the Platform or Content that cannot be settled through information negotiation shall be settled by binding individual arbitration administered by the American Arbitration Association. The arbitrator’s decision will include the essential findings and conclusions upon which the arbitrator based the award. Judgment on the arbitration award may be entered in any court with proper jurisdiction. The arbitrator may award declaratory or injunctive relief only on an individual basis and only to the extent necessary to provide relief warranted by the claimant’s individual claim.
        </p>
        <p>
        13.2.   EACH USER AND THE COMPANY ACKNOWLEDGE AND AGREE THAT (I) THE PARTIES ARE WAIVING THE RIGHT TO A TRIAL BY JURY AS TO ALL ARBITRABLE DISPUTES; AND (II) TO THE FULLEST EXTENT PERMITTED BY LAW, THE PARTIES ARE WAIVING THE RIGHT TO PARTICIPATE AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS ACTION LAWSUIT, CLASS-WIDE ARBITRATION,
        PRIVATE ATTORNEY GENERAL ACTION, OR ANY OTHER REPRESENTATIVE PROCEEDING AS TO ALL DISPUTES.
        </p>
        <p>
        13.3.   These Terms will be interpreted in accordance with the laws of the State of Missouri and the United States of America, without regard to conflict-of-law provisions. Judicial proceedings (other than small claims actions) that are excluded from the agreement to arbitrate set forth above must be brought in state or federal court in St. Louis County, Missouri, unless we both agree to some other location. The parties both consent to venue and personal jurisdiction in those courts located in St. Louis County, Missouri.
        </p>
        <h3>14. General Provisions.</h3>
        <p>
        14.1.   Except as they may be supplemented by additional terms and conditions, policies, guidelines, or standards, these Terms constitute the entire Agreement between the Company and Users pertaining to the subject matter hereof, and supersede any and all prior oral or written understandings or agreements between the Company and Users in relation to the access to and use of the Platform.
        </p>
        <p>
        14.2.   No joint venture, partnership, employment, or agency relationship exists between any User and the Company as a result of this Agreement or use of the Platform.
        </p>
        <p>
        14.3.   These Terms do not and are not intended to confer any rights or remedies upon any person other than the parties.
        </p>
        <p>
        14.4.   If any provision of these Terms is held to be invalid or unenforceable, such provision will be struck and will not affect the validity and enforceability of the remaining provisions.
        </p>
        <p>
        14.5.   The Company’s failure to enforce any right or provision in these Terms will not constitute a waiver of such right or provision unless acknowledged and agreed to by us in writing. Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise permitted under the Law.
        </p>
        <p>
        14.6.   Users may not assign, transfer, or delegate this Agreement, or any rights and obligations hereunder, without the Company’s prior written consent. The Company may without restriction assign, transfer or delegate this Agreement and any rights and obligations hereunder, at its sole discretion, with thirty (30) days prior notice. Users’ right to terminate this Agreement at any time remains unaffected.
        </p>
        <p>
        14.7.   Unless specified otherwise, any notices or other communications to Users permitted or required under this Agreement, will be provided electronically and given by the Company via email, the Platform notification, or a designated messaging service.
        </p>

        <h3>15. Privacy Policy.</h3>
        <p>
        15.1.   Introduction.
        </p>
        <p>
        15.1.1. The Company respects Users’ privacy and is committed to protecting it through compliance with this Privacy Policy. This Privacy Policy describes the types of information the Company may collect from Users or that Users may provide when
        visiting the Platform, and the Company’s practices for collecting, using, maintaining, protecting, and disclosing that information.
        </p>
        <p>
        15.1.2. This Privacy Policy applies to information the Company collects: (i) on its Website;
        </p>
        <p>
        (ii)    in email, text, and other electronic messages between Users and this Website;
        </p>
        <p>
        (iii)   through mobile and desktop applications Users download from this Website, which provide dedicated non-browser-based interaction between Users and this Website; (iv) when Users interact with our advertising and applications on third-party websites and services, if those applications or advertising include links to this policy; and (v) other reasonable sources.
        </p>
        <p>
        15.1.3. This Policy does not apply to information collected by: (i) the Company offline or through any other means, including (without limitation) on any other website operated by the Company or any third party; or (ii) any third party, including (without limitation) through any application or content (such as advertising) that may link to or be accessible from or on the Website
        </p>
        <p>
        15.1.4. A User that does not agree with this Privacy Policy may choose to not use the Website or otherwise access the Platform. By accessing or using this Website or the Platform, Users agree to this privacy policy.
        </p>
        <p>
        15.2.   Children Under the Age of 16. The Website is not intended for children under 16 years of age. No one under age 16 may provide any information to or on the Website. The Company does not knowingly collect personal information from children under 16. If a User is under 16, do not use or provide any information on this Website or through any of its features, register on the Website, engage in transactions through the Website, use any of the interactive or public comment features of this Website, or provide any information about oneself to us, including one’s name, address, telephone number, email address, or any screen name or user name one may use. If the Company learns that it has collected or received personal information from a child under 16 without verification of parental consent, it will delete that information. If a User believes the Company might have any information from or about a child under 16, please send an email to support@thesummerstay.com.
        </p>
        <p>
        15.3.   Information Collected by the Company. The Company may collect several types of information from and about Users of the Website, including information: (i) by which a User may be personally identified, such as name, postal address, e-mail address, telephone number (“personal information”); (ii) that is about a User but individually does not identify such User; and (iii) about a User’s internet connection, the equipment used to access our Website, and usage details. The Company may collect this information: (x) directly from a User when such User provides it to the Company; (y) automatically as a User navigates through the Website (such as usage details, IP addresses, and information collected through cookies, web beacons, and other tracking technologies); and (z) from third parties (such as business partners).
        </p>
        <p>
        15.4.   Information Provided to the Company. Information the Company may collects on or through the Website may include: (i) information that Users provide by filling in forms on the Website, including (without limitation) information provided at the time of registering to use our Website, posting material, or requesting further services; (ii) records and copies of correspondence between Users and the Company, including (without limitation) email addresses; (iii) Users’ responses to surveys that the Company might ask Users to
        complete for research purposes; (iv) details of transactions Users carry out through the Website, including (without limitation) financial information provided through the Website; and (v) Users’ search queries on the Website. Users also may provide information to be published or displayed (“posted”) on public areas of the Website, or transmitted to other Users or third parties. Such User Content posted on and transmitted to others is done at a User’s own risk. Although the Company may limit access to certain pages, no security measures are perfect or impenetrable. Additionally, the Company cannot control the actions of other Users of the Website with whom a User may choose to share User Content. Therefore, the Company cannot and does not guarantee that User Content will not be viewed by unauthorized persons.
        </p>
        <p>
        15.5.   Automatic Data Collection. As a User navigates through and interacts with the Website, the Company may use automatic data collection technologies to collect certain information about the User’s equipment, browsing actions, and patterns, including (without limitation) details of visits to the Website, and information about a User’s computer and internet connection (such as the IP address, operating system, and browser type). The Company also may use these technologies to collect information about the User’s online activities over time and across third-party websites or other online services (behavioral tracking). The information collected automatically may only include statistical data (and not personal information) but the Company may maintain it or associate it with personal information collected in other ways or received from third parties. Doing so helps the Company improve the Website, and deliver a better and more personalized service, including by enabling us to: (i) estimate audience size and usage patterns; (ii) store information about Users’ preferences, allowing customization of the Website according to individual interests; (iii) speed up searches; and (iv) recognize a User when such User returns to the Website. Technologies used for such automatic data collection may include (without limitation): (x) cookies (or browser cookies); (y) flash cookies; and (z) web beacons.
        </p>
        <p>
        15.6.   Third Parties. Some content or applications (such as advertisements) on the Website may be served by third-parties using cookies or other technologies to collect information about Users using the Website. The information collected by such third parties may be used to provide Users with interest-based (behavioral) advertising or other targeted content. The Company does not control these third parties’ tracking technologies or how they may be used. Users with questions about an advertisement or other targeted content should contact the responsible provider directly.
        </p>
        <p>
        15.7.   How Information is Used. The Company may use information collected about Users or that Users provide to the Company, including any personal information: (i) to present the Website and its contents to Users; (ii) to provide Users with information, products, or services that requested by Users from us; (iii) to fulfill any other purpose for which Users provide it; (iv) to provide Users with notices about their accounts; (v) to carry out the Company’s obligations, and enforce its rights arising from any contracts entered into between Users and the Company (such as for billing and collection); (vi) to notify Users about changes to the Website or any products or services offered or provided though it;
        (vii)   in any other way the Company may describe when Users provide the information; and (viii) for any other purpose with a User’s consent.
        </p>
        <p>
        15.8.   Disclosure of Information. The Company may disclose aggregated information about Users without restriction, and personal information collected or provided as described in this Privacy Policy: (i) to our subsidiaries and affiliates; (ii) to contractors, service
        providers, and other third parties we use to support our business; (iii) to a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of the Company’s assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal information held by the Company about Users is among the assets transferred; (iv) to fulfill the purpose for which such information is provided; (v) for any other purpose disclosed by the Company when a User provides the information; and (vi) with a User’s consent. The Company may also disclose Users’ personal information: (x) to comply with any court order, law, or legal process, including to respond to any government or regulatory request; (y) to enforce or apply these Terms and relating agreements (such as for billing and collection purposes); and (z) if the Company believes such disclosure is necessary or appropriate to protect the rights, property, or safety of the Company, Users, or others.
        </p>
        <p>
        15.9.   California Residents. California law may provide California residents with additional rights regarding our use of personal information. To learn more about such California privacy rights, visit https://oag.ca.gov/privacy/ccpa. In addition, California’s “Shine the Light” law (Civil Code § 1798.83) permits Users of the Application that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an email to personalinformation@summerstay.com. .
        </p>
        
        <h3>16. Definitions.</h3>
        <p>
        16.1.   Certain Definitions. For the purposes of these Terms, the following terms shall have the meanings set forth below:
        </p>
        <p>
        “Account” means a account on the Platform registered by a User. 
        </p>
        <p>
        “Agreement” means the legally binding agreement between a User and the Company by such User’s acceptance of these Terms, creation of an Account, and/or use of the Platform.
        </p>
        <p>
        “Application” means the mobile application(s) through which the Platform is accessible.
        </p>
        <p>
        “Company Content” means any proprietary content of the Company, and any content licensed or authorized for use by or through the Company from a third party.
        </p>
        <p>
        “Content” means Company Content, User Content, and any other content uploaded or published on the Platform.
        </p>
        <p>
        “Damage Claim” means any claim initiated by a Sublessor against a Sublessee alleging damage to the Premises, or to any personal property on or about the Premises.
        </p>
        <p>
        “Law” means any applicable federal, state, or local law or regulation, including (without limitation) zoning laws.
        </p>
        <p>
        “Lease Agreement” means a lease agreement for a Premises entered into by and between an Owner and a Sublessor.
        </p>
        <p>
        “Listing” means a Premises published by a Sublessor on the Platform.
        </p>
        <p>
        “Owner” means the legal owner or lessor of a Premises being offered for sublease by a Sublessor to a Sublessee through the Platform.
        </p>
        <p>
        “Payment Method” means a financial instrument that you have added to your Account, such as a credit card, debit card, or PayPal account.
        </p>
        <p>
        “Payout” means a payment initiated by the Company to a Member for services performed in connection with the Platform.
        </p>
        <p>
        “Payout Method” means a financial instrument that you have added to your Account, such as a PayPal account, direct deposit, a prepaid card, or a debit card.
        </p>
        <p>
        “Platform” means the Company’s online marketplace that enables Sublessors to publish Listings, and to communicate and transact directly with Sublessees. The Platform includes the Application, the Website, and any other websites, mobile applications, or other platforms through which the Company makes such services available.
        </p>
        <p>
        “Premises” means a property owned by an Owner, leased by a Sublessor, and offered by a Sublessor to a Sublessee to sublease through the Platform.
        </p>
        <p>
        “Privacy Policy” means the policy set forth in Section 15 hereof, which policy Users are deemed to have agreed to by use of the Platform.
        </p>
        <p>
        “Service Fees” means any Sublessee Fee and/or any Sublessor Fee.
        </p>
        <p>
        “Sublease Agreement” means a sublease agreement for a Premises entered into by and between a Sublessor and a Sublessee that results from the Platform.
        </p>
        <p>
        “Sublessee” means a User that subleases a Premises from a Sublessor through the Platform.
        </p>
        <p>
        “Sublessee Fees” means any fees the Company charges a Sublessee for the Company’s services, which fees are due and owing to the Company upon the provision of such services.
        </p>
        <p>
        “Sublessor” means a lessor of a Premises pursuant to a lease agreement with the Owner of such Premises, who subleases such Premises to a Sublessee through the Platform.
        </p>
        <p>
        “Sublessor Fees” means any fees the Company charges a Sublessor for the Company’s services, which fees are due and owing to the Company upon the provision of such services.
        </p>
        <p>
        “Terms” means these terms and conditions, as updated by the Company from time to time, and any other terms or policies enacted by the Company from time to time.
        </p>
        <p>
        “User” means a registered user of the Platform, including a Sublessor or Sublessee.
        </p>
        <p>
        “User Content” means any text, photos, audio, video, or other content owned or licensed by a User, and uploaded by a User onto Platform.
        </p>
        <p>
        “Website” means the Company’s website through which the Platform available, www.thesummerstay.com, including any subdomains thereof.
        </p>
        <p>
        16.2.   Other Definitions. Other defined terms have the meanings given to such terms in other Sections these Terms.
        </p>

        <h3>17. Contact Information. </h3>
        <p>To ask questions or comment about these Terms (including the Privacy Policy), contact us at: <a href='mailto:support@thesummerstay.com' className='SimpleButton '>support@thesummerstay.com</a></p>
      </div>

      <Footer />
    </div>

  );
}