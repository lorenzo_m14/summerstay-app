import React, {Component}from 'react'

import PropTypes from 'prop-types'
import { GET } from '../constants/requests.js'
import { LISTING_TYPES, } from '../constants/index.js'
import { PARSE_QUERY, MERGE_ARRAYS, BUILD_QUERY } from '../constants/functions.js'
import { CustomTextbox, CustomCheckbox, OpenedDatePicker } from '../constants/inputs.js'
import ReactGA from 'react-ga'

import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'

import CustomLoading from '../components/utilities/loadingBar.js'
import ListingItem from '../components/listingItem.js'
import NLButton from '../components/utilities/NoLinkButton.js'
import Map from '../components/map/map.js'
import SearchBar from '../components/utilities/searchBar.js'
import { Divider, Paper, Popper, InputAdornment } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import ReactPaginate from 'react-paginate';

import moment from 'moment'
import { debounce, omitBy, isNil } from "lodash";
import { Helmet } from 'react-helmet';

import { connect } from 'react-redux'
import {updateListings, filterListings} from '../store/actions/passThrough.js'


import '../CSS/ListingsMain.css'

const styles={
  PopperPaper:{
    display:'flex', 
    flexDirection:'column',
    padding: 30, 
    minWidth: 200, 
    marginTop: 5,
    borderRadius: 10,
  }
}
const STAY_TYPES = MERGE_ARRAYS({title: LISTING_TYPES, subText:['I am looking for my own space.','I am looking for my own room.','I am looking to share a room.']})

class ListingsMain extends Component {
  constructor(props){
    super(props);
    this.initial = {
      startDate: null,
      endDate: null,
      focusedInput: 'startDate',
      beds: 1, 
      baths: 1, 
      minPrice: 10,
      maxPrice: 9999,
      listingAddress: {
        location: '', 
        lat: 42.358990, 
        lng: -71.058630
      },
      types:[],
      scale: 12,
      currentPage: 0 // for paginated requests
    }

    this.state = {
      width: window.innerWidth,
      loading: false,
      snap: false, 
      locationError: false, 
      showMap: false,
      hoverListingId: null,
      
      datePopper: null, 
      pricePopper: null, 
      bbPopper: null, 
      typesPopper: null,

      dateChanged: false, 
      locationChanged: false,
      priceChanged: false, 
      bbChanged: false, 
      typesChanged: false,
      
      dateDisplay: null,
      bedsDisplay: null, 
      bathsDisplay: null, 
      minPriceDisplay: null,
      maxPriceDisplay: null,
      typesDisplay: null,

      startDate: this.initial.startDate,
      endDate: this.initial.endDate,
      focusedInput: this.initial.focusedInput,
      beds: this.initial.beds, 
      baths: this.initial.baths, 
      minPrice: this.initial.minPrice,
      maxPrice: this.initial.maxPrice,
      listingAddress: this.initial.listingAddress,
      types: this.initial.types,

      currentPage: 0,
      
      dateError: false
    };
  }

  componentDidMount(){
    const search = PARSE_QUERY(this.props.location.search)
    this.prevScrollY = window.scrollY;
    window.addEventListener('scroll', this.handleScroll);
    window.scrollTo(0, 0);
    this.setState({loading:true})

    if( 'loc' in search) {  
      var address = ''
      
      geocodeByAddress(search.loc.split(/(?=[A-Z])/).join(' '))
      .then(results => {
        address = results[0].formatted_address

        return getLatLng(results[0])
      })
      .then(result => {
        
        if(result) {
          this.setState({ 
            listingAddress: {
              location: address, 
              lat: result.lat, 
              lng: result.lng
            },
            locationChanged: true,
            snap: true,
          }, () => { this.getListings() });
        } else {
          
        }

        
      })
      .catch(error => {
        this.getListings()
      });

    } else{
      this.getListings()
    }
  }

  // shouldComponentUpdate = (nextProps) => {
    // return this.props.location.search !== nextProps.location.search
  // }

  componentWillUnmount=()=> {
    window.removeEventListener('scroll', this.handleScroll);
  }

  updateQueryInRoute = () => {

  }

  getListings = (page) => {
    const {
      startDate, 
      endDate,
      listingAddress,
      scale,
      beds, 
      baths, 
      minPrice,
      maxPrice,
      types,
      bbChanged,
      priceChanged,
      typesChanged,
    } = this.state

    this.setState({ loading:true })
    const listingsUrl = '/api/v1/listings'

    // query params contain date, location, and pagination
    let queryParams = {
      "start_date": startDate && startDate.format('YYYY-MM-DD'),
      "end_date": endDate && endDate.format('YYYY-MM-DD'),
      "latitude": listingAddress && listingAddress.lat,
      "longitude": listingAddress && listingAddress.lng,
      "scale": scale,
    }

    // reset page param to 1 if any other filters are changed
    let pageParams = null
    if (page) {
      pageParams = {
        "page": page + 1, // pagination component is zero indexed while server is 1 indexed
      }
    } else {
      pageParams = {
        "page": 1,
      }
      this.setState({ currentPage: 0 })
    }

    // Optional params: price, bb, type
    const priceParams = priceChanged === true ? {
      "min_price": minPrice,
      "max_price": maxPrice, 
    } : null

    const bbParams = bbChanged === true ? 
    {
      "bedrooms": null,
      "beds": beds,
      "baths": baths,
    } : null

    const typeParams = typesChanged === true ? 
    {
      "stay_type": types,
    } : null

    const amenitiesQueryParams = {
      // TODO: add amenities in future
    }
    
    queryParams = {...queryParams, ...pageParams, ...priceParams, ...bbParams, ...typeParams, ...amenitiesQueryParams}

    // remove null/undefined query params when sending to server; server cannot parse "null" value in url
    const filteredQueryParams = omitBy(queryParams, isNil)
    const searchParams = new URLSearchParams(filteredQueryParams).toString()
    const requestUrl = `${listingsUrl}?${searchParams}`
    ReactGA.event({
      category: 'Search',
      action: `queryParams: ${searchParams}`, 
      label: 'Listing Search'
    })
    GET(requestUrl)
    .then(data => {
      this.props.onUpdateListings(data.listings)
      this.props.onFilterListings(data.listings)

      this.setState({ 
        loading:false,
        snap: false,
        metadata: data.metadata,
      })

      // TODO: Update react router query params on reload
      // this.props.history.push({
      //   pathname: '/listings',
      //   search: "?" + new URLSearchParams(filteredQueryParams).toString()
      // })
    })
    .catch(error => {
      this.setState({ loading: false })
    });
  }

  handlePageClick = data => {
    window.scrollTo(0, 0)

    let currentPage = data.selected
    this.setState({ currentPage }, () => {
      this.getListings(currentPage)
    });
  };

  handleScroll=(event)=> {
    if(this.prevScrollY < window.scrollY && this.state.showFilter===true){
      this.setState({showFilter: false})
    }
    this.prevScrollY = window.scrollY;
  }

  manageDate=(type)=>{
    if (type === 'save'){
      if (this.state.startDate && this.state.endDate){
        this.setState(prevState=>({
          datePopper: null,
          dateChanged: true, 
          dateDisplay: `${prevState.startDate.format("MMM Do")} - ${prevState.endDate.format("MMM Do")}`,
          focusedInput: this.initial.focusedInput
        }))
        this.getListings()
      } else{
        this.setState({dateError: true})
      }
      
    } else if(type === 'clear'){
      this.setState({
        datePopper: null,
        dateChanged: false, 
        dateDisplay: null,
        startDate: this.initial.startDate,
        endDate: this.initial.endDate,
        focusedInput: this.initial.focusedInput
      },
      () => { this.getListings() })
    }
  }

  onDatesChange = ({ startDate, endDate })=>{

    if(startDate && endDate){
      // needed because of how props are returned from component
      setTimeout(()=>
        this.setState({focusedInput: this.initial.focusedInput}), 500
      )
      
    }
    this.setState({startDate, endDate})
  }
  dayBlocked=(day)=>{
    return day.isBefore(moment().format('YYYY-MM-DD'), 'day') || day.isSame(new Date(), 'day');
  }

  updateAddress = (location, lat, lng) =>{
    let listingAddress={location:location, lat:lat, lng:lng}
    this.setState({ listingAddress: listingAddress, locationChanged: true, snap:true }, () => { this.getListings() });
    // this.props.onFilterByLocation(lat, lng)
  }

  refreshListings = (lat, lng) => {
    let listingAddress = {location: '', lat: lat, lng: lng}
    this.setState({ listingAddress: listingAddress, locationChanged: true, snap: true }, () => { this.getListings() })
  }
  
  clearLocation=()=>{
    // let la={lat: this.initial.listingAddress.lat, lng: this.initial.listingAddress.lng}
    this.setState({ listingAddress: this.initial.listingAddress, locationChanged: false }, () => { this.getListings() });
  }

  applyFilter=(type)=>{
    const { minPrice, maxPrice } = this.state
    switch(type) {
      case 'bb':
        this.setState(prevState=>({
          [type+'Popper']: null,
          [type+'Changed']: true, 
          bedsDisplay: prevState.beds, 
          bathsDisplay: prevState.baths, 
        }), () => { this.getListings() })
        break
      case 'price':
        let newMinPrice = minPrice ? minPrice : this.initial.minPrice
        let newMaxPrice = maxPrice ? maxPrice : this.initial.maxPrice
        if (newMaxPrice < newMinPrice) {
          // set min to less than 20 of max if min < max
          if (newMaxPrice < this.initial.minPrice + 20) {
            newMaxPrice = this.initial.minPrice + 20
          }
          newMinPrice = Math.max(this.initial.minPrice, maxPrice - 20)
        }
        const changed = newMinPrice !== this.initial.minPrice && newMaxPrice !== this.initial.maxPrice
        this.setState({
          [type+'Popper']: null,
          [type+'Changed']: true, 
          minPrice: newMinPrice,
          maxPrice: newMaxPrice,
          minPriceDisplay: newMinPrice, 
          maxPriceDisplay: newMaxPrice,
        }, () => { this.getListings() })
        break  
      default:
        this.setState(prevState=>({
          [type+'Popper']: null,
          [type+'Changed']: true, 
          [type+'Display']: prevState[type]
        }), () => { this.getListings() })
    }
  }

  removeFilter=(type)=>{ // I could probably just reset the filter params

    switch(type) {
      case 'bb':
        this.setState({
          [type+'Popper']: null,
          [type+'Changed']: false,
          bedsDisplay: null, 
          bathsDisplay: null, 
          beds: this.initial.beds, 
          baths: this.initial.baths, 
        }, () => { this.getListings() })
        break;
      case 'price':
        this.setState({
          [type+'Popper']: null,
          [type+'Changed']: false,
          minPriceDisplay: null, 
          maxPriceDisplay: null,
          minPrice: this.initial.minPrice,
          maxPrice: this.initial.maxPrice,
        }, () => { this.getListings() })
        break;
      default:
        this.setState({
          [type+'Popper']: null,
          [type+'Changed']: false,
          [type+'Display']: null,
          [type]: this.initial[type]
        }, () => { this.getListings() })
    }
  }

  // remove all filters except Location
  removeFilters = () => {

    this.manageDate('clear')
    const types = ['price', 'bb', 'types']
    types.map(type => {
      switch(type) {
        case 'bb':
          this.setState({
            [type+'Popper']: null,
            [type+'Changed']: false,
            bedsDisplay: null, 
            bathsDisplay: null, 
            beds: this.initial.beds, 
            baths: this.initial.baths, 
          })
          break;
        case 'price':
          this.setState({
            [type+'Popper']: null,
            [type+'Changed']: false,
            minPriceDisplay: null, 
            maxPriceDisplay: null,
            minPrice: this.initial.minPrice,
            maxPrice: this.initial.maxPrice,
          })
          break;
        default:
          this.setState({
            [type+'Popper']: null,
            [type+'Changed']: false,
            [type+'Display']: null,
            [type]: this.initial[type]
          })
      }
    })
    this.getListings()
  }

  handleFilterChange = (type, value)=>{
    if(type === 'baths' || type === 'beds'){
      this.setState(prevState=>({
        [type]:prevState[type]+value
      }))
    }
    else if(type === 'minPrice' || type === 'maxPrice'){
      const isEmpty = !value
      const isValidPrice = parseInt(value) && value > 0 && value < 10000
      if (isEmpty || isValidPrice) {
        const newPrice = isEmpty ? value : parseInt(value)
        this.setState({ [type]: newPrice })
      }
    }
    else if(type === 'types'){
      let newTypes= []
      if(value.checked){
        newTypes = [...this.state.types, value.value]
      }else {
        newTypes = [...this.state.types].filter(i=> i !== value.value)
      }
      this.setState({[type]:newTypes})
      
    }else{
      this.setState({[type]:value})
    }
  }

  handlePoppers=(type, e)=>{
    this.setState({
      datePopper: null, 
      pricePopper: null, 
      bbPopper: null, 
      typesPopper: null,
      [type]: this.state[type] ? null : e.currentTarget  
    })
  }

  constructMarks=(min, max)=>{
    // let third =  Math.floor(((max - min)/3) / 10) * 10
    let priceMarks = [{value: min,label: `$${min}`},{value: max,label: `$${max}`}]

    return priceMarks
  }

  updateHoverTooltip = (listingId) => {
    this.setState({ hoverListingId: listingId })
  }

render(){
  const { metadata } = this.state
  const { appSize, classes } = this.props
  const isMobile = appSize.isSmallComputer
  const isMediumPhone = appSize.isMediumPhone
  const isSmallPhone = appSize.isSmallPhone
  // const priceMarks = this.constructMarks(this.initial.price[0],this.initial.price[1])

  return (
      <div style={{marginTop:70, backgroundColor:'#f9fafc'}}>
        <Helmet>
          <title>Find Your Stay - SummerStay</title>
        </Helmet>
        <CustomLoading loading={this.state.loading}/> 

        <div className="MainListing">
          
          {/* if not mobile - show map on left, else hide map and show button for showing mobile map */}
          {
            this.state.showMap || !isMobile ? 
              <div style={{width: isMobile ? '100%' : '45vw', height: '100%', position: 'fixed', left: 0, top: isMobile ? 200 : 55, marginBottom: 20}}>
                <Map 
                  lat={this.state.listingAddress.lat} 
                  lng={this.state.listingAddress.lng}
                  snap={this.state.snap}
                  zoom={!this.state.locationChanged ? 12: 12}
                  refreshListings={this.refreshListings} 
                  hoverListingId={this.state.hoverListingId}
                />
              </div>
            : null
          }
          
          {
          
            isMobile ?
              <div style={{position: 'fixed', bottom: 25, width: '100%', zIndex: 3, paddingTop: -10, paddingBottom: -10, paddingRight: -10, paddingLeft: -10 }}>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <NLButton title={this.state.showMap ? 'listings' : 'map'} type={this.state.showMap ? 'MediumButton Purple' : 'MediumButton SolidPurple'} onClick={()=>this.setState({ showMap: !this.state.showMap })} />
                </div>
              </div>
            : null
          }
          
          <div style={{display:'flex', flexDirection:'column', position: 'fixed', width: isMobile ? 'calc(100% - 40px)' :'55vw', marginLeft: isMobile ? 0 : '45vw', 
                      marginBottom:5, padding:20, paddingBottom: 0, backgroundColor:'#fff', boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.05), 0 1px 4px 0 rgba(0, 0, 0, 0.05)', paddingRight: 20}}>
            <div style={{display:'flex', flex:1, width: isMobile ? '100%' : '93%' }} onClick={this.closeFilter}>
              <SearchBar initialVal={this.state.listingAddress.location} error={this.state.locationError} placeholder='Try "San Francisco"' main={false} 
                clear={this.clearLocation} updateLocation={this.updateAddress} problem={(val)=>this.setState({locationError: val})}/>
              <div style={{height:50}}/>
            </div>
            <div style={{display:'flex', overflow: 'auto'}}>
              <div>
                <NLButton title={this.state.dateDisplay ? `${this.state.dateDisplay}`:'Dates'} 
                  onClick={(e)=>this.handlePoppers('datePopper', e)} 
                type={Boolean(this.state.datePopper) || this.state.dateDisplay ? "FilterButton FilterSelected" : "FilterButton"}/>
                <Popper 
                  open={Boolean(this.state.datePopper)}
                  anchorEl={this.state.datePopper}
                >
                  <Paper className={classes.PopperPaper} elevation={2}>
                    {
                      this.state.dateError && <p style={{color: 'red'}}>Make sure to select a start date and end date before saving</p>
                    }
                    
                    <OpenedDatePicker
                      numberOfMonths={isMobile ? 1 : 2}
                      startDate={this.state.startDate} 
                      endDate={this.state.endDate} 
                      
                      focusedInput={this.state.focusedInput} 
                      onFocusChange={focusedInput =>this.setState({ focusedInput })} 
                      onDatesChange={this.onDatesChange} 
                      isDayBlocked={this.dayBlocked}
                      initialVisibleMonth={null} 
                    />
                    
                    
                    <div style={{height: 20}}/>
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                      
                      <NLButton title='Clear' onClick={()=>this.manageDate('clear')} 
                          type='MiniMediumButton GrayPurple'/>
                          
                      <NLButton title='Save' onClick={()=>this.manageDate('save')} 
                          type= 'MiniMediumButton SolidPurple'/>
                    </div>
                  </Paper>
                  
                </Popper>
              </div>
              <div>
                <NLButton title={this.state.minPriceDisplay !== null ? `$${this.state.minPriceDisplay} - $${this.state.maxPriceDisplay}`:'Weekly Price'} 
                          onClick={(e)=>this.handlePoppers('pricePopper', e)} 
                          type={Boolean(this.state.pricePopper) || this.state.minPriceDisplay !== null ? "FilterButton FilterSelected" : "FilterButton"}/>
                <Popper 
                  open={Boolean(this.state.pricePopper)}
                  anchorEl={this.state.pricePopper}
                >
                  <Paper className={classes.PopperPaper} elevation={2}>
                    <div style={{marginTop:20, marginBottom: 20}}>
                      <div style={{display:'flex', justifyContent:'space-between', alignItems: 'center'}}>
                          <CustomTextbox
                            style={{ maxWidth:100, marginRight:10 }}
                            value={this.state.minPrice}
                            id="max-price"
                            label='Min Price'
                            type="text"
                            full
                            onChange={(e)=>this.handleFilterChange('minPrice', e.target.value)}
                            InputProps={{
                              startAdornment:<InputAdornment position="start">$</InputAdornment>
                            }}
                          />  
                          <hr color='#787878' size='1.5' width='10' margin='20' />
                          <CustomTextbox
                            style={{ maxWidth:100, marginLeft:10 }}
                            value={this.state.maxPrice}
                            id="min-price"
                            label='Max Price'
                            type="text"
                            full
                            onChange={(e)=>this.handleFilterChange('maxPrice', e.target.value)}
                            InputProps={{
                              startAdornment:<InputAdornment position="start">$</InputAdornment>,
                            }}
                          />                         
                      </div>
                    </div>
                    <Divider/>
                    <div style={{height: 20}}/>
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                      
                      <NLButton title='Clear' onClick={()=>this.state.priceChanged ? this.removeFilter('price') : undefined} 
                          type='MiniMediumButton GrayPurple'/>
                          
                      <NLButton title='Save' onClick={()=>this.applyFilter('price')} 
                          type= 'MiniMediumButton SolidPurple'/>
                    </div>
                    
                  </Paper>
                  
                </Popper>
              </div>
              <div>
                <NLButton title={this.state.bedsDisplay && this.state.bathsDisplay? `${this.state.bedsDisplay}+ Beds & ${this.state.bathsDisplay}+ Baths` :'Beds & Baths'} 
                  onClick={(e)=>this.handlePoppers('bbPopper', e)} 
                type={Boolean(this.state.bbPopper) || this.state.bedsDisplay ? "FilterButton FilterSelected" : "FilterButton"}/>
                <Popper 
                  open={Boolean(this.state.bbPopper)}
                  anchorEl={this.state.bbPopper}
                >
                  <Paper className={classes.PopperPaper} elevation={2}>
                    {
                      ['beds', 'baths'].map(i=>
                        <div key={i} style={{display:'flex', alignItems:'center', justifyContent: 'space-between', marginBottom: 20}}>
                          <p style={{margin: 0, marginRight: 10}}>{i.charAt(0).toUpperCase() + i.slice(1)}</p>
                          <div style={{display:'flex',alignItems:'center'}}>
                          <div>
                            <NLButton title='-' onClick={()=>(i === 'beds' && this.state[i] !== 1) || (i === 'baths' && this.state[i] !== .5)
                                    ? this.handleFilterChange(i,i === 'beds' ? -1: -.5) : undefined} 
                              type={(i === 'beds' && this.state[i] !== 1) || (i === 'baths' && this.state[i] !== .5)
                                     ? 'MiniMediumButton LargeText SolidWhitePurple' : 'MiniMediumButton Disabled LargeText'}/>
                          </div>
                          <p style={{margin: 0, textAlign:'center',marginLeft: 10, marginRight: 10, width: 25}}>{this.state[i]}</p>
                          <div>
                            <NLButton title='+' onClick={()=>this.state[i] !== 10? this.handleFilterChange(i,i === 'beds' ? 1: .5) : undefined} 
                              type={this.state[i] !== 10 ? 'MiniMediumButton LargeText SolidWhitePurple' : 'MiniMediumButton Disabled LargeText'}/>
                          </div>
                          </div>
                        </div>
                        )
                    }
                    <Divider/>
                    <div style={{height: 20}}/>
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                      <NLButton title='Clear' onClick={()=>this.state.bbChanged ? this.removeFilter('bb') : undefined} 
                          type= 'MiniMediumButton GrayPurple'/>
                      <NLButton title='Save' onClick={()=>this.applyFilter('bb')} 
                          type= 'MiniMediumButton SolidPurple'/>
                    </div>
                  </Paper>
                  
                </Popper>
              </div>
              <div>
                <NLButton title={this.state.typesDisplay ? `${this.state.typesDisplay.length} Types of Stays` :'Type of Stay'} 
                  onClick={(e)=>this.handlePoppers('typesPopper', e)} 
                  type={Boolean(this.state.typesPopper) || this.state.typesDisplay ? "FilterButton FilterSelected" : "FilterButton"}/>
                <Popper 
                  open={Boolean(this.state.typesPopper)}
                  anchorEl={this.state.typesPopper}
                >
                  <Paper className={classes.PopperPaper} elevation={2}>
                    {
                      STAY_TYPES.map(t=>
                        <div key={t.title} style={{marginBottom: 20}}>
                          <CustomCheckbox 
                            checked={this.state.types.includes(t.title)}
                            onChange={(e)=>this.handleFilterChange('types', e.target)}
                            label={t.title}
                            subText={t.subText}
                            value={t.title}
                          />
                        </div>
                        
                        )
                    }

                    
                    <Divider/>
                    <div style={{height: 20}}/>
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                      <NLButton title='Clear' onClick={()=>this.state.typesChanged ? this.removeFilter('types') : undefined} 
                          type= 'MiniMediumButton GrayPurple'/>
                      <NLButton title='Save' onClick={()=>this.applyFilter('types')} 
                          type= 'MiniMediumButton SolidPurple'/>
                    </div>
                  </Paper>
                  
                </Popper>
              </div>
            </div>
            {/* <NLButton title ="Filter Search" onClick={this.showFilter} type="ListingSearchButton"/> */}
          </div>
          
          {
            !this.state.showMap || !isMobile ? 
                <div style={{ display: 'flex', flexDirection: 'column', marginLeft: isMobile? 0 : '45vw', marginTop: (isMediumPhone ? 150: 130), minHeight: 'calc(100vh - 90px)'}}>
                  <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap'}}>
                    {
                      this.props.filteredListings.map((listing)=>
                        <ListingItem 
                          key={listing.id} 
                          dates={{startDate: this.state.startDate, endDate: this.state.endDate}} 
                          listing={listing}
                          updateHover={this.updateHoverTooltip}
                        />
                      )
                    }
                    
                  </div>

                  {
                    metadata && this.props.filteredListings.length > 0 ?

                    <div style={{ maxWidth: '100%', textAlign: 'center', marginBottom: 60 }}>
                      {/* Consider moving this div into a separate component */}
                      <ReactPaginate
                        previousLabel={'prev'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        forcePage={this.state.currentPage}
                        pageCount={this.state.metadata ? this.state.metadata.total_pages : 1}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={'pagination'}
                        subContainerClassName={'pagination'}
                        activeClassName={'active'}
                      />
                      {/* Calculate initial to last listing of pagination out of total. conditional on if last page */}
                      <p>Showing listings {metadata.offset + 1}-{metadata.offset + metadata.per_page < metadata.total_count ? metadata.offset + metadata.per_page : metadata.total_count} of {metadata.total_count}</p>
                    </div>
                    : null
                  }

                  {
                    metadata && this.props.filteredListings.length === 0 && !this.state.loading ? 
                    <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginTop: '20%'}}>
                      <h1>Whoops, we didn't find any listings</h1>
                      <h3>Try widening your search by <NLButton title='removing all filters' type='PurpleTransparent InlineText' onClick={()=> this.removeFilters()} /></h3>

                    </div>
                    : null
                  }
                </div>
            : null
          }
        </div>
      </div>
     
    );
  }
}


ListingsMain.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
      listings: state.housingApp.listings, 
      filteredListings: state.housingApp.filteredListings,
      appSize: state.housingApp.appSize
  }
}
const mapDispatchToProps  = dispatch => {
  return{
      onUpdateListings: (newListings) => dispatch(updateListings(newListings)), 
      onFilterListings: (newListings) => dispatch(filterListings(newListings))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(ListingsMain));