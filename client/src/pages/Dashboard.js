import React, { Component } from 'react'
import '../CSS/App.css'
import '../CSS/Buttons.css'

import NLButton from '../components/utilities/NoLinkButton.js'
import ModalWrapper from '../components/utilities/Modal.js'
import UserWizard from '../components/dashboard/userWizard.js'
import DashNotifications from '../components/dashboard/notifications.js'
import DashListings from '../components/dashboard/listings.js'
import DashSubListing from '../components/dashboard/subListing.js'
import DashStays from '../components/dashboard/stays.js'
import DashSubStays from '../components/dashboard/subStay.js'
import DashConversations from '../components/dashboard/conversations.js'
import DashPayments from '../components/dashboard/payments.js'
import DashPayouts from '../components/dashboard/payouts.js'
import DashSettings from '../components/dashboard/settings.js'
import NoMatch from '../pages/NoMatch.js'
import CircleLoad from '../components/utilities/circleLoad.js'

import { HUMANIZE, PARSE_QUERY } from '../constants/functions.js'
import { GET } from '../constants/requests.js'
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom"
import { Helmet } from 'react-helmet';

import { connect } from 'react-redux'
import { loginUser, reset } from '../store/actions/passThrough.js'


const sideBarTitles = ['notifications','chats', 'listings','payouts','stays','payments','settings']

class Dashboard extends Component {
    constructor(){
      super();
      this.state={
        loading:false,
        show:null,
        selectedListing: null, 
        selectedStay: null, 
        showIntro: false,
        logoutModal: false,
        logoutLoading: false
      }
    }

    componentDidMount(){
        this.setPage();
        if(this.props.user && !this.props.user.user_type){
            this.setState({ showIntro: true })
        }
    }

    componentDidUpdate(previousProps, previousState){
        if (previousProps.location.pathname !== this.props.location.pathname) {
            this.setPage();
        }
    }

    setPage() {
        let urlPaths = this.props.location.pathname.split('/');
        const dashboardSelection = 2;
        let path = urlPaths[dashboardSelection] ? urlPaths[dashboardSelection] : 'listings'
        this.setState({show: path})
        window.scrollTo(0,0)
    }

    logout=()=>{
        this.setState({logoutLoading: true})
        GET('/api/v1/users/logout')
        .then(data => {
            //DO NOT CHANGE THIS ORDER
            this.props.history.push('/login')
            this.props.onReset(true)
            console.log('Redirecting to login')
        })
        .catch(error => {
            this.setState({logoutLoading: false})
        })
        
    }

    // TODO: remove type from all signatures
    changePage=(type, route)=>{
        console.log("Changing page");
        // this.setState({show:type})
        if(route) {
            this.props.history.push(route);
        }
    }
    
    selectListing=(selectedListing)=>{
        this.setState({ selectedListing: selectedListing })
        this.changePage('listings', `/dashboard/listings/${selectedListing.id}/info`)
    }

    selectStay=(selectedStay)=>{
        this.setState({ selectedStay: selectedStay })
        this.changePage('stays', `/dashboard/stays/${selectedStay.id}/info`)
    }

    

  render() {
    const isMobile = this.props.appSize.isSmallComputer
    return (

        <div style={{display:"flex",flexDirection:'column',alignItems:'flex-start',marginTop:70}}>
            <Helmet>
                <title>Dashboard - SummerStay</title>
            </Helmet>
            <ModalWrapper
                open={this.state.logoutModal}
                onClose={()=>this.setState({logoutModal: false})}
                style={{maxWidth: 250}}
            >
                <h3 style={{marginTop: 0, marginBottom: 0}}>Logout</h3>  
                <p>Are you sure you want to logout?</p>  
                {
                    !this.state.logoutLoading ? 
                        <div style={{display:'flex', justifyContent:'space-between'}}>
                            <NLButton title='Cancel' onClick={()=>this.setState({logoutModal: false})} type="MediumButton Gray"/>
                            <NLButton title ='Logout' onClick={this.logout} type="MediumButton SolidPurple"/>
                        </div>
                    :
                        <div style={{height: 41}}>
                            <CircleLoad/>
                        </div>
                }            
                
            </ModalWrapper>
            <ModalWrapper
                open={this.state.showIntro}
                onClose={()=>undefined}
                style={{maxWidth: 400}}
                disableEnforceFocus
            >
                {/* TODO: Make this its own form/wizard component */}
                <UserWizard close={()=>this.setState({showIntro: false})}/>
            </ModalWrapper>
                    
            {/* <CustomLoading loading={this.state.loading}/>  */}
            <div style={{display:"flex",width:'100%'}}>
                
                <div style={{display:"flex",flex: 1,flexDirection:'column',width: isMobile ? 99 : '20%', 
                            alignItems:'flex-start',paddingTop:0, backgroundColor:'#fff', minHeight:'calc(100vh - 70px)'}}>

                    {
                        sideBarTitles.map(barTitle =>
                            <NLButton key={barTitle} title ={isMobile ? '': HUMANIZE(barTitle)} icon={barTitle}
                                onClick={()=>this.changePage(barTitle,`/dashboard/${barTitle}`)}
                                type={this.state.show === barTitle ? "DashMain On" : "DashMain Off"}/> 
                        )
                    }

                    <NLButton title ={isMobile ? '':'Logout'} icon='exit'
                        onClick={()=>this.setState({logoutModal: true})} type="DashMain Off"/>
                </div>
                <div style={{borderRight:'1px solid #e0e0e0'}}/>
                
                <div style={{display:"flex",width: isMobile ? 'calc(100% - 99px)' : '80%',justifyContent:'center', position:'relative', 
                backgroundColor:'#f9f9fc'}}>
                    {/* nested routes if user props has been passed */}
                        {
                           this.props.user ? 
                        
                                <Switch>
                                    <Redirect exact from={'/dashboard'} to='/dashboard/listings' />
                                    <Route path={`/dashboard/notifications`}  
                                        render={(props) => { 
                                        return <DashNotifications {...props} onPageChange={this.changePage}/>
                                    }}/>
                                    <Route path={`/dashboard/chats`}  
                                        render={(props) => { 
                                        return <DashConversations {...props}/>
                                    }}/>
                                    <Route exact path={`/dashboard/listings`}  
                                        render={(props) => { 
                                        return <DashListings {...props} selectListing={this.selectListing}/>
                                    }}/>
                                    <Route path={`/dashboard/listings/:listing_id`}  // sublisting page
                                        render={(props) => { 
                                        return <DashSubListing {...props} selectedListing={this.state.selectedListing} onPageChange={this.changePage}/>
                                    }}/>
                                    <Route exact path={`/dashboard/stays`}  
                                        render={(props) => { 
                                        return <DashStays {...props} selectStay={this.selectStay}/>
                                    }}/>
                                    <Route path={`/dashboard/stays/:stay_id`}  
                                        render={(props) => { 
                                        return <DashSubStays {...props} selectedStay={this.state.selectedStay} onPageChange={this.changePage}/>
                                    }}/>
                                    <Route path={`/dashboard/payments`}  
                                        render={(props) => { 
                                        return <DashPayments {...props}/>
                                    }}/>
                                    <Route path={`/dashboard/payouts`}  
                                        render={(props) => { 
                                        return <DashPayouts {...props}/>
                                    }}/>
                                    <Route path={`/dashboard/settings`}  
                                        render={(props) => { 
                                        return <DashSettings {...props}/>
                                    }}/>
                                    <Redirect to={'/error'} component={NoMatch} /> 
                                </Switch> : null
                       }
 
                </div>
                {/* <div style={{width:'15%',borderLeft:'1px solid #e0e0e0', backgroundColor:'#f9fafc'}}/> */}
            </div>
        </div>
      
    ); 
  }
}
const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}
const mapDispatchToProps  = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user)), 
        onReset: (mandatoryClear) => dispatch(reset(mandatoryClear)),

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard);
