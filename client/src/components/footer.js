import React, { PureComponent } from 'react'
import logo from '../Assets/SSIcon.png'
import Button from './utilities/Button.js'
import { connect } from 'react-redux'
import '../CSS/App.css'
import { BrowserRouter as Router, Route, Link } from "react-router-dom"

class Footer extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  render() {
    const { appSize } = this.props 
    const isMediumPhone = appSize.isMediumPhone

    return (
      <footer className="Footer">
        <div style={{display:'flex', flex: 1, alignItems:'flex-start', justifyContent:'center', flexWrap: 'wrap' }}>
          {/* <div style={{display:'flex', flex: 1, flexWrap:'wrap',justifyContent: 'space-between' }}> */}
          <div style={{display:'flex', flex: 1, flexDirection: isMediumPhone ? 'column' : 'row', justifyContent: 'space-between' }}>
            <div style={{display:'flex', flexDirection:'column', marginTop: 18, marginBottom: 18, marginRight: 5}}>
              <Link to="/">
                <img src={logo} alt="SummerStay" height="80" width="auto"/>
              </Link>
            </div>
            <div style={{display:'flex', flexDirection:'column', marginLeft: 5, marginRight: 5}}>
              <h3 style={{color:'#282c3b', marginBottom:10}}>Documentation</h3>
              <Button title ="Terms and Conditions" link="/terms" type="SimpleButton GrayPurple"/>
              <Button title ="Privacy Policy" link="/privacy" type="SimpleButton GrayPurple"/>
              <Button title ="Policies" link="/policies" type="SimpleButton GrayPurple"/>
              {/* <Button title ="Community Guidelines" link="/community-guidelines" type="SimpleButton GrayPurple"/> */}
            </div>
            <div style={{display:'flex', flexDirection:'column', marginLeft: 5, marginRight: 5}}>
              
              <h3 style={{color:'#282c3b', marginBottom:10}}>Product</h3>
              <Button title ="How It Works" link="/how-it-works" type="SimpleButton GrayPurple"/>
              <Button title ="FAQ" link="/how-it-works?FAQ=true" onClick={()=>this.props.pageScroll ? this.props.pageScroll() : undefined} type="SimpleButton GrayPurple"/>
              <Button title ="Find Your Stay" link="/listings" type="SimpleButton GrayPurple"/>
              <Button title ="Payment Plans" link="/payment-plans" type="SimpleButton GrayPurple"/>
              <Button title ="List Your Place" link="/host" type="SimpleButton GrayPurple"/>
            </div>
            
            
            <div style={{display:'flex', flexDirection:'column', marginLeft: 5, marginRight: 5}}>
              <h3 style={{color:'#282c3b', marginBottom:10}}>Company</h3>
              <Button title ="About" link="/?about=true" onClick={()=>this.props.pageScroll ? this.props.pageScroll() : undefined} type="SimpleButton GrayPurple"/>
              <Button title ="Join Our Team" link="/campus-managers" type="SimpleButton GrayPurple"/>
              <Button title ="Support" link="/support" type="SimpleButton GrayPurple"/>
            </div>
            
          </div>
        </div>
      </footer>
    )
  }
}
  
const mapStateToProps = state => {
  return {
      appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(Footer);