import React from 'react'
import '../../CSS/App.css'

import { DISPLAY_NUMBER } from '../../constants/functions.js'
import Divider from '@material-ui/core/Divider'
import format from "date-fns/format"
import parse from "date-fns/parse"
import addDays from "date-fns/addDays"


export default function ChargeLineItems(props){
  
  const { details, type, typeDetails } = props

  return (
      <div>
        <div style={{display:'flex', justifyContent:'space-between', alignItems: 'center'}}>
          <div style={{display:'flex', flexDirection:'column', maxWidth:'50%'}}>
            <h2 style={{marginBottom:5, marginTop:0,wordBreak: 'break-word' }}>{details.listing.listing_name}</h2>
            <p style={{marginBottom: 10}}>{`${format(details.dates.start, "MMM do")}`} - {`${format(details.dates.end, "MMM do")}`}</p>
          </div>
          <div style={{display:'flex'}}>
            <div style={{display:'flex', flexDirection:'column', alignItems:'center',marginRight: 20}}>
              <h4 style={{marginBottom:5, marginTop:5}}>Payment Plan</h4>
              <p style={{marginBottom: 10}}>{type === 0 ? "Standard" : "Pay-As-You-Stay"}</p>
            </div>
            
            <div style={{display:'flex', flexDirection:'column',alignItems:'center',}}>
              <h4 style={{marginBottom:5, marginTop:5}}>Total Cost</h4>
              <p style={{marginBottom: 10}}>${DISPLAY_NUMBER(typeDetails.reservation_cost.total_cost)}</p>
            </div>
              
          </div>
        </div>
        <div style={{display:'flex', flexDirection:'column', borderRadius: 5,marginTop:20, marginBottom: 20}}>
          <div style={{display:'flex', justifyContent:'space-between',padding: 10, paddingTop:0}}>
            <p style={{margin: 0, width: 200}}>Payment Due Date</p>
            <p style={{margin: 0, width: 100, textAlign:'right'}}>Amount</p>
          </div>
          <Divider/>
          {
            typeDetails.charges.map( (c, i) =>
              <div style={{display: 'flex',justifyContent:'space-between', padding: 10}} key={c.id}>
                <p style={{margin: 0,width: 200}}>{ i === 0  ? 'Host Acceptance' : `${format((parse(c.due_by,'yyyy-MM-dd',new Date())), "MMM dd, yyyy")}`}</p>
                <p style={{margin: 0,width: 100, textAlign:'right'}}>{"$" + DISPLAY_NUMBER(c.total_cost)}</p>
              </div>
            )
          }
          <Divider/>
          <div style={{display: 'flex',justifyContent:'space-between', padding: 10, paddingBottom: 0}}>
            <p style={{margin: 0,width: 200}}>{format(addDays(details.dates.end, 30), "MMM dd, yyyy")}</p>
            <p style={{margin: 0,width: 200 }}>Deposit Returned</p>
            <p style={{margin: 0,width: 100, textAlign:'right'}}>{"$" + DISPLAY_NUMBER(typeDetails.reservation_cost.deposit)}</p>
          </div>

        </div>
        {
          type === 0 ? 
          <p className="SubTitleText" style={{fontSize: 12}}>The first payment is charged upon host acceptance of a booking request. The second payment is due by the date on your receipt and can be paid at any time prior by visiting the Payments tab on your dashboard.</p>
          :
          <p className="SubTitleText" style={{fontSize: 12}}>The first payment is charged upon host acceptance of a booking request. Subsequent payments are charged on the date on your receipt to the payment method selected on your booking receipt.</p>
        }
       
      </div>
  )
}

