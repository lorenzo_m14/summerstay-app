import React, { Component } from 'react';
import '../../CSS/App.css';
import '../../CSS/ListingsMain.css'
import ReactGA from 'react-ga'

import NLButton from '../utilities/NoLinkButton.js'
import { DISPLAY_NUMBER, PARSE_QUERY, SAVE_ROUTE } from '../../constants/functions.js'
import { DatePicker } from '../../constants/inputs.js'
import { DATE_FORMAT } from '../../constants/index.js'
import moment from 'moment'
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import PaymentInfo from './paymentInfo.js'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import Tooltip from '@material-ui/core/Tooltip'
import Support from '@material-ui/icons/ContactSupport'

// need component did mount that makes a request to the server for the listing
class BookingBox extends Component {
  constructor(props){
    super(props);
    this.state = {
      startDate: null,
      endDate: null,
      focusedInput: null,
      numWeeks:1, 
      weekError: false,
      paymentType: 'standard',
      badSelection: false
    };
    this.daysBlocked=[]
    this.invalid = false
  }

  componentDidMount(){
    if(this.props.listing.unavailable_dates){
      this.daysBlocked = this.props.listing.unavailable_dates.map(d=>moment(d))
    }

    const query = PARSE_QUERY(window.location.search)

    if ('startDate' in query && 'endDate' in query){
      const startDate = moment(query.startDate, DATE_FORMAT, true)
      const endDate = moment(query.endDate, DATE_FORMAT, true)
      if(startDate.isValid() && endDate.isValid()){
        const numWeeks = this.getNumWeeks(startDate, endDate)
        const validRange = !this.checkForBlockedDates(startDate, endDate)
        if(numWeeks>=4 && validRange && 
          startDate.isSameOrAfter(this.props.listing.start_date) && endDate.isSameOrBefore(this.props.listing.end_date))
          this.setState({numWeeks, startDate, endDate})

      }
    }
  }

  bookingRequest=(history)=>{

    if(this.props.user === null){
      let rr = `/listings/view/${this.props.listing.id}`
      if(this.state.startDate && this.state.endDate)
        rr = `/listings/view/${this.props.listing.id}?startDate=${this.state.startDate.format(DATE_FORMAT)}&endDate=${this.state.endDate.format(DATE_FORMAT)}`
        
      SAVE_ROUTE(rr)
      history.push('/login')
    }else if(this.state.numWeeks < 4 ){
      this.setState({weekError: true})
    }else{
      ReactGA.event({
        category: 'Booking',
        action: `User checking booking for ${this.props.listing.id}`, 
        label: 'Check Booking'
      })
      history.push({
        pathname:`/listings/book/${this.props.listing.id}`, 
        state: { 
          details: {
            listing: this.props.listing, 
            cost_params: this.props.cost_params, 
            weeks: this.state.numWeeks,
            dates: {
              start: this.state.startDate.toDate(),
              end: this.state.endDate.toDate()
            } 
          }
        }
      })
    }
  }

  roundUp=(num, precision)=> {
    let p = Math.pow(10, precision)
    return (Math.ceil(num * p) / p)
  }

  onDatesChange = ({ startDate, endDate }) => {
    const isInvalid = this.checkForBlockedDates(startDate, endDate)

    if(isInvalid) {
      this.invalid = true
      // TODO: Display some error message
      this.setState({badSelection: true})
      if (this.state.focusedInput === "startDate") {
        this.setState({
          numWeeks: 0,
          startDate: null,
        })
      } else if (this.state.focusedInput === "endDate") {
        this.setState({
          numWeeks: 0,
          endDate: null,
        })
      }
    } else {
      const numWeeks = this.getNumWeeks(startDate, endDate);

      this.setState({
        badSelection: false,
        numWeeks: numWeeks,
        startDate: startDate,
        endDate: endDate,
      })
    }
  }

  getNumWeeks = (startDate, endDate) => {
    if(startDate === null || endDate === null) {
      return 0
    }
    const inclusiveDays = endDate.diff(startDate, 'days') + 1
    return Math.ceil(inclusiveDays/7.0)
  }

  checkForBlockedDates=(start, end)=> {
    for (let i=0; i<this.daysBlocked.length; i++) {
      // moment compare for date range
      if (this.daysBlocked[i].isBetween(start, end, 'days')) {
        return true
      }
    }
    return false
  }

  isBlocked = day => {
    return this.daysBlocked.some(date => day.isSame(date, 'day')) 
          || day.isAfter(this.props.listing.end_date, 'day') 
          || day.isBefore(this.props.listing.start_date, 'day') 
          || day.isSame(new Date(), 'day')
  }

  onFocusChange=(focusedInput)=> {
    if(!this.invalid)
      this.setState({ focusedInput });
    else
      this.invalid = false
  }

  render() {
    const { classes, listing, appSize } = this.props
    const isMobile = appSize.isSmallComputer

    const details = {
      price: this.props.listing.price,
      cost_params: this.props.cost_params,
      weeks: this.state.numWeeks,
    }

    return (
        <Paper elevation={1} className='BookingBox'>
          <div style={{display:'flex', flexDirection:'column'}}>
            <div style={{display:'flex', flexDirection:'row', marginBottom:10}}>
              <h2 style={{margin:0}}>${DISPLAY_NUMBER(details.price*(1+details.cost_params.service_fee))}</h2>
              <h4 style={{margin:0, alignSelf: 'flex-end'}}>&nbsp;per week</h4>
              <Tooltip title='Utilities are included in the weekly price' placement='top'>
                <Support className="GrayText" style={{fontSize: 16}}/>
              </Tooltip>
            </div>
            {/* <Divider/> */}
            {/* <h4 style={{margin:0, marginTop:10, marginBottom: 5}}>SummerStay Dates</h4> */}
            {
              this.state.badSelection && <p style={{margin:0, color: 'red'}}>That is an invalid date selection. Please choose again.</p>
            }
            <div style={{display:'flex',justifyContent:'space-around', marginBottom:10 }}>
              <DatePicker
                anchorDirection="right"
                onDatesChange={this.onDatesChange}
                isDayBlocked={this.isBlocked}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
                onFocusChange={this.onFocusChange}
                focusedInput={this.state.focusedInput}
                keepOpenOnDateSelect={false}
                numberOfMonths={isMobile ? 1 : 2}
                minDate={listing ? moment(listing.start_date) : null}
                maxDate={listing ? moment(listing.end_date) : null}
              />
            </div>
            
            {
              this.state.weekError ? <p style={{textAlign:'center',margin:0,marginBottom: 5, color:'red'}}>You must book for at least 4 weeks.</p>: null
            }
            <PaymentInfo details={details}/>
          </div>
          
            <Route render={({ history }) => (
              <NLButton title ='Check Booking'
                onClick={()=>this.bookingRequest(history)} type="LargeButton"/> 
            )}/>

          <div style={{display:'flex', justifyContent:'center', textAlign:'center'}}>
            <p className="SoftText" style={{margin:10, marginBottom:0, display:'inline-block'}}>You won't be charged yet</p>
          </div>
          
        </Paper>
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user,
      appSize: state.housingApp.appSize 
  }
}
export default connect(mapStateToProps)(BookingBox);