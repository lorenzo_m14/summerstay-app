import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import CircleLoad from '../utilities/circleLoad.js'
import NLButton from '../utilities/NoLinkButton.js'
import Button from '../utilities/Button.js'
import StartConversationBox from './startConversationBox.js'

import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import '../../CSS/Payment.css'
import '../../CSS/ListingsMain.css'
import NoPhoto from '../../Assets/noPhoto.png'
const BlueCheckbox = withStyles({
  root: {
    padding: 0,
    '&$checked': {
      color: '#6c4ef5',
    },
  },
  checked: {},
})(props => <Checkbox color="default" {...props} />);

const continuousPaymentFields = ['terms', 'payAsYouStay'];
const upfrontPaymentFields = [ 'terms']

class ConfirmPayment extends Component {
  constructor(props) {
    super(props);
    this.state={
      // certify: false,
      terms: false,
      payAsYouStay: false
    }
  }

  // Making sure the user has checked all the boxes before
  // proceeding to book their SummerStay
  handleChange= (type, checked) =>{
    this.setState({ ...this.state, [type]: checked });
  }

  render() {
    const { classes, details, paymentType, isMobile } = this.props;
    const len = details.listing.images ? details.listing.images.length > 0 : false

    // don't want to put allowed in a state where user can change the state from browser.
    // We always want to ensure that user MUST check all required boxes before allowing submit
    let requiredFields = this.props.paymentType === 1 ? continuousPaymentFields : upfrontPaymentFields
    let allowed = requiredFields.every(item => this.state[item] === true )

    return (
        <Paper elevation={1} className='ConfirmPayment'>
          <div style={{display:'flex', marginBottom:10}}>
            <div>
              <h2 style={{margin:0}}>Confirm Request</h2>
              <p style={{margin:0, marginTop:10}}>
              The Host will have five days to respond to the booking. By confirming your request you will not be charged until the host accepts your booking.
              </p>
            </div>
          </div>

          <Divider/>

          <div style={{display:'flex', flex: 1, marginTop: 10, marginBottom:20}}>
            <div style={{backgroundImage: len ? `url(${details.listing.images[0].url})` : `url(${NoPhoto})`, backgroundSize: 'cover',
                                          backgroundRepeat: 'no-repeat',backgroundPosition: 'center', overflow:'hidden',
                                          height: 100, width:100,borderRadius: 5, borderTopRightRadius:0, borderBottomRightRadius:0}}>                
            </div> 
            <div style={{display:'flex', flexDirection:'column', marginLeft: 10}}>
              <h3 style={{margin:5, wordBreak: 'break-word'}}>{details.listing.listing_name}</h3>
              
              <h4 style={{margin:5}}>Host: <span style={{fontWeight:400}}>{details.listing.host.first_name}</span></h4>
              {/* <h4 style={{margin:5}}>Payment Program: <span style={{fontWeight:400}}>{paymentType === 0 ? 'Standard' : 'Pay-as-you-Stay'}</span></h4> */}
            </div>
          </div>
          <StartConversationBox message={this.props.hostMessage} 
                          updateMessage={this.props.updateMessage}/>
          {
            paymentType === 1 ?
              <div style={{display:'flex', alignItems: isMobile ? 'flex-start' : 'center', marginTop: 20}}>
                <BlueCheckbox
                  checked={this.state.payAsYouStay}
                  onChange={(e)=>this.handleChange('payAsYouStay',e.target.checked)}
                  value="checkedB1"
                />
                <p style={{margin: 4}}>
                  I understand I have chosen a recurring payment program. 
                </p>
              </div>
            : null
          }
          {/* <div style={{display:'flex', alignItems:'flex-start', marginTop: paymentType === 1 ? 0 : 20}}>
            <BlueCheckbox
              checked={this.state.certify}
              onChange={()=>this.handleChange('certify')}
              value="checkedB2"
            />
            <p style={{margin: 4}}>
              I certify that <span style={{fontWeight: 600}}>I will ONLY be charged if the host accepts my booking</span>.
            </p>
          </div> */}
          <div style={{display:'flex', alignItems: isMobile ? 'flex-start' : 'center', marginBottom: 20}}>
            <BlueCheckbox
              checked={this.state.terms}
              onChange={(e)=>this.handleChange('terms', e.target.checked)}
              value="checkedB3"
            />
            <p style={{margin: 4}}>
              I agree to SummerStay's <Button title="Terms and Conditions" link="/terms" target="_blank" type="InlineText"/>.
            </p>
          </div>
          {
            !this.state.loading ? 
              <NLButton title ='Confirm Request to Book' onClick={allowed ? this.props.submit : undefined} 
                  type={allowed ? "LargeButton" : "LargeButton Gray"}/>
            : <CircleLoad/>
          }

        </Paper>
     
    );
  }
}
export default ConfirmPayment;