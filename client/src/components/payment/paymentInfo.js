import React, { Component } from 'react'
import '../../CSS/App.css'

import Divider from '@material-ui/core/Divider'
import Tooltip from '@material-ui/core/Tooltip'
import Support from '@material-ui/icons/ContactSupport'
import { DISPLAY_NUMBER } from '../../constants/functions.js'

class PaymentInfo extends Component {

    roundUp=(num, precision)=> {
        let p = Math.pow(10, precision)
        return (Math.ceil(num * p) / p)
    }


  render() {
    const { details } = this.props
    const cost_params = details.cost_params

    const weeklyRent = (details.price*(1+cost_params.service_fee))
    const totalRent = this.roundUp((weeklyRent * details.weeks),2)

    const deposit = this.roundUp(((details.price) * cost_params.deposit_weeks),2)
    // const service_fee = this.roundUp(weekPrice * cost_params.service_fee,2)
    const processingFee = this.roundUp(((totalRent + deposit) * cost_params.stripe_fee),2)
    const totalPrice = totalRent + deposit + processingFee // need to add abck secureity deposit 

    const pricingRows =[ {title:`$${DISPLAY_NUMBER(weeklyRent)} x ${details.weeks} Weeks`, price: totalRent, 
                    tooltip:'Rent is calculated per week. Example: 5 weeks & 1 day = 6 weeks. Utilities and Service Fee are included in the rent price.'}, 
                  {title:`Processing Fee`, price: processingFee, 
                    tooltip:"This allows us to process your payment information securely with Stripe's Payment Platform."},
                  {title:`Security Deposit`, price: deposit, 
                    tooltip:'This deposit will be refunded in full if there is no damage to your SummerStay within 30 days of moving out.'},  
                  ]
                  
    return (
          <div style={{display:'flex', flexDirection:'column'}}>
            {
              pricingRows.map(r=>
              <div key={r.title} style={{display:'flex', flexDirection:'column'}}>
                  <Divider />
                  <div style={{display:'flex',justifyContent:'space-between'}}>
                    <div style={{display:'flex', alignItems:'center'}}>
                      <p className="GrayText" style={{ margin:10}}>{r.title}</p>
                      <Tooltip title={r.tooltip} placement="bottom">
                        <Support className="GrayText" style={{fontSize: 16}}/>
                      </Tooltip>
                    </div>
                    <p className="GrayText" style={{margin:10}}>
                      ${ DISPLAY_NUMBER(r.price) }
                    </p>
                  </div>
              </div>  
              )
            }
            <Divider />
            <div style={{display:'flex',justifyContent:'space-between'}}>
              <div style={{display:'flex', alignItems:'center'}}>
                <p style={{margin:10}}>
                  Total
                </p>
              </div>
              <p style={{margin:10}}>
                ${ DISPLAY_NUMBER(totalPrice) }
              </p>
            </div> 
          </div>
          
    ); 
  }
}



export default PaymentInfo;