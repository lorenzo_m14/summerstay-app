import React, {Component} from 'react'
import { PLAID_API_KEY, PLAID_ENV } from '../../constants/index.js'
import { POST, GET } from '../../constants/requests.js'
import '../../CSS/Buttons.css'
import '../../CSS/ListingsMain.css'
import PlaidLink from 'react-plaid-link'
import { injectStripe, CardNumberElement,CardExpiryElement,CardCvcElement } from 'react-stripe-elements'
import NLButton from '../utilities/NoLinkButton.js'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import BankIcon from '@material-ui/icons/AccountBalance'


import visa from 'payment-icons/min/single/visa.svg'
import amex from 'payment-icons/min/single/amex.svg'
import discover from 'payment-icons/min/single/discover.svg'
import mastercard from 'payment-icons/min/single/mastercard-old.svg'
import unknown from 'payment-icons/min/mono/default.svg'

import InputBase from '@material-ui/core/InputBase'
import PropTypes from 'prop-types'
import '../../CSS/Payment.css'
import { connect } from 'react-redux'
import NativeSelect from '@material-ui/core/NativeSelect'
import FormControl from '@material-ui/core/FormControl'
import Paper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'
import CircleLoad from '../utilities/circleLoad.js'

import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  input: {
    border: '1px solid #a6a6a6',
    borderRadius: 5,
    fontFamily: 'Helvetica Neue, sans-serif',
    fontSize: 16,
    fontWeight: 400,
    marginBottom:10,
    '&:hover': {
      backgroundColor: '#fff',
      border: '1px solid #6c4ef5'
    },
    
    '& .MuiInputBase-input':{  
      // padding: 5, 
      borderRadius: 5,
      padding: 10,  
      
      color: '#000',
      '&::placeholder': {
        opacity:1,
        color: '#a6a6a6',
      },
    },  
    '&.Mui-focused':{
      border: '1px solid #6c4ef5',
    }
  },
  error: {
    border: '1px solid red',
    borderRadius: 5,
    fontFamily: 'Helvetica Neue, sans-serif',
    fontSize: 16,
    fontWeight: 400,
    marginBottom:10,
    '&:hover': {
      backgroundColor: '#fff',
      border: '1px solid #6c4ef5'
    },
    
    '& .MuiInputBase-input':{  
      // padding: 5, 
      borderRadius: 5,
      padding: 10,  
      
      color: '#000',
      '&::placeholder': {
        opacity:1,
        color: '#a6a6a6',
      },
    },  
    '&.Mui-focused':{
      border: '1px solid red',
    }
  }
});

const SelectInput = withStyles(theme => ({
  input: {
    marginBottom:10,
    position: 'relative',
    backgroundColor: '#fff',
    border: '1px solid #a6a6a6',
    borderRadius: 5,
    padding: '10px 10px 10px 10px',
    // Use the system font instead of the default Roboto font.
 
    fontFamily: 'Helvetica Neue, sans-serif',
    fontSize: 16,
    fontWeight: 400,
    '&:hover': {
      backgroundColor: '#fff',
      border: '1px solid #6c4ef5'
    },
    '&::placeholder': {
      opacity:1,
      color: '#a6a6a6',
    },
    '&:focus': {
      borderRadius: 5,
      backgroundColor: '#fff',
      border: '1px solid #6c4ef5',

    },
    '& .Mui-error':{
      borderRadius: 5,
      border: '1px solid red'
    }
  }
}))(InputBase);

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state={
      name: '', 
      address:'', 
      city:'',
      zip: '', 
      state: '', 
      brand: 'unknown', 


      selectMethod: 'card',
      paymentMethods: [],

      cardFocus: false,
      cvcFocus: false, 
      expireFocus: false,
      loading: false,
      verified: false,
      // addNew: false,
      paymentMethodType: null,

      initialLoad: false, 
    }
    this.cards = {
      "visa": visa, 
      "amex": amex,
      "american express": amex,
      "discover": discover,
      "mastercard": mastercard,
      "unknown": unknown
    }
    this.checkPayment = this.checkPayment.bind(this)
  }


  componentDidMount=()=>{
    
    this.setState({loading: true})
    GET('/api/v1/payment_methods')
    .then(data => {
      if (data.payment_methods.length>0){
        this.setState({
          paymentMethods: data.payment_methods, 
          loading: false
        })
        this.updatePaymentMethodSelect(data.payment_methods[0].id)
      }else{
        
        this.setState({loading: false})
        this.updatePaymentMethodSelect('card')
      }
    })
    .catch(error => { 
        // TODO: add error handling here
    });

  }

  // Plaid returns token and metadata on success.
  handlePlaidSuccess = (token, metadata) => {
    this.setState({loading: true})
    // send request to server with params:
    // payment_method_type=ach, Plaid token and account_id (from metadata object)

    if(token && metadata && metadata.account_id){
      const send_data = { 
        payment_method_type: "bank_account",
        token: token,
        account_id: metadata && metadata.account_id 
      }
      
      this.serverUpdate(send_data)
    }else{
      this.setState({verified: false, loading: false})
      this.props.error("We can't validate this account. Make sure the information is up to date and accurate.")
    }
  }

  // Do nothing (for now on Plaid exit)
  handlePlaidExit = () => {
    // TODO: add a request to the server on error
  }

  // Deals with Stripe Payment Validation
  // Used to make a new payment merhod
  async checkPayment(ev){
    this.setState({
      nameError: false,
      addressError: false,
      cityError: false,
      zipError: false,
      stateError: false,
    })

    let error = false
    if(this.state.name === ''){
      this.setState({nameError: true})
      error = true
    }
    if(this.state.address === ''){
      this.setState({addressError: true})
      error = true
    }
    if(this.state.city === ''){
      this.setState({cityError: true})
      error = true
    }
    if(this.state.zip === ''){
      this.setState({zipError: true})
      error = true
    }
    if(this.state.state === ''){
      this.setState({stateError: true})
      error = true
    }
    if(!error){
      this.setState({loading: true})
      let {token} = await this.props.stripe.createToken({
        name: this.state.name, 
        address_line1: this.state.address, 
        address_city: this.state.city, 
        address_state: this.state.state, 
        address_zip: this.state.zip
      });
      
      if(token){
        const send_data = { 
          payment_method_type: "card",
          token: token.id,
        }
        this.serverUpdate(send_data)
  
      }else{
        this.setState({loading: false})
        this.props.error("We can't validate this card. Make sure the information is up to date and accurate.")
      }
    }
  }

  serverUpdate=(send_data)=>{
    POST('/api/v1/payment_methods', send_data)
    .then(data => {
      if(data.is_existing_source){
        this.updatePaymentMethodSelect(data.payment_method.id)
      }else{
        this.setState({
          paymentMethods: [...this.state.paymentMethods, data.payment_method]
        })
        this.updatePaymentMethodSelect(data.payment_method.id)
        
      }
      this.setState({ loading: false })
    })
    .catch(error => {
      this.setState({loading: false})
      this.props.error(error.error)
    });
  }
  // Changes the graphic shown for a card
  // only visual change
  handleCard=(event)=>{
    let brand = 'unknown'
    const acceptedCardTypes = ['visa','mastercard','amex','discover']
    if(acceptedCardTypes.includes(event.brand)) {
      brand = event.brand
    }

    this.setState({brand: brand})
  }

  setFocus=(type)=>{
    this.setState({cardFocus: false, cvcFocus: false, expireFocus: false})
    if(type !== 'noFocus'){
      this.setState({[type]: true})
    }
  }

  // Used for the selector when an item is chosen by the user
  // The first if statement is used for items in the selector list that require an action
  // in this case that is adding a new bank account or card
  // the else is the standard case where you are pushing the payment method id to the parent component
  // when doing this you also need to push the type of that id (bank_account or card)
  updatePaymentMethodSelect=(val)=>{ 

    if( val === 'card' || val === 'bank_account'){ 
      this.selectPaymentType(val) 
      if(this.props.updatePaymentID)
        this.props.updatePaymentID(null) // clear the paymentID if it exsists
      this.setState({
        selectMethod: val, 
        verified: false
      })
    }else{ 
      let type = val.split('_')[0] === 'ba' ? 'bank_account' : 'card' 
      this.setState({
        selectMethod: val, 
        verified: true
      })
      this.selectPaymentType(type) 
      if(this.props.updatePaymentID)
        this.props.updatePaymentID(val)
    }
  }

   // we have two payment types right now, default type is card
  selectPaymentType = (val) => {

    // This is used to make an update call to how payments are calculated
    if (this.props.recalculate && val !== this.state.paymentMethodType){ 
      this.props.recalculate(val) // when the type is changed recal
    }
    this.setState({paymentMethodType: val})
  }

  // Used to update the payment info for a user
  // Name, Address, ect not card into tho
  handleState=(type, val)=>{
    this.setState({
      [type]: val, 
      verified: false
    })
  }

  renderCard = (brand) => {
    if (Object.keys(this.cards).includes(brand)) {
      return this.cards[brand]
    } else {
      return this.cards.unknown
    }
  }

  render() {
    const { classes } = this.props;
    const card = {
      base: {
        fontWeight: 400,
        fontFamily: 'Roboto, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',
        '::placeholder': {
          color: '#898989',
        },
        ':-webkit-autofill': {
          color: '#898989',
        },
      },
      invalid: {
        color: '#E25950',
        '::placeholder': {
          color: '#FFCCA5',
        },
      },
    }
    const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 
      'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 
      'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 
      'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 
      'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']

    const headerStyle = {margin: 0, marginBottom: 10}
    const isLargePhone = this.props.appSize.isLargePhone
    const isMediumPhone = this.props.appSize.isMediumPhone
    const isSmallPhone = this.props.appSize.isSmallPhone

    return (
      <Paper elevation={this.props.update ? 0 : 1} className='CheckoutForm' >
        <div style={{display:'flex', marginBottom:20}}>
          <div>
            {this.props.update ? 
              <h3 style={{margin:0}}>Add Payment Method</h3>
              :
              <h2 style={{margin:0}}>Payment Information</h2>
            }
            <p style={{margin:0, marginTop:10}}>
             Connecting a bank account allows us to reduce your processing fee.
            </p>
          </div>
        </div>
        {/* <Divider/> */}
        {/* <h4 style={{margin:0, marginTop:10}}>Payment Method</h4>
        <p className="GrayText" style={{margin:0, marginTop:10}}>
         By using a bank account we can reduce your processing fee.
        </p> */}
        <Select
          select
          value={this.state.selectMethod}
          onChange={(event)=>this.updatePaymentMethodSelect(event.target.value)}
          
          // variant="outlined"
          helperText='Select a saved payment method or add a new one'
          input={<SelectInput />}
        >
          {this.state.paymentMethods.map((m,i) => (
            <MenuItem key={i} value={m.id}>
                {
                   m.object === 'card' ?
                   <div style={{display:'flex' , alignItems:'center'}}>
                    <img src={this.renderCard(m.brand.toLowerCase())} height='20px' alt="Logo" />
                    <p style={{margin: 0,marginLeft: 10}}>{" **** **** **** "+m.last4}</p>
                   </div>
                   : 
                   <div style={{display:'flex' , alignItems:'center'}}>
                     <BankIcon style={{ marginLeft: 5, fontSize: 22 }}/>
                     <p style={{margin: 0,marginLeft: 10}}>{"**** **** **** "+m.last4} </p>
                   </div>
                }
            </MenuItem>
          ))}
          <Divider/>
          <MenuItem value={'card'}>
            <div style={{display:'flex', alignItems:'center'}}>
              <img src={this.cards.unknown} height='20px' alt="Logo" />
               <p style={{margin: 0, marginLeft: 10}}>Add Card</p>
            </div>
          </MenuItem>
          <MenuItem  value={'bank_account'}>
            <div style={{display:'flex', alignItems:'center'}}>
              <BankIcon style={{ marginLeft: 5, fontSize: 22 }}/>
              <p style={{margin: 0, marginLeft: 15}}>Add Bank Account</p>
            </div>
          </MenuItem>
      </Select>
        {/* TODO: change this to a toggle to render ACH or Card payments */}
        {
          // (this.state.paymentMethods.length === 0 && !this.state.initialLoad) &&
          // <div style={{display:'flex'}}>
          //   <NLButton title="Card Info" onClick={()=>this.selectPaymentType("card")} type='MediumButton'/>
          //   <NLButton title="Bank Account" onClick={()=>this.selectPaymentType("bank_account")} type='MediumButton'/>
          // </div>
          
        }
        
        {
          this.state.selectMethod === 'bank_account' &&
          this.state.paymentMethodType === 'bank_account' &&
          <div>
            <PlaidLink
                className="LargeButton Gray"
                // style={{width:'100%',padding: 8,backgroundColor:'#6c4ef5',fontSize: '15pt', color: '#fff', borderRadius: 5,
                //         fontWeight:700, marginTop: 20}}
                clientName="SummerStay"
                env={`${PLAID_ENV}`}
                product={["auth"]}
                publicKey={`${PLAID_API_KEY}`}
                selectAccount={true}
                onLoad={this.handleOnLoad}
                onExit={this.handlePlaidExit}
                onSuccess={this.handlePlaidSuccess}>
                  Connect Bank Account
            </PlaidLink>
          </div>
        }
        {
          this.state.selectMethod === 'card' &&
          this.state.paymentMethodType === 'card' &&

            <div style={{display:'flex', alignContent: 'center'}}>
              <div style={{display:'flex', flexDirection:'column', width: '100%'}}>
                <div style={{display:'flex', flexDirection:'column'}}>
                  <h4 className="SkinnyHeader" style={headerStyle}>Name</h4>
                  <InputBase
                    // error={this.state.nameError}
                    value={this.state.name}
                    className={this.state.nameError ? classes.error : classes.input}
                    onFocus={(e)=>this.setFocus('noFocus')}
                    onChange={e=>this.handleState('name',e.target.value)}
                    placeholder="Cardholder Name"
                  />
                </div>
                <div style={{display:'flex', flexDirection:'column', alignContent: 'center'}}>
                  
                  <h4 className="SkinnyHeader" style={headerStyle}>Billing Info</h4>
                  <InputBase
                    
                    value={this.state.address}
                    className={this.state.addressError ? classes.error : classes.input}
                    onFocus={(e)=>this.setFocus('noFocus')}
                    onChange={e=>this.handleState('address',e.target.value)}
                    placeholder="Billing Address"
                  />
                  <div style={{display:'flex', flex: 1, justifyContent:'space-between', alignItems: 'center'}}>
                    <InputBase
                      
                      value={this.state.city}
                      className={this.state.cityError ? classes.error : classes.input}
                      onFocus={(e)=>this.setFocus('noFocus')}
                      onChange={e=>this.handleState('city',e.target.value)}
                      placeholder="City"
                    />   
                    <div style={{width:10}}/>   
                    <FormControl error={true}>

                      <NativeSelect
                        // error={this.state.stateError}
                        value={this.state.state}
                        onChange={e=>this.handleState('state',e.target.value)}
                        input={<SelectInput name="State" id="state-helper" />}
                      >
                        <option value="">State</option>
                        {
                          states.map(s=>
                            <option key={s} value={s}>{s}</option>
                            )
                        }
                      </NativeSelect>
                    </FormControl>
                    <div style={{width:10}}/>
                    <InputBase
                      
                      value={this.state.zip}
                      className={this.state.zipError ? classes.error : classes.input}
                      onFocus={(e)=>this.setFocus('noFocus')}
                      onChange={e=>this.handleState('zip',e.target.value)}
                      placeholder="ZIP"
                    />
                </div>
              </div>
              <div style={{display: 'flex', flexDirection: 'column' }}>
                <h4 className="SkinnyHeader" style={headerStyle}>Card Information</h4>
                <div style={{marginBottom: 20, display: 'flex', alignItems: !isMediumPhone ? 'center' : null, flexDirection: isMediumPhone ? 'column' : 'row'}}>
                  <div style={{display: 'flex', flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                    <img src={this.cards[this.state.brand]} height='25px' alt="Logo" />
                    <div className={this.state.cardFocus ? "PaymentFocusBig" : "PaymentOuterBig"} style={{marginLeft: 5, width: isMediumPhone ? '100%' : 'auto'}}>
                      <CardNumberElement
                        onFocus={(e)=>this.setFocus('cardFocus')}
                        onChange={this.handleCard}
                        style={card}/>
                    </div>
                    {
                      !isMediumPhone ? 
                      <React.Fragment>
                        <div className={this.state.expireFocus ? "PaymentFocusSmall" : "PaymentOuterSmall"} style={{marginLeft: 10}}>
                          <CardExpiryElement 
                            onFocus={(e)=>this.setFocus('expireFocus')}
                            style={card}/>
                        </div>
                        <div className={this.state.cvcFocus ? "PaymentFocusSmall" : "PaymentOuterSmall"} style={{marginLeft: 10, minWidth: 45}}>
                          <CardCvcElement 
                            onFocus={(e)=>this.setFocus('cvcFocus')}
                            style={card}/>
                        </div>
                      </React.Fragment>
                    : null
                    }

                  </div>
                  {
                    isMediumPhone ? 
                      <div style={{display: 'flex', flexDirection: 'row', marginLeft: isMediumPhone ? 44 : 10, marginTop: isMediumPhone ? 10 : 0}}>
                        <div className={this.state.expireFocus ? "PaymentFocusSmall" : "PaymentOuterSmall"}>
                          <CardExpiryElement 
                          onFocus={(e)=>this.setFocus('expireFocus')}
                          style={card}/>
                        </div>
                        <div className={this.state.cvcFocus ? "PaymentFocusSmall" : "PaymentOuterSmall"} style={{marginLeft: 10}}>
                          <CardCvcElement 
                            onFocus={(e)=>this.setFocus('cvcFocus')}
                          style={card}/>
                        </div>
                      </div>
                    : null
                  }
                  
                </div>
              </div>

              </div>
            </div>
        }
        <div style={{display:'flex', flex: 1, alignItems:'flex-end'}}>
          <div style={{display:'flex',flexDirection:'column',flex: 1, marginTop: 10}}>
            {
              !this.state.loading && 
                (this.state.selectMethod !== 'bank_account' && this.state.paymentMethodType !== 'bank_account') &&
                <NLButton title ={this.state.verified ? "Verified!" : 'Verify Payment Method'} onClick={this.state.verified ? undefined : this.checkPayment} 
                  type={this.state.verified ? "MediumButton SolidGreen" : "MediumButton"}/>
            }
            {
              this.state.loading && <CircleLoad/>
            }
          {
            // !this.state.loading ? 
            //   <NLButton title ={this.state.verified ? "Verified!" : 'Verify Payment Method'} onClick={this.state.verified ? undefined : this.checkPayment} 
            //       type={this.state.verified ? "MediumButton SolidGreen" : "MediumButton"}/>
              
            // : <CircleLoad/>
          }
          </div>
        </div>
      </Paper>

    );
  }
}
CheckoutForm.propTypes = {
  classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => {
  return {
      user: state.housingApp.user, 
      appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(injectStripe(withStyles(styles)(CheckoutForm)));