import React from 'react';
import PaymentTypeBox from './paymentTypeBox.js'

export default function PaymentTypeSelector(props){
  
  return (
    <div style={{display: 'flex', flex: 1, flexWrap: 'wrap', justifyContent: 'center'}}>
      <PaymentTypeBox details={props.details} type={0} typeDetails={props.standard} selectType={props.selectType}
          selected={props.currentType === 0} />
      <PaymentTypeBox details={props.details} type={1} typeDetails={props.continuous} selectType={props.selectType}
          selected={props.currentType === 1}/>
    </div>
  );
}
