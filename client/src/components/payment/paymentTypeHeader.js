import React, { Component } from 'react'
import '../../CSS/App.css'
import NLButton from '../utilities/NoLinkButton.js'

import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import format from "date-fns/format"

import ModalWrapper from '../utilities/Modal.js'

import Arrow from '@material-ui/icons/ArrowForward'
import ChargeLineItems from './chargeLineItems.js'

const styles= theme =>({

  root:{
    display:'flex', 
    flexDirection:'column',
    justifyContent:'space-between',
  },

})
// need component did mount that makes a request to the server for the listing
class PaymentTypeHeader extends Component {
  constructor(props){
      super(props);
      this.state = {
        showDetails: false
      };
  }

  handleModal=()=>{
    this.setState(prevState=>(
      {showDetails:!prevState.showDetails}
    ))
  }
  render() {
    const { classes, details, type, typeDetails } = this.props
    
    return (
        <div className={classes.root}>
          <ModalWrapper
            open={this.state.showDetails}
            onClose={this.handleModal}
            style={{maxWidth:600}}
          >
            {
              typeDetails &&
              <ChargeLineItems details={details} type={type} typeDetails={typeDetails} />
              
            }
            <NLButton title ='Close' onClick={this.handleModal} type="LargeButton"/> 
          </ModalWrapper>
          <div style={{display:'flex', flexDirection:'row', marginBottom:10}}>
              {
                type === 0 ?
                  <div>
                      <h2 style={{margin:0}}>Standard Payment Program</h2>
                      <p style={{margin:0, marginTop:10}}>
                          Pay your summer rent before you move in with two payments.
                      </p>
                  </div>
                :   
                  <div>
                      <h2 style={{margin:0}}>Pay-As-You-Stay Program</h2>
                      <p style={{margin:0, marginTop:10}}>
                          Complete bi-weekly payments during your stay.
                      </p>
                  </div>
              }
          </div>
          {/* <Divider/> */}
          <div style={{display:'flex', justifyContent: 'space-between', alignItems:'center',marginTop:10, marginBottom: 10}}>
            <p style={{margin:0}}>SummerStay Dates</p>
            <NLButton title ='View Payments' onClick={this.handleModal} type="MiniMediumButton"/> 
          </div>
          
          <div style={{display:'flex',  height:45, margin:0, borderRadius:5, marginBottom:10,justifyContent:'space-around', alignItems:'center',border: '1px solid #898989'}}>
            <p style={{margin:0}}> {format(details.dates.start, "MMMM do")} </p>
            <Arrow style={{fontSize: 18}}/>
            <p style={{margin:0}}> {format(details.dates.end, "MMMM do")} </p>
          </div>
          
        </div>
      
    )
  }
}


PaymentTypeHeader.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(PaymentTypeHeader);