import React, { Component } from 'react'
import '../../CSS/App.css'
import '../../CSS/ListingsMain.css'
import NLButton from '../utilities/NoLinkButton.js'
import { connect } from 'react-redux'

import Paper from '@material-ui/core/Paper'
import DetailedPaymentInfo from './detailedPaymentInfo.js'
import MinimalPaymentInfo from './minimalPaymentInfo.js'
import PaymentTypeHeader from './paymentTypeHeader.js'
import CircleLoad from '../utilities/circleLoad.js'

// need component did mount that makes a request to the server for the listing
class PaymentTypeBox extends Component {

  render() {
    const { classes, details, type, selected, typeDetails, minimal } = this.props
    const isSmallPhone = this.props.appSize.isSmallPhone

    return (
        <Paper elevation={1} className='PaymentBox' style={{ minWidth: isSmallPhone ? '70vw' : null, border: selected ? '2px solid #6c4ef5' : '2px solid #fff'}}>
          <div style={{display:'flex', flexDirection:'column'}}>
            <PaymentTypeHeader details={details} type={type} typeDetails={typeDetails}/>
            {
              typeDetails && !minimal &&
                <DetailedPaymentInfo details={typeDetails.reservation_cost} type={type}/>
            }
            {
              typeDetails && minimal &&
                <MinimalPaymentInfo details={typeDetails.reservation_cost}/>
            }
            {
              !typeDetails &&
                <div style={{display:'flex', flex: 1, justifyContent:'center', alignItems:'center'}}>
                  <CircleLoad/>
                </div>
            }
          </div>
          {
              this.props.selectType ? 
                <div style={{display:'flex',  margin:5, marginTop:0}}>
                  <NLButton title ='Select'
                      onClick={()=>this.props.selectType(type)} type="LargeButton"/> 
                </div> : null
          }
        </Paper>
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
      appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(PaymentTypeBox);