import React, { Component } from 'react'
import '../../CSS/App.css'

import Paper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {CustomTextbox} from '../../constants/inputs.js'


const styles= theme =>({

  root:{
    display:'flex', 
    flexDirection:'column',
    justifyContent:'space-between',
    maxWidth:330, 
    margin:10, 
    padding: 20
  },

})
// need component did mount that makes a request to the server for the listing
class StartConversationBox extends Component {
    
  render() {
    const {classes, message} = this.props

    return (
        <div>
          <h4 style={{marginTop: 0, marginBottom: 10}}>Send a note with your booking request</h4>
        <CustomTextbox
            placeholder='Hello!'
            full
            multiline
            rows='3'
            value={message}
            onChange={(event)=>this.props.updateMessage(event.target.value)}
        />
        </div>
    ); 
  }
}


StartConversationBox.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(StartConversationBox);