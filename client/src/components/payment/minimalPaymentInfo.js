import React, { Component } from 'react'
import '../../CSS/App.css'

import Divider from '@material-ui/core/Divider'

import { DISPLAY_NUMBER } from '../../constants/functions.js'

class MinimalPaymentInfo extends Component {

  render() {
    const { details } = this.props
    
    return (
          <div style={{display:'flex', flexDirection:'column'}}>
            <Divider/>
            <div style={{display:'flex',justifyContent:'space-between'}}>
              <p style={{margin:10}}>
                Total 
              </p>
              <p style={{margin:10, marginBottom: 0}}>
                ${ DISPLAY_NUMBER(details.total_cost) }
              </p>
            </div>
          </div>
    ); 
  }
}



export default MinimalPaymentInfo;