import React from 'react'
import '../../CSS/App.css'

import Divider from '@material-ui/core/Divider'
import Tooltip from '@material-ui/core/Tooltip'
import Support from '@material-ui/icons/ContactSupport'

import { DISPLAY_NUMBER } from '../../constants/functions.js'

export default function DetailedPaymentInfo(props){

  const { details, type } = props

  const pricingRows = [{title:`${details.num_weeks} Weeks Rent`, price: (details.total_rent+details.renter_service_fee), 
                            tooltip:'Rent is calculated per week. Example: 5 weeks & 1 day = 6 weeks. Utilities are included in the rent price.'}, 
                        {title:`Pay-As-You-Stay Fee`, price: (details.continuous_fee), 
                            tooltip:'Financing is expensive for us. To allow us to keep offering this option, we add a small service fee.'},
                        {title:`Processing Fee`, price: (details.stripe_fee), 
                            tooltip:"This allows us to process your payment information securely with Stripe's Payment Platform."},
                        {title:`Security Deposit`, price: (details.deposit), 
                            tooltip:'This deposit will be refunded in full if there is no damage to your SummerStay within 30 days of moving out.'}]
  
  return (
        <div style={{display:'flex', flexDirection:'column'}}>
          {
            pricingRows.map(r=>{
              if(type === 0 && r.title === 'Service Fee') return null
              const color = r.title === 'Processing Fee' && details.payment_method_type === 'bank_account'? '#6c4ef5':'#898989'
      
              return <div key={r.title} style={{display:'flex', flexDirection:'column'}}>
                <Divider />
                <div style={{display:'flex',justifyContent:'space-between'}}>
                  <div style={{display:'flex', alignItems:'center'}}>
                    <p style={{color:color, margin:10}}>{r.title}</p>
                    <Tooltip title={r.tooltip} placement="bottom">
                      <Support style={{color:color,fontSize: 16}}/>
                    </Tooltip>
                  </div>
                  <p style={{color:color, margin:10}}>
                    ${ DISPLAY_NUMBER(r.price) }
                  </p>
                </div>
              </div>  
            })
          }
          <Divider />
          <div style={{display:'flex',justifyContent:'space-between'}}>
            <p style={{margin:10}}>
              Total 
            </p>
            <p style={{margin:10}}>
              ${ DISPLAY_NUMBER(details.total_cost) }
            </p>
          </div>
        </div>
  ); 
}
