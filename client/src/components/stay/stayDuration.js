import React, { Component } from 'react'
import '../../CSS/App.css'
import moment from 'moment'

class StayDuration extends Component {
  render () {
    const { stay } = this.props
    const formatStartDate = moment(stay.start_date).format("MMM D, YYYY")
    const formatEndDate = moment(stay.end_date).format("MMM D, YYYY")

    return (
      <div style={{display:'flex', marginBottom: 20}}>
        <div style={{display:'flex', flexDirection:'column',marginRight: 20 }}>
          <h3 style={{margin: 0, marginBottom: 10, }}>Move in</h3>
          <p style={{margin: 0}}>{formatStartDate}</p>
        </div>
        <div style={{display:'flex', flexDirection:'column' }}>
          <h3 style={{margin: 0, marginBottom: 10, }}>Move out</h3>
          <p style={{margin: 0}}>{formatEndDate}</p>
        </div>
      </div>
    )
  }
}
export default StayDuration
