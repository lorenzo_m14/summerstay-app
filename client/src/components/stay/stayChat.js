import React, { Component } from 'react'
import { POST } from '../../constants/requests.js'
import { SAVE_ROUTE } from '../../constants/functions.js'
import MessageBox from '../../components/messageBox.js'

import { connect } from 'react-redux'
import PropTypes from 'prop-types'

class StayChat extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showChat: true, 
      loading: false,
      selectedConversation: null
    }
  }

  componentDidMount() {
    this.setState({loading: true})
    this.createConversation()
  }

  createConversation = history => {
    if (this.props.user !== null) {
      this.setState({ loading: true })
      const send_data = {
        listing_id: this.props.stay.listing.id,
        recipient_id: this.props.stay.host.id
      }
      POST('/api/v1/conversations', send_data)
        .then(data => {
          this.setState({
            showChat: true,
            loading: false,
            selectedConversation: data.conversation.id
          })
        })
        .catch(error => {
          this.setState({
            showError: true,
            loading: false
          })
        })
    } else {
     
      SAVE_ROUTE(`/dashboard/stays/${this.props.stay.id}/chats`)
      history.push('/login')
    }
  }

  render () {

    return (
      <div style={{ display: 'flex', marginTop: 30}}>
        
        <MessageBox enableChat={this.state.showChat} 
        selectedConversation={this.state.selectedConversation}/>
      </div>
    )
  }
}

StayChat.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
  return {
    user: state.housingApp.user
  }
}
export default connect(mapStateToProps)(StayChat)