import React, { Component } from 'react'
import RoomIcon from '@material-ui/icons/RoomOutlined'
import CallIcon from '@material-ui/icons/Call'

class ContactInfo extends Component {

  render() {
    //TODO: need host phone number 
    const {  stay} = this.props
    const hostPhone = stay.stay_info.phone_number ? stay.stay_info.phone_number : 'No Number Available' 
    const hostAddress = stay.stay_info.address ? stay.stay_info.address :'no address available' 
    const addressAptNumber = stay.stay_info.apt_number ? ` Apt. ${stay.stay_info.apt_number}`: ''
    const googleMapsURL = 'https://www.google.com/maps/search/?api=1&query=' + encodeURIComponent(hostAddress+addressAptNumber)

    return ( 
      <div>
        {/* Phone Number */}
        <div>
          <a href={`:tel${hostPhone}`} target='_blank' rel='noopener noreferrer' style={{ textDecoration: 'none', color: '#21222f' }} >
            <div style={{ display: 'flex', alignItems: 'center' }} >
              <CallIcon />
              <p style={{ margin: 10 }}>{hostPhone}</p>
            </div>
          </a>
        </div>

        {/* Address */}
        <div>
            <a href={googleMapsURL} target='_blank' rel='noopener noreferrer' style={{ textDecoration: 'none', color: '#21222f' }} >
              <div style={{ display: 'flex', alignItems: 'center' }} >
                <RoomIcon />
                <p style={{ margin: 10 }}>{hostAddress+addressAptNumber}</p>
              </div>
            </a>
        </div>
      </div>
    ) 
  }
}
export default ContactInfo