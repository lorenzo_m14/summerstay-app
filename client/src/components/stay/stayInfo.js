import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import '../../CSS/App.css'

import { POST } from '../../constants/requests.js'
import Info from '../listing/info.js'
import ModalWrapper from '../utilities/Modal.js'
import TypeForm from '../utilities/typeform.js'
import parse from "date-fns/parse"

import Button from '../utilities/Button.js'
import NoPhoto from '../../Assets/noPhoto.png'
import ImageCarousel from '../utilities/imageCarousel.js'
import ContactInfo from './contactInfo.js'
import CancelButton from '../utilities/cancelButton.js'
import NLButton from '../utilities/NoLinkButton'
import { connect } from 'react-redux'

class StayInfo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      error: false, 
      loading: false, 
      showClaim: false
    }
  }
  
  cancel = () => {
    if (!this.state.loading) {
      this.setState({ loading: true })
      POST(`/api/v1/reservations/${this.props.stay.id}/cancel`, {})
        .then(data => {
          this.setState({ loading: false })
          this.props.history.replace('/dashboard/stays')
        })
        .catch(error => {
          this.setState({
            error: true,
            loading: false
          })
        })
    }
  }
  

  render () {
    const { stay } = this.props
    const { listing, status } = stay
    const image_data =
    listing && listing.images
      ? listing.images.map(image => ({ source: image.url }))
      : [{ source: NoPhoto }]
    const start = parse(stay.start_date,'yyyy-MM-dd',new Date())

    return (
      <div style={{ display: 'flex', flexDirection: 'column', flex: 1, width: '100%',marginTop: 20, marginBottom: 20 }} >
        <ModalWrapper
          open={this.state.showClaim}
          onClose={() => this.setState({ showClaim: false, claimId: null})}
          style={{padding: 0, maxWidth: 600}}
        >
          {
            this.props.user && stay && listing ? 
              <TypeForm 
              formUrl={`https://thesummerstay.typeform.com/to/skocNO?host=false&user_id=${this.props.user.id}&listing_id=${listing.id}&reservation_id=${stay.id}`}/>
            : null
          }
          

        </ModalWrapper>
        {
          listing &&
            
            <ImageCarousel imageData={image_data} />
            
        }
        <div style={{display:'flex', justifyContent:'space-between', marginTop: 20}}>
          { 
            status === 'Approved' ?
                <ContactInfo stay={stay}/>
            : <div/>
          }
          <div>
            <Button title='Payment Information' link='/dashboard/payments' type='MediumButton' />
          </div>
        </div>
        <Info listing={listing} specific={status === 'Approved'} stay={stay}/>
      
        {
          // ADD MORE INFO TO PROMPT
        }
        <div style={{display:'flex', marginBottom: 20}}>
          <CancelButton cancel={this.cancel} title="Cancel Your Stay">
            <p>Please refer to our Terms and Conditions for details on how payments will be refunded. By confirming your cancellation you may be forfeiting a portion of your payment.</p>
          </CancelButton>
          
            {
              new Date() - start > 0 ?
                <NLButton title='Submit Claim' onClick={()=>this.setState({showClaim: true})} type="MediumButton Gray MedMargin"/>
              : null
          }
          
          
        </div>
        

      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user, 
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(withRouter(StayInfo))

