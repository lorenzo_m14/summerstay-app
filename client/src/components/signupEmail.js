import React, { Component } from 'react'
import { CustomTextbox } from '../constants/inputs'
import { connect } from 'react-redux'


class SignupEmail extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      email: ''
    }
  }

  saveEmail = (input) => {
    this.setState({ email: input})
  }

  checkEmail = (email) => {
    return false
  }

  render() {
    const { email } = this.state
    return (
      <div className='StaticMain' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#F4F4F5', height: '25vh' }}>
        <div>
          <h1>Keep in Touch</h1>
          <CustomTextbox
            autoComplete='off'
            // error={this.checkEmail}
            value={email}
            id='email-signup'
            label='Enter Email Here'
            type='text'
            onChange={this.addEmail}
            variant='outlined'
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(SignupEmail)