import React, { PureComponent } from 'react';
import '../../CSS/App.css';
import '../../CSS/CreateListing.css'
import { connect } from 'react-redux'
import ModalWrapper from '../utilities/Modal.js'

import { Paper,InputAdornment }from '@material-ui/core';
import { DatePicker } from '../../constants/inputs.js'

import moment from 'moment'

import {CustomTextbox} from '../../constants/inputs.js'
import { GET } from '../../constants/requests.js'
import NLButton from '../utilities/NoLinkButton.js'
import ContinueButton from './continueButton.js'
import Button from '../utilities/Button.js'

class Payment extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      priceError:false,
      dateError: false,
      finishedData: null,
      focusedInput: null,
      // referralCode: null,
      // verifiedReferralCode: null, // verifiedReferralCode sent to POST /reservations request
      // referralCodeError: null,
      // isReferralVerified: false,
    }
  }  

  componentDidMount() {
    // check if there's a LeadDyno affiliate code for the session
    // if it exists, set it as the default referral code
    // const referralCode = window.LeadDyno.devTools.getAffiliateCode()
    // console.log("Referral code from cookies: " + referralCode)
    // if (referralCode) {
    //   this.props.onUpdateListing('referral_code', referralCode)
    // }
  }


  check=(finished)=>{
    let error = false
    // if(!this.props.listing.price || this.props.listing.price === 0){
    //   this.setState({dateError: true})
    //   error=true
    // }



    // check price
    if(!this.props.listing.price || this.props.listing.price < 1000 ) { // needs to be 1000 because it is in cents
      this.setState({priceError: true})
      error = true
    }

    // check dates
    if (!this.props.listing.start_date || !this.props.listing.end_date) {
      this.setState({dateError: true})
      error = true
    }
    
    
    if(!error){
      if (this.props.listing.price > 100000) {
        this.setState({finishedData: finished})
      }
      else {
        this.props.handleNav('forward', finished)
      }
    }
  }

  handlePriceWarning=(shouldContinue)=>{
    const finished = this.state.finishedData
    if (shouldContinue) {
      this.props.handleNav('forward', finished)
    }
    this.setState({finishedData: null})
  }

  updatePrice= (event) =>{
    let val = parseInt(event.target.value.replace(',',''))
    val = isNaN(val) ? null : val
    if(!val || val < 10){
      this.setState({priceError: true})
      this.props.onUpdateListing('price', (val*100))
    } else if(val>=10 && val<=10000){
      this.setState({priceError: false})
      this.props.onUpdateListing('price', (val*100))
    }

  }
  updateDates=({startDate, endDate})=>{
    if (startDate){
      let s = startDate.format("YYYY-MM-DD")
      this.props.onUpdateListing('start_date', s)
    }
    if (endDate){
      let e = endDate.format("YYYY-MM-DD")
      this.props.onUpdateListing('end_date', e)
    }
    this.setState({ dateError: false })
  }


  updateReferralCode = (e) => {
    this.setState({ 
      referralCode: e.target.value,
      referralCodeError: null
    })
  }

  // make request here in child since verification is localized to child component
  verifyReferralCode = () => {
    const { referralCode } = this.state
    GET(`/api/v1/verify_referral_code?referral_code=${referralCode}`)
        .then(data => {
          this.setState({
            isReferralVerified: true,
          })
          // if verification successful, call prop update
          this.props.onUpdateListing('referral_code', referralCode)
        })
        .catch(error => {
          this.setState({
            referralCodeError: error.error,
            isReferralVerified: false
          })
        });
  }

 
  render() {
    const { appSize, listing, isMobile } = this.props
    const listing_price = listing.price ? `${listing.price/100}` : ''
    const isSmallPhone = appSize.isSmallPhone


    const shouldRenderVerified = this.state.isReferralVerified || listing.referral_code

    return (
        
      <Paper className='SubPageRoot' elevation={0}>
        <ModalWrapper onClose={()=>this.handlePriceWarning(true)} open={this.state.finishedData !== null} >
          <h2 style={{marginTop: 0}}>Weekly rent warning</h2>
          <p><b>Are you sure this is your weekly rent?</b> You may have entered your monthly rent accidentally.</p>
          <div style={{ display:'flex', justifyContent: 'space-between', marginTop:10 }}>
            <NLButton onClick={()=>this.handlePriceWarning(false)} type='MediumButton' title='Go back' />
            <NLButton onClick={()=>this.handlePriceWarning(true)} type='MediumButton' title="Yes, I'm sure" />
          </div>
        </ModalWrapper>


        <div className="CreateMainHeader">         
          <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>When are you looking to sublet?</h2>
          <p className="SoftText">Potential renters will only be able to submit Booking Requests within this date range.</p>
        </div> 
      
        <div>
          <div style={{ display: 'inline-block' }}>
            <DatePicker
              startDateId="startDate"
              endDateId="endDate"
              startDate={listing.start_date ? moment(listing.start_date) : null}
              endDate={listing.end_date ? moment(listing.end_date) : null}
              numberOfMonths={isMobile ? 1 : 2}
              onDatesChange={this.updateDates}
              focusedInput={this.state.focusedInput}
              small={isMobile ? true : false}
              // withFullScreenPortal={isMobile ? true : false}
              onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
            />
            {
              this.state.dateError ? <p style={{ textAlign: 'center', margin:0, marginBottom: 5, color:'red' }}>You must select valid dates.</p> 
              : null
            }

          </div>
        </div>
        <h2 className="SkinnyHeader" style={{marginTop: 40, marginBottom: 0}}>How much is your weekly rent?</h2>
        <p className="SoftText">This cost should include utilities. <Button title='Learn more about setting rent.' link='/policies/how_should_i_price_my_place' target='_blank' type="InlineText"/></p>
        
        <div>
          <CustomTextbox
            placeholder='Weekly Rent'
            full={isSmallPhone}
            value={listing_price.length>3? `${Number(listing_price).toLocaleString()}`:listing_price}
            error={this.state.priceError}
            onChange={this.updatePrice}
            InputProps={{
              startAdornment:<InputAdornment position="start">$</InputAdornment>
            }}
          />
          { this.state.priceError ? <p style={{ margin: 0, color: 'red'}}>Please enter a valid amount.</p>
            : null
          }
        </div>

        {/* <h2 className="SkinnyHeader" style={{marginTop: 40, marginBottom: 0}}>Were you referred?</h2>
        <p className="SoftText">Enter referral code here.</p>
        <div style={{display:'flex'}}>
          <CustomTextbox
            placeholder='4-character code'
            full={isMobile}
            value={listing.referral_code}
            error={this.state.referralCodeError}
            helperText={this.state.referralCodeError}
            onChange={this.updateReferralCode}
          /> */}
          {/* 
          <NLButton
            title ={shouldRenderVerified ? "Verified!" : 'Verify'}
            type={shouldRenderVerified ? "SimpleButton GreenWhite MedMargin" : "SimpleButton MedMargin"}
            onClick={shouldRenderVerified ? undefined : this.verifyReferralCode} 
          /> 
        </div> */}

        <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
          currStep={this.props.currStep} handleNav={this.props.handleNav}/>

      </Paper> 
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
      appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(Payment);
