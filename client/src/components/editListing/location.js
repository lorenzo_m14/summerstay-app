import React, { PureComponent } from 'react';
import '../../CSS/App.css';
import '../../CSS/CreateListing.css'

import Paper  from '@material-ui/core/Paper';
import { CustomTextbox } from '../../constants/inputs.js'

import SearchBar from '../utilities/searchBar.js'
import GeneralMap from '../map/generalMap.js'
import ContinueButton from './continueButton.js'

class Location extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      locationError:false, 
      apartmentError: false, 
      showError: false
    }
  }  
  
  isFloat=(n)=>{
    return Number(n) === n && n % 1 !== 0
  }

  check=(finished)=>{
    if(!this.state.locationError && this.props.listing.address !== '') {
      this.props.handleNav('forward',finished)
    } else {
      this.setState({locationError: true})
    }
  }

  updateAddress = (address, lat, lng, fullAddress) => {
    this.props.onUpdateListing('address', address)
    this.props.onUpdateListing('latitude', lat)
    this.props.onUpdateListing('longitude', lng)
    this.props.onUpdateListing('full_address', fullAddress)
  }

  addApartment = (event) => {
    this.setState({ apartmentError: false })
    this.props.onUpdateListing('apt_number', event.target.value)
  }

  problem = (val) => {
    this.setState({locationError: val})
  }

  render() {
    const { listing, isMobile } = this.props

    return (
        
      <Paper className='SubPageRoot' elevation={0}>
        <div className="CreateMainHeader">
          <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>Is your address correct?</h2>     
          {
            !this.state.locationError ? <p className="SoftText">Only confirmed guests will see the exact location of your listing.</p> : 
            <p style={{color:'red'}}>Make sure you have a valid location.</p> 
          } 
        </div>
                
        <div style={{display: 'flex', justifyContent: isMobile ? 'flex-start' : 'space-between', 
                      flexWrap: isMobile ? 'wrap' : 'nowrap', width: '100%', position: 'relative'}}>
            <div style={{ zIndex: 2, display: 'flex', width: '100%', paddingRight: isMobile ? 0 : 10 }} >
              <SearchBar 
                initialVal={listing.address}
                placeholder='Enter your address ...'
                main
                error={this.state.locationError}
                problem={this.problem}
                updateLocation={this.updateAddress}
              />
            </div>
            <CustomTextbox
              style={{marginTop: isMobile ? 10 : 0, width: isMobile ? '100%' : null }}
              autoComplete='off'
              full={isMobile}
              error={this.state.apartmentError}
              value={listing.apt_number || ''}
              id="outlined-search"
              label="Apartment #"
              type="text"
              onChange={this.addApartment}
              variant="outlined"
            />
        </div>
        
        
        <div style={{display:'flex', flex: 1, marginTop: 20, marginBottom: 50}}>
          <GeneralMap specific loc = {{lat: listing.latitude, lng: listing.longitude}}/>
        </div>
        
 
        <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
            currStep={this.props.currStep} handleNav={this.props.handleNav}/>
      </Paper> 
      
    ); 
  }
}

export default Location;
