import React, { Component } from 'react'
import '../../CSS/App.css'
import '../../CSS/CreateListing.css'

import ActiveStorageProvider from 'react-activestorage-provider'
import { PUT, DELETE } from '../../constants/requests.js'
import { MAX_NUM_IMAGES, MAX_IMAGE_SIZE } from '../../constants/index.js'

import NLButton from '../utilities/NoLinkButton.js'
import Paper from '@material-ui/core/Paper'
import CircleLoad from '../utilities/circleLoad.js'
import ContinueButton from './continueButton.js'

import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';

// can render either existing image or "uploading" image, conditionally render styling/delete button
const SortableImage = SortableElement(({image, deletePicture}) => 
    <div key={image.id} className={'grabbable'} style={{position: 'relative', zIndex: 10}}>
        {
            !image.isUpload &&
            <NLButton onClick={()=>deletePicture(image)} icon="clear" type="TrashButton"/>
        }
        <img 
            style={{width:144, height:144, margin:5, borderRadius:5, objectFit: 'cover', filter: image.isUpload ? 'grayscale(80%)' : '', }} 
            src={image.url} 
            alt="img"
        />
    </div>
);
    
const SortableImages = SortableContainer(({images, deletePicture}) => 
    <div style={{display:'flex', flexWrap: 'wrap'}}>
      {images.map((image, index) => (
        <SortableImage key={`item-${image.id}`} index={index} image={image} deletePicture={deletePicture} />
      ))}
    </div>
);

class Photos extends Component {
    constructor(props){
        super(props);
        this.state={
            sortedPictures: [],
            pictureUploadData: [],
            pictureError:'',
            completed: 0,
            uploading: false, 
            deleting: false
        }
      }

      componentDidMount() {
        const { listing } = this.props
        this.setState({
            sortedPictures: listing && listing.images ? listing.images :  [],
        })
      }

      componentDidUpdate(prevProps) {
        const { listing } = this.props
        // Typical usage (don't forget to compare props):
        if (listing.images !== prevProps.listing.images) {
            this.setState({
                sortedPictures: listing && listing.images ? listing.images :  [],
            })
        }
      }


    check=(finished)=>{

        let error = false
        if(!this.props.listing.images || this.props.listing.images.length<2){
            this.setState({pictureError:'Please upload at least two photos'})
            error = true
        }
        if(!error) {
            // make request to save image ordering
            this.sortPictures(this.state.sortedPictures)
            this.props.handleNav('forward',finished) //should always call skip bc photos are separately saved
        }
    }

    // adds pictures to UI (not yet uploaded to server)
    addPictures=(event)=>{
        console.log("Adding pictures")
        let pd = [...this.state.pictureUploadData];
        let files = Array.from(event.target.files)
        let renderFiles = files.map(f => {
            return { id: f.name, isUpload: true, url: URL.createObjectURL(f)} 
        })
        let sizes = files.findIndex(f => f.size > MAX_IMAGE_SIZE);
        const totalNumImages = files.length + this.state.sortedPictures.length

        let pictureError = null;
        if(totalNumImages > MAX_NUM_IMAGES) {
            pictureError = `Listing cannot have more than 10 images`
        } else if(sizes !== -1) {
            pictureError = `Images must be less than 10MB in size`
        } else if (files.length === 0) {
            pictureError = `Must upload at least one image`
        }

        if (pictureError) {
            this.setState({pictureError})
            return null
        } else  {
            this.setState({
                pictureUploadData: pd.concat(renderFiles), 
                pictureError:'',
                uploading: true
            });
            event.target.value = '' // clear the files
            return files
        }
    }

    // deletes picture from server and updates UI
    deletePicture=(image)=>{
        this.setState({
            pictureError: ''
        })
        if(this.props.listing.images && this.props.listing.images.length>=3 && !this.state.deleting){
            console.log("Deleting picture");
            this.setState({
                deleting: true
            })
            const send_data = { image_id: image.id }
            DELETE(`/api/v1/listings/${this.props.listing.id}/delete_image`, send_data)
            .then(data => {
                const listing_images = this.props.listing.images.filter((i) => i.id !== image.id)
                this.sortPictures(listing_images)
                this.setState({
                    deleting: false
                })
            })
            .catch(error => {
                
                this.setState({
                    deleting: false,
                    pictureError: 'For some reason we weren\'t able to delete your photos. Try reloading the page.'
                })
            });
        }else if(!this.props.listing.images || this.props.listing.images.length<3) {
            this.setState({
                pictureError: 'You need to have at least two images.'
            })
        }
    }
    
    startUpload=()=>{
        this.setState({
            uploading: true
        })
    }
    // updates parent listing state with new images and updates UI
    savePictures=(response)=> {
        if(response.listing) {
            // clear the UI of uploaded images
            this.setState({ 
                pictureUploadData: [],
                uploading: false,
                pictureError:'' 
            }, () => this.props.handleSuccessfulUpdate(response.listing))
        }
    }

    handleUploadSubmit=(response)=> {
        if (response && response.listing && response.listing.images) {
            this.sortPictures(response.listing.images)
        } else {
            console.log("Error in image upload!")
        }
    }

    // makes call to sort picture positions
    sortPictures=(pictures) => {
        const sorted_image_ids = pictures.map(image => image.id)
        const send_data = {
            sorted_image_ids: sorted_image_ids
        }
        
        PUT(`/api/v1/listings/${this.props.listing.id}/sort_images`, send_data)
        .then(data => {
            this.savePictures(data)
        })
        .catch(error => {
            this.setState({
                deleting: false,
                pictureError: 'For some reason we weren\'t able to save your photos. Try reloading the page.'
            })
        });
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            this.setState(({sortedPictures}) => ({
                sortedPictures: arrayMove(sortedPictures, oldIndex, newIndex),
            }), () => this.sortPictures(this.state.sortedPictures));
        }
    };

    error=(error)=>{
        console.log(error)
        this.setState({
            pictureError: "For some reason we weren't able to upload your photos. Try reloading the page.", 
            pictureUploadData: []
        })
    }

  // Using ActiveStorageProvider component
  // - allows direct uploads to S3 bucket
  // - upload process occurs on handleUpload prop in the component
  // - views separate currently saved photos with new uploads
  // - can delete saved photos and upload new ones
  render() {
    const {listing} = this.props;
    const { sortedPictures, pictureUploadData } = this.state;

    return (
        <Paper className='SubPageRoot' elevation={0} style={{height:610}}>
            
            <div className="CreateMainHeader">
                <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>Add photos to your listing.</h2>
                
                {
                    this.state.pictureError === '' ? 
                    // <p className="GrayText" style={{marginBottom: 10}}>Upload <strong>at least two photos</strong> to your listing <br/> 
                    //     {!isMobile ? <i>Tip - take landscape photos at chest level, not from your head looking down!</i> : null} </p>

                    // Tips:
                    // - Drag your photos to reorder them
                    // - Take landscape pictures
                    <p className="SoftText">Quality photos have shown to be the most important aspect of a listing. Upload 2-10 photos to show off your listing.</p>
                    : <p style={{color:'red', marginBottom: 10}}>{this.state.pictureError}</p>
                }
                
            </div>
            <div style={{display:'flex'}}>
                <NLButton title ="Add Photos" onClick={()=>this.fileInput.click()} type="MediumButton"/>
            </div>
            
            <ActiveStorageProvider
                directUploadsPath={'/api/v1/direct_uploads'}
                headers={{
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }}
                endpoint={{
                    path: `/api/v1/listings/${listing.id}`,
                    model: 'Listing',
                    attribute: 'images',
                    method: 'PUT',
                }}
                onError={this.error}
                onSubmit={(response) => this.handleUploadSubmit(response)}
                multiple
                render={({ handleUpload, uploads, ready }) => (
                <div style={{display: 'flex', flexDirection: 'column'}}>
                    <input 
                        style={{display:'none'}} 
                        type='file' 
                        disabled={!ready}
                        onChange={(event)=>{
                            const files = this.addPictures(event); 
                            if(files) {
                                handleUpload(files) 
                            }
                            
                        }}
                        ref={fileInput => this.fileInput = fileInput}
                        multiple
                        accept="image/png, image/jpeg"
                    />

                    <div style={{height:5}}/>
                </div>
                )}
            />
                    
                <div style={{display:'flex',flexDirection: 'column',justifyContent:'flex-start', marginTop: 10, minHeight: 100, maxHeight: 375, overflow: 'auto'}}>                    
                    <div>
                        {
                            this.state.deleting || this.state.uploading ? 
                                <div style={{position:'absolute',left: 0,right: 0,marginLeft: 'auto',marginRight: 'auto', zIndex: 99}}>
                                    <CircleLoad/>
                                </div>
                                : null
                        }
                        {
                            sortedPictures && 
                            <SortableImages
                                axis="xy"
                                images={[...sortedPictures,...pictureUploadData]} // pictureUpload (currently pendingdata) is concatenated for formatting reasons
                                onSortEnd={this.onSortEnd}
                                deletePicture={this.deletePicture} 
                                pressDelay={100}
                            />
                        }
                    </div>
                </div>
                            
                          
                
            
            <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
              currStep={this.props.currStep} handleNav={this.props.handleNav}/>
        </Paper> 
      
    ); 
  }
}

export default Photos;