import React, { PureComponent } from 'react'
import '../../CSS/App.css'
import '../../CSS/CreateListing.css'

import Paper from '@material-ui/core/Paper'
import { LISTING_TYPES } from '../../constants/index.js'
import { MERGE_ARRAYS } from '../../constants/functions.js'
import {  CustomCheckbox, CustomCount } from '../../constants/inputs.js'
import ContinueButton from './continueButton.js'

class Room extends PureComponent {
    constructor(props){
      super(props)
      this.state={
 
        bedsError: false, 
        bathsError: false, 
        bedroomError: false,
        disableBedroom: false
      }
      this.listingTypes = MERGE_ARRAYS({title: LISTING_TYPES, subText:['Guests have the whole place to themselves. This usually includes a bedroom, a bathroom, and a kitchen.', 'Guests have their own private room for sleeping. Other areas could be shared.','Guests sleep in a bedroom area that could be shared with others.']})
    }
    componentDidMount(){
      if(!this.props.listing.bedroom  && (this.props.listing.listing_type === 'Shared Room' || this.props.listing.listing_type === 'Single Room')){
        this.props.onUpdateListing('bedroom', 1)
      }
    }
    check=(finished)=>{

      let error = false
      if(this.props.listing.bathroom <= 0 || !this.props.listing.bathroom){
        this.setState({bathsError: true})
        error = true
      }
      if(this.props.listing.beds <= 0 || !this.props.listing.beds){
        this.setState({bedsError: true})
        error = true
      }
      if(this.props.listing.bedroom <= 0 || !this.props.listing.bedroom){
        this.setState({bedroomError: true})
        error = true
      }

      if(!error) {
        this.props.handleNav('forward',finished)
      }

    }

    updateType=(event)=>{ // Refrence LISTING_TYPES in constants/index.js to see all types
      if(event.target.value === 'Shared Room' || event.target.value === 'Single Room'){
        this.props.onUpdateListing('bedroom', 1)
      }
      this.props.onUpdateListing('listing_type', event.target.value)

    }


  render() {
    const { listing, isMobile, isLargePhone } = this.props

    return (
        
        <Paper className='SubPageRoot' elevation={0}>
          <div className="CreateMainHeader">
            <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>What type of place are you listing?</h2>
            <p className="SoftText"> If you are listing a single or shared room please elaborate on the living arrangement in the listing description.</p>
          </div>
            
          <div style={{display:"flex",flex: 1, flexWrap: 'wrap' }}>
            <div style={{display:"flex", flexDirection:"column",marginRight: 30}}>              
              {
                this.listingTypes.map(t=>
                  <div style={{marginBottom: 20}} key={t.title}>
                    <CustomCheckbox 
                      checked={t.title === listing.listing_type}
                      onChange={this.updateType}
                      label={t.title}
                      subText={isLargePhone ? null : t.subText}
                      value={t.title}
                    />
                  </div>
                )
              }
              
            </div>

            <div style={{display:'flex', flexDirection:'column'}}>
              <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>How many bedrooms, beds, and baths?</h2>
              <p className="SoftText">Only include those that your renter will have access to.</p>
              <div style={{marginLeft: isLargePhone ? 0 : 58, marginRight: isLargePhone ? 10 : 0}}>
              <div style={{marginBottom: 20}}>
                <CustomCount 
                  value={listing.bedroom}
                  error={this.state.bedroomError}
                  disabled={listing.listing_type === 'Single Room' || listing.listing_type === 'Shared Room'}
                  min={0}
                  max={10}
                  step={1}
                  
                  title='Bedrooms'
                  onChange={(val)=>this.props.onUpdateListing('bedroom', val)}
                />
              </div>
              <div style={{ marginBottom: 20}}>
                <CustomCount 
                  value={listing.beds}
                  error={this.state.bedsError}
                  min={0}
                  max={10}
                  step={1}
                  title='Beds'
                  onChange={(val)=>this.props.onUpdateListing('beds', val)}
                />
              </div>
              <div style={{ marginBottom: 20}}>
                <CustomCount 
                  value={listing.bathroom}
                  error={this.state.bathsError}
                  min={0}
                  max={10}
                  step={.5}
                  title='Baths'
                  onChange={(val)=>this.props.onUpdateListing('bathroom', val)}
                />
              </div>
              </div>
              
            </div>
          </div>

          {/* can also pass isMobile to ContinueButton and use MiniMediumButton */}
          <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
                    currStep={this.props.currStep} handleNav={this.props.handleNav}/>
        </Paper> 
      
    ); 
  }
}

export default Room;