import React, { PureComponent } from 'react'
import '../../CSS/App.css'

import Paper from '@material-ui/core/Paper'
import { CustomTextbox } from '../../constants/inputs.js'
import ContinueButton from './continueButton.js'

class Details extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      nameError: false, 
      summaryError: false
    }
    this.MAX_NAME_LENGTH = 50
    this.MIN_NAME_LENGTH = 10
    this.MIN_SUMMARY_LENGTH = 50 
  }

  check=(finished)=>{
    
    this.setState({
      summaryError: false,
      nameError: false
    })
    let error = false
    if(!this.props.listing.summary || this.props.listing.summary.length<this.MIN_SUMMARY_LENGTH){
      this.setState({summaryError: true})
      error = true
    }
    if(!this.props.listing.listing_name || this.props.listing.listing_name.length<this.MIN_NAME_LENGTH ){
      this.setState({nameError: true})
      error = true
    }
    if(!error) {
      this.props.handleNav('forward',finished)
    }

  }

  handleChange=(type, val)=>{
    if(type === 'listing_name' && this.state.nameError && val.length>=this.MIN_NAME_LENGTH){
      this.setState({nameError: false})
    }
    if(type === 'summary' && this.state.summaryError && val.length>=this.MIN_SUMMARY_LENGTH){
      this.setState({summaryError: false})
    }
    this.props.onUpdateListing(type, val)
  }

  render() {
    const {listing } = this.props;

    return (
        
      <Paper className='SubPageRoot' elevation={0}>
        <div className="CreateMainHeader">
          <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>Give your listing a name.</h2>
          {
            !this.state.nameError ? 
            <p className="SoftText">Listing names are displayed to renters throughout the search process.</p> : 
            <p style={{color:'#e62929'}}>Your listing name must be at least {this.MIN_NAME_LENGTH} characters.</p>
          }
        </div>
        <div style={{display: 'flex',flexDirection:'column', justifyContent:'space-between'}}>
          
          
          {/* <p style={{marginBottom: 0}}>Get renters interested with a quick phrase. </p> */}
          <div style={{display:"flex", position:'relative'}}>
          <p className="GrayText ExtraSmallText" style={{position:'absolute', top: -22, right: 0}}>{listing.listing_name ? `${listing.listing_name.length}/${this.MAX_NAME_LENGTH}` : `0/${this.MAX_NAME_LENGTH}`}</p>
              <CustomTextbox
                full
                placeholder=''
                onInput = {(e) =>{
                  e.target.value = e.target.value.slice(0,this.MAX_NAME_LENGTH)
                }}
                error={this.state.nameError}
                value={listing.listing_name ? listing.listing_name : ''}
                onChange={(event)=>this.handleChange('listing_name', event.target.value)}
              />

          </div>
          <h2 className="SkinnyHeader" style={{marginTop: 60, marginBottom: 0}}>Describe your place to renters.</h2>
         
          {
            !this.state.summaryError ? 
             <p className="SoftText">Write a quick summary to highlight what's special about your listing, the neighborhood, and local transportation.</p> : 
            <p style={{color:'#e62929'}}>Your summary must be at least {this.MIN_SUMMARY_LENGTH} characters. A quality summary will help renters make a decision.</p>
          }
          {/* <p className="GrayText" style={{marginBottom: 0}}>Please give some insight into your listing by elaborating on its features and amenities.</p> */}
          <div style={{display:"flex", flex: 1,flexDirection:"column",}}>
              <CustomTextbox
                placeholder=''
                full
                multiline
                rows="11"
                error={this.state.summaryError}
                value={listing.summary ? listing.summary : ''}
                onChange={(event)=>this.handleChange('summary', event.target.value)}
              />

          </div>
        </div>
           

            <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
                    currStep={this.props.currStep} handleNav={this.props.handleNav}/>
      </Paper> 
    );
  }
}

export default Details;