import React from 'react';
import NLButton from '../utilities/NoLinkButton.js'
import { connect } from 'react-redux';

// checks if currStep is the last section to be completed
function checkIfLast(currStep, sectionsStatus) {
	for (let i=0; i<sectionsStatus.length; i++) {
    if (sectionsStatus[i].step === currStep && sectionsStatus[i].isFinished || sectionsStatus[i].step !== currStep && !sectionsStatus[i].isFinished) {
      return false
    }
	}
	return true
}



// Continue button - uses state to determine if save, continue or finish is rendered
function ContinueButton(props) {
  let allFinished = props.sectionsStatus.findIndex(s=>!s.isFinished) === -1 // All steps are completed if no false is found
  let isLast = checkIfLast(props.currStep, props.sectionsStatus)
  let currStepStatus = props.sectionsStatus.filter(s => s.step === props.currStep)[0].isFinished
  const buttonType = 'MediumButton'
	let backButton = <NLButton title ="Back" onClick={() => props.handleNav('back')} type={`${buttonType} Disabled MedMargin`}/>
	let nextButton = <NLButton title ={(props.listing.active || currStepStatus)  ? "Next" : "Skip"}  onClick={() => props.handleNav('skip')} type={`${buttonType} Disabled MedMargin`}/>
	let continueButton


	if (props.currStep === 'Location') {
		backButton = <NLButton title ="Back" onClick={null} type={`${buttonType} Disabled MedMargin`}/>
	}
	if (props.currStep === 'Details') {
		nextButton = <NLButton title ={props.listing.active ? "Next" : "Skip"}  onClick={null} type={`${buttonType} Disabled MedMargin`}/>
	}


  if (!props.listing.ready && (isLast || allFinished)) {
    continueButton = <NLButton title="Complete Listing"  onClick={()=>props.check(true)} type={`${buttonType} SolidPurple MedMargin`}/>
	} else if (!props.listing.ready && !isLast && !allFinished) {
    continueButton = <NLButton title="Continue" onClick={()=>props.check(false)} type={`${buttonType} SolidPurple MedMargin`}/>
  } else {
    continueButton = <NLButton title="Save" onClick={()=>props.check(false)} type={`${buttonType} SolidPurple MedMargin`}/>
  } 
  
  if (props.appSize.isMediumPhone) {
    return (
      <div style={{display: 'flex', position:'fixed', justifyContent: 'flex-end', alignItems: 'center', left: 0, 
        bottom: 0, width: '100vw', height: 60, backgroundColor: 'white'}}>
          <div style={{ marginRight: '1vw' }}>{backButton} {nextButton} {continueButton}</div> 
      </div>
    )
  } else {
    return (
      <div style={{display: 'flex', position:'absolute', right: 20, bottom: 20}}>
        <div>{backButton} {nextButton} {continueButton}</div> 
      </div>
    )
  }
 
}
const mapStateToProps = state => {
  return {
      appSize: state.housingApp.appSize 
  }
}

export default connect(mapStateToProps)(ContinueButton)
