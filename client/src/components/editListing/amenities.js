import React, { PureComponent } from 'react';
import '../../CSS/App.css';
import '../../CSS/CreateListing.css'

import Paper from '@material-ui/core/Paper';
import { AMENITIES, LISTING_TYPES} from '../../constants/index.js'
import { MERGE_ARRAYS } from '../../constants/functions.js'
import {  CustomCheckbox } from '../../constants/inputs.js'
import ContinueButton from './continueButton.js'

const amenities = Object.entries(AMENITIES)

export function CheckAmenities(listing){
  // checks if array is empty
  return Boolean(Array.isArray(listing.amenities) && listing.amenities.length)
}

class Amenities extends PureComponent {
    constructor(props){
      super(props)
      this.state={
        amenitiesError: false
      }
      this.amenities = amenities //['is_air','is_furnished','is_heating','is_internet','is_kitchen','is_laundry','is_tv']
      this.listingTypes = MERGE_ARRAYS({title: LISTING_TYPES, subText:['Guests have the whole place to themselves. This usually includes a bedroom, a bathroom, and a kitchen.', 'Guests sleep in a bedroom area that could be shared with others.','Guests have their own private room for sleeping. Other areas could be shared.']})
    }

    check=(finished)=>{
        if(CheckAmenities(this.props.listing)){
            this.props.handleNav('forward',finished)
        }else{
            this.setState({amenitiesError: true})
        }
    }

    updateAmenities= (event, shouldSelect) => {
      this.setState({amenitiesError: false})
      let { amenities } =  this.props.listing;
      let type = event.target.value

      if(type === 'none'){
        this.props.onUpdateListing('amenities', ['none'])
      } else {
        if(shouldSelect) {
          // add this type to amenities array if not already included
          if(!amenities.includes(type)) {
            amenities = amenities.filter(a => a !== 'none')
            amenities.push(type)
          }
        } else {
          amenities = amenities.filter(a => a !== type)
        }
        this.props.onUpdateListing('amenities', amenities)
      }
    }


  render() {
    const { listing } = this.props

    return (
        
        <Paper className='SubPageRoot' elevation={0}>
          <div className="CreateMainHeader">
            <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>Which amenities are included?</h2>
            {
                !this.state.amenitiesError ? 
                <p className="SoftText">Additional amenities can be added to your listing description.</p>
                : <p style={{color: '#e62929'}}>Please select an amenity option.</p>
            }
          </div>
            
        <div style={{display:'flex', flexWrap:'wrap', width: 'calc(100% - 30px)', maxHeight: 450}}>
            {
                this.amenities.map(a=>
                <div key={a[0]} style={{width:170}}>
                    <CustomCheckbox 
                    checked={listing.amenities.includes(a[0])}
                    onChange={this.updateAmenities}
                    label={a[1]}
                    value={a[0]}
                    />
                </div>
                )
            }
            </div>

          {/* can also pass isMobile to ContinueButton and use MiniMediumButton */}
          <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
                    currStep={this.props.currStep} handleNav={this.props.handleNav} />
        </Paper> 
      
    ); 
  }
}

export default Amenities;