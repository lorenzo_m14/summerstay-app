import React, { PureComponent } from 'react';
import '../../CSS/App.css';
import '../../CSS/CreateListing.css'

import { Paper }from '@material-ui/core';

import { connect } from 'react-redux'

import ContinueButton from './continueButton.js';
import PassbaseWrapper from '../utilities/passbaseWrapper.js'

class Verification extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      userError: false, 
    }
  }  

  check=(finished)=>{
    
    if(this.props.user && (this.props.user.id_verify_status==='InReview' || this.props.user.id_verify_status==='Approved') ){
      this.props.handleNav('forward', finished)
    } else {
      this.setState({userError: true})
    } 
  }
 

    
  render() {
    const { listing, isMobile } = this.props

    return (
        
      <Paper className='SubPageRoot' elevation={0}>
        <div className="CreateMainHeader">         
          <h2 className="SkinnyHeader" style={{marginTop: 0, marginBottom: 0}}>Verify your identity</h2>
          {
            this.state.userError ? <p style={{ textAlign: 'center', margin:0, marginBottom: 5, color:'red' }}>Please make sure to verify your identity.</p> 
            : <p className="SoftText">To ensure quality listings for our users we require all hosts to verify their identity with a government-issued photo ID.</p>
          }
        
        </div> 
        <PassbaseWrapper/>

        <ContinueButton listing={this.props.listing} ready={this.props.ready} check={this.check} sectionsStatus={this.props.sectionsStatus} 
          currStep={this.props.currStep} handleNav={this.props.handleNav}/>

      </Paper> 
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user
  }
}
export default connect(mapStateToProps)(Verification);

