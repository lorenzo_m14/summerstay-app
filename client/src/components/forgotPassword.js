import React, {Component} from 'react'
import '../CSS/App.css'
import { POST } from '../constants/requests.js'

import NLButton from './utilities/NoLinkButton.js'
import { CustomTextbox } from '../constants/inputs.js'
import { LOCAL_EMAIL_CHECK } from '../constants/functions.js'
import CircleLoad from './utilities/circleLoad.js'

class ForgotPassword extends Component {
  constructor(props){
    super(props)
    this.state = {
      email: '', 
      loading: false, 
      success: false, 
      emailError: false,
      serverError: false,
      serverResponse: '', 
      formatErrorOnClick: false
    };
  }
  
  forgotPassword=()=>{

    if (LOCAL_EMAIL_CHECK(this.state.email) && !this.state.loading && !this.state.success){
      this.setState({
        emailError: false, 
        loading: true
      })
      const send_data = {
        email: this.state.email
      }
      POST('/api/v1/users/password', send_data)
      .then(data => {
          this.setState({
            success: true, 
            loading: false
          })
      })
      .catch(error => {
        this.setState({ 
          loading: false, 
          success: false,
          emailError: true,
          serverError: true,
          serverResponse: error.error
         })
      });
    } else{
      this.setState({
        emailError: true, 
        formatErrorOnClick: true
      })
    }
  }

  handleEmail=(event)=>{
    this.setState({
      email: event.target.value,
      success: false, 
      serverError: false, 
      serverResponse: '', 
      emailError: false, 
      formatErrorOnClick: false
    })
    if (!LOCAL_EMAIL_CHECK(this.state.email)) {
      this.setState({ emailError: true })
    }
  }
  
  render() {
    return (
      <div style={{display:'flex', flexDirection:'column'}}>
        <h2 style={{marginTop:0}}>Forgot Password?</h2>
        <p>Provide the email you signed up with and we'll send you a link to reset your password.</p>
        { this.state.serverError ? <p style={{ color:'red' }}>{this.state.serverResponse} Please try again.</p> : null }
        { this.state.formatErrorOnClick ? <p style={{ color: 'red' }}>Please make sure the input is valid.</p> : null }

        <CustomTextbox
          error={this.state.emailError}
          value={this.state.email}
          id="outlined-search"
          label="Email"
          type="email"
          full
          onChange={this.handleEmail}
        />
        <div style={{height: 20}}/>
        {
          !this.state.loading ? 
            <NLButton title={this.state.success ? 'Check Your Email' : 'Reset Password'} 
            type={this.state.success ? "MediumButton SolidGreen" : "MediumButton"}
            onClick={this.forgotPassword} />
          :
          <CircleLoad/>
        }
      </div>
    ); 
  }
}
  
export default ForgotPassword;
