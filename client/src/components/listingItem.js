import React from 'react'
import NoPhoto from '../Assets/noPhoto.png'

import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import ListingHeader from './listing/listingHeader.js'
import '../CSS/ListingsMain.css';
import '../CSS/Static.css'
import { connect } from 'react-redux'
import ReactGA from 'react-ga'

function recordClick(listing){
  ReactGA.event({
    category: 'Search',
    action: `listing: ${listing.id} in ${listing.location_metadata.city}`, 
    label: 'Listing Clicked'
  })
}


function ListingItem(props){
  
  const { listing, appSize } = props
  const len = listing.images ? listing.images.length > 0 : false
  const url = props.dates.startDate && props.dates.endDate ? 
  `/listings/view/${listing.id}?startDate=${props.dates.startDate.format('YYYY-MM-DD')}&endDate=${props.dates.endDate.format('YYYY-MM-DD')}`
  : `/listings/view/${listing.id}`
  const { isSize1300 } = appSize
  return(
        
    <Link 
      to={url}
      onClick={()=>recordClick(listing)}
      target="_blank" // makes a new tab
      className={isSize1300 ? "ListingItemRootRow" :"ListingItemRootColumn"}
      >
      <div  
        className={isSize1300 ? "ListingItemSubRow ListingItemSubShadow" :"ListingItemSubColumn ListingItemSubShadow"}
        onMouseEnter={() => props.updateHover(listing.id)}
        onMouseLeave={() => props.updateHover(null)}
      >
          
        {/* <div style={{backgroundImage: len ? `url(${listing.images[0].url})` : `url(${NoPhoto})`, backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',backgroundPosition: 'center', overflow:'hidden',
                    height: '83%', width:'100%',borderRadius: 5,boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1), 0 3px 10px 0 rgba(0, 0, 0, 0.1)'}}>
        </div> */}
        <img 
          className={isSize1300 ? "ListingItemImageRow" :"ListingItemImageColumn"}
          src={len ? listing.images[0].url : NoPhoto} 
          alt="Listing Cover Image"
        />
        {
          !isSize1300 ?
            <div style={{display:"flex",flex:1,flexDirection:'column',justifyContent:'space-between', margin:10, minWidth: 1}}>
              <h3 className="SkinnyHeader OverFlowEllipsis" style={{ margin:0 }}>{listing.listing_name}</h3> 
              <div style={{display:"flex",justifyContent:'space-between',flexDirection:"row"}}>
                <p className="GrayText" style={{margin:0}}>{listing.listing_type} &middot; {listing.beds === 1? listing.beds+" bed": listing.beds+" beds" } &middot; {listing.bathroom === 1? listing.bathroom+" bath" : listing.bathroom+" baths" }</p>
                <div style={{display:"flex",justifyContent:'flex-end',flexDirection:"row"}}>
                  <h3 style={{margin:0, color:"#6c4ef5"}}>${Math.floor(listing.price/100)}</h3>
                  <p className='SmallListingsText'>/week</p>
                </div>
              </div>
            </div>
          : 
            <div style={{display:"flex",flex:1,flexDirection:'column',justifyContent:'space-between',margin: 20, minWidth: 1}}>
              <ListingHeader listing={listing} size="medium"/>
              <div style={{display:"flex",justifyContent:'flex-end',flexDirection:"row"}}>
                <h3 style={{margin:0, color:"#6c4ef5"}}>${Math.floor(listing.price/100)}</h3>
                <p className='SmallListingsText'>/week</p>
              </div>
            </div>
        }          
      </div>
    </Link>

  )
}
const mapStateToProps = state => {
    return {
      appSize: state.housingApp.appSize
    }
  }
  
export default connect(mapStateToProps)(ListingItem)