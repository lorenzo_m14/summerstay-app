import React, { Component } from 'react'
import { Marker } from 'react-google-maps'
import { PlaceInfoWindow } from './infoWindow.js'
import ReactGA from 'react-ga'

class PlaceMarker extends Component {
    constructor(props) {
      super(props)
      this.state = {
        hover: false,
      }
    }
  
    clickTooltip=()=> {

      const { place } = this.props 
      ReactGA.event({
        category: 'Search',
        action: `listing tooltip: ${place.id} in ${place.location_metadata.city}`, 
        label: 'Tooltip Clicked'
      })
      this.props.updateTooltip(place.id)
    }
  
    closeWindow=()=> {
      this.props.updateTooltip(null)
    }

    handleMouseOver=()=> {
      this.setState({hover: true})
    }

    handleMouseOut=()=> {
      this.setState({hover: false})
    }
  
    render() {
      const {place, show, hover} = this.props
      const price = String(Math.floor(place.price/100))
      const hovered = this.state.hover || hover || show
      const svgWidth = price.length > 3 ? '5' : '8'
      // TODO: if hover prop is true, change the styling to highlight the Marker
      return(
        <Marker  
            onMouseOver={this.handleMouseOver}      
            onMouseOut={this.handleMouseOut}
            zIndex={hovered ? 2 : 1}
            markerWithLabel={window.MarkerWithLabel}
            icon={{ url: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="70" height="30" viewBox="0 0 50 20"><rect x="0" y="1" rx="6" ry="6" width="40" height="18" style="fill:${hovered ? 'rgb(108,78,245)' : 'white'};" stroke="rgb(108,78,245)" stroke-width="1"/><g fill-rule="evenodd"><text style="font-size:11px;font-family:Helvetica, Neue, sans-serif;font-weight:600;fill:${hovered ? 'white' : 'black'};"><tspan x="${svgWidth}" y="14">$${price}</tspan></text></g></svg>`}}
            position={{
                lat: parseFloat(place.latitude),
                lng: parseFloat(place.longitude)
            }}
            onClick={this.clickTooltip}>
            {show && (
                <PlaceInfoWindow
                  key={`info${place.id}`}
                  place={place}
                  closeWindow={this.closeWindow}
                />
            )}
        </Marker>
        
      );
    }
  }
  
export default PlaceMarker
