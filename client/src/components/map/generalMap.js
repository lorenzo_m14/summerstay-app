import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps" 
import GeneralMarker from './generalMarker.js'


const MapComponent = withGoogleMap((props) =>
    
    <GoogleMap
        defaultOptions={{
        streetViewControl: false,
        scaleControl: false,
        mapTypeControl: false,
        panControl: false,
        scrollwheel: false,
        zoomControl: true,
        rotateControl: false,
        fullscreenControl: false,
        zoomControlOptions: { position: window.google.maps.ControlPosition.TOP_RIGHT },
        gestureHandling: "greedy",
        maxZoom: props.specific ? 16 : 15,
        }}
        center={props.center}
        zoom={props.zoom} >
          {
            props.specific ?   
            <Marker id={"singleMarker"} position={{ lat: parseFloat(props.location.lat), lng: parseFloat(props.location.lng) }}/>
            :
            <GeneralMarker id={"singleMarker"} lat={parseFloat(props.location.lat)} lng={parseFloat(props.location.lng)}/>
          }
         

    </GoogleMap>

)

// height: this.props.small ? '380px' : '450px'
class GeneralMap extends Component {
    
    render() {
      return(
        <div style={{width: '100%', height: '100%' }}>
          <MapComponent
            center={this.props.loc}
            zoom={13}
            containerElement={
              <div style={{ height: `100%` }} />
            }
            mapElement={
              <div style={{ height: `100%` }} />
            }
            specific={this.props.specific}
            location={this.props.loc}
            places={[1]}
          />
        </div>
      );
    }
  }


  export default GeneralMap;