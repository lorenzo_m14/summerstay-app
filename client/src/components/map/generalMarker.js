import React, { Component } from 'react'
import { Marker } from 'react-google-maps'

class PlaceMarker extends Component {
  
    render() {
      const {id, lat, lng } = this.props
      return(
        <Marker        
            markerWithLabel={window.MarkerWithLabel}

            icon={{ url: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="100" width="100"><circle opacity="0.3" cx="60" cy="60" r="40" stroke="rgb(26,39,62)" stroke-width="2" fill="rgb(26,134,247)" /></svg>`}}
            position={{
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            }}
            >
        </Marker>
      );
    }
  }
  
export default PlaceMarker
