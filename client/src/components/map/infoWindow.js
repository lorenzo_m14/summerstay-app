import React, { Component } from 'react'
import { InfoWindow } from 'react-google-maps'
import NoPhoto from '../../Assets/noPhoto.png'
import Button from '../utilities/Button.js'
import ReactGA from 'react-ga'

export class PlaceInfoWindow extends Component {

  recordClick=()=>{

    const { place } = this.props
    ReactGA.event({
      category: 'Search',
      action: `listing from map: ${place.id} in ${place.location_metadata.city}`, 
      label: 'Listing Clicked'
    })
  }

  render() {
    const { place } = this.props

    return(
      <InfoWindow onCloseClick={this.props.closeWindow} >
        <div style={{display:'flex', flexDirection:'column',marginLeft: 6, marginTop:6}}>
          <img 
              style={{height: 200, borderRadius:5, objectFit: 'cover',}}
              src={place.images ? place.images[0].url : NoPhoto} 
              alt="Listing"
          />
          <div style={{display:'flex', justifyContent:'space-between', marginBottom: 10}}>
            <div style={{display:'flex', flex: 1,flexDirection:'column'}}>
              <h2>{place.listing_name}</h2>
              <p className="GrayText" style={{margin:0}}>{place.listing_type} &middot; {place.beds === 1? place.beds+" bed": place.beds+" beds" } &middot; {place.bathroom === 1? place.bathroom+" bath": place.bathroom+" baths" }</p>
            </div>
            <div style={{marginTop: 15}}>
              <Button title ='View' link={`/listings/view/${place.id}`} onClick={this.recordClick} target="_blank" type="MiniMediumButton"/>  
            </div>
            
          </div>
          <p style={{margin:0}}>{place.summary.substring(0,140)+"..."}</p>
          <h3 style={{alignSelf: 'flex-end',margin:0, color:"#6c4ef5"}}>${Math.floor(place.price/100)}</h3>
          
        </div>
      </InfoWindow>
    );
  }
}

export default PlaceInfoWindow