import React, {Component} from 'react';
import { withGoogleMap, GoogleMap } from "react-google-maps" 
import { connect } from 'react-redux'
import PlaceMarker from './placeMarker.js'
import '../../CSS/Map.css'
import NLButton from '../utilities/NoLinkButton.js';


const MapComponent = withGoogleMap((props) =>{
  let ref;
  return <GoogleMap
      ref={mapRef => ref = mapRef}
      onCenterChanged={() => props.resetCenter(ref.getCenter())}
      defaultOptions={{
        streetViewControl: false,
        scaleControl: false,
        mapTypeControl: false,
        panControl: false,
        scrollwheel: false,
        zoomControl: true,
        rotateControl: false,
        fullscreenControl: false,
        zoomControlOptions: { position: window.google.maps.ControlPosition.TOP_RIGHT }, 
        gestureHandling: "greedy"
      }}
      center={props.currCenter || props.center} // keep from reverting to original center after clicking tooltip
      defaultCenter={props.center}
      onClick={()=>props.updateTooltip(null)}
      zoom={props.zoom} >
      {
        props.places.map(p=>
          <PlaceMarker 
            key={`place${p.id}`} 
            show={props.tooltipID === p.id}
            hover={props.hoverListingId == p.id}
            updateTooltip={props.updateTooltip}
            place={p}
          />
        )
      }
  </GoogleMap>
  })
// Used to display listings

//, width: '600px', height: 'calc(100vh - 70px)'}}>

class Map extends Component {
    constructor(props){
      super(props)
      this.state ={
        tooltipID: null, 
        center: null, // center at time of server call for listings
        currCenter: null, // center updated on map change
        showRefresh: false,
      }
    }
    shouldComponentUpdate(nextProps, nextState){
      // update map center and UI to change locations on newly searched place
      if (nextProps.filteredListings !== this.props.filteredListings) {
        this.setState({ showRefresh: false, center: null })
      }

      return nextProps.lat !== this.props.lat || nextProps.lng !== this.props.lng || nextState.tooltipID !== this.state.tooltipID ||
              nextProps.zoom !== this.props.zoom || nextProps.filteredListings !== this.props.filteredListings || 
              nextProps.snap !== this.props.snap || nextState.center !== this.state.center || nextProps.hoverListingId !== this.props.hoverListingId
    }
    
    // resetsCenter object whenever map is dragged -> in future can call refreshListings whenever map is dragged
    resetCenter =(latlng)=> {
      this.setState({ center: latlng, showRefresh: true })
    }

    refreshListings =()=> {
      this.props.refreshListings(this.state.center.lat(), this.state.center.lng())
      this.setState({ showRefresh: false })
    }

    updateTooltip=(id)=>{
      this.setState({
        tooltipID: id
      })
    }
    render() {
      return(
        <div style={{position: 'relative', marginTop: 16, width: '100%', height: '100%' }}> 
          { this.state.showRefresh ? 
            <div style={{position: 'absolute', zIndex: 1, display: 'flex', width: '80%', paddingLeft: '10%', justifyContent: 'center', top: 20}}><NLButton onClick={()=> this.refreshListings() } type='MiniMediumButton SolidPurple' title='Refresh Search' /></div>
            : null
          }
          
          <MapComponent
            resetCenter={this.resetCenter}
            updateTooltip={this.updateTooltip}
            hoverListingId={this.props.hoverListingId}
            tooltipID={this.state.tooltipID}
            snap={this.props.snap}
            center={{
              lat: this.props.lat,
              lng: this.props.lng
            }}
            currCenter={this.state.center}
            zoom={this.props.zoom}
            containerElement={
              <div style={{ height: `100%` }} />
            }
            mapElement={
              <div style={{ height: `100%` }} />
            }
            places={this.props.filteredListings}
          />
        </div>
      );
    }
  }
  
  const mapStateToProps = state => {
    return {
        filteredListings: state.housingApp.filteredListings,
    }
  }

  export default connect(mapStateToProps)(Map);