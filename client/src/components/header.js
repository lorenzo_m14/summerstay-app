import React, { Component } from 'react'
import logo from '../Assets/SSLogo.png'
import logoIcon from '../Assets/SSIcon.png'
import '../CSS/Headers.css'
import Button from './utilities/Button.js'
import NLButton from './utilities/NoLinkButton.js'
//import Login from './login.js'
import AvatarWrapper from './utilities/avatar.js'

import Drawer from '@material-ui/core/Drawer'
import Divider from '@material-ui/core/Divider'

import { BrowserRouter as Router, Route, Link } from "react-router-dom"

import { connect } from 'react-redux'

class Header extends Component {
  constructor(){
		super();
		this.state = {
      showLogin: false,
      width: window.innerWidth,
      right: false,
		};
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  toggleDrawer = (open) => {
    this.setState({
      right: open,
    });
  };
  login=()=>{
    this.setState({
        showLogin: true
    })
  }
  cancel=()=>{
      this.setState({
          showLogin: false
      })
  }
    

    render() {
      const { appSize } = this.props
      const isMobile = appSize.isSmallComputer
      const isSmallPhone = appSize.isSmallPhone
      
      const sideList = (
        <div style={{display:"flex",flexDirection:"column"}}>
          <div style={{display:'flex', justifyContent:'space-between', alignItems:'center', margin: 10, 
            marginRight: 20, marginLeft: 10}}>
            <div>
              <img src={logoIcon} alt="SummerStay" height="45" width="auto"/>
            </div>
            <NLButton onClick={()=>this.toggleDrawer(false)} icon='closeBig' type="MediumButton SolidWhitePurple"/>
          </div>
          
          
          <Divider />
          <div style={{display:"flex",flexDirection:"column", alignItems:'center'}}>
            <Button title ="HOME" link="/" type="ListButtonBlack"/>
            <Button title ="FIND YOUR STAY" link="/listings" type="ListButtonBlack"/>
            <Button title ="LIST YOUR PLACE" link="/host" type="ListButtonBlack"/>
            <Button title ="PAYMENT PLANS"  link="/payment-plans" type="ListButtonBlack"/>
            <Button title ="HOW IT WORKS"  link="/how-it-works" type="ListButtonBlack"/>
          </div>
          <Divider />
          <div style={{display:"flex",flexDirection:"column",alignItems:'center'}}>
            {
              this.props.user !== null ? 

                <Button title ="MY DASHBOARD" link='/dashboard/listings' type="ListButton"/>: 
                <React.Fragment>
                  <Button title ="LOGIN" link="/login" type="ListButton"/>
                  <Button title ="SIGN UP" link="/signup" type="ListButton"/>
                </React.Fragment>  
            }
          </div>
            
        </div>
        
      );
      if(!isMobile){
        return (
          
            <div className="Header" >
              <div style={{marginLeft: 20}}>  
                <Link to="/" >
                  <img src={logo} alt="SummerStay" height="45" width="auto"/>
                </Link>                   
              </div>
              <div style={{display:'flex', alignItems:'flex-end',maxWidth:"60em", marginRight: 20}}>
                <Button title ="Find Your Stay" link="/listings" type="MediumButton SmallText"/>
                <Button title ="List Your Place" link="/host" type="UnderlinedButton MedMargin SmallText"/>
                <Button title ="Payment Plans"  link="/payment-plans" type="UnderlinedButton MedMargin SmallText"/>
                <Button title ="How It Works"  link="/how-it-works" type="UnderlinedButton MedMargin SmallText"/>
                {
                  this.props.user !== null ?
                  <Link to="/dashboard/listings" style={{textDecoration:'none'}}>
                      <div style={{marginLeft: 10, marginRight: 10}}>
                        
                          <AvatarWrapper user={this.props.user} size='medium'/>
                        
                      </div>
                      </Link>
                    :
                    // <Button title ="My Dashboard" link='/dashboard/listings' type="UnderlinedButton MedMargin SmallText"/>: 
                    <React.Fragment>
                      <Button title ="Login" link="/login" type="UnderlinedButton PurpleClearBottom MedMargin SmallText"/>
                      <Button title ="Sign Up" link="/signup" type="MediumButton SolidPurple MedMargin SmallText"/>
                    </React.Fragment>
                    
                }
                
              </div>
            </div>
          
        );
      }else{
        return (
          
            <div className="Header">
              <div style={{marginLeft: 20}}>  
                <Link to="/" >
                  {
                    !isSmallPhone ? 
                    <img src={logo} alt="SummerStay" height="45" width="auto"/>
                    : <img src={logoIcon} alt="SummerStay" height="45" width="auto"/>

                  }
                  
                </Link>                   
              </div>
              
              <div style={{flexDirection:"row" ,maxWidth:"40em", marginRight: 20}}>
                <NLButton title ="" icon="menu" onClick={()=>this.toggleDrawer(true)} 
                   type="MediumButton"
                />
                <Drawer anchor="top" open={this.state.right} onClose={()=>this.toggleDrawer(false)}>
                  <div
                    tabIndex={0}
                    role="button"
                    onClick={()=>this.toggleDrawer(false)}
                    onKeyDown={()=>this.toggleDrawer(false)}
                  >
                    {sideList}
                  </div>
                  
                </Drawer>
              </div>
            </div>
          
        );
      }
      
    }
  }
  
  const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
  }
 
  export default connect(mapStateToProps)(Header);