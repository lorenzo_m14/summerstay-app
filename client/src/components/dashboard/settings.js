import React, { Component } from 'react';
import '../../CSS/App.css'
import '../../CSS/Dashboard.css'

import NLButton from '../utilities/NoLinkButton.js'
import CircleLoad from '../utilities/circleLoad.js'
import AvatarWrapper from '../utilities/avatar.js'
import ActiveStorageProvider from 'react-activestorage-provider'
import Divider from '@material-ui/core/Divider';
import PassbaseWrapper from '../utilities/passbaseWrapper.js'
import PhoneInput from 'react-phone-input-2'
import '../../CSS/PhoneInput.css'

import { GET, POST, PUT } from '../../constants/requests.js';
import { CustomTextbox } from '../../constants/inputs.js'
import { SANITIZE_PHONE_NUMBER, IS_VALID_PHONE } from '../../constants/functions.js'
import { USER_TYPES, DISABLE_ID_VERIFICATION } from '../../constants/index.js'

import { connect } from 'react-redux'
import { loginUser } from '../../store/actions/passThrough.js'
import ModalWrapper from '../utilities/Modal.js';
import SchoolInfo from '../utilities/schoolInfo';

class DashboardSettings extends Component {  
    constructor(props){
        super(props)
        this.state={
            modal: null,

            phoneNumber: '',
            verifiedNumber: null,
            phoneToken: '',
            phoneNumberState: 0, 
            phoneLoading: false,
            phoneError: false,
            phoneTokenError: false,

            pictureError:'',
            uploading: false,

            schoolName: null,
            // description: '',
        }
    }

    // adds pictures to UI (not yet uploaded to server)
    addImage=(event)=>{
        const files = Array.from(event.target.files);
        if(files[0].size < 10000000){
            this.setState({
                pictureError:'',
                uploading: true
            });
            return files
        }else{
            this.setState({pictureError: `${files[0].name} is too large`})
            return null
        }
    }

    handleUpdate=(type, val)=>{
        this.setState({[type]: val})
    }
    
    handleSchoolNameUpdate=(value)=>{
        this.setState({ schoolName: value })
    }

    closeModal=()=>{
        this.setState({
            modal: null,
            phoneNumber: '',
            verifiedNumber: null,
            phoneToken: '',
            phoneNumberState: 0, 
            phoneLoading: false,
            phoneError: false,
            phoneTokenError: false,
        })
    }

    localPhoneCheck=()=>{
        
        this.setState({
            phoneLoading: true, 
            phoneError: false
        })
        const sanitizedNumber = SANITIZE_PHONE_NUMBER(this.state.phoneNumber)

        if(!IS_VALID_PHONE(sanitizedNumber)) {
            this.setState({
                phoneError: true,
                phoneErrorMessage: "Invalid phone number!",
                phoneLoading: false
            })
        } else {
            const send_data = {
                phone_number: sanitizedNumber
            }
            POST('/api/v1/users/send_phone_token', send_data)
            .then(data => {
                this.setState({
                    phoneNumberState: 1,
                    phoneLoading: false, 
                    verifiedNumber: sanitizedNumber
                })
            })
            .catch(error => {
                this.setState({
                    phoneError: true,
                    phoneErrorMessage: error.error,
                    phoneLoading: false
                })
            });
        }
    }

    finalPhoneVerification=()=>{
        if(this.state.phoneToken !== ''){
            this.setState({
                phoneLoading: true, 
                phoneTokenError: false
            })
            const send_data = {
                phone_number: this.state.verifiedNumber, 
                token: this.state.phoneToken
            }
            POST('/api/v1/users/verify_phone_number', send_data)
            .then(data => {
                GET('/api/v1/me')
                .then(data => {
                    this.props.onLoginUser(data.user)
                    this.closeModal()
                })
                .catch(error => {
                    this.closeModal()
                });  
            })
            .catch(error => {
                this.setState({
                    phoneTokenError: true,
                    phoneErrorMessage: error.error,
                    phoneLoading: false
                })
            });
        }else{
            this.setState({phoneTokenError: true})
        }
    }

    onPhotoFinish=(data)=>{
        this.props.onLoginUser(data.user);
        this.setState({
            uploading: false
        })
    }
    handleSchoolNameUpdate = (value) => {
        this.setState({ schoolName: value })
    }

    handleNotStudent=()=>{
        const send_data = {
            user: {
                school_name: null, 
                user_type: USER_TYPES.nonStudent
            }
        }
        this.handleSchoolNameUpdate(null)

        this.updateUser(send_data)
    }

    saveSchoolName=()=>{
        const changedSchoolName = this.state.schoolName && this.state.schoolName !== this.props.user.school_name
        if(changedSchoolName){
            const send_data = {
                user: {
                    school_name: this.state.schoolName, 
                    user_type: USER_TYPES.student 
                }
            }
            this.updateUser(send_data)
        }
    }

    updateUser=(send_data)=>{
        if(!this.state.loading){
            this.setState({loading: true})
            PUT('/api/v1/users/update_user', send_data)
            .then(data => {
                this.props.onLoginUser(data.user)
                this.setState({loading:false})
                this.closeModal()
            })
            .catch(error => {
                this.setState({
                    error: true,
                    errorMessage: error.error,
                    loading: false
                })
            })
        }
    }

    // for now, only schoolName is included
    saveUser=()=> {
        const changedSchoolName = this.state.schoolName && this.state.schoolName !== this.props.user.school_name
        //const changedDescription = this.state.description && this.state.description !== this.props.user.description
        const hasChanged = changedSchoolName // && changedDescription

        if(hasChanged) {
            const requestBody = {
                user: {
                    "school_name": this.state.schoolName ? this.state.schoolName : null
                }
            }
            PUT('/api/v1/users/update_user', requestBody)
            .then(data => {
                this.props.onLoginUser(data.user)
            })
            .catch(error => {
                this.setState({
                    error: true,
                    errorMessage: error.error,
                    loading: false
                })
            });
        } else {
            console.log("No changes!")
        }
    }

    render() {
        const isMobile = this.props.appSize.isSmallComputer
        // renders image based on current state, or old user image
        return (
           
                <div className="DashboardSubPageMain">
                    
                    <ModalWrapper
                        open={this.state.modal === 'phoneNumber'}
                        onClose={this.closeModal}
                    >
                        <h3 style={{marginTop: 0, marginBottom: 0}}>Phone Number</h3>
                        {
                            !this.state.phoneError && !this.state.phoneTokenError &&
                            <p>Update your phone number. We’ll send you a text to verify your number.</p>
                        }
                        {
                            this.state.phoneError &&
                            <p style={{color: 'red'}}>{this.state.phoneErrorMessage}</p>  
                        }
                        {
                            this.state.phoneTokenError &&
                            <p style={{color: 'red'}}>{this.state.phoneErrorMessage}</p>  
                        }
                        
                        {
                            this.state.phoneNumberState === 0 && !this.state.phoneLoading? 
                                <PhoneInput
                                    containerClass="react-tel-input"
                                    country={'us'}
                                    preferredCountries={['us']}
                                    value={this.state.phoneNumber}
                                    onChange={phone =>this.handleUpdate('phoneNumber', phone)}
                                />
                            : null
                        }
                        {
                            this.state.phoneNumberState === 1 && !this.state.phoneLoading ? 
                                <CustomTextbox
                                    value={this.state.phoneToken}
                                    id="outlined-search"
                                    label='Verification Code'
                                    type="text"
                                    full
                                    onChange={(e)=>this.handleUpdate('phoneToken', e.target.value)}
                                /> 
                            : null
                        }
                        {
                            this.state.phoneLoading ? 
                                <div style={{height: 57}}>
                                    <CircleLoad/>
                                </div>
                                : null
                        }

                        {
                            this.state.phoneNumberState === 0 && 
                                <div style={{display:'flex', justifyContent:'space-between',marginTop: 20}}>
                                    <NLButton title='Cancel' onClick={this.closeModal} type="MediumButton Gray"/>
                                    <NLButton title='Send Code' onClick={this.localPhoneCheck} type="MediumButton SolidPurple"/>
                                </div>
                        }
                        {
                            this.state.phoneNumberState === 1 && 
                                <div style={{display:'flex', justifyContent:'space-between',marginTop: 20}}>
                                    <NLButton title='Back' onClick={()=>this.handleUpdate('phoneNumberState', 0)} type="MediumButton Gray"/>
                                    <NLButton title='Confirm' onClick={this.finalPhoneVerification} type="MediumButton SolidPurple"/>
                                </div>
                        }
                        
                    </ModalWrapper>
                    <ModalWrapper
                        open={this.state.modal === 'schoolName'}
                        onClose={this.closeModal}
                    >
                        {
                            !this.state.loading ? 
                                <div>
                                    <SchoolInfo updateSchoolName={this.handleSchoolNameUpdate}/>
                                    <NLButton title="Does not apply" onClick={this.handleNotStudent} type="InlineText Purple"/>
                                </div>
                            : 
                                <div style={{height: 75}}>
                                    <CircleLoad/>
                                </div>
                        }
                        <div style={{display:'flex', justifyContent:'space-between',marginTop: 20}}>
                            <NLButton title='cancel' onClick={this.closeModal} type="MediumButton Gray"/>
                            <NLButton title='Save' onClick={this.saveSchoolName} type="MediumButton SolidPurple"/>
                        </div>
                    </ModalWrapper>
                    {
                        this.props.user &&
                        <div className="DashboardSubPageWrapper">
                            <div className="DashboardSubPageHeader">
                                <div style={{display:"flex",flexDirection:'column'}}>
                                    <h1 className="SkinnyHeader" style={{marginBottom:0}}>Settings</h1>
                                
                                    <p className="SoftText">Modify your profile and preferences.</p>
                                </div>
                            
                            </div>
                        
                        
                            {/* image uploading */}
                            <ActiveStorageProvider
                                directUploadsPath={'/api/v1/direct_uploads'}
                                headers={{
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                                }}
                                endpoint={{
                                    path: `/api/v1/users/update_user`,
                                    model: 'User',
                                    attribute: 'image',
                                    method: 'PUT',
                                }}
                                onSubmit={this.onPhotoFinish}
                                render={({ handleUpload, uploads, ready }) => (
                                    <div>
                                        <input 
                                            style={{display:'none'}} 
                                            type='file' 
                                            disabled={!ready}
                                            onChange={(event)=>{
                                                const file = this.addImage(event); 
                                                if(file) handleUpload(file) 
                                            }}
                                            ref={fileInput => this.fileInput = fileInput}
                                            
                                            accept="image/png, image/jpeg"
                                        />
                                    </div>
                                )}
                            />

                        <div className="LightShadow" style={{display:'flex', flexDirection:'column', maxWidth: 400, 
                            backgroundColor: '#fff', borderRadius: 5, padding: 20}}>

                            <div style={{display:'flex', flexDirection:'column', marginBottom: 20}}>
                                
                                <div style={{display:'flex', alignItems:'center', marginBottom: 10}}>
                                    <AvatarWrapper user={this.props.user} size='large'/>
                                    <h3 style={{margin: 10}}>{this.props.user.first_name+' '+this.props.user.last_name}</h3>
                                </div>
                                {
                                    !this.state.uploading ?
                                        <div>
                                            <NLButton title={this.props.user.image ? 'Update Photo' : 'Add Photo'}
                                                onClick={()=>this.fileInput.click()} type='MediumButton Gray' /> 
                                        </div>
                                        : <CircleLoad/>
                                }
                                </div>
                                {
                                    !DISABLE_ID_VERIFICATION ?
                                    <div>
                                        <Divider/>
                                        <h3>Identity Verification</h3>
                                        <div style={{marginBottom: 20}}>
                                            <PassbaseWrapper/>
                                        </div>
                                    </div>
                                    : null
                                }
                                <Divider/>
                                <h3>Email</h3>
                                <p>{this.props.user.email}</p>
                                <Divider/>
                                <h3>Phone Number</h3>
                                {
                                    this.props.user.phone_number &&
                                    <p>{this.props.user.phone_number}</p>
                                }
                                
                                <div style={{marginBottom: 20}}>
                                    <NLButton title={this.props.user.phone_number ? 'Update Phone Number' : 'Add Phone Number'}
                                        onClick={()=>this.handleUpdate('modal','phoneNumber')} type='MediumButton Gray' />
                                </div>

                                <Divider/>
                                <h3>School Name</h3>
                                {
                                    this.props.user.school_name &&
                                        <p>{this.props.user.school_name}</p>
                                }
                                
                                <div style={{marginBottom: 20}}>
                                <NLButton title={this.props.user.school_name ? 'Update School' : 'Add School'}
                                    onClick={()=>this.handleUpdate('modal','schoolName')} type='MediumButton Gray' />   
                                </div>  
                                

                            </div>

                        </div>
                    }
                </div> 
        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}
const mapDispatchToProps = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(DashboardSettings)
