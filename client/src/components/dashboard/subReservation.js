import React, { Component } from 'react'

import { POST, GET } from '../../constants/requests.js'
import { PARSE_QUERY } from '../../constants/functions.js'
import { DISPLAY_NUMBER, SAVE_ROUTE} from '../../constants/functions.js'

import ErrorModal from '../utilities/errorModal.js'
import ModalWrapper from '../utilities/Modal.js'
import TypeForm from '../utilities/typeform.js'
import Divider from '@material-ui/core/Divider'
import StaysImage from '../../Assets/stay.png'
import format from "date-fns/format"
import parse from "date-fns/parse"

import ReservationHandler from './reservationHandler.js'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import DashboardReservation from './listItems/dashboardReservation.js'

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    alignItems: 'center',
    position: 'relative'
  }
})

class SubReservation extends Component {
  constructor () {
    super()
    this.state = {
      loading: false,
      error: false,
      errorMessage: '',
      modalData: {
        type: null,
        res: null
      },
      showClaim: false,
      claimId: null, 
      calculatedPayouts: null, 
      referalPayout: null
    }
  }

  componentDidMount () {
    window.scrollTo(0, 0)
  }

  // TODO: add claims modal popup, needs approved reservations checks, etc.
  // checkQueryForClaims = () => {
  //   const query = PARSE_QUERY(window.location.search)
  //   if ('claim' in query && query['claim']) {
  //       this.setState({ showClaim: true })
  //   }
  // }

  calculateSchedule = (id)=>{
    this.props.loading(true)
    GET(`/api/v1/reservations/${id}/compute_payouts`)
    .then(data => {
      const index = data.payouts.findIndex(p=> p.is_referral) 

      if(index !== -1){
        const refPayout = data.payouts[index]
        this.setState({
          referalPayout: refPayout
        })
        data.payouts.splice(index, 1)
      }
      
      this.setState({
        calculatedPayouts: data.payouts
      })
      this.props.loading(false)
    })
    .catch(error => {
      this.props.loading(false)
    })
  } 

  handleReservation = (res, type) => {
    if (type !== 'payment' && type !== null) {
      // Approving or declining the reservation
      this.props.loading(true)
      POST(`/api/v1/reservations/${res.id}/${type}`, {})
      .then(data => {
        this.setState({
          modalData: {
            type: null,
            res: null
          }
        })
        this.props.updated(type, data.reservation)
        this.props.loading(false)
      })
      .catch(error => {
        this.setState({
          error: true, 
          errorMessage: error.error
        })
        this.props.loading(false)
        this.closeDataModal()
      })
    } else if (type === 'payment') {
      SAVE_ROUTE(this.props.history.location.pathname)
      this.props.onPageChange('payouts', '/dashboard/payouts')
    }
  }
  
  checkAction = (res, type) => {
    if (type === 'approve' && this.props.user.merchant_id === null) {
      this.setState({
        modalData: {
          type: 'payment',
          res: null
        }
      })
    } else if (type === 'approve' || type === 'decline') {
      this.setState({
        modalData: {
          type: type,
          res: res
        }
      })
    } else if (type === 'message') {
      this.props.message(res.guest.id)
    } else if (type === 'claim') {
      this.setState({
        showClaim: true,
        claimId: res.id
      })
    } else if(type === 'cancel'){
      this.handleReservation(res, type)
    }
  }

  closeDataModal = () => {
    this.setState({
      modalData: {
        type: null,
        res: null
      }
    })
  }

  render () {
    const { classes } = this.props
    const listing = this.props.selectedListing
    
    return (
      <div className={classes.root}>
        <ErrorModal message={this.state.errorMessage} show={this.state.error} close={()=> this.setState({error:false})}/>
        <ModalWrapper
          open={this.state.modalData.type !== null}
          onClose={this.closeDataModal}
        >
          <ReservationHandler
            data={this.state.modalData}
            handleReservation={this.handleReservation}
            close={this.closeDataModal}
          />
        </ModalWrapper>

        <ModalWrapper
          open={this.state.calculatedPayouts ? true : false}
          onClose={() => this.setState({ calculatedPayouts: null})}
        >
          {
            this.state.calculatedPayouts &&
            <div>
              <h3>Payout Schedule</h3>
              {
                this.state.referalPayout && 
                <p>Referral code bonus of ${DISPLAY_NUMBER(this.state.referalPayout.payout_amount)} paid on {format((parse(this.state.referalPayout.due_by,'yyyy-MM-dd',new Date())), "MMM dd, yyyy")}</p>
              }
              <div style={{display:'flex', flexDirection:'column', marginTop: 20,paddingTop: 10, }}>
                    <div style={{display:'flex', justifyContent:'space-between', marginBottom: 10}}>
                        <p className="GrayText" style={{margin: 0, width: 150}}>Date</p>
                        <p className="GrayText" style={{margin: 0, width: 100, textAlign:'right'}}>Amount</p>
                        
                    </div>
                    <Divider/>
                    {
                        this.state.calculatedPayouts.map(p =>
                            <div style={{display:'flex', flexDirection:'column'}} key={p.payout_num}>
                                <div style={{display: 'flex',alignItems:'center',justifyContent:'space-between', 
                                    paddingTop: 10, paddingBottom: 10, height: 32}} >
                                    <p style={{margin: 0,width: 150}}>{format((parse(p.due_by,'yyyy-MM-dd',new Date())), "MMM dd, yyyy")}</p>
                                    <p style={{margin: 0,width: 100, textAlign:'right'}}>${DISPLAY_NUMBER(p.payout_amount)}</p>
                                </div>
                                <Divider/>
                            </div>
                        )
                    }
                </div>
            </div>
          }
        </ModalWrapper>
        <ModalWrapper
          open={this.state.showClaim}
          onClose={() => this.setState({ showClaim: false, claimId: null})}
          style={{padding: 0, maxWidth: 600}}
        >
          {
            this.props.user && listing ? 
              <TypeForm 
              formUrl={`https://thesummerstay.typeform.com/to/skocNO?host=true&user_id=${this.props.user.id}&listing_id=${listing.id}&reservation_id=${this.state.claimId}`}/>
            : null
          }
          

        </ModalWrapper>

        {
          listing.reservations.length === 0 && !this.state.loading ?
            <div className='NothingToShowGraphic' >
              <h2 className='SoftText ExtraSkinnyHeader'>You don't have any pending reservations yet</h2>
              <img src={StaysImage} height='auto' width='70%' alt='no listings' />
              <h1 className="SoftText ExtraSkinnyHeader">Tell your friends about your place.</h1>
            </div>
          : null
        }
        { 
          listing.reservations.map(r => 
            <DashboardReservation
              key={r.id}
              checkAction={this.checkAction}
              calculateSchedule={this.calculateSchedule}
              reservation={r}
            />
          ) 
          
        }
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user
  }
}

SubReservation.propTypes = {
  classes: PropTypes.object.isRequired
}
export default connect(mapStateToProps)(withStyles(styles)(SubReservation))
