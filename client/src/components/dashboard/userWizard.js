import React, { Component } from 'react'
import SchoolInfo from '../utilities/schoolInfo.js'
import PassbaseWrapper from '../utilities/passbaseWrapper.js'
import StepNavigationButtons from '../utilities/stepNavigationButtons.js'
import NLButton from '../utilities/NoLinkButton.js'
import CircleLoad from '../utilities/circleLoad.js'
import { PUT } from '../../constants/requests.js'
import { USER_TYPES, DISABLE_ID_VERIFICATION } from '../../constants/index.js'

import { connect } from 'react-redux'
import { loginUser } from '../../store/actions/passThrough.js'

class UserWizard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            stage:0, 
            schoolName: null, 
            schoolError: false,
            loading: false
        }
    }

    handleSchoolNameUpdate = (value) => {
        this.setState({ 
            schoolName: value, 
            schoolError: false
         })
    }

    handleStageUpdate=(type, newStage)=>{
        if((!this.state.schoolName || this.state.schoolName==='') && this.state.stage === 0){
            this.setState({ schoolError: true })
            return
        }
        if(type === 'next' && this.state.stage === 0){
            const send_data = {
                user: {
                    school_name: this.state.schoolName, 
                    user_type: USER_TYPES.student 
                }
            }
            this.updateUser(send_data, newStage)
        }else if(type === 'finish' && DISABLE_ID_VERIFICATION){
            const send_data = {
                user: {
                    school_name: this.state.schoolName, 
                    user_type: USER_TYPES.student 
                }
            }
            this.updateUser(send_data, newStage)
            this.props.close()
        }else if(type === 'finish'){
            this.props.close()
        }else{
            this.setState({stage: newStage})
        }
    }

    handleNotStudent=()=>{
        const send_data = {
            user: {
                school_name: null, 
                user_type: USER_TYPES.nonStudent
            }
        }
        this.handleSchoolNameUpdate(null)
        if(DISABLE_ID_VERIFICATION){
            const newStage = this.state.stage
            this.updateUser(send_data, newStage)
            this.props.close()
        }else{
            const newStage = this.state.stage+1
            this.updateUser(send_data, newStage)
        }
        
    }

    updateUser=(send_data,newStage)=>{
        if(!this.state.loading){
            this.setState({loading: true})
            PUT('/api/v1/users/update_user', send_data)
            .then(data => {
                this.props.onLoginUser(data.user)
                this.setState({
                    stage: newStage, 
                    loading: false
                })
            })
            .catch(error => {
                this.setState({
                    error: true,
                    errorMessage: error.error,
                    loading: false
                })
            })
        }
    }

  render() {
    const canMoveOn = (this.state.stage === 0 ) || (this.state.stage === 1) 
    return(
      <div style={{position:'relative',display: 'flex', flexDirection:'column'}}>
        {
            this.state.stage === 0 ? 
            <div>
                <h2 style={{marginTop: 0, marginBottom: 0}}>Welcome to SummerStay</h2>  
                <p>Please tell us where you go to school:</p>    
                {
                    !this.state.loading ? 
                        <div style={{marginTop:20, marginginBosttom: 20}}>
                            <SchoolInfo error={this.state.schoolError} updateSchoolName={this.handleSchoolNameUpdate}/>
                            <div style={{height: 10}}/>
                            <NLButton title="I'm not a student" onClick={this.handleNotStudent} type="InlineText Dark Purple"/>
                        </div>
                    : 
                        <div style={{height: 75}}>
                            <CircleLoad/>
                        </div>
                }
            </div>
            : null
        }
        {
            this.state.stage === 1 && !DISABLE_ID_VERIFICATION? 
            <div>
                <h2 style={{marginTop: 0, marginBottom: 0}}>Government ID Verification</h2>  
                <p>Please take five minutes to verify your identity. You may return to this step when creating a listing or submitting a booking request.</p>  
                <div style={{display:'flex', justifyContent:'center', height: 75}}>
                    <PassbaseWrapper/>
                </div>
            </div>
            : null
        }
        <div style={{display:'flex', alignSelf:'flex-end',marginTop: 20}}>
            <StepNavigationButtons canMoveOn={canMoveOn} onChange={this.handleStageUpdate} backDisabled={this.state.stage === 0}
                stage={this.state.stage} totalStages={!DISABLE_ID_VERIFICATION ? 2 : 1} finish disabled={this.state.loading}/>
        </div>
      </div>
    )
  }
} 

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
    }
}
const mapDispatchToProps  = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user)), 
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(UserWizard);
