import React, { Component } from 'react';
import { GET, POST } from '../../constants/requests.js'
import '../../CSS/App.css';
import '../../CSS/Dashboard.css'
import ListingsImage from '../../Assets/listing.png'

import NLButton from '../utilities/NoLinkButton.js'
import DashboardListing from './listItems/dashboardListing.js'
import CircleLoad from '../utilities/circleLoad.js'
import CreateListingBox from '../utilities/createListingBox'
import ErrorModal from '../utilities/errorModal.js'

import ModalWrapper from '../utilities/Modal.js'

import { connect } from 'react-redux'

class DashboardListings extends Component {
    constructor(props){
        super(props)
        this.state={
            openCreate: false,
            listings: [], 
            loading: false, 
            makingListing: false, 
            requestError: false, 
            requestErrorMessage: false, 
        }
    }

    componentDidMount=()=>{
        this.setState({loading: true})

        GET('/api/v1/my_listings')
        .then(data => {
            this.setState({
                listings: data.listings.sort(this.listSort), 
                loading: false
            })
          // do something with User #5
        })
        .catch(error => {
          this.setState({loading: false})
        });
        
    }
    listSort(a, b) {
        let resA = a.reservations.length
        let resB = b.reservations.length
        
        if (resA > resB) 
          return -1

        if (resA < resB) 
          return 1

        return 0;
    }

    createListing = (address, type, history) =>{
        this.setState({makingListing: true})
        const send_data = {
            listing: {
                listing_type: type,
                address: address.location,
                latitude: address.lat,
                longitude: address.lng,
                full_address: address.fullAddress,
            }   
        }
        POST('/api/v1/listings', send_data)
        .then(data => {
            history.push(`/listings/edit/${data.listing.id}`)
        })
        .catch(error => {
            const errorKeys = Object.keys(error.error)
            const firstErrorMessage = error.error[errorKeys[0]]
            this.setState({ makingListing: false, requestError: true, requestErrorMessage: firstErrorMessage, loading: false })
        });
        
    }
    toggleCreate=()=>{
        this.setState(prevState=>({openCreate: !prevState.openCreate}))
    }

    render() {

        const isMobile = this.props.appSize.isSmallComputer
        const isLargePhone = this.props.appSize.isLargePhone

        return (

            <div className="DashboardSubPageMain"> 
                <ErrorModal message={this.state.requestErrorMessage} show={this.state.requestError} close={()=> this.setState({requestError:false})}/>
                
                <ModalWrapper 
                    open={this.state.openCreate}
                    onClose={this.toggleCreate}
                >
                    <h1 style={{marginTop:0}}>List your place, get offers</h1>
                    <p style={{ margin:0}}>No financial information is required until you choose to accept a booking. Listing your place takes 10 minutes, start accepting requests today.</p>
                    <CreateListingBox loading={this.state.makingListing} createListing={this.createListing}/>
                </ModalWrapper>

                <div className="DashboardSubPageWrapper">
                    <div className="DashboardSubPageHeader">
                        <div className="DashboardHeaderText">
                            <h1 className="SkinnyHeader" style={{marginBottom: 0, marginRight: 5}}>Listings</h1>
                            { !isLargePhone ? <p className="SoftText">See your places here.</p> :<p className="SubTitleText"></p> }
                        </div>
                        <div style={{marginBottom: 15}}>
                            <NLButton title ={isLargePhone ? "Create": "Create Listing"} onClick={this.toggleCreate} type="MediumButton SolidPurple LightShadow"/>
                        </div>
                    </div>
                    
                    <div style={{display:"flex", flexDirection:'column', alignItems:'center' }}> 
                        {
                            this.state.listings.length===0 && this.state.loading ? 
                                <div style={{marginTop: 20}}>
                                   <CircleLoad/>   
                                </div> 
                                 
                             : null
                        }
                        {
                        this.state.listings.length>0 && !this.state.loading ?
                        this.state.listings.map(l => 
                            <DashboardListing key={l.id} listing={l} selectListing={this.props.selectListing}/>
                            ) 
                            : null
                        }
                        {
                        this.state.listings.length===0 && !this.state.loading ?
                            <div className='NothingToShowGraphic'>
                                
                                <img src={ListingsImage} height='auto' width='70%' alt="no listings"/>
                                <h1 className="SoftText ExtraSkinnyHeader">List your place to earn cash.</h1>
                            </div>
                            : null
                        }
                        
                    </div>
                </div>
            </div> 
          
        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}

export default connect(mapStateToProps)(DashboardListings)
