import React, { Component } from 'react'
import '../../CSS/App.css'
import { GET, POST } from '../../constants/requests.js'
import NotificationsImage from '../../Assets/payment.png'
import DashboardNotification from './listItems/dashboardNotification.js'
import CircleLoad from '../utilities/circleLoad.js'
import { connect } from 'react-redux'

class DashboardNotifications extends Component {  
    constructor(props){
        super(props)
        this.state={
            notifications:[],
            loading: false
        }
    }
    
    componentDidMount=()=>{
        this.setState({loading: true})

        GET('/api/v1/notifications')
        .then(data => {
            this.setState({
                notifications: data.notifications, // this needs to get changed 
                loading: false
            })
        })
        .catch(error => {
          this.setState({ loading: false })
          
        });

    }

    markAsRead=(id, url)=>{
 
        POST(`/api/v1/notifications/${id}/mark_as_read/`, {})
        .then(data => {
            this.props.onPageChange(url.split('/')[2], url)
        })
        .catch(error => {
            
        });
    }

    render() {
        const isMobile = this.props.appSize.isSmallComputer
        const isMediumPhone = this.props.appSize.isMediumPhone
        return (
                    <div className="DashboardSubPageMain">
                        <div className="DashboardSubPageWrapper">
                            <div className="DashboardSubPageHeader">
                                <div className='DashboardHeaderText'>
                                    
                                       
                                    <h1 className="SkinnyHeader"style={{marginBottom:0}}>Notifications </h1>
                                    
                                    { !isMediumPhone ? <p className="SoftText" > See what's new with your listings and potential requests.</p> : null }
                                </div>
                            </div>
                           
                            <div style={{display:"flex", flexDirection: 'column', alignItems:'center'}}> 
                                {
                                    this.state.notifications.length>0 && !this.state.loading?
                                        this.state.notifications.map(n => 
                                            <DashboardNotification key ={n.id} notification={n} markAsRead={this.markAsRead}/>
                                        ) : null
                                }
                                {
                                    this.state.notifications.length === 0 && !this.state.loading?
                                        <div className='NothingToShowGraphic'>
                                            <img src={NotificationsImage} height='auto' width='70%' alt="no notifications"/>
                                            <h1 className="SoftText ExtraSkinnyHeader">You have no notifications.</h1>
                                        </div>
                                        : null
                                }
                                {
                                    this.state.notifications.length===0 && this.state.loading?  
                                    <div style={{marginTop: 20}}>
                                        <CircleLoad/>   
                                    </div>   
                                    : null

                                }
                            </div>
                        </div>
                    </div> 

        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}

export default connect(mapStateToProps)(DashboardNotifications)

