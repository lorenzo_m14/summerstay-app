import React, { Component } from 'react'
import '../../CSS/App.css'
import '../../CSS/Dashboard.css'
import { CLIENT_URL, STRIPE_CLIENT } from '../../constants/index.js'
import { BUILD_QUERY, SAVE_ROUTE } from '../../constants/functions.js'
import { GET } from '../../constants/requests.js'
import Stripe from '../../Assets/powered_by_stripe.svg'
import { Link } from "react-router-dom"

import Button from '../utilities/Button.js'
import NLButton from '../utilities/NoLinkButton.js'
import DashPayouts from './listItems/dashboardPayouts.js'
import CircleLoad from '../utilities/circleLoad.js'
import SecurityImage from '../../Assets/payment.png'

import ErrorModal from '../utilities/errorModal.js'
import { connect } from 'react-redux'


class DashboardPayouts extends Component {  
    constructor(props){
        super(props)
        this.state={
            loading: false,
            error: false,
            errorMessage: '',
            paymentState: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
            updatePayment: false,
            paymentMethods: [], 
            payouts:[],
            method: null
        }
    }

    componentDidMount=()=>{
        
        
        if(this.props.user.merchant_id){
            this.setState({loading: true})
            GET('/api/v1/payouts')
            .then(data => {
                this.setState({
                    payouts: data.stays, // this needs to get changed 
                    loading: false
                })
            })
            .catch(error => {
            
            this.setState({ loading: false })
            });
        }

    }
// methods should handle stripe connect button press
    handleStripeConnect=()=>{
        if(this.props.user.merchant_id === null) {
            console.log("Create a new express account for user")
            this.createExpressConnectRoute()
        } else {
            // make request to retrieve connect express login url
            console.log("Open express dashboard for user")

            GET('/api/v1/payouts/connect_login')
            .then(data => {
                window.open(data.login_url);
            })
            .catch(error => {
                console.log(error)
                this.setState({
                    error: true, 
                    errorMessage: error.error
                })
            });   
        }
    }

    createExpressConnectRoute=()=>{
        // SAVE_ROUTE('/dashboard/payouts')
        localStorage.setItem('paymentState', JSON.stringify(this.state.paymentState))

        const { user } = this.props

        const expressBaseUrl = "https://connect.stripe.com/express/oauth/authorize"
        const redirectUrl = `${CLIENT_URL}/handle/payment`
        const queryParams = {
            "redirect_uri": redirectUrl,
            "client_id": STRIPE_CLIENT,
            "state": this.state.paymentState,
            "stripe_user[email]": user.email,
            "stripe_user[first_name]": user.first_name,
            "stripe_user[last_name]": user.last_name,
            "stripe_user[business_type]": "individual",
            // TODO: Flesh out the product description
            "stripe_user[product_description]": "Subletting with SummerStay"
        }

        window.location.href =`${expressBaseUrl}?${BUILD_QUERY(queryParams)}`


        // Old url construction (no escape chars)
        // let userUrl = CLIENT_URL + '/dashboard';
        // window.location.href =`https://connect.stripe.com/express/oauth/authorize?redirect_uri=${CLIENT_URL}/handle/payment&client_id=${STRIPE_CLIENT}&state=${this.state.paymentState}&stripe_user[email]=${this.props.user.email}&stripe_user[url]=${userUrl}&stripe_user[first_name]=${this.props.user.first_name}&stripe_user[last_name]=${this.props.user.last_name}`
    }

    
      getReceipt=(link)=>{
        console.log(link)
        window.open(link)
      }
      
    render() {
        const { appSize, user } = this.props
        const isLargePhone = appSize.isLargePhone
        const isSmallPhone = appSize.isSmallPhone
        const hasConnectedAccount = user && user.merchant_id
        const connectAccount = user && user.connect_account_status
        const enabled = connectAccount && connectAccount === 'Enabled'
        const due = connectAccount && connectAccount === 'Due'
        const pending = connectAccount && connectAccount === 'Pending'
        const verified = connectAccount && connectAccount === 'Verified'
        const pastDue = connectAccount && connectAccount === 'PastDue'
        const disabled = connectAccount && connectAccount === 'Disabled'
        return (
                <div className="DashboardSubPageMain">
                    <ErrorModal message={this.state.errorMessage} show={this.state.error} close={()=> this.setState({error:false})}/>

                    
                    <div className="DashboardSubPageWrapper">
                        <div className="DashboardSubPageHeader">
                            <div className="DashboardHeaderText">
                                <h1 className="SkinnyHeader" style={{marginBottom: 0, marginRight: 5}}>Payouts</h1>
                                {!isLargePhone ? <p className="SoftText">Manage your money.</p> : <p className="SubTitleText"></p>} 
                            </div>

                        </div>
                        {
                                this.props.user &&
                                    <div style={{background:'#fff', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1), 0 4px 8px 0 rgba(0, 0, 0, 0.1)',
                                        marginBottom: 15, padding: 20, borderRadius: 5, display:'flex', justifyContent:'space-between', flexWrap:'wrap'}}>
                                        <div style={{display:'flex', flexDirection:'column', maxWidth: isLargePhone ? null : '70%'}}>
                                            <div style={{display:'flex', flexDirection: isSmallPhone ? 'column': 'row'}}>
                                                <h2 className="SkinnyHeader" style={{marginBottom:5, marginTop:0}}>Host Account</h2>
                                                <div style={{marginLeft: isSmallPhone ? 0 : 10}}>
                                                    <a href='https://stripe.com/' target='_blank'>
                                                        <img src={Stripe} style={{height: 29}} alt='Powered by Stripe'/>
                                                    </a>
                                                </div>
                                            </div>
                                            {
                                                !hasConnectedAccount &&
                                                <div>
                                                    <p className="SoftBold" style={{marginBottom: 0}}>Become a Host with SummerStay by connecting your information.</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>This Stripe backed account allows hosts to receive payments effortlessly by connecting their bank account or debit card. </p>
                                                </div>

                                            }
                                            {
                                                hasConnectedAccount && connectAccount && enabled &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>In Good Standing</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>Stripe requires additional information at times to keep host accounts connected. Hosts should monitor their account status to ensure compliance.</p>
                                                </div>
                                            }
                                            {
                                                hasConnectedAccount && due &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>Information Required</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>Stripe requests additional information to keep your account in good standing. This is required to continue receiving payments.</p>
                                                </div>
                                            }
                                            {
                                                hasConnectedAccount && pending &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>Verifying your information</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>We are currently verifying the information in your host account. We will keep you updated on the status.</p>
                                                </div>
                                            }
                                            {
                                                hasConnectedAccount && verified &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>In Good Standing</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>Stripe requires additional information at times to keep host accounts connected. Hosts should monitor their account status to ensure compliance.</p>
                                                </div>
                                            }
                                            {
                                                hasConnectedAccount && pastDue &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>PAST DUE</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>You need to update your account information as soon as possible. You will stop receiving payouts if you do not update this information. If you have questions <Button link='/support' title='contact us' target="_blank" type="InlineButton Purple"/>.</p>
                                                </div>
                                            }
                                            {
                                                hasConnectedAccount && disabled &&
                                                <div>
                                                    <p style={{marginBottom: 10, color:'#008589'}}>DISABLED</p>
                                                    <p className="SoftText" style={{marginBottom: 0}}>Your host account has been disabled, and you will stop receiving payouts. Please update your information in your Stripe Connect account or <Button link='/support' title='contact us' target="_blank" type="InlineButton Purple"/> for help.</p>
                                                </div>
                                                
                                            }
                                            
                                        </div>
                                        <div style={{display:'flex', flexDirection:'column', flex: isLargePhone ? 1 : null, marginTop: isLargePhone ? 20: 0}}>
                                        <NLButton title={this.props.user.merchant_id == null ? 'Connect with Stripe': 'Stripe Dashboard'} 
                                            onClick={this.handleStripeConnect} type="MediumButton SolidPurple LightShadow"/>
                                        </div>
                                        {/* <NLButton title={this.props.user.merchant_id == null ? 'Stripe Setup': 'Stripe Dashboard'} 
                                            onClick={this.handleStripeConnect} type="MediumButton SolidPurple LightShadow"/> */}
                                     
                                    </div>
                            }
                        <div style={{display:"flex", flex: 1, flexDirection:'column',alignItems:'center'}}> 
                        
                            {
                                this.state.payouts.length===0 && this.state.loading?  
                                <div style={{marginTop: 20}}>
                                    <CircleLoad/>   
                                </div>  
                                : null
                            }
                            
                            {
                            this.state.payouts.length>0 && !this.state.loading?
                            this.state.payouts.map(s => 
                                <DashPayouts key={s.id} stay={s} getReceipt={this.getReceipt}/>
                            ) : null
                            }

                            {
                                this.state.payouts.length===0 && !this.state.loading ? 
                                <div className='NothingToShowGraphic'>
                                    <img src={SecurityImage} height='auto' width='70%' alt="no payouts"/>
                                    <h1 className="SoftText ExtraSkinnyHeader">You have no payouts.</h1>
                                </div>
                                : null
                            }
                        
                    </div>
                    </div>
                </div> 

        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}

export default connect(mapStateToProps)(DashboardPayouts)

