import React, { Component } from 'react'

import NLButton from '../utilities/NoLinkButton.js'
import { GET, POST } from '../../constants/requests.js'

import CustomLoading from '../utilities/loadingBar.js'

import Info from '../listing/info.js'
import SubReservation from './subReservation.js'
import SubChat from './subChat.js'

import {BrowserRouter as Router,Route,Link,Switch,Redirect} from 'react-router-dom'

import { connect } from 'react-redux'

class SubListing extends Component {
  constructor () {
    super()
    this.state = {
      loading: false,
      show: null,
      selectedChat: null,
      listing: null
    }
  }

  componentDidMount () {
    // make request to fetch selected listing, if not passed by props
    this.setPage()
    if (!this.props.selectedListing) {
      this.updateLoading(true)
      let { listing_id } = this.props.match.params
      this.getListing(listing_id)
    } else {
      const listing = { ...this.props.selectedListing }
      this.setState({
        listing: listing
      })
    }
    window.scrollTo(0, 0)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevState.show !== this.state.show) {
      this.setState({ selectedChat: null })
    }
  }

  // fetch own listing
  getListing = (listing_id) => {
    GET(`/api/v1/my_listings/${listing_id}`)
    .then(data => {
      let { listing } = data
      this.setState({
        listing: listing,
        loading: false
      })
    })
    .catch(error => {})
  }

  setPage () {
    let urlPaths = this.props.location.pathname.split('/')
    let path = urlPaths[4]
    this.setState({ show: path })
    window.scrollTo(0, 0)
  }
  // changePage method takes in a type (for UI display)
  // and takes in a route, to push onto history
  changePage = (type, route) => {
    this.setState({ show: type })
    if (route) {
      this.props.history.replace(route)
    }
  }

  message = guestID => {
    const send_data = {
      listing_id: this.state.listing.id,
      recipient_id: guestID
    }
    this.updateLoading(true)
    POST('/api/v1/conversations', send_data)
      .then(data => {
        this.setState({
          // selectedChat: data.conversation.id,
          show: 'chats'
        })
        this.props.history.replace({
          pathname: `/dashboard/listings/${this.state.listing.id}/chats`,
          state: {
            conversation_id: data.conversation.id
          }
        })
        this.updateLoading(false)
        // this.changePage('chats',`/dashboard/listings/${this.state.listing.id}/chats` )
      })
      .catch(error => {
        this.updateLoading(false)
      })
  }

  // we should update the listing's reservations to match server
  // approval could autodecline several other requests
  updateListing = (type, reservation) => {
    let { listing } = this.state
    if (type === 'approve') {
      this.getListing(listing.id)
    } else if (type === 'decline') {
      this.getListing(listing.id)
    }
  }

  updateLoading = val => {
    this.setState({ loading: val })
  }

  editListing = () => {
    this.props.history.push(`/listings/edit/${this.state.listing.id}`)
  }

  render () {
    const { listing } = this.state

    const requests = listing ? listing.reservations.filter(r => r.status === 'Waiting').length : 0
   
    const isLargePhone = this.props.appSize.isLargePhone
    const isMediumPhone = this.props.appSize.isMediumPhone
    return (
      <div className="SubListingRoot">
        <div style={{width: '100%'}}>
          <CustomLoading loading={this.state.loading} />
        </div>
        
        <div className="SubListingHeader">
          <div style={{ width: isLargePhone ? 20: 102, marginLeft: isMediumPhone? 5 : 10, marginRight: isMediumPhone? 5 : 10 }}>
            <NLButton title='' icon='bigBack' onClick={() => this.props.onPageChange('listings', '/dashboard/listings/')}
              type='BigBack'/>
          </div>
          <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center'}}>
            <NLButton title='View' onClick={() => this.changePage('info',`/dashboard/listings/${listing.id}/info`)}
              type={this.state.show === 'info' ? 'MediumButton SolidPurple SmallText' : 'MediumButton SolidClear SmallText'}/>
            <div style={{ height: 35, borderRight: '1px solid #e0e0e0' }} />
            <div style={{ position: 'relative' }}>
              {
                requests > 0 ? 
                  <div style={{ display: 'flex', justifyContent: 'center',alignItems: 'center', backgroundColor: '#FB3640',
                                height: 10, width: 10, borderRadius: 5, position: 'absolute', right: -5, top: -5 }}/>
                : null
              }
              <NLButton title={isMediumPhone ? 'Res' :'Reservations'}
                onClick={() => this.changePage( 'reservations',`/dashboard/listings/${listing.id}/reservations`)}
                type={ this.state.show === 'reservations' ? 'MediumButton SolidPurple SmallText' : 'MediumButton SolidClear SmallText'}
              />
            </div>

            <div style={{ height: 35, borderRight: '1px solid #e0e0e0' }} />
            <NLButton title='Chats' onClick={() => this.changePage('chats',`/dashboard/listings/${listing.id}/chats`)}
              type={this.state.show === 'chats' ? 'MediumButton SolidPurple SmallText' : 'MediumButton SolidClear SmallText'}/>
          </div>
          {
            !isMediumPhone ?
              <NLButton title={isLargePhone ? 'Edit' : 'Edit Listing'} onClick={this.editListing} type={'MediumButton MedMargin SmallText'}/>
            : null
          }
          

          {/* <div style={{height:40, width:50}}/> */}
        </div>
        
        <div className="SubListingMain">
          {/* ensure the state has a listing before displaying nested routes */}
          {
            listing ? 
              <Switch>
                <Route path={`/dashboard/listings/:listing_id/info`}
                  render={props => <Info {...props} listing={listing} specific={true}/>}
                />
                <Route
                  path={`/dashboard/listings/:listing_id/reservations`}
                  render={props => <SubReservation {...props} selectedListing={listing} message={this.message}
                        updated={this.updateListing}onPageChange={this.props.onPageChange} loading={this.updateLoading}/>
                  }
                />
                <Route
                  path={`/dashboard/listings/:listing_id/chats`}
                  render={props => <SubChat {...props} selectedListing={listing} loading={this.updateLoading} />}
                />
                <Redirect exact from={'*'} to={`/dashboard/listings/${listing.id}/info`}/>
              </Switch>
            : null
          }
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user, 
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(SubListing)
