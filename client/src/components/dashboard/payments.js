import React, { Component } from 'react'
import '../../CSS/App.css'
import '../../CSS/Dashboard.css'
import {  STRIPE_API_KEY } from '../../constants/index.js'
import { GET } from '../../constants/requests.js'

import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from '../payment/checkoutForm.js';

import NLButton from '../utilities/NoLinkButton.js'
import DashboardCharges from './listItems/dashboardCharges.js'
import CircleLoad from '../utilities/circleLoad.js'
import PaymentImage from '../../Assets/process.png'

import ErrorModal from '../utilities/errorModal.js'
import { connect } from 'react-redux'

import ModalWrapper from '../utilities/Modal.js'


class DashboardPayments extends Component {  
    constructor(props){
        super(props)
        this.state={
            loading: false,
            error: false,
            errorMessage: '',
            stays: [],
            updatePayment: false,
            paymentMethods: [], 
            method: null,
        }
    }

    componentDidMount=()=>{
        this.setState({loading: true})
        
        GET('/api/v1/stays/charges')
        .then(data => {
            this.setState({
                stays: data.stays, // this needs to get changed 
                loading: false
            })
        })
        .catch(error => {
          
          this.setState({ loading: false })
        });

        if(this.props.user.stripe_id){
            GET('/api/v1/payment_methods')
            .then(data => {
                this.setState({
                    paymentMethods: data.payment_methods, 
                    method: data.payment_methods.length>0 ? data.payment_methods[0].id : null
                })
            })
            .catch(error => {
                
            });
        }
    }

    errorHandler=(message)=>{
        this.setState({
          error: true, 
          errorMessage: message
        })
      }

    togglePayment=()=>{
        this.setState(prevState=>({updatePayment: !prevState.updatePayment}))
    }
    
    handleState=(type, val)=>{
        this.setState({
          [type]: val, 
          verified: false
        })
      }
      getReceipt=(link)=>{
        window.open(link)
      }
    render() {
        const isLargePhone = this.props.appSize.isLargePhone

        return (
                <div className="DashboardSubPageMain">
                    <ErrorModal message={this.state.errorMessage} show={this.state.error} close={()=> this.setState({error:false})}/>
                    <ModalWrapper
                        open={this.state.updatePayment}
                        onClose={this.togglePayment}
                        style={{padding: 0, maxWidth:510}}
                        disableEnforceFocus
                    >
                        <StripeProvider apiKey={`${STRIPE_API_KEY}`}>
                                <Elements>
                                    <CheckoutForm update error={this.errorHandler}/>
                                </Elements>
                        </StripeProvider>
                    </ModalWrapper>
                    
                    <div className="DashboardSubPageWrapper">
                        <div className="DashboardSubPageHeader">
                            <div className="DashboardHeaderText">
                                <h1 className="SkinnyHeader" style={{marginBottom: 0, marginRight: 5}}>Payments</h1>
                                {!isLargePhone ? <p className="SoftText">Review your transactions.</p> : <p className="SubTitleText"></p>} 
                            </div>
                            {
                                this.props.user &&
                                    <div style={{marginBottom: 15}}>
                                        <NLButton title ={isLargePhone ? "Update": "Update Payment Method"} onClick={this.togglePayment} type="MediumButton SolidPurple LightShadow"/>
                                    </div>
                            }
                            
                        </div>
                        
                        {/* <div style={{display:"flex", flexDirection:'column',alignItems:'space-between'}}> 
                            
                        {
                        // TODO:    Should check to see if user has merchant id.
                        //          If user has no merchant_id, redirect to Express account creation
                        //          else let the onClick action be request to '/payouts/connect_login'

                            this.props.user ? 
                            <Paper style={{ display:'flex',justifyContent: 'space-between',flexWrap:'wrap',padding: 10, 
                                            marginTop: 20,marginBottom: 20,minHeight: 150}}>
      
                                <div style={{display:'flex', flex: 1,flexDirection:'column', justifyContent:'space-between',borderRadius: 5, 
                                        border: '1px solid #e0e0e0', minWidth:200,
                                        backgroundImage:`url(${SecureSmall})`,backgroundSize: 'auto 100%', backgroundRepeat:'no-repeat	',
                                        backgroundPosition:'right top', padding: 20, paddingBottom: 20, margin: 10}}>
                                    <div>
                                        <h2 style={{marginTop: 0}}>Payment Method</h2>
                                        <p className="GrayText" >Update your payment information</p>
                                    </div>
                                    
                                    <NLButton title="Update Credit Card"  onClick={this.togglePayment} type="MediumButton"/>
                                
                                </div>
                                
                                
                            </Paper>
                            
                            :null
                        }
                        </div> */}

                        <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}> 
                            {
                                this.state.stays.length===0 && this.state.loading?  
                                <div style={{marginTop: 20}}>
                                    <CircleLoad/>   
                                </div>  
                                : null
                            }
                            {
                            this.state.stays.length>0 && !this.state.loading?
                            this.state.stays.map(s => 
                                <DashboardCharges key={s.id} stay={s} getReceipt={this.getReceipt}/>
                                ): null
                            }


                            {
                                this.state.stays.length===0 && !this.state.loading ? 
                                <div className='NothingToShowGraphic'>
                                    <img src={PaymentImage} height='auto' width='70%' alt="no payments"/>
                                    <h1 className="SoftText ExtraSkinnyHeader">You have no payments to make.</h1>
                                </div>
                                : null
                            }
                        
                        </div>
                    </div>
                </div> 

        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}

export default connect(mapStateToProps)(DashboardPayments)

