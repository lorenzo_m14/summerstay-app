import React, { Component } from 'react'
import '../../CSS/App.css'
import '../../CSS/Dashboard.css'

import { GET } from '../../constants/requests.js'

import StaysImage from '../../Assets/stay.png'

import Button from '../utilities/Button.js'
import DashboardStay from './listItems/dashboardStay.js'
import CircleLoad from '../utilities/circleLoad.js'

import { connect } from 'react-redux'

class DashboardStays extends Component {
  constructor (props) {
    super(props)
    this.state = {
      stays: [],
      loading: false, 
    }
  }

  componentDidMount = () => {
    this.setState({ loading: true })
    GET('/api/v1/stays')
    .then(data => {
      this.setState({
        stays: data.stays, // this needs to get changed
        loading: false
      })
    })
    .catch(error => {
      console.log(error)
    })
  }

  render () {

    const isMobile = this.props.appSize.isSmallComputer
    const isLargePhone = this.props.appSize.isLargePhone

    return (
      <div className="DashboardSubPageMain">
        <div className="DashboardSubPageWrapper">
          <div className="DashboardSubPageHeader">
            <div className="DashboardHeaderText">
              <h1 className="SkinnyHeader" style={{marginBottom: 0, marginRight: 5}}>Stays</h1>
              { !isLargePhone ? <p className="SoftText">The places you're staying.</p> :<p className="SubTitleText"></p> }
            </div>
            <div style={{ marginBottom: 15 }}>
              <Button title={isLargePhone ? 'Find a Stay' : 'Find A SummerStay'} link='/listings' type='MediumButton SolidPurple LightShadow'/>
            </div>
          </div>
        
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            {
              this.state.stays.length === 0 && this.state.loading ? 
                <div style={{ marginTop: 20 }}>
                  <CircleLoad />
                </div>
              : null
            }
            {
              this.state.stays.length > 0 && !this.state.loading ? 
                this.state.stays.map(s => 
                  <DashboardStay key={s.id} stay={s} selectStay={this.props.selectStay} />
                )
                : null
            }
            {
              this.state.stays.length === 0 && !this.state.loading ? 
                <div className='NothingToShowGraphic'>
                  <img src={StaysImage} height='auto' width='70%' alt='no stays' />
                  <h1 className="SoftText ExtraSkinnyHeader">Search to find your SummerStay.</h1>
                </div>
              : null
            }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.housingApp.user,
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(DashboardStays)
