import React, { Component } from 'react';

import Button from '../utilities/Button.js'
import NLButton from '../utilities/NoLinkButton.js'
import { CustomCheckbox } from '../../constants/inputs.js'

import '../../CSS/Dashboard.css'

class ReservationHandler extends Component {
    constructor(props){
        super(props);
        this.state = {
            approveCheckbox: false, 
            approveError: false, 
        };
    }

    handleDecision=()=>{
        let error = false
        if(this.props.data.type === 'approve'){
            error = !this.state.approveCheckbox
            this.setState({
                approveError: error
            })
        }

        if(!error){
            this.props.handleReservation(this.props.data.res, this.props.data.type)
        }    
    }

    handleCheckbox=(event)=>{
        if(event.target.checked){
            this.setState({approveError: false})
        }
        this.setState({
            approveCheckbox: event.target.checked
        })
    }

  render() {
        const { data } = this.props
    return (
        <div>
            {
                data.type === 'approve' ?
                    <div style={{display:'flex', flex:1,flexDirection:'column',justifyContent:'space-between'}}>

                        <div style={{display:'flex', flexDirection:'column', alignItems:'center', marginBottom: this.state.approveError ? 0: 22}}>
                            <h2 style={{margin: 0}}>Approve Booking Request</h2>
                            {
                                this.state.approveError &&
                                <p style={{color:'#e62929', margin: 0}}>Make sure to agree to our Terms and Conditions.</p>
                            }
                        </div>

                        <CustomCheckbox
                            checked={this.state.approveCheckbox}
                            onChange={this.handleCheckbox}
                            value='terms'
                        >
                            <p className="SoftBold" style={{margin: 0}}>I agree to SummerStay's <Button title="Terms and Conditions" link="/terms" target="_blank" type="InlineText"/>.</p>
                        </CustomCheckbox>
                        
                        <p></p>
                        <div style={{display:'flex', justifyContent:'center'}}>
                            {/* <div style={{display:'flex', flexDirection:'column',width:150}}>
                                <NLButton title ='Close' 
                                    onClick={props.close} type="MediumButton Gray"/> 
                            </div> */}
                            <div>
                                <NLButton title ='Approve Request' 
                                    onClick={this.handleDecision} type="MediumButton SolidPurple"/> 
                            </div>
                        </div>
                    </div>
                : null
            }
            {
                data.type === 'decline' ?
                <div style={{display:'flex', flex:1,flexDirection:'column',justifyContent:'space-between'}}>
                    <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                        <h2 style={{marginTop: 0}}>Decline Reservation</h2>
                        <p className="GrayText" style={{textAlign:'center'}}>Are you sure? This action is irreversible.</p>
                    </div>
                    <div style={{display:'flex', justifyContent:'space-between', marginTop: 20}}>
                        <div style={{display:'flex', flexDirection:'column',width:150}}>
                            <NLButton title ='No, Go Back' 
                                onClick={this.props.close} type="MediumButton Gray"/> 
                        </div>
                        <div style={{display:'flex',flexDirection:'column',width:150}}>
                            <NLButton title ="Decline"
                                onClick={this.handleDecision} type="MediumButton Gray"/> 
                        </div>
                    </div>
                </div>
            : null
            }
            {
                data.type === 'payment' ?
                <div style={{display:'flex', flex:1,flexDirection:'column',justifyContent:'space-between'}}>
                    <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                        <h2 style={{marginTop: 0}}>Setup Payment</h2>
                        <p className="GrayText" style={{textAlign:'center', marginLeft: 10, marginRight:10}}>We don’t know where to send your payouts. Please create your Stripe account before you accept a reservation.</p>
                    </div>
                    <div style={{display:'flex', justifyContent:'space-between'}}>
                        <div style={{display:'flex', flexDirection:'column',width:150}}>
                            <NLButton title ='Close' 
                                onClick={this.props.close} type="MediumButton Gray"/> 
                        </div>
                        <div style={{display:'flex',flexDirection:'column'}}>
                            <NLButton title ="Setup Stripe account"
                                onClick={this.handleDecision} type="MediumButton"/> 
                        </div>
                    </div>
                </div>
                : null
            }
        </div>
    ); 
  }
}

export default ReservationHandler;