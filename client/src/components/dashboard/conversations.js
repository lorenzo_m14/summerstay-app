import React, { Component } from 'react';
import '../../CSS/App.css'
import '../../CSS/Dashboard.css'

import SubChat from './subChat.js'
import { connect } from 'react-redux'


class DashboardConversations extends Component {  
   
    render() {
        const isMediumPhone = this.props.appSize.isMediumPhone
        return (
            <div className="DashboardSubPageMain">
                <div className="DashboardSubPageWrapper">
                    <div className="DashboardSubPageHeader">
                        <div className='DashboardHeaderText'>
                            <h1 className="SkinnyHeader" style={{marginBottom:0}}>Chat</h1>
                            { !isMediumPhone ? <p className="SoftText">Confirm the details of your SummerStay.</p> : null }
                            
                        </div>
                    </div>
                    
                    <SubChat/>
                    
                </div>
            </div> 

        ); 
    }
}

const mapStateToProps = state => {
    return {
        user: state.housingApp.user,
        appSize: state.housingApp.appSize
    }
}

export default connect(mapStateToProps)(DashboardConversations)

