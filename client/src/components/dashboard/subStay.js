import React, { Component } from 'react'

import NLButton from '../utilities/NoLinkButton.js'
import { GET, POST } from '../../constants/requests.js'

import StayInfo from '../stay/stayInfo.js'
import CustomLoading from '../utilities/loadingBar.js'
import StayChat from '../stay/stayChat.js'

import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'


class SubStay extends Component {
  constructor () {
    super()
    this.state = {
      loading: false,
      show: null,
      selectedChat: null,
      stay: null,
      listing: null,
      showClaim: false,
      error: false
    }
  }

  componentDidMount () {
    // make request to fetch selected listing, if not passed by props
    this.setPage()
    if (!this.props.selectedStay) {
      this.updateLoading(true)
      let { stay_id } = this.props.match.params
      GET(`/api/v1/reservations/${stay_id}`)
        .then(data => {
          this.setState({
            stay: data.reservation,
            listing: data.reservation.listing,
            loading: false
          })
        })
        .catch(error => {
          this.setState({ loading: false })
        })
      // TODO: get listing info --> might be able to get from above
    } else {
      const stay = { ...this.props.selectedStay }
      const listing = { ...this.props.selectedStay.listing }
      this.setState({
        stay: stay,
        listing: listing
      })
    }
    window.scrollTo(0, 0)
  }

  setPage () {
    let urlPaths = this.props.location.pathname.split('/')
    let path = urlPaths[4]
    this.setState({ show: path })
    window.scrollTo(0, 0)
  }

  // changePage method takes in a type (for UI display)
  // and takes in a route, to push onto history
  changePage = (type, route) => {
    this.setState({ show: type })
    if (route) {
      this.props.history.replace(route)
    }
  }

  message = guestID => {
    const send_data = {
      listing_id: this.state.listing.id,
      recipient_id: guestID
    }
    this.updateLoading(true)
    POST('/api/v1/conversations', send_data)
      .then(data => {
        this.setState({
          // selectedChat: data.conversation.id,
          show: 'chats'
        })
        this.props.history.replace({
          pathname: `/dashboard/stays/${this.state.stay.id}/chats`,
          state: {
            conversation_id: data.conversation.id
          }
        })
        this.updateLoading(false)
        // this.changePage('chats',`/dashboard/listings/${this.state.listing.id}/chats` )
      })
      .catch(error => {
        this.updateLoading(false)
      })
  }

  updateLoading = val => {
    this.setState({ loading: val })
  }

  render () {
    const { stay, show, listing } = this.state
    const isLargePhone = this.props.appSize.isLargePhone
    const isMediumPhone = this.props.appSize.isMediumPhone
    const isSmallPhone = this.props.appSize.isSmallPhone
    return (
      <div className="SubListingRoot">
        <div style={{width: '100%'}}>
          <CustomLoading loading={this.state.loading} />
        </div>
        <div className="SubListingHeader">
          <div style={{ width: 50, marginLeft: 10, marginRight: 10 }}>
            <NLButton title='' icon='bigBack' onClick={() => this.changePage('stays', '/dashboard/stays/')} type='BigBack' />
          </div>
          <div style={{display: 'flex',justifyContent: 'center',alignItems: 'center'}}>
            <NLButton title='View'
              onClick={() => this.changePage('info', `/dashboard/stays/${stay.id}/info`)}
              type={show === 'info' ? 'MediumButton SolidPurple SmallText' : 'MediumButton SolidClear SmallText'}
            />
            <div style={{ height: 35, borderRight: '1px solid #e0e0e0' }} />
            <NLButton title='Chat' 
              onClick={() =>this.changePage('chats',`/dashboard/stays/${stay.id}/chats`)}
              type={show === 'chats' ? 'MediumButton SolidPurple SmallText' : 'MediumButton SolidClear SmallText'}
            />
          </div>
          {
            !isSmallPhone &&
              <div style={{ height: 40, width: 70 }}></div>
          }
          
        </div>
        <div className="SubListingMain">
          {/* ensure the state has a stay before displaying nested routes */}
          {
            stay ? 

              <Switch>
                <Route
                  path={`/dashboard/stays/:stay_id/info`}
                  render={props => <StayInfo stay={stay} changePage={this.props.changePage} />}
                />
                <Route
                  path={`/dashboard/stays/:stay_id/chats`}
                  render={props => <StayChat stay={stay} selectedListing={listing} />}
                />
                <Redirect
                  exact
                  from={'*'}
                  to={`/dashboard/stays/${stay.id}/info`}
                />
              </Switch>
            : null
          }
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user, 
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(SubStay)
