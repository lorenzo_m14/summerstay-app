import React, { Component } from 'react'
import '../../CSS/App.css'
import { GET } from '../../constants/requests.js'

import MessageBox from '../messageBox.js'
import CircleLoad from '../utilities/circleLoad.js'
import ListingsImage from '../../Assets/listing.png'
import PaymentImage from '../../Assets/payment.png'

import Paper from '@material-ui/core/Paper'

import { connect } from 'react-redux'
import AvatarWrapper from '../utilities/avatar.js'

import format from 'date-fns/format'
import parseISO from 'date-fns/parseISO'

import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const styles = theme => ({
  chatMain: {
    display: 'flex',
    flexDirection: 'row',
    margin: 20
  },
  chat: {
    display:'flex',
    cursor: 'pointer',
    margin: 10,
    marginTop: 0,
    padding: 10
  }
})

class SubChat extends Component {
  constructor (props) {
    super(props)
    this.state = {
      conversations: [],
      selectedChatID: null,
      loading: false
    }
  }

  componentDidMount = () => {
    window.scrollTo(0, 0)
    this.setState({ loading: true })
    let route = '/api/v1/conversations'

    if (this.props.selectedListing) {
      route = `/api/v1/conversations?listing_id=${
        this.props.selectedListing.id
      }`
    }
    GET(route)
      .then(data => {
        this.setState({
          conversations: data.conversations.sort(this.messageSort),
          selectedChatID:
            data.conversations.length > 0 ? data.conversations[0].id : null,
          loading: false
        })
        if (this.props.selectedChat || this.props.history.location.state) {
          let id = this.props.selectedChat
            ? this.props.selectedChat
            : this.props.history.location.state.conversation_id
          this.setState({ selectedChatID: id })
        }
      })
      .catch(error => {
        this.setState({
          loading: false
        })
      })
  }

  messageSort (a, b) {
    let MesA = a.last_message ? parseISO(a.last_message.updated_at) : null
    let MesB = b.last_message ? parseISO(b.last_message.updated_at) : null
    if (!MesA && !MesB) return 0
    if (!MesA) return 1
    if (!MesB) return -1

    if (MesA - MesB > 0) return -1

    if (MesA - MesB < 0) return 1

    return 0
  }

  selectChat = id => {
    this.setState({ selectedChatID: id })
  }

  render () {
    const { classes, appSize } = this.props
    const isMobile = appSize.isSmallComputer
    const isLargePhone = appSize.isLargePhone
    const numChar = isMobile ? 15 : 25
    const listWidth = isLargePhone ? '100%' : (isMobile ? 200 : 300) 
    const isListingSubChat = this.props.hasOwnProperty('location') ? this.props.location.pathname.includes('/dashboard/listings/') : false

    return (
      <div>
        { 
          this.state.conversations.length === 0 && !this.state.loading ? 
          <div style={{display: 'flex', flex: 1, justifyContent: 'center'}}>
            <div className='NothingToShowGraphic'>
              <img src={PaymentImage} height='auto' width='70%' alt="no messages"/>
              <h1 className="SoftText ExtraSkinnyHeader">{ isListingSubChat ? 'You have no chats.' : 'Request to book and chat with hosts.'}</h1>
            </div>
          </div>
          : null
        }
        {
          this.state.conversations.length === 0 && this.state.loading && 
            <CircleLoad />
        }
        {
          
          this.state.conversations.length > 0 && !this.state.loading && 
            <div style={{display: 'flex', flexDirection: isLargePhone ? 'column' : 'row', 
                  marginTop: 10,backgroundColor: 'transparent'}}>
              <div style={{display: isLargePhone ? 'flex' : null, width: listWidth, overflowX: 'auto',
                    marginRight: isLargePhone ? 0 : 10, maxHeight: isLargePhone ? 200 : 500, marginTop: isLargePhone ? 0 : 60, 
                    marginBottom: isLargePhone ? 20 : 0 }}>
                {
                  this.state.conversations.length > 0 && !this.state.loading ? 
                    this.state.conversations.map(c => (
                      <Paper key={c.id} className={classes.chat} onClick={() => this.selectChat(c.id)}
                        elevation={this.state.selectedChatID ===c.id ? 0: 1}
                        style={{ height: !isLargePhone ? 68 : 'auto', boxShadow: this.state.selectedChatID ===c.id ? '0 2px 4px 0 rgba(0, 0, 0, 0.15), 0 4px 8px 0 rgba(0, 0, 0, 0.15)' : null}}>
                          <div>
                            <AvatarWrapper user={c.user} size='medium'/>
                          </div>
                          <div style={{ display: 'flex', flex: 1, flexDirection:'column', marginLeft: 10}}>
                            <div style={{ display: 'flex', justifyContent:'space-between'}}>
                              <h3 style={{ margin: 5, marginBottom: 0 }}>{c.user.first_name}</h3>
                              {
                                !isLargePhone && 
                                  <p className="GrayText ExtraSmallText" style={{ margin: 5}}>
                                  { c.last_message && format(parseISO(c.last_message.created_at), 'MM/dd/yy') }
                                  </p>
                              }
                            </div>
                            {
                              c.last_message && !isLargePhone? 
                                <p className="GrayText" style={{wordWrap: 'break-word',margin: 5, marginTop: 10, overflow: 'hidden' }}>
                                  {c.last_message.context}
                                </p>
                            : null
                            }
                          </div>
                      </Paper>
                    ))
                  : null
                  
                }
              </div>
              {
                this.state.conversations.length > 0 ?
                  <MessageBox
                    enableChat={this.state.selectedChatID !== null}
                    selectedConversation={this.state.selectedChatID}
                    isLargePhone={isLargePhone}
                  />
                : null
              }
              
            </div>
        }
      </div>
    )
  }
}
SubChat.propTypes = {
  classes: PropTypes.object.isRequired
}
const mapStateToProps = state => {
  return {
    user: state.housingApp.user,
    appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(withStyles(styles)(SubChat))
