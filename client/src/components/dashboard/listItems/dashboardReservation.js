import React, { Component } from 'react';
import Button from '../../utilities/Button.js'
import NLButton from '../../utilities/NoLinkButton.js'
import { DISPLAY_NUMBER } from '../../../constants/functions.js'

import moment from 'moment'
import AvatarWrapper from '../../utilities/avatar.js'
import CancelButton from '../../utilities/cancelButton.js'

import '../../../CSS/Dashboard.css'

export default function DashboardReservation(props){
    
    const { reservation } = props
    const { start_date, end_date, expires_at, status, request_message} = reservation
    
    const start = moment(start_date)
    const end = moment(end_date)
    const start_to_now = start.diff(moment(), 'days')
    const exp = moment(expires_at).diff(moment(), 'days')
    const total_days = end.diff(start, 'days') + 1 // want to include start date
    const total_weeks = Math.ceil(total_days/7.0)

    const approved = status==='Approved'
    const waiting = status==='Waiting'
    return (
        <div className="DashReservation">
            <p style={{color: approved ? '#008589':'#e62929', margin: 0}}>{approved ? 'APPROVED RESERVATION' : 'PENDING REQUEST' }</p>
            <div style={{display:'flex', alignItems: 'center', justifyContent:'space-between', marginTop: 10, marginBottom: 10}}>
                <div style={{display:'flex', alignItems:'center'}}>
                    <AvatarWrapper user={reservation.guest} size='medium'/>
                    <h3 className="SkinnyHeader" style={{margin:10}}>
                        {reservation.guest.first_name +" "+ reservation.guest.last_name}
                    </h3>
                </div>
                
                <div style={{display:'flex', flexDirection:'column', justifyContent:'center',alignItems:'flex-end'}}>
                    <p className="SubTitleText" style={{fontSize: 24, margin:0,}}>
                    ${ DISPLAY_NUMBER(reservation.reservation_cost.destination_amount) }
                    </p>
                    {
                        waiting && <NLButton title='Payout Details' onClick={()=>props.calculateSchedule(reservation.id)} type="SimpleButton Purple NoMargin"/> 
                    }
                    {
                        approved && <Button link='/dashboard/payouts' title='Payout Details'  type="SimpleButton Purple NoMargin"/> 
                    }
                </div>
                
            </div>
            
            <p style={{margin: 0}}>
            {`${start.format("MMM DD")}`} - {`${end.format("MMM DD")}`} &middot; {total_weeks +' weeks'}
            </p>
            {
                waiting && request_message &&
                <p className="GrayText" style={{marginBottom: 0}}>
                    { 
                        request_message.length > 170 ? 
                        request_message.substring(0,170) +'...':
                        request_message
                    }
                </p> 
            }
            
                
            <div style={{marginBottom: 9}}>
                <NLButton title={`Message Renter`} 
                    onClick={()=>props.checkAction(reservation,'message')} type="SimpleButton Purple"/>
            </div> 
            {
                waiting ? 
                <div style={{display:'flex'}}>
                    
                    <NLButton title='Approve Request' 
                        onClick={()=>props.checkAction(reservation,'approve')} type="MediumButton SolidPurple"/>  
                
                
                    <NLButton title='Decline' 
                        onClick={()=>props.checkAction(reservation,'decline')} type="MediumButton Gray MedMargin"/>
                    
                </div>
                : null
            }
            {
                approved ? 
                <div style={{display:'flex'}}>
                    {
                        start_to_now <= 0 ?
                            <NLButton title='Submit Claim' 
                                onClick={()=>props.checkAction(reservation,'claim')} type="MediumButton Gray"/>
                         : null
                    }
                    {
                        start_to_now >= 7 ?
                            <CancelButton cancel={()=>props.checkAction(reservation,'cancel')} title="Cancel Reservation" />
                        : null
                    }
                    
                    
                </div>
                : null
            }
            
            {
                waiting ?
                <p className="GrayText" style={{marginTop:15, marginBottom: 0}}>This reservation will be automatically declined in {exp > 1 ?`${exp} days.` : `${moment(expires_at).diff(moment(), 'hours')} hours.`} </p> 
                : null
            }
                
            
                    
              

                
            
            {/* <div style={{ display: 'flex', alignItems:'center',height: 40, backgroundColor: reservation.status==='Approved' ? '#50c878': '#6c4ef5aa', 
                            borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <h3 style={{color: '#fff',margin:10}}>Status: {reservation.status}</h3>
            </div> */}

        </div>
    ); 
  }


