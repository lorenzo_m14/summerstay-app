import React from 'react';

import NoPhoto from '../../../Assets/noPhoto.png'
import Button from '../../utilities/Button.js'

import LocationIcon from '@material-ui/icons/RoomOutlined'
import StatusIcon from '@material-ui/icons/DonutLargeOutlined'
import TypeIcon from '@material-ui/icons/MeetingRoomOutlined'
import PriceIcon from '@material-ui/icons/AttachMoney'
import { DISPLAY_NUMBER } from '../../../constants/functions.js'
import { connect } from 'react-redux';

function DashboardListing(props){

    const { listing, appSize } = props
    const { isSmallPhone, isLargePhone, isSmallComputer, isSize1100 } = appSize

    const len = listing.images ? listing.images.length > 0 : false
    const requests =listing.reservations.filter(r=>r.status === 'Waiting').length
    const fontSize = !isSmallComputer ? 'inherit' : 14
    const iconSize = !isSmallComputer ? 35 : 25

    const status = listing.active ? 'active' : (!listing.active && listing.ready ? 'in-active' : 'incomplete')
    const statusColor = status === 'active' ? '#008589' : '#e62929'
    const statusText = status === 'active' ? 'Active' : (status === 'in-active' ? 'Inactive' : 'Incomplete')

    return (
        <div onClick={()=>props.selectListing(listing)} className="DashListing" style={{flexDirection: isSize1100 ? 'column': 'row'}}>
          {
            requests >0 ? 
                <div style={{display:'flex',justifyContent:'center', alignItems:'center', backgroundColor:'#e62929', 
                    height: 20, minWidth: 20, borderRadius: 11, position:'absolute', right:-10, top:-10}}>
                  <p className="ExtraSmallText" style={{color: '#fff', position:'relative', margin:0}}>{requests}</p>
                </div>
                : null
          }

          {/* <div style={{backgroundImage: len ? `url(${listing.images[0].url})` : `url(${NoPhoto})`, backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',backgroundPosition: 'center', overflow:'hidden',
                        height: 200, width:300,borderRadius: 5, borderTopRightRadius:0, borderBottomRightRadius:0}}>                
          </div> */}
          <img src ={len ? listing.images[0].url : NoPhoto } style={{objectFit: 'cover',height:200, width: isSize1100 ? '100%' : 300, borderRadius:5}}  alt="Listing"/>
          <div style={{display:'flex', flex:1, flexDirection:'column', justifyContent:'space-between', minWidth: 0}}>
            <div style={{display:'flex', margin: 20, marginBottom:5, justifyContent:'space-between'}}>
              <div style={{display:'flex', flexDirection:'column', minWidth: 0}}>
                <h2 className="SkinnyHeader OverFlowEllipsisWrap" style={{ margin: 0 }}>{listing.listing_name !== null ? listing.listing_name: "No Name Yet"}</h2>
                
                <div style={{display:'flex'}}>
                  <LocationIcon className="SubTitleText GrayText"/>
                  <p className="SubTitleText GrayText OverFlowEllipsisWrap" style={{ margin: 0 }}>{listing.address}</p>
                </div>
                
                {/* {
                  !isLargePhone &&
                    <div style={{display:'flex'}}>
                      <LocationIcon className="SubTitleText GrayText"/>
                      <p className="SubTitleText GrayText" style={{ margin: 0, textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{listing.address}</p>
                    </div>
                } */}
                
              </div>
              <div>
                <Button onClick={(e)=>e.stopPropagation()} icon='edit' 
                        link={`/listings/edit/${listing.id}`}  type="CircleButton MediumShadow"/>  
              </div>
              {/* <div style={{ display: 'flex', position:'absolute', top: 10, right: 10, alignItems:'center',height: 40, 
                            backgroundColor: listing.active ? '#41d0ad': '#6c4ef5', 
                        borderRadius: 5}}>

                <h3 style={{color: '#fff',margin:10}}>{listing.active ? 'Active' : `Unfinished`}</h3>
              </div> */}

            </div>
            
            <div style={{display:'flex', flex:1, margin: 20, marginTop: 10}}>
              {/* <p className="SubTitleText" style={{ margin:0}}>{bookings}{bookings !==1 ? ' Bookings': ' Booking'} &middot; {requests}{requests !==1 ? ' Requests': ' Request'}</p> */}
              <div style={{display:'flex', alignItems:'center', marginRight: isSmallPhone ? 10 : '15%'}}>
                <TypeIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                <div>
                  <p className="SubTitleText DarkText" style={{fontSize:fontSize, margin:0}}>Type</p>
                  <p className="SubTitleText LightBold" style={{fontSize:fontSize, margin:0}}>{listing.listing_type}</p>
                </div>
              </div>
              <div style={{display:'flex', alignItems:'center',marginRight: isSmallPhone ? 10 : '15%'}}>
                <StatusIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                <div>
                  <p className="SubTitleText DarkText" style={{ fontSize:fontSize, margin:0}}>Status</p>
                  <p className="SubTitleText LightBold" style={{fontSize:fontSize, margin:0, color: statusColor }}>{statusText}</p>
                </div>
              </div>
              {
                !isLargePhone &&
                  <div style={{display:'flex', alignItems:'center'}}>
                    <PriceIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                    <div>
                      <p className="SubTitleText DarkText" style={{fontSize:fontSize, margin:0}}>Weekly Rent</p>
                      <p className="SubTitleText LightBold" style={{fontSize:fontSize, margin:0}}>{DISPLAY_NUMBER(listing.price)}</p>
                    </div>
                  </div>
              }
              
              
            </div>
            {/* <div style={{ display: 'flex', alignItems:'center',height: 40, backgroundColor: listing.active ? '#50c878': '#6c4ef5aa', borderBottomRightRadius:5}}>
                <h3 style={{color: '#fff',margin:10}}>Status: {listing.active ? "Posted" : 'Unfinished'}</h3>
            </div> */}
          </div>
        </div>
    ); 
  }


const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(DashboardListing)