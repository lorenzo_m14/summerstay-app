import React from 'react';
import format from "date-fns/format"
import parseISO from "date-fns/parseISO"

export default function DashboardNotification(props){

    const notif = props.notification
    return (
        <div onClick={()=>props.markAsRead(notif.id, notif.notification_url)} className="DashNotif">  
{/* style={{display:'flex', flex:1,borderBottom: '1px solid #e0e0e0'}} */}
          {/* TODO: In future, we probably want to construct urls on client side (ie hyperlinks in text, bolding, etc) */}
          {/* using href because notification passes back absolute url */}
          {/* <Link to={notif.notification_url}> */}

            <div style={{display:'flex', alignItems: 'center'}}>
              
              <div style={{height: 10, minWidth: 10, borderRadius: 5, backgroundColor: !notif.read ? '#6c4ef5': 'trasparent', margin: 15}}/>
              <div style={{display:'flex', flexDirection:'column', marginLeft: 10, marginTop:5}}>
                <p style={{margin:0}}>
                  {notif.content}
                </p>
                <p className="GrayText ExtraSmallText">{`${format(parseISO(notif.created_at), "EEEE, MMMM do")}`}</p>
              </div>

            </div>
          {/* </Link> */}
          </div>
    ); 
  }

