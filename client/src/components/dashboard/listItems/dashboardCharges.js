import React from 'react';
import { DISPLAY_NUMBER } from '../../../constants/functions.js'
import Divider from '@material-ui/core/Divider';
import moment from 'moment'
import NLButton from '../../utilities/NoLinkButton.js'

export default function DashboardCharge(props){

    const stay = props.stay
    const { start_date, end_date } = stay
    const start = moment(start_date)
    const end = moment(end_date)
    const total_days = end.diff(start, 'days') + 1 // want to include start date
    const total_weeks = Math.ceil(total_days/7.0)

    const paymentType = stay.charges[0].charge_type
    const charges = stay.charges
    return (
        <div className="DashCharge" >
            <div style={{display:'flex', flexDirection:'column',flex: 1,overflowX:'auto'}}>
                <div style={{display:'flex', justifyContent:'space-between', alignItems: 'center',minWidth:400}}>
                    <div style={{display:'flex', flexDirection:'column', maxWidth:'70%', minWidth:200}}>
                        <h2 className="SkinnyHeader" style={{marginBottom:5, marginTop:0, wordBreak: 'break-word'}}>{stay.listing.listing_name}</h2>
                        <p style={{margin: 0}}>
                            {`${start.format("MMM DD")}`} - {`${end.format("MMM DD, YYYY")}`} &middot; {total_weeks +' weeks'}
                        </p>                    
                    </div>
                    <div style={{display:'flex'}}>
                        <div style={{display:'flex', flexDirection:'column', alignItems:'center',marginRight: 20}}>
                            <p className="GrayText" style={{marginBottom:5, marginTop:5}}>Payment Plan</p>
                            <p style={{marginBottom: 10}}>{paymentType === 0 ? "Standard" : "Pay-As-You-Stay"}</p>
                        </div>
                        <div style={{display:'flex', flexDirection:'column',alignItems:'center',}}>
                            <p className="GrayText" style={{marginBottom:5, marginTop:5}}>Total Cost</p>
                            <p style={{marginBottom: 10}}>${DISPLAY_NUMBER(stay.reservation_cost.total_cost)}</p>
                        </div>
                    </div>
                </div>
                
                
                <div style={{display:'flex', flexDirection:'column', marginTop: 20,paddingTop: 10,minWidth:400}}>
                    <div style={{display:'flex', justifyContent:'space-between', marginBottom: 10,}}>
                        <p className="GrayText" style={{margin: 0, width: 150}}>Due Date</p>
                        <p className="GrayText" style={{margin: 0, width: 80, marginRight: 10,textAlign:'right'}}>Amount</p>
                        <p className="GrayText" style={{margin: 0, width: 80}}>Status</p>
                        <p className="GrayText" style={{margin: 0, width: 100, textAlign:'right'}}>Download</p>
                        
                    </div>
                    <Divider/>
                    {
                        charges.map( (c,i) =>
                            <div style={{display:'flex', flexDirection:'column'}} key={c.id}>
                                <div style={{display: 'flex',alignItems:'center',justifyContent:'space-between', 
                                paddingTop: 10, paddingBottom: 10, height: 32}} >
                                    <p style={{margin: 0,width: 150}}>{ i === 0  ? 'Host Acceptance' : `${moment(c.due_by).format("MMM DD, YYYY")}`}</p>
                                    {/* <div style={{flex: 1, marginBottom: 10,borderBottom:'1px dotted black', alignSelf:'flex-end'}}/> */}
                                    <p style={{margin: 0,width: 80, textAlign:'right',  marginRight: 10}}>{"$" + DISPLAY_NUMBER(c.total_cost)}</p>
                                    
                                    <p style={{margin: 0,width: 80}}>{c.status === 'Unpaid' ? 'Upcoming' : c.status}</p>
                                    {
                                        // display receipt (Paid) or pay invoice (Unpaid Standard Payment) or nothing (Unpaid Subscription)
                                        c.receipt_url ? 
                                        <NLButton title='Receipt' onClick={()=>props.getReceipt(c.receipt_url)} type="MiniMediumButton"/> : 
                                        (c.invoice_url ?
                                            <NLButton title='Pay Invoice' onClick={()=>props.getReceipt(c.invoice_url)} type="MiniMediumButton"/> : 
                                            <div style={{width: 100}}/>
                                        )
                                    }
                                    
                                    
                                </div>
                                <Divider/>
                            </div>
                        )
                    }
                </div>
                
            </div>
            {/* <div style={{display:'flex', flexDirection:'column', width:170, marginLeft: 20, marginTop:10}}>
                 {
                    paymentType === 0 && stay.status !== "Waiting" ? 
                        <div>
                            <NLButton title="Finish Payment" onClick={()=>this.props.pay()} type="MediumButton"/>
                            <div style={{height: 20}}/>
                        </div>
                    : null
                } 
                 <NLButton title="Update Credit Card"  onClick={()=>props.updateCard()} type="MediumButton"/>
                <div style={{height: 20}}/> 
                <NLButton title ="Get Receipt"  onClick={()=>props.receipt(stay)} type="MediumButton"/>
            </div> */}
        </div>
    ); 
  }


