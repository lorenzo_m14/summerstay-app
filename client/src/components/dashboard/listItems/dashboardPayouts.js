import React from 'react';
import { DISPLAY_NUMBER } from '../../../constants/functions.js'
import Divider from '@material-ui/core/Divider';
import moment from 'moment'

export default function DashboardPayout(props){

    const stay = props.stay
    const { start_date, end_date } = stay
    const start = moment(start_date)
    const end = moment(end_date)
    const total_days = end.diff(start, 'days') + 1 // want to include start date
    const total_weeks = Math.ceil(total_days/7.0)

    const payouts = stay.payouts
    const total_payout = stay.payouts.reduce((total, payout)=>total+payout.payout_amount, 0)

    return (
        <div className="DashCharge">
            <div style={{display:'flex', flexDirection:'column',flex: 1,overflowX:'auto'}}>
                <div style={{display:'flex', justifyContent:'space-between', alignItems: 'center',minWidth:400}}>
                    <div style={{display:'flex', flexDirection:'column', maxWidth:'70%', minWidth:200}}>
                        <h2 className="SkinnyHeader" style={{marginBottom:5, marginTop:0, wordBreak: 'break-word'}}>{stay.listing.listing_name}</h2>
                        <p style={{margin: 0}}>
                            {`${start.format("MMM DD")}`} - {`${end.format("MMM DD, YYYY")}`} &middot; {total_weeks +' weeks'}
                        </p>
                    </div>
                    <div style={{display:'flex'}}>
                        <div style={{display:'flex', flexDirection:'column',alignItems:'center',}}>
                            <p className="GrayText" style={{marginBottom:5, marginTop:5}}>Total Payout</p>
                            <p style={{marginBottom: 10}}>${DISPLAY_NUMBER(total_payout)}</p>
                        </div>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'column', marginTop: 20,paddingTop: 10,minWidth:400}}>
                    <div style={{display:'flex', justifyContent:'space-between', marginBottom: 10,}}>
                        <p className="GrayText" style={{margin: 0, width: 150}}>Date</p>
                        <p className="GrayText" style={{margin: 0, width: 80, marginRight: 10, textAlign:'right'}}>Amount</p>
                        <p className="GrayText" style={{margin: 0, width: 80}}>Status</p>
                        
                    </div>
                    <Divider/>
                    {
                        payouts.map(p =>
                            <div style={{display:'flex', flexDirection:'column'}} key={p.id}>
                                <div style={{display: 'flex',alignItems:'center',justifyContent:'space-between', 
                                    paddingTop: 10, paddingBottom: 10, height: 32}} >
                                    <p style={{margin: 0,width: 150}}>{moment(p.due_by).format("MMM DD, YYYY")}</p>
                                    <p style={{margin: 0,width: 100, textAlign:'right',marginRight: 10}}>{"$" + DISPLAY_NUMBER(p.payout_amount)}</p>
                                    <p style={{margin: 0,width: 80}}>{p.status}</p>

                                </div>
                                <Divider/>
                            </div>
                        )
                    }
                </div>
                
            </div>
        </div>
    ); 
  }


