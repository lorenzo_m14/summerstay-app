import React, { Component } from 'react'
import format from 'date-fns/format'
import parse from 'date-fns/parse'

import LocationIcon from '@material-ui/icons/RoomOutlined'
import StatusIcon from '@material-ui/icons/DonutLargeOutlined'
import TypeIcon from '@material-ui/icons/MeetingRoomOutlined'
import PriceIcon from '@material-ui/icons/AttachMoney'
import { DISPLAY_NUMBER } from '../../../constants/functions.js'

import { Route, Redirect } from 'react-router-dom'
import NoPhoto from '../../../Assets/noPhoto.png'
import Home from '@material-ui/icons/Home'
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays'
import { connect } from 'react-redux';
import Dashboard from '../../../pages/Dashboard.js'

function DashboardStay (props) {
  const { stay, appSize } = props
  const { isLargePhone, isSmallComputer, isSize1100 } = appSize

  const listing = stay.listing
  const len = listing.images ? listing.images.length > 0 : false
  const startDate = parse(stay.start_date, 'yyyy-MM-dd', new Date())
  const endDate = parse(stay.end_date, 'yyyy-MM-dd', new Date())
  const weeks = Math.ceil(differenceInCalendarDays(endDate, startDate) / 7)
  const fontSize = !isSmallComputer ? 'inherit' : 14
  const iconSize = !isSmallComputer ? 35 : 25

  const statusColor = stay.status === 'Approved' ? '#008589' : '#e62929'

  return (
      <div onClick={()=>props.selectStay(stay)} className="DashListing" style={{flexDirection: isSize1100 ? 'column': 'row'}}>

          <img src ={len ? listing.images[0].url : NoPhoto } style={{objectFit: 'cover',height:200, width: isSize1100 ? '100%' : 300, borderRadius:5}}  alt="Listing"/>
          <div style={{display:'flex', flex:1, flexDirection:'column', justifyContent:'space-between', minWidth: 0}}>
            <div style={{display:'flex', margin: 20, marginBottom:5, justifyContent:'space-between'}}>
              <div style={{display:'flex', flexDirection:'column', minWidth: 0}}>
                <h2 className="SkinnyHeader OverFlowEllipsisWrap" style={{margin: 0 }}>{listing.listing_name !== null ? listing.listing_name: "No Name Yet"}</h2>
            
                <div style={{display:'flex'}}>
                  <LocationIcon className="SubTitleText GrayText"/>
                  <p className="SubTitleText GrayText OverFlowEllipsisWrap" style={{ margin: 0 }}>{stay.stay_info.address} { stay.stay_info.apt_number ? `Apt. ${stay.stay_info.apt_number}`: ''}</p>
                </div>
                
                {/* {
                  !isLargePhone && stay.status === 'Approved' &&
                    <div style={{display:'flex'}}>
                      <LocationIcon className="SubTitleText GrayText"/>
                      <p className="SubTitleText GrayText" style={{ margin: 0, textOverflow: 'ellipsis', whiteSpace: 'nowrap', overflow: 'hidden'}}>{stay.stay_info.address} { stay.stay_info.apt_number ? `Apt. ${stay.stay_info.apt_number}`: ''}</p>
                    </div>
                } */}
                
              </div>
            </div>
            
            <div style={{display:'flex', flex:1, margin: 20, marginTop: 10}}>
              {/* <p className="SubTitleText" style={{ margin:0}}>{bookings}{bookings !==1 ? ' Bookings': ' Booking'} &middot; {requests}{requests !==1 ? ' Requests': ' Request'}</p> */}
              <div style={{display:'flex', alignItems:'center', marginRight: '15%'}}>
                <TypeIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                <div>
                  <p className="SubTitleText DarkText" style={{fontSize:fontSize, margin:0}}>Type</p>
                  <p className="SubTitleText LightBold" style={{fontSize:fontSize, margin:0}}>{listing.listing_type}</p>
                </div>
              </div>
              <div style={{display:'flex', alignItems:'center',marginRight: '15%'}}>
                <StatusIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                <div>
                  <p className="SubTitleText DarkText" style={{ fontSize:fontSize, margin:0}}>Status</p>
                  <p className="SubTitleText LightBold" style={{fontSize:fontSize, fontWeight: 450, margin:0, color: statusColor}}>
                    {
                      stay.status === 'Approved'
                        ? stay.status
                        : `${stay.status} for approval`
                    }
                  </p>
                </div>
              </div>
              {
                !isLargePhone &&
                  <div style={{display:'flex', alignItems:'center'}}>
                    <PriceIcon className="SubTitleText DarkText" style={{fontSize: iconSize, marginRight: 10}}/>
                    <div>
                      <p className="SubTitleText DarkText" style={{fontSize:fontSize, margin:0}}>Price</p>
                      <p className="SubTitleText LightBold" style={{fontSize:fontSize, margin:0}}>{DISPLAY_NUMBER(stay.reservation_cost.total_cost)}</p>
                    </div>
                  </div>
              }
              
              
            </div>
          </div>
        </div>

  )
}

const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(DashboardStay)