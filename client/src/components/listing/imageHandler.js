import React, { Component, Fragment } from 'react';

import Carousel, { Modal, ModalGateway } from 'react-images';


export default class ImageHandler extends Component{
  state = {
    selectedIndex: 0,
    lightboxIsOpen: false,
  };
  toggleLightbox = (selectedIndex) => {
    this.setState(state => ({
      lightboxIsOpen: !state.lightboxIsOpen,
      selectedIndex,
    }));
  };
  render() {
    const { images, isLoading } = this.props;
    const { selectedIndex, lightboxIsOpen } = this.state;

    return (
      <Fragment>
        {
          !isLoading ? 
            <Gallery>
              {
                images ? 
                  images.map(({ caption, source }, j) => 
                    <Image onClick={() => this.toggleLightbox(j)} key={j}>
                      <img
                        alt={caption}
                        src={source}
                        className="ImageHandler"
                      />
                    </Image>
                  )
                : <p className="GrayText" style={{margin:0}}>This SummerStay has no photos.</p>
              }
            </Gallery>
           : null
        }
        
        <ModalGateway>
          {
            lightboxIsOpen && !isLoading ? 
              <Modal onClose={this.toggleLightbox} style={{zIndex: 20}}>
                <Carousel
                  // components={{ FooterCaption }}
                  currentIndex={selectedIndex}
                  // formatters={{ getAltText }}
                  // frameProps={{ autoSize: 'height' }}
                  views={images}
                />
              </Modal>
            : null
          }
        </ModalGateway>
      </Fragment>
    );
  }
}

const gutter = 2;

const Gallery = (props) => (
  <div
    style={{
      overflow: 'hidden',
      marginLeft: -gutter,
      marginRight: -gutter,
    }}
    {...props}
  />
);

const Image = (props) => (
  <div
    style={{
      backgroundColor: '#eee',
      boxSizing: 'border-box',
      float: 'left',
      margin: gutter,
      overflow: 'hidden',
      height: '20vh',
      maxHeight: 200,
      position: 'relative',
      width: `calc(33.3% - ${gutter * 2}px)`,
      
    }}
    {...props}
  />
);