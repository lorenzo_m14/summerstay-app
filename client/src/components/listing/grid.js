import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
}));


export default function ImageGridList(props) {
    const classes = useStyles();
    
    const numRows = Math.ceil(props.tileData.map(t=>t.cols).reduce((total, val)=>total+val)/props.col)
    const totalHeight = (props.cellHeight+5)*numRows
  return (
    <div className={classes.root}>
      <GridList style={{width: '100%',height: totalHeight}} cellHeight={props.cellHeight} cols={props.col}>
        {props.tileData.map(tile => (
          <GridListTile key={tile.title} cols={tile.cols}>
            <img src={tile.img} alt={tile.title} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}