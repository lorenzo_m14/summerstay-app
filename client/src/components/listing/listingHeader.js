import React from 'react'

function ListingSpecifics(props){
    const { listing } = props
    return(
        <p className="SoftText"style={{margin: 0}}>
            {listing.bedroom !== 1 ? listing.bedroom + ' bedrooms': listing.bedroom + ' bedroom'} &middot; {listing.beds !== 1 ? listing.beds + ' beds' : listing.beds + ' bed'} &middot; {listing.bathroom !== 1 ? listing.bathroom + ' baths' : listing.bathroom + ' bath'}
        </p>
    )
}

function listingName(listing){
    return listing.listing_name ? listing.listing_name : 'No Name Yet'
}

export default function ListingHeader(props) {
  
  const { isOverflowWrap, listing, size} = props
  const allData = listing.bedroom && listing.beds && listing.bathroom && listing.listing_type
  return (
    <div>
        {
            allData && size === 'large' && 
            <div>
                <p className="PurpleText"style={{margin: 0 }}>{listing.listing_type.toUpperCase()}</p>
                <h1 className={isOverflowWrap ? 'OverFlowEllipsisWrap' : 'OverFlowEllipsis'} style={{marginTop: 0,marginBottom: 10}}>
                    {listingName(listing)}
                </h1>
                <ListingSpecifics listing={listing}/>
            </div>
        }
        {
            allData && size === 'medium' && 
            <div>
                <p className="PurpleText"style={{margin: 0 }}>{listing.listing_type.toUpperCase()}</p>
                <h2 className={isOverflowWrap ? 'SkinnyHeader OverFlowEllipsisWrap' : 'SkinnyHeader OverFlowEllipsis'} style={{ margin:0 }}>
                    {listingName(listing)}
                </h2> 
                <ListingSpecifics listing={listing}/>
            </div>
        }
    </div>
  )
}


