import React, { Component } from 'react';
import { connect } from 'react-redux'

import AC from '../../Assets/amenities/A_CIcon.svg'
import Furnished from '../../Assets/amenities/furnishedIcon.svg'
import Gym from '../../Assets/amenities/gymIcon.svg'
import Heat from '../../Assets/amenities/heatingIcon.svg'
import Wifi from '../../Assets/amenities/wifiIcon.svg'
import Kitchen from '../../Assets/amenities/kitchenIcon.svg'
import Laundry from '../../Assets/amenities/laundryIcon.svg'
import Car from '../../Assets/amenities/parkingIcon.svg'
import Pool from '../../Assets/amenities/poolIcon.svg'
import TV from '../../Assets/amenities/tvIcon.svg'
import Pets from '../../Assets/amenities/petFriendlyIcon.svg'

import { AMENITIES } from '../../constants/index.js' 

const map = {
  air: AC,
  furnished: Furnished,
  gym: Gym,
  heating: Heat,
  internet: Wifi,
  kitchen: Kitchen,
  laundry: Laundry,
  parking: Car,
  pool: Pool,
  tv: TV,
  pet: Pets
}
class IconList extends Component {
  constructor() {
    super()
    this.state={ }
  }

  render() {
    const { appSize } = this.props
    const noAmenities = this.props.listing.amenities.includes('none')
    const Amenities = Object.entries(AMENITIES)

    const { isSize500, isMediumPhone, isLargePhone, isSmallComputer, isSize1200, isSize1300 } = appSize

    let iconWidth = isSize500 ? '50%' : (isMediumPhone ? '33%' : ( isSmallComputer ? '25%' : (isSize1200 ? '50%' : (isSize1300 ? '33%' : '25%'))))

    console.log(isSize500, isMediumPhone, isLargePhone, isSmallComputer, isSize1300)
    console.log('icon', iconWidth)

    return (
      <div style={{display:'flex', flexWrap:'wrap', justifyContent: 'flex-start', marginBottom:10}}>
  
        {
          noAmenities ? 
            <p className="GrayText" style={{margin: 0}}>This SummerStay has no amenities.</p>
          : 
          Amenities.map( a => {
            if(this.props.listing.amenities.includes(a[0]) && a[0] !== 'none'){
              const Comp = map[a[0]]
                return  (
                    <div key={a[1]} style={{display:'flex', flexDirection:'row', alignItems:'center', marginTop: 20, width: iconWidth}}>
                      <img src={Comp} height='auto' width='40px' alt='amenities' />
                      <p style={{ margin:0, marginLeft: 3 }}>&nbsp;{a[1]}</p>
                    </div>
                )
            }
            return null
              
          })
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
      appSize: state.housingApp.appSize
  }
}
export default connect(mapStateToProps)(IconList)