import React, { Component } from 'react'
import '../../CSS/App.css'

import IconList from './iconList.js'

import ImageHandler from './imageHandler.js'
import Divider from '@material-ui/core/Divider'
import StayDuration from '../stay/stayDuration.js'

import GeneralMap from '../map/generalMap.js'
import ListingHeader from './listingHeader.js'

const spacingMargin = 30
const subMargin = 10
class Info extends Component {
  render () {
    const { listing, isMobile, specific, stay } = this.props

    let image_data =
      listing && listing.images
        ? listing.images.map(image => ({
          source: image.url,
          caption: 'SummerStay Image'
        }))
        : null
    return (
      <div style={{ display: 'flex', flexDirection: 'column', flex: 1, width: '100%', marginBottom: 20 }}>
        <div style={{marginTop: spacingMargin, marginBottom: 20}}>
          <ListingHeader size="large" listing={listing} isOverflowWrap={true} />
        </div>
        <div className="SoftBold"style={{display:'flex', justifyContent:'space-between', flexWrap:'wrap'}}>
          {
            stay && stay.start_date && stay.end_date &&
            <div>
              <StayDuration stay={stay} />
            </div>
          }
        </div>
        <Divider />
        <h2 className="GrayBold SkinnyHeader" style={{marginTop: spacingMargin}}>Description</h2>
        <div style={{ display: 'flex', flexDirection: 'column', marginBottom: (spacingMargin-subMargin) }}>
          {
            listing.summary ? 
              listing.summary.split('\n').map((p, i) => 
                <p className="SoftText"key={i} style={{ margin: 0, marginBottom: subMargin, overflowWrap: 'break-word' }}>{p}</p>
              )
            : 
            <p className="SoftText" style={{ margin: 0, marginBottom: subMargin }}>
              This SummerStay has no description.
            </p>  
          }
        </div>
        <Divider />
        <h2 className="GrayBold SkinnyHeader" style={{marginTop: spacingMargin}}> Amenities</h2>
        <IconList listing={listing} />
        <div style={{height:subMargin}}/>
        <Divider />
        <h2 className="GrayBold SkinnyHeader" style={{marginTop: spacingMargin}}>Location</h2>
        <div style={{ width: '100%', height: isMobile ? '30vh' : '45vh', maxHeight: '450px', marginBottom: subMargin }}>
          <GeneralMap specific={specific}loc={{ lat: listing.latitude, lng: listing.longitude }} />
        </div>
        { !specific ? <p className='GrayText ExtraSmallText' style={{ marginTop: 0, marginBottom: spacingMargin }} >Exact location provided upon successful booking.</p> : null}
        
        {/* <GridList tileData={image_data || placeholderImages} col={2} cellHeight={250}/> */}
        {
          !stay &&
            <div>
              <Divider />
              <h2 className="GrayBold SkinnyHeader" style={{marginTop: spacingMargin}}>Photos</h2>
              <ImageHandler isLoading={false} images={image_data} />
            </div>
        }
        
      </div>
    )
  }
}

export default Info
