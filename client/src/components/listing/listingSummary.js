import React, { Component } from 'react'

class ListingSummary extends Component {
  render () {
    const listing = this.props.listing
    return (
      <div style={{ display: 'flex', flexDirection: 'column', marginTop: 20, marginBottom: 20 }}>
        {
          listing.summary ? 
            listing.summary.split('\n').map((p, i) => (
              <p key={i} style={{ margin: 0, marginBottom: 0 }}>
                {p}
              </p>
            ))
          : 
            <div style={{ display: 'flex', alignSelf: 'center', margin: 10 }}>
              <p className="GrayText" style={{margin: 0 }}>
                This SummerStay has no summary.
              </p>
            </div>
        }
      </div>
    )
  }
}

export default ListingSummary
