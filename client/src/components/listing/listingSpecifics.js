import React from 'react'
import Home from '@material-ui/icons/Home'

export default function ListingSpecifics(props) {
  
  const { listing } = props
  return (
    <div style={{marginBottom: 20}}>
      {
        listing.bedroom && listing.beds && listing.bathroom ? 
          <p className="SoftText"style={{margin: 0}}>
            {listing.bedroom !== 1 ? listing.bedroom + ' bedrooms': listing.bedroom + ' bedroom'} &middot; {listing.beds !== 1 ? listing.beds + ' beds' : listing.beds + ' bed'} &middot; {listing.bathroom !== 1 ? listing.bathroom + ' baths' : listing.bathroom + ' bath'}
          </p>
          : 
          <p className="GrayText" style={{margin: 0}}>This SummerStay has no specifics.</p>
      }
      
    </div>
  )
}


