import React, { Component } from 'react'

import ExpansionPanelWrapper from './expansionPanelWrapper.js'
import { connect } from 'react-redux'


class QuestionAnswers extends Component {
  constructor(props){
    super(props)
    this.state={
      expandedIndex: null
    }
  }
  componentDidMount(){
    this.setState({
      selected: this.props.details[0].title, 
      allowed: true
     })
  }
  handleExpansion=(index)=>{
    if(index === this.state.expandedIndex)
        this.setState({expandedIndex: null})
    else
        this.setState({expandedIndex: index})
  }
  render() {
    const { details, children, appSize } = this.props
    return (
      <div  style={{display:'flex', flexDirection: appSize.isLargePhone ? 'column':'row'}}>
          <div style={{display: 'flex', flex: 1}}>
        {
            children
        }
        </div>
        <div style={{display: 'flex', flexDirection: 'column', flex: 1}}>
            {
                details.map((d, i)=>
                <ExpansionPanelWrapper
                  expanded={this.state.expandedIndex === i}
                  onChange={()=>this.handleExpansion(i)}
                  question={d.question}
                  answer={d.answer}
                />
                )
            }
        </div>
      </div>
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(QuestionAnswers)
