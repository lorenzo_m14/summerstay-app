import React from 'react';
import '../../CSS/Buttons.css';
import Check from '@material-ui/icons/Done'
import Arrow from '@material-ui/icons/KeyboardArrowRight'
import Delete from '@material-ui/icons/Delete'
import Menu from '@material-ui/icons/Menu'
import Send from '@material-ui/icons/NearMeOutlined'
import Money from '@material-ui/icons/AttachMoney'
import CreditCard from '@material-ui/icons/CreditCard'
import Home from '@material-ui/icons/HomeOutlined'
import Settings from '@material-ui/icons/SettingsOutlined'
import People from '@material-ui/icons/PeopleOutlined'
import Exit from '@material-ui/icons/ExitToApp'
import Listings from '@material-ui/icons/LocationCityOutlined'
import BigBack from '@material-ui/icons/ArrowBackIos'
import Close from '@material-ui/icons/Clear'
import Refresh from '@material-ui/icons/Refresh'
import Notification from '@material-ui/icons/NotificationsOutlined'
import Chat from '@material-ui/icons/ChatBubbleOutlined'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import DownArrow from '@material-ui/icons/ArrowDownwardOutlined'

const NLButton = ({ title, onClick, type, icon}) => (
    <button className={type} onClick={onClick}>
        {icon === 'bigBack' ? <BigBack style={{ fontSize: 22 ,fontWeight:500}}/> : null}

        {/* correspond the icons with their SummerStay names */}
        {icon === 'payments' ? <CreditCard style={{ fontSize: 25 }}/> : null}
        {icon === 'payouts' ? <Money style={{ fontSize: 25 }}/> : null}
        {icon === 'stays' ? <Home style={{ fontSize: 25 }}/> : null}
        {icon === 'settings' ? <Settings style={{ fontSize: 25 }}/> : null}
        {icon === 'campus_rep' ? <People style={{ fontSize: 25 }}/> : null}
        {icon === 'listings' ? <Listings style={{ fontSize: 25 }}/> : null}
        {icon === 'notifications' ? <Notification style={{ fontSize: 25 }}/>: null}
        {icon === 'chats' ? <Chat style={{ fontSize: 25 }}/>: null}
        {icon === 'exit' ? <Exit style={{ fontSize: 25 }}/> : null}
        {icon === 'forward' ? <NavigateNextIcon style={{ fontSize: 35 }}/> : null}
        {icon === 'back' ? <NavigateBeforeIcon style={{ fontSize: 35 }}/> : null}
        {title}
        {icon === 'downArrow' ? <DownArrow style={{ fontSize: 16 }}/> : null}
        {icon === 'close' ? <Close style={{ fontSize: 19 }}/> : null}
        {icon === 'closeBig' ? <Close style={{ fontSize: 25 }}/> : null}
        {icon === 'refresh' ? <Refresh style={{ fontSize: 18 }}/> : null}
        {icon === 'check' ? <Check style={{ fontSize: 19 }}/> : null}
        {icon === 'arrow' ? <Arrow style={{ fontSize: 20 }}/> : null}
        {icon === 'clear' ? <Delete style={{ textAlign:'center',color:'#fff',fontSize: 20 }}/> : null}
        {icon === 'menu' ? <Menu style={{ fontSize: 20 }}/> : null}
        {icon === 'send' ? <Send style={{ fontSize: 20 }}/> : null}
        
    </button>
  );
  
export default NLButton;