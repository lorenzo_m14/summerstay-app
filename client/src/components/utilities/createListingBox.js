import React, {Component} from 'react'
import '../../CSS/App.css'
import { LISTING_TYPES } from '../../constants/index.js'
import CircleLoad from './circleLoad.js'

import NLButton from '../utilities/NoLinkButton.js'
import SearchBar from '../utilities/searchBar.js'
import { CustomSelect } from '../../constants/inputs.js'

import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import MenuItem from '@material-ui/core/MenuItem'
import { Route } from 'react-router-dom'


const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'left',
    flexWrap: 'wrap',
    marginBottom:'20px',
    
  }
});

class CreateListingBox extends Component {
  state = {
    listingType: '',
    listingAddress: {
      location: '', 
      lat: null, 
      lng: null
    },
    // labelWidth: 100,
    typeError: false, 
    addressError: false, 
    makingListing: false, 
  };

  handleChange = event => {
    this.setState({ listingType: event.target.value });
  }

  updateAddress = (location, lat, lng, fullAddress) =>{
    let listingAddress={location:location, lat:lat, lng:lng, fullAddress: fullAddress}
    this.setState({ listingAddress: listingAddress });
    this.forceUpdate()
  }
  isFloat=(n)=>{
    return Number(n) === n && n % 1 !== 0
  }
  createListing = (history) =>{
    let error = false
    if(this.state.listingType === ''){
      error = true
      this.setState({typeError: true})
    }else{
      this.setState({typeError: false})
    }
    if(this.state.listingAddress.location === '' || this.state.addressError){
        error = true
        this.setState({addressError: true})
    }else{
      this.setState({addressError: false})
    }
    if(!error){
        this.props.createListing(this.state.listingAddress, this.state.listingType, history)
    }
  }
  
  render() {
    const { classes, loading } = this.props;

    return (
        <div style={{display: 'flex',flexDirection: 'column'}}>
            
            {
              this.state.addressError ? <p style={{textAlign:'center',margin:0, color:'red'}}>Select a valid address</p>: <div style={{height:18}}></div>
            }
            <div>
              <form className={classes.root} autoComplete="off">
                <h4 style={{marginTop:0, marginBottom: 0}}>What type of place do you have?</h4>
                <CustomSelect
                    label='Listing type'
                    error={this.state.typeError}
                    value={this.state.listingType}
                    onChange={this.handleChange}
                >
                    {
                      LISTING_TYPES.map(t=>
                        <MenuItem key={t} value={t}><em>{t}</em></MenuItem>
                      )
                    }
                </CustomSelect>
                <h4 style={{marginTop: 0}}>Where do you live?</h4>
                <div style={{overflow: 'visible', position: 'relative',zIndex: 0, width:'100%'}}>
                  <SearchBar initialVal={''} placeholder="Address" main={true} 
                  error={this.state.addressError} problem={(val)=>this.setState({addressError: val})} updateLocation={this.updateAddress}/>
                </div>
              </form>
              {
                !loading ? 
                <Route render={({ history}) => (
                  <NLButton title ="Continue" onClick={() => loading ? undefined : this.createListing(history)} type="LargeButton"/>
                  
                )}/>
                : <CircleLoad/>
              }
            </div>
        </div>
        
      
    ); 
  }
}

CreateListingBox.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateListingBox);
