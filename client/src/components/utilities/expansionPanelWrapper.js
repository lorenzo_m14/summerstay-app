import React, { Component } from 'react'
import {  withStyles } from '@material-ui/core/styles'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel'
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import Plus from '@material-ui/icons/Add'
import Minus from '@material-ui/icons/Remove'
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'


import { connect } from 'react-redux'

const ExpansionPanel = withStyles({
  root: {
    
    marginBottom: 5,
    borderRadius: 5,
    boxShadow: 'none',
    backgroundColor: '#fff',
    // '&:not(:last-child)': {
    //   borderBottom: 0,
    // },
    '&:before': {
      display: 'none',
    },
    // '&$expanded': {
    //   margin: 'auto',
    // },
    transition: 'all .5s',
  },
  expanded: {
    
    backgroundColor: '#F6F9FC',
  },
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    // backgroundColor: '#d5d9e0',
    // borderBottom: '1px solid #e0e0e0',
    
    borderRadius: 5,
    // marginBottom: -1,
    minHeight: 56,
    color: '#21222f',
    fontWeight:500,
    '&$expanded': {
      
      minHeight: 56,
    },
  },
  content: {

    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {
    color: '#2E3440',
    borderRadius: 5
  },

})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    paddingTop: 0,
    paddingBottom: 10, 
    paddingRight: 20, 
    paddingLeft: 20,
    color: '#585d66',
  },
}))(MuiExpansionPanelDetails);

class ExpansionPanelWrapper extends Component{

    render(){
    const { expanded, question, answer, link } = this.props
    return(
        <ExpansionPanel expanded={expanded} onChange={this.props.onChange}>
            {
                link ? 
                    <Link to={link}>
                        <ExpansionPanelSummary
                            expandIcon={expanded ? <Minus style={{color: '#6c4ef5'}}/> : <Plus style={{color: '#6c4ef5'}}/>}
                            aria-controls="panel2bh-content"
                            id="panel2bh-header"
                        >
                            {question}
                        </ExpansionPanelSummary>
                    </Link> 
                    : 
                    <ExpansionPanelSummary
                        expandIcon={expanded ? <Minus style={{color: '#6c4ef5'}}/> : <Plus style={{color: '#6c4ef5'}}/>}
                        aria-controls="panel2bh-content"
                        id="panel2bh-header"
                    >
                        {question}
                    </ExpansionPanelSummary>
            }
            
            <ExpansionPanelDetails>
                {answer}
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}
}

export default ExpansionPanelWrapper