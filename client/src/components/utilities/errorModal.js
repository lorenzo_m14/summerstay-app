import React from 'react';
import ModalWrapper from './Modal.js'
import NLButton from './NoLinkButton.js'

import '../../CSS/App.css'

export default function ErrorModal(props){
    return (
        <ModalWrapper
            open={props.show}
            onClose={props.close}
        > 
            <div style={{display:'flex', flex:1,flexDirection:'column',justifyContent:'space-between'}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                    <h2 style={{marginLeft: 10, marginRight:10}}>That didn't work.</h2>
                    <p className="GrayText" style={{textAlign:'center',marginLeft: 10, marginRight:10}}>{props.message}</p>
                </div>
                <NLButton title ='Okay' onClick={props.close} type="MediumButton"/>     
            </div>
        </ModalWrapper>
    )
}
