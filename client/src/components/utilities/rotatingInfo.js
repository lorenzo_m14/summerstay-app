import React, { Component } from 'react'
import NLButton from './NoLinkButton.js'
import Slide from '@material-ui/core/Slide'

import { connect } from 'react-redux'

class RotatingInfo extends Component {
  constructor(props){
    super(props)
    this.state={
      selected: null, 
      allowed: false
    }
  }
  componentDidMount(){
    this.setState({
      selected: this.props.details[0].title, 
      allowed: true
     })
  }
  update=(val)=>{
    if( val !== this.state.selected){
      this.setState({
        selected: val,
        allowed: false
      })
      setTimeout(()=>{ // This is used to smooth the animation
        this.setState({allowed: true})
      }, 200);
    }
  }
  render() {
    const { details, children, appSize } = this.props

    return (
      <div  style={{display:'flex', flexDirection:'column',background:'#fff', marginTop: 20}}>
        {
          children.props.children[0]
        }
        <div style={{display:'flex', flexDirection: appSize.isMediumPhone ? 'column': 'row'}}>
          <div style={{display:'flex', flex: 1,flexDirection:'column', minHeight: appSize.isLargePhone ? 0 : 400}}>
            {
              children.props.children[1]
            }
            <div style={{display:'flex', flexDirection: 'column'}}>
              { !appSize.isMediumPhone ?
                  details.map((d,i)=>
                    <NLButton title={`\xa0\xa0\xa0${i+1}\xa0\xa0\xa0 ${d.title}`} onClick={()=>this.update(d.title)}
                      type={this.state.selected === d.title ? 'SwitchOn' : 'SwitchOff'}/>
                  )
                : null
              }
            </div>
          </div>
          <div style={{display:'flex', flex: 1, flexDirection:'column', 
                      alignItems:'center',marginTop: appSize.isMediumPhone ? 20: 0 }}>
          { 
            !appSize.isMediumPhone ?
              details.map((d,i)=>{
                let open = this.state.selected === d.title && this.state.allowed
                return <Slide direction={open ? 'up': 'down'} in={open} mountOnEnter unmountOnExit >
                          <div style={{display:'flex', flexDirection:'column'}}>
                            <img src={d.img} width='100%' alt={d.title}/>
                            <p className="SubTitleText" style={{color: '#2E3440'}}><span style={{fontWeight: 600, color: '#2E3440' }}>{i+1}. {d.title}</span> {d.content}</p>
                          </div>
                        </Slide>
              })
              : 
              details.map((d,i)=>
                <div style={{display:'flex', flexDirection:'column', marginBottom: 20}}>
                  <img src={d.img} width='100%' alt={d.title}/>
                  <p className="SubTitleText" style={{color: '#2E3440'}}><span style={{fontWeight: 600, color: '#2E3440' }}>{i+1}. {d.title}</span> {d.content}</p>
                </div>
              )
          }
        
        </div>
      </div>
        

      </div>
      
    ); 
  }
}
const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(RotatingInfo)
