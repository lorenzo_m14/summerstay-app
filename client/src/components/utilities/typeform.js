import React from 'react'
import * as typeformEmbed from '@typeform/embed'

class TypeForm extends React.Component {
  constructor(props) {
    super(props);
    this.el = null;
  }
  componentDidMount() {
    if (this.el) {

      typeformEmbed.makeWidget(this.el, this.props.formUrl, {
        hideFooter: true,
        hideHeaders: true,
        opacity: 0
      });
    }
  }
  render() {

    return (
      <div style={{height: 600}} ref={(el) => this.el = el} />
    )
  }
}

export default TypeForm;