import React from 'react';
import ModalWrapper from './Modal.js'
import NLButton from './NoLinkButton.js'

export default function SuccessModal(props){
    return (
        <ModalWrapper
            open={props.show}
            onClose={props.close}
        >
            <div style={{display:'flex', flex:1,flexDirection:'column',justifyContent:'space-between'}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                    <h2 style={{marginLeft: 10, marginRight:10}}>Congratulations!!</h2>
                    <p className="GrayText" style={{textAlign:'center',marginLeft: 10, marginRight:10}}>{props.message}</p>
                </div>
                <NLButton title ='Continue' onClick={props.close} type="MediumButton"/>     
            </div>
        </ModalWrapper>
    ); 
}
