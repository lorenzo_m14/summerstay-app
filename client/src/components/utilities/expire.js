import React, { Component } from 'react';
import Slide from '@material-ui/core/Slide';

class Expire extends Component {
    constructor(props) {
      super(props);
      this.state = {visible:false}
    }
  
    componentWillReceiveProps(nextProps) {
      // reset the timer if children are changed
      if (nextProps.children !== this.props.children) {
        this.setTimer();
        this.setState({visible: true});
      }
    }
  
    componentDidMount() {
      this.setTimer();
    }
  
    setTimer() {
      // clear any existing timer
      if (this._timer != null) {
        clearTimeout(this._timer)
      }
  
      // hide after `delay` milliseconds
      this._timer = setTimeout(function(){
        this.props.reset()
        this.setState({visible: false});
        this._timer = null;
        
      }.bind(this), this.props.delay);
    }
  
    componentWillUnmount() {
      clearTimeout(this._timer);
    }
  
    render() {
      return(
         <Slide direction="down" in={this.state.visible}>
            <div style={{display: 'flex', backgroundColor:this.props.failed ? '#ed0c0c22':'#6c4ef522', color:'#000', borderRadius: 5, 
                        justifyContent: 'center', alignItems: 'center', position: 'absolute', left: 0,right: 0, 
                        width: 400, height: 50, border: '1px solid #000', marginLeft: 'auto',marginRight: 'auto', marginTop: 20
                    }}>
                <p style={{margin: 5}}>
                    {this.props.children}
                </p>
            </div>
        </Slide>
      )}
  }

export default Expire