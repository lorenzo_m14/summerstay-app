
import React, { Component } from 'react'
import Passbase from "@passbase/button"
import VerifyButton from "@passbase/button/react"
import { PASSBASE_API_KEY } from '../../constants/index.js'
import NLButton from './NoLinkButton.js'
import Button from './Button.js'
import CircleLoad from './circleLoad.js'
import ModalWrapper from './Modal.js'

import ApprovedIcon from '@material-ui/icons/VerifiedUserOutlined'
import InReviewIcon from '@material-ui/icons/Autorenew'
import DeniedIcon from '@material-ui/icons/ErrorOutline'

import { POST } from '../../constants/requests.js'

import { connect } from 'react-redux'
import { loginUser } from '../../store/actions/passThrough.js'

class PassbaseWrapper extends Component {
    constructor(props){
        super(props)
        this.state={
            error: false,
            loading: false
        }
    }
    verifyFinished=(error, authKey, additionalAttributes) => {
        if(!error && !this.state.loading){
            const send_data = {
                "auth_key": authKey,
            }
            this.setState({loading: true})
            POST('/api/v1/users/verify_id', send_data)
            .then(data => {
                this.props.onLoginUser(data.user)
                if(this.props.onUpdated){
                    this.props.onUpdated()
                }
                this.setState({ loading: false })
            })
            .catch(error => {
                this.setState({ 
                    error: true, 
                    loading: false
                 })
            })
        }else{
            this.setState({ error: true })
        }
        
    }
  
      
    render() {
        const { user } = this.props
        // Status
        // Unapproved
        // InReview
        // Approved
        // Denied
        const wrapperStyle = {display:'flex', backgroundColor: '#fff', width: '100%'} 
        const wrapperP = {marginBottom: 0, fontSize: 16}
      return (
          <div>
              <ModalWrapper
                open={this.state.error}
                onClose={()=>this.setState({error: false})}
              >
                <h2 style={{marginTop: 0}}>Something went wrong</h2>
                <p>Sorry about that... It looks like there was a problem submitting this request.</p>
                <NLButton title ="Close" onClick={()=>this.setState({error: false})}  type="LargeButton Gray"/>
              </ModalWrapper>

            {
                user && user.id_verify_status === 'Unapproved' && !this.state.loading &&
                    <VerifyButton
                        apiKey={PASSBASE_API_KEY}
                        onFinished={this.verifyFinished}
                        additionalAttributes={{ 
                            userID: user.id
                        }}
                        prefillAttributes={{
                            email: user.email
                        }}
                        theme={{
                            accentColor: "#6c4ef5",
                            font: "Helvetica Neue", 
                            darkMode: false
                        }}
                    />
            }
            {
                user && user.id_verify_status === 'InReview' && // InReview
                <div style={wrapperStyle}>
                    <InReviewIcon style={{color: '#41d09c', fontSize: 30 }}/>
                    <div style={{marginLeft: 10}}>
                        <h3 style={{margin: 0}}>Identity Under Review</h3>
                        <p className="SubTitleText" style={wrapperP}>You will be notified when your status is updated.</p>
                    </div>
                </div>
            }
            {
               user && user.id_verify_status === 'Approved' && // Approved
               <div style={wrapperStyle}>
                    <ApprovedIcon style={{color: '#41d09c', fontSize: 30 }}/>
                    <div style={{marginLeft: 10}}>
                        <h3 style={{margin: 0}}>Identity Approved</h3>
                        <p className="SubTitleText" style={wrapperP}>Thank you for verifying your identity.</p>
                    </div>
                </div>
            }
            {
                user && user.id_verify_status === 'Denied' && // Denied
                <div style={wrapperStyle}>
                    <DeniedIcon style={{color: '#FB3640', fontSize: 30 }}/>
                    <div style={{marginLeft: 10}}>
                        <h3 style={{margin: 0}}>Identity Denied</h3>
                        <p className="SubTitleText" style={wrapperP}>Please contact <Button title="support" link="/support" target="_blank" type="InlineText"/> with questions or an appeal.</p>
                    </div>
                </div>
            }
            {
                (!user || this.state.loading) && <CircleLoad/>
            }
        </div>
        
      ); 
    }
  }
  
const mapStateToProps = state => {
    return {
        user: state.housingApp.user
    }
}
const mapDispatchToProps  = dispatch => {
    return{
        onLoginUser: (user) => dispatch(loginUser(user)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(PassbaseWrapper);

  