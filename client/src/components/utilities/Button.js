import React from 'react';
import '../../CSS/Buttons.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import Check from '@material-ui/icons/Done'
import Arrow from '@material-ui/icons/KeyboardArrowRight'
import Delete from '@material-ui/icons/Delete'
import Menu from '@material-ui/icons/Menu'
import Send from '@material-ui/icons/Send'
import Money from '@material-ui/icons/AttachMoney'
import Home from '@material-ui/icons/HomeOutlined'
import Settings from '@material-ui/icons/SettingsOutlined'
import People from '@material-ui/icons/PeopleOutlined'
import Exit from '@material-ui/icons/ExitToApp'
import Listings from '@material-ui/icons/LocationCityOutlined'
import BigBack from '@material-ui/icons/ArrowBackIos'
import Notification from '@material-ui/icons/NotificationsOutlined'
import Chat from '@material-ui/icons/MessageOutlined'
import Edit from '@material-ui/icons/Edit'

const Button = ({ title, onClick, link, type, target, check, arrow, clear, menu, send, icon}) => (
    <Link to={link} className={type} target={target} onClick={onClick}>
        {icon === 'bigBack' ? <BigBack style={{ fontSize: 22 ,fontWeight:500}}/> : null}
        {icon === 'money' ? <Money style={{ fontSize: 25 }}/> : null}
        {icon === 'home' ? <Home style={{ fontSize: 25 }}/> : null}
        {icon === 'settings' ? <Settings style={{ fontSize: 25 }}/> : null}
        {icon === 'people' ? <People style={{ fontSize: 25 }}/> : null}
        {icon === 'exit' ? <Exit style={{ fontSize: 25 }}/> : null}
        {icon === 'listings' ? <Listings style={{ fontSize: 25 }}/> : null}
        {icon === 'notifications' ? <Notification style={{ fontSize: 25 }}/>: null}
        {icon === 'chat' ? <Chat style={{ fontSize: 25 }}/>: null}
        {icon === 'edit' ? <Edit style={{ fontSize: 25 }}/>: null}
        {title}
        {check ? <Check style={{ fontSize: 19 }}/> : null}
        {arrow ? <Arrow style={{ fontSize: 20 }}/> : null}
        {clear ? <Delete style={{ textAlign:'center',color:'#ffffff',fontSize: 20 }}/> : null}
        {menu ? <Menu style={{ fontSize: 20 }}/> : null}
        {send ? <Send style={{ fontSize: 20 }}/> : null}
    </Link>
  );
  
export default Button;