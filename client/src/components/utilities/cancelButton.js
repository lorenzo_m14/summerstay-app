import React, { Component } from 'react'
import NLButton from '../utilities/NoLinkButton.js'

import ModalWrapper from '../utilities/Modal.js'
import { CustomTextbox } from '../../constants/inputs.js'


const cancelText = 'cancel'

class CancelButton extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      error: false,
      userEntry: ''
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleChange = (event) => {
    this.setState({ userEntry: event.target.value })
  }

  check = () => {
    if (this.state.userEntry === cancelText) {
      this.setState({ error: false })
      this.handleClose()
      this.props.cancel()
    } else {
      this.setState({ error: true })
    }
  }

  render () {
    let { open, error, userEntry } = this.state

    return (
      <div>
        <ModalWrapper open={open} onClose={this.handleClose}>
          <h2 style={{marginTop: 0}}>Confirm Cancellation</h2>
          <p> In order to cancel your stay please type: <b>cancel</b> </p>
          {
            this.props.children
          }
          <CustomTextbox autoFocus error={error} label='cancel'
            value={userEntry} onChange={this.handleChange} full />
          <div style={{ display:'flex', marginTop:10 }}>
            <NLButton onClick={this.handleClose} type='MediumButton Gray' title='Back' />
            <NLButton onClick={this.check} type='MediumButton Red MedMargin' title='Cancel Stay' />
          </div>
        </ModalWrapper>
        <NLButton type='MediumButton Gray' onClick={this.handleClickOpen} title={this.props.title} />
      </div>
    )
  }
}

export default CancelButton
