
import React from 'react';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    left: 0,
    right: 0,
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth:400,
    minWidth: 100,
    marginTop:100,
    borderRadius:5,
    backgroundColor: '#fff',
    boxShadow: theme.shadows[5],
    padding: 20,
    outline: 'none',
  }
}));

export default function ModalWrapper(props) {
  const classes = useStyles();
  const shouldEnforceFocus = props.disableEnforceFocus ? true : false
    
  return (
    <Modal 
        open={props.open} 
        onClose={props.onClose}
        disableEnforceFocus={shouldEnforceFocus}
    >
      <div className={classes.paper} style={props.style}>
        {
            props.children
        }
      </div>
    </Modal>  
  );
}
