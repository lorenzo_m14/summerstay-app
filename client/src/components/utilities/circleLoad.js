import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles(theme => ({
    load: {
      color:'#6c4ef5'
    }
  }));


function CustomColorLoading(props) {
    const classes = useStyles();

  return (
    <div style={{display:'flex', justifyContent:'center'}}>
        <CircularProgress className={classes.load}/>   
    </div>  
  );
}

export default CustomColorLoading;