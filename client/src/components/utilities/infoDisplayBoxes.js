import React from 'react';
import Button from './Button.js'
import '../../CSS/InfoDisplayBoxes.css'
import { connect } from 'react-redux';
// props: card_1, card_2, card_3
function InfoDisplayBoxes(props) {

  const {details, highlightColor, appSize} = props
  const isSmallComputer = appSize.isSmallComputer
  const extraStyle = isSmallComputer ? { flexDirection:'column', alignItems:'center'} : {flexDirection:'row'}
  return (
    <div className='Cards' style={extraStyle}>
      { details.map((d,i) => 
      <div className={i === 0 ? 'Card HeavyShadow': 'Card LightShadow'} 
            style={{backgroundColor: i===0 ? highlightColor : '#fff', 
                    width: isSmallComputer ? '100%': 'default', maxWidth: isSmallComputer ? 400: null}} key={d.title}>
        <div style={{display:'flex',justifyContent:'flex-end', marginTop: 30, marginRight: 40, marginBottom: 10}}>
        <h1 className="SkinnyHeader" style={{margin: 0,color:i===0 ?'#fff' : '#484848'}}>0{i+1}</h1>
        </div>
        
        <div style={{display:'flex', flex: 1, position:'relative', justifyContent:'center'}}>
          
        
          <div style={{display:'flex', flexDirection:'column', justifyContent:'space-between'}}>
            
            <div style={{width:'75%', alignSelf:'center', position:'relative'}}>
              <h4 style={{marginTop: 10,  color: i===0 ?'#fff': 'default' }}>{d.title}</h4>
              <p style={{color: i===0 ?'#fff': '#5E6C77'}}>{d.summary}</p>
            </div>
            
            <div style={{ display:'flex', alignItems:'flex-end',paddingBottom: 35, paddingLeft: '12.5%'}}>
            
              <Button link='/how-it-works' title="HOW IT WORKS" type={i===0 ?"UnderlinedButton White": "UnderlinedButton Purple"}/>
            </div>
            
          </div>
        </div> 
        
        </div>
          // <Card className='card' key={d.title}>
          //   <CardContent>
          //     <div className={ classes.title }>
          //       {d.icon}
          //       <h2>{d.title}</h2>
          //     </div>
          //     <p>{d.summary}</p>
          //   </CardContent>
          // </Card>
          )
      }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    appSize: state.housingApp.appSize
  }
}

export default connect(mapStateToProps)(InfoDisplayBoxes)