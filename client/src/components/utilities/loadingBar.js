import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';

import LinearProgress from '@material-ui/core/LinearProgress';



const ColorLinearProgress = withStyles({
  colorPrimary: {
    backgroundColor: '#a0c6ef',
  },
  barColorPrimary: {
    backgroundColor: '#6c4ef5',
  },
})(LinearProgress);


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width:'100%',
    position:'fixed', 
    top:70, 
    zIndex:3,
  },
  margin: {
    margin: 0,
  },
}));

function CustomLoading(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>

      {
        props.loading ? <ColorLinearProgress className={classes.margin} /> : null
      }
    </div>
  );
}

export default CustomLoading;