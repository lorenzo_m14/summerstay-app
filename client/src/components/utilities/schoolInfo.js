import React from 'react'
import { SCHOOL_NAMES } from '../../constants/index.js'
import { CustomTextbox } from '../../constants/inputs.js'
import Autocomplete from '@material-ui/lab/Autocomplete'

import { connect } from 'react-redux'

function SchoolInfo(props) {

  return (
    <div  style={{display:'flex', flexDirection:'column',background:'#fff', marginTop: 10}}>
      <Autocomplete
        id="combo-box"
        freeSolo
        options={SCHOOL_NAMES}
        getOptionLabel={option => option}
        defaultValue={props.user && props.user.school_name}
        onChange={(e,value)=>props.updateSchoolName(value)}
        renderInput={params => (
        <CustomTextbox {...params} 
          error={props.error}
          label="Enter School Name" 
          variant="outlined" 
          fullWidth 
          onChange={(e)=>props.updateSchoolName(e.target.value)}
        />
        )}
      />     
    </div>
  ); 
}


const mapStateToProps = state => {
  return {
    user: state.housingApp.user
  }
}

export default connect(mapStateToProps)(SchoolInfo)

