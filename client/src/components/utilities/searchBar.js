import React from 'react'
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete'
import { AltCustomTextbox, CustomTextbox } from '../../constants/inputs.js'

import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import '../../CSS/ListingsMain.css'
import Clear from '@material-ui/icons/Clear'
import SearchIcon from'@material-ui/icons/Search'

const styles = {
    root: {
      padding: '4px 8px',
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      maxWidth: 1200,
    },

    input: {
      padding: '4px 8px',
      marginLeft: 8,
      flex: 1,
    },
    
  };

class ListingSearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        address: null,
        width: null,
        height: null, 
        error: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.error !== this.state.error || nextProps.initialVal !== this.props.initialVal ||
       this.state.address !== nextState.address || nextProps.error !== this.props.error
    )
  }

  handleChange = address => {
    this.setState({ address: address });
  };

  handleSelect = address => {
    let address_results = null
    geocodeByAddress(address)
    .then(results => {
      address_results = results[0]
      return getLatLng(results[0])
    })
    .then((latLng) => {
      this.props.problem(false)
      let full_address = this.handleAddress(address_results)
      this.setState({
        address, 
        full_address,
      })
      this.props.updateLocation(address, latLng.lat, latLng.lng, full_address)
    })
    .catch(error => {
      this.props.problem(true)
    });
  };

  // NOTE: for when we reverse geocode the address
  handleAddress = results => {
    console.log(results)
    let street_num, street_name, line_1, city, state, zip_code, country
    const address_components = results.address_components
    for(const comp of address_components) {
      let types = comp.types
      if (types.includes("street_number")) {
        street_num = comp.long_name
      }
      else if (types.includes("route")) {
        street_name = comp.long_name
      }
      else if (types.includes("locality") || types.includes("sublocality")) {
        city = comp.long_name
      }
      else if (types.includes("administrative_area_level_1")) {
        state = comp.short_name
      }
      else if (types.includes("country")) {
        country = comp.short_name
      }
      else if (types.includes("postal_code")) {
        zip_code = comp.long_name
      }
    }
    line_1 = undefined
    if (street_num && street_name) {
      line_1 = street_num + " " + street_name
    }

    let full_address = {
      line_1, city, state, zip_code, country,
    }

    return full_address
  }

  clear = () => {
    if(!this.props.main){
      this.props.clear()
    }else{
      this.props.problem(true)
    }
    
    this.setState({address: ''})
  }

  onError = (status, clearSuggestions) => {
    this.props.problem(true)
    // this.setState({error: true})
    clearSuggestions()
  }

  render() {
    const classes = this.props
    
    return (
       
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
        onError={this.onError}
        searchOptions={this.props.main ? {types: ['address']} : {}}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
//  style={{border: this.props.error? '1px solid red':null}}
            <div style={{display:'flex', flexDirection:'column', flex: 1, position:'relative'}} > 
              {/* alignItems: 'center', padding: this.props.main ? 10 : 5, paddingRight:10, paddingLeft:10,backgroundColor: '#fff', borderRadius:3 */}
                <div style={{ display:'flex'}}>
                  {
                    this.props.alt ? 
                    <AltCustomTextbox
                      error={this.props.error}
                      // value={this.state.address}
                      {...getInputProps({
                          
                          className: 'location-search-input',
                      })}
                      full
                      value={this.state.address !== null ? this.state.address : this.props.initialVal}
                      placeholder={this.props.placeholder}
                      style={{ backgroundColor: '#fff' }}
                      InputProps={{
                        startAdornment:<SearchIcon style={{fontSize: 22 ,fontWeight:500, color: '#c8c3d3'}}/>
                        
                      }}
                    /> : 
                      <CustomTextbox
                      error={this.props.error}
                      // value={this.state.address}
                      {...getInputProps({
                          
                          className: 'location-search-input',
                      })}
                      full
                      value={this.state.address !== null ? this.state.address : this.props.initialVal}
                      placeholder={this.props.placeholder}
                      style={{ backgroundColor: '#fff' }}
                      InputProps={{
                        endAdornment:
                        <div className='ClearButton' >
                          <Clear onClick={this.clear} style={{fontSize: 22 ,fontWeight:500}}/>
                        </div>
                      }}
                      />
                  }
                </div>
                {
                  !this.props.alt ?
                    <div style={{display:'flex', backgroundColor: '#fff',width:'100%', flexDirection:'column', position:'absolute', top: '100%',left: 0, 
                            borderBottomLeftRadius: 3, borderBottomRightRadius: 3}} >
                        {loading && <div style={{backgroundColor: '#ffffff', width: '100%'}}>Loading...</div>}
                        {suggestions.map(suggestion => {
                          if(true){ //suggestion.types[0]==='street_address' || !this.props.main
                          
                            const className = suggestion.active
                            ? 'suggestion-item--active'
                            : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                            ? { backgroundColor: '#d5d9e0', zIndez:10,borderRadius:3, padding:5, paddingLeft:10,display:'flex', alignItems:'center',   width: 'calc(100% - 15px)', cursor: 'pointer' }
                            : { backgroundColor: '#ffffff', zIndez:10,borderRadius:3, padding:5, paddingLeft:10, display:'flex', alignItems:'center',  width: 'calc(100% - 15px)', cursor: 'pointer' };
                            return (
                                <div
                                    {...getSuggestionItemProps(suggestion, {
                                        className,
                                        style,
                                    })}
                                >
                                    <span>{suggestion.description}</span>
                                </div>
                            );
                          }
                          return null
                        })}
                    </div>
                  : null
                }
                
            </div>

        )}
      </PlacesAutocomplete>
    );
  }
}
ListingSearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(ListingSearchBar);