import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
    small: {
      width: 30,
      height: 30,
    },
    medium: {
        width: 38,
        height: 38,
    }, 
    large: {
      width: 60,
      height: 60,
    },
  }));

export default function AvatarWrapper (props){
    const classes = useStyles();
    const {user, size} = props
    return(
        <div className="LightShadow" style={{borderRadius:'50%'}}>
            {
                user.image && user.image.url? 
                    <Avatar className={classes[size]} alt={user.first_name} src={user.image.url} />
                    : 
                    <Avatar className={classes[size]} style={{color: '#fff',backgroundColor: '#6c4ef5'}}>
                        {user.first_name.substring(0, 1)}
                    </Avatar>
            }
        </div>
    )
}

  
