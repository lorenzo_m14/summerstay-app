import React, { Component } from 'react'
import Carousel from '@brainhubeu/react-carousel'
import '@brainhubeu/react-carousel/lib/style.css'
import LinearProgress from '@material-ui/core/LinearProgress'
import NLButton from './NoLinkButton';


class CardCaousel extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0
    }
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  componentWillUnmount() {
      window.removeEventListener('resize', this.handleWindowSizeChange);
  }
  handleChange=(val)=>{
    const windowWidth = window.innerWidth
    if( val === -1){
      this.setState(prevState=>({
          index: prevState.index === 0 ? prevState.index : prevState.index+val
        })
      )
    }
    if( val === 1){
      if(windowWidth<=900 && windowWidth>450){
        this.setState(prevState=>({
            index: prevState.index === this.props.slides.length-2 ? prevState.index : prevState.index+val
          })
        )
      }else if(windowWidth<=450){
        this.setState(prevState=>({
            index: prevState.index === this.props.slides.length-1 ? prevState.index : prevState.index+val
          })
        )
      }else{
        this.setState(prevState=>({
            index: prevState.index === this.props.slides.length-3 ? prevState.index : prevState.index+val
          })
        )
      }
    }
    
    
  }

  render() {
    const { slides } = this.props

    return(
      <div style={{position:'relative',display: 'flex', flexDirection:'column'}}>
        <div style={{position:'absolute',top:'75px', left: -15, zIndex: 1}}>
          <NLButton onClick={()=>this.handleChange(-1)} icon='back' type='CircleButton LargeCircle MediumShadow SolidWhitePurple'/>
        </div>
          {/* <div style={{display:'flex', flex: 1, flexDirection:'column'}}>
          <LinearProgress variant="determinate" value={((this.state.index+1)/(slides.length-1))*100} />
          </div> */}
        <div style={{position:'absolute',top:'75px', right: -15, zIndex: 1}}> 
          <NLButton onClick={()=>this.handleChange(1)}icon='forward' type='CircleButton LargeCircle MediumShadow SolidWhitePurple'/>
        </div>
        
        <Carousel 
          value={this.state.index}
          onChange={this.handleChange}
          slidesPerPage={3}
          addArrowClickHandler
          draggable
          breakpoints={{
            900: {
              slidesPerPage: 2,
              slidesPerScroll: 1,
              clickToChange: false,
              centered: false,
              infinite: false,
            },
            450: {
              slidesPerPage: 1,
              slidesPerScroll: 1,
              clickToChange: false,
              centered: false,
              infinite: false,
            },
          }}
          slides={ slides }
        />
      </div>
    )
  }
} 
export default CardCaousel