import React, { Component } from 'react'
import Slide from '@material-ui/core/Slide'
import NLButton from './NoLinkButton.js'

class RefButtonGroup extends Component{
  constructor(props){
    super(props);
    this.state = {
      animation: false
    };
  }
  componentDidMount(){
    setTimeout(()=> {
      this.setState({
        animation: true
      })
    }, 1000);
    
  }

  render(){
    return (
      <Slide direction="right" in={this.state.animation}
      mountOnEnter unmountOnExit>
      <div style={{display:'flex', flexDirection:'column'}}>
          {
              this.props.details.map((b,i)=>
                <NLButton title={b.title} onClick={()=>this.props.nav(b.ref, i)} 
                  type={"MediumButton" }/>
              )
          }
      </div>
      </Slide>
    ); 
  }
}
export default RefButtonGroup

