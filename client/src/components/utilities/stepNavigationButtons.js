import React, { Component } from 'react'
import NLButton from './NoLinkButton.js';

class StepNavigationButton extends Component {

    changeStage=(type)=>{
        var newStage = this.props.stage
        if(type === 'next' && this.props.stage < (this.props.totalStages-1)){ //because we start at stage 0
            newStage += 1
            this.props.onChange('next',newStage) 
        }
        else if(type === 'back' && this.props.stage > 0){
            newStage -= 1
            this.props.onChange('back',newStage)
        }
        else if (type === 'next' && this.props.finish && this.props.stage === (this.props.totalStages-1)){
            this.props.onChange('finish',newStage)
        }
        
    }

    render() {
        const { canMoveOn, disabled, finish, stage, totalStages, backDisabled } = this.props
        return(
            <div style={{display:'flex'}}>
                <NLButton title ='Back' onClick={!disabled ? ()=>this.changeStage('back') : undefined} 
                        type={backDisabled ? "MediumButton Clear MedMargin" : "MediumButton Disabled MedMargin" }/> 
                <NLButton title={(finish && stage === totalStages-1) ? 'Finish':'Next'} 
                        onClick={ canMoveOn && !disabled ? ()=>this.changeStage('next') : undefined } 
                        type={!canMoveOn ? "MediumButton Disabled MedMargin": "MediumButton SolidPurple MedMargin"}/>
            </div>
        )
    }
} 
export default StepNavigationButton