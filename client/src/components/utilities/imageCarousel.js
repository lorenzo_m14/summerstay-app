import React from 'react'
import Carousel from 'react-images'

export default function ImageCarousel(props){

  return (
    <Carousel
      hideControlsWhenIdle={false}
      styles={{ container: base => ({...base,height: 'auto',maxHeight: '60vh', maxWitdth:'100%',backgroundColor: '#fff',borderRadius: 10}),
        footer: (base, state) => ({...base,position: 'absolute',padding: 0,margin: 0}),
        navigationPrev: base => ({...base,borderRadius: 5,backgroundColor: '#fff',width: 50,top: '90%', zIndex: 1,
          left: '0%',color: '#6c4ef5',border: '1px solid #6c4ef5',':hover': {backgroundColor: '#6c4ef5',color: '#fff'},
          ':active': {backgroundColor: '#00d7ff',transform: 'translateY(1px)'}}),
        navigationNext: base => ({...base,borderRadius: 5,backgroundColor: '#fff',width: 50,position: 'absolute',top: '90%', zIndex: 1,
          right: '0%',color: '#6c4ef5',border: '1px solid #6c4ef5',':hover': {backgroundColor: '#6c4ef5',color: '#fff'},
          ':active': {backgroundColor: '#00d7ff',transform: 'translateY(1px)'}}),
        view: (base, state) => ({...base,position: 'relative',overflow: 'hidden',transition: 'filter 300ms',
          '& > img': {height: 'auto', maxHeight: '60vh',width: 'auto'}})
      }}
      currentIndex={0}
      views={props.imageData}
    /> 
  )
}
