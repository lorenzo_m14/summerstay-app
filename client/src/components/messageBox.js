import React, { Component } from 'react'
import '../CSS/App.css'
import { GET, POST } from '../constants/requests.js'
import { GENERAL_ERROR } from '../constants/index.js'
import format from 'date-fns/format'
import parseISO from 'date-fns/parseISO'
import NLButton from './utilities/NoLinkButton.js'
import ErrorModal from './utilities/errorModal.js'
import LocationIcon from '@material-ui/icons/RoomOutlined'
import AvatarWrapper from './utilities/avatar.js'

import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core/styles'

import PropTypes from 'prop-types'
import { connect } from 'react-redux'


const styles = theme => ({
  messageField: {
    flex: 1,
    backgroundColor:'#fff',
    border: '1px solid #e0e0e0',
    // boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1), 0 4px 8px 0 rgba(0, 0, 0, 0.1)',
    borderRadius: 5,
    '& .MuiOutlinedInput-multiline': {
      padding: '15px 70px 15px 10px'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#fff'
      },
      '&:hover fieldset': {
        borderColor: '#fff'
      },
      '&.Mui-focused fieldset': {
        borderColor: '#fff'
      }
    }
  },
  message: {
    display: 'flex',
    flex: 1,
    // justifyContent: 'space-be',
    flexDirection: 'column',
    minWidth: 150,
    maxWidth: 1000,
     
    backgroundColor:'transparent'
  }
})

class MessageBox extends Component {
  constructor (props) {
    super(props)
    this.state = {
      message: '',
      allMessages: [],
      userInfo: null,
      listingInfo: null,
      showError: false,
      errorMessage: '',
      loading: false, 
    }
    this.MAX_MESSAGE_LENGTH = 9999
  }
  componentDidMount () {
    if (this.props.selectedConversation) {
      this.getMessages()
    }
  }
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: 'auto' })
  }
  componentDidUpdate () {
    this.scrollToBottom()
  }

  componentWillReceiveProps (nextProps) {
    if (
      nextProps.selectedConversation !== this.props.selectedConversation &&
      nextProps.selectedConversation !== null
    ) {
      GET(`/api/v1/conversations/${nextProps.selectedConversation}/messages`)
        .then(data => {
          this.dataUpdate(data)
        })
        .catch(error => {
          this.setState({ showError: true, errorMessage: error.error, loading: false })
        })
    }
  }

  getMessages = () => {
    GET(`/api/v1/conversations/${this.props.selectedConversation}/messages`)
      .then(data => {
        this.dataUpdate(data)
      })
      .catch(error => {
        console.log(error)
        this.setState({ showError: true, errorMessage: error.error, loading: false })
      })
  }
  dataUpdate=(data)=>{
    this.setState({
      message: '',
      userInfo: data.user,
      listingInfo: data.listing,
      allMessages: data.messages
    })
  }

  keyPress = (e) => {
    // sendMessage on enter pressed
    if(e.keyCode == 13 && !e.shiftKey){
      e.preventDefault()
      this.sendMessage()
    } 
  }

  sendMessage = () => {
    if (this.state.message.trim() !== '' && !this.state.loading) {
      const { message } = this.state
      this.setState({ loading: true})

      const send_data = {
        context: message,
        user_id: this.props.user.id
      }

      POST(
        `/api/v1/conversations/${this.props.selectedConversation}/messages`,
        send_data
      )
        .then(data => {
          this.setState(prevState => ({
            message: '',
            showError: false,
            allMessages: [...prevState.allMessages, data.message], 
            loading: false
          }))
        })
        .catch(error => {
          this.setState({ showError: true, errorMessage: error.error, loading: false })
        })
    }
  }

  writeMessage = event => {
    this.setState({ message: event.target.value })
  }
  closeModal=()=>{
    this.setState({showError: false})
  }
  render () {
    const { classes, isLargePhone } = this.props
    const dateFormat = isLargePhone ? 'MMM do' : 'MMM do h:mm a'
    const datePosition = isLargePhone ? -60 : -110
    const messageWidth = isLargePhone ? '50%' : '60%'
    return (
     
        <div className={classes.message} style={{height: isLargePhone ? '55vh': 500}}>
          <ErrorModal message={this.state.errorMessage} show={this.state.showError} close={()=> this.setState({showError:false})}/>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', paddingBottom: 5}} >
            
            <div style={{ display: 'flex', flexDirection: 'column'}}>
              {
                this.state.userInfo !== null ? 
                  <div style={{display:'flex', alignItems:'center'}}>
                    
                    <AvatarWrapper user={this.state.userInfo} size='medium'/>
                    <div style={{marginLeft: 10}}>
                      <h2 style={{ margin: 0}}>{this.state.userInfo.first_name}</h2>
                      {
                        this.state.listingInfo  && !isLargePhone? 
                          <div style={{display:'flex'}}>
                            <LocationIcon className="SubTitleText GrayText"/>
                            <p className="SubTitleText GrayText" style={{margin: 0}}>{this.state.listingInfo.listing_name}</p>
                          </div>
                        : <div/>
                      }
                    </div>
                      
                  </div>
                : <div/>
              }
          
            </div>
            {
              this.state.listingInfo && <NLButton title={isLargePhone ? '' : 'refresh'} onClick={this.getMessages} icon='refresh' type='SimpleButton LargeText NoMargin'/>
            }
            
            
          </div>

          <div style={{display: 'flex',flexDirection: 'column',flex: 1,minHeight: 100,
                        justifyContent: 'flex-end',border: '1px solid #e0e0e0',backgroundColor: '#fff',borderRadius: 5}}>
            <div style={{display: 'flex',flexDirection: 'column',overflowY: 'auto'}}>
              {
                this.state.allMessages.length > 0 ? 
                  this.state.allMessages.map(m =>
                    m.user_id === this.props.user.id ? 
                      <div key={`m${m.id}`} style={{ alignSelf: 'flex-end', position: 'relative', padding: 10, margin: 5, borderRadius: 7, borderBottomRightRadius: 0,
                      marginRight: 10, backgroundColor: '#6c4ef5', maxWidth: messageWidth}}>
                        
                        <div>
                        {
                          m.context.split('\n').map((p, i) =>
                            p.length > 0 ? 
                              <p key={`m${m.id}p${i}`} style={{wordWrap: 'break-word',color: '#FFF',margin: 0}}>{p}</p>
                            : <div key={`m${m.id}p${i}`} style={{ height: 12 }} />
                          )
                        }
                        </div>
                        <p className="GrayText" style={{position: 'absolute',left: datePosition,bottom: 0,fontSize: 12,margin: 0}}>
                          {format(parseISO(m.created_at), dateFormat)}
                        </p>
                      </div>
                   : 
                      <div key={`m${m.id}`} style={{ position: 'relative', padding: 10,margin: 5, marginLeft:10,borderRadius: 7,borderBottomLeftRadius: 0,
                          backgroundColor: '#f9f9fc',maxWidth: messageWidth}}>
                        <div>
                        {
                          m.context.split('\n').map((p, i) =>
                            p.length > 0 ? 
                              <p key={`m${m.id}p${i}`} style={{ wordWrap: 'break-word',margin: 0}}>{p}</p>
                              : <div key={`m${m.id}p${i}`} style={{ height: 12 }} />
                          )
                        }
                        </div>
                        <p className="GrayText" style={{ position: 'absolute',right: datePosition,bottom: 0,fontSize: 12,margin: 0}}>
                          {format(parseISO(m.created_at), dateFormat)}
                        </p>
                      </div>
                    )
                 : 
                  <div style={{display: 'flex',margin: 5,marginTop: 0,alignItems: 'center',justifyContent: 'center'}}>
                    <p className="GrayText" style={{padding: 5,margin: 0}}>
                      Send the host a message to start a conversation
                    </p>
                  </div>
                }
              <div style={{ float: 'left', clear: 'both' }} ref={el => {this.messagesEnd = el}}/>
            </div>
          </div>

          <div style={{display: 'flex',alignItems: 'center',position: 'relative'}}>
            <TextField
              id='outlined-multiline-flexible'
              multiline
              rowsMax={5}
              value={this.state.message}
              onKeyDown={this.keyPress}
              onChange={this.writeMessage}
              className={classes.messageField}
              variant='outlined'
              margin='dense'
              //helperText={`${this.state.message.length}/${this.MAX_MESSAGE_LENGTH}`}
              inputProps={{
                maxLength: this.MAX_MESSAGE_LENGTH,
              }}
            />
            {
              this.state.listingInfo &&
                <div style={{ position: 'absolute', zIndex: 1, right: 5, bottom: 12 }}>
                  <NLButton icon='send' onClick={this.sendMessage} type='CircleButton SmallCircle LightShadow'/>
                </div>
            }
            
          </div>
        </div>
    
    )
  }
}
MessageBox.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
  return {
    user: state.housingApp.user
  }
}

export default connect(mapStateToProps)(withStyles(styles)(MessageBox))
