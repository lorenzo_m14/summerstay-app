import React, { Component } from 'react'
import '../../CSS/App.css'

import { POST } from '../../constants/requests.js'

import { connect } from 'react-redux'
import CreateListingBox from '../utilities/createListingBox.js'
import ErrorModal from '../../components/utilities/errorModal.js'

class HomeOptionBox extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      requestError: false,
      requestErrorMessage: ''
    };
  }
  

  createListing = (address, type, history) =>{
    this.setState({ requestError: false })
    if(this.props.user===null){
      const temp = {
        roomType: type, 
        address: address
      }
      localStorage.setItem('tempData', JSON.stringify(temp))
      history.push('/signup')
    }else{
      this.setState({loading: true})
      const send_data = {
        listing: {
            listing_type: type,
            address: address.location,
            latitude: address.lat,
            longitude: address.lng,
            full_address: address.fullAddress,
        },
      }
      POST('/api/v1/listings', send_data)
      .then(data => {
        history.push(`/listings/edit/${data.listing.id}`)
      })
      .catch(error => {
        const errorKeys = Object.keys(error.error)
        const firstErrorMessage = error.error[errorKeys[0]]
        this.setState({ requestError: true, requestErrorMessage: firstErrorMessage, loading: false })
      });

    }
  }
  
  render() {

    return (
        <div className="OptionBox">
          <h1 style={{marginTop:0}}>
            List Your Place
          </h1>
          <p style={{ margin:0, color: '#5E6C77'}}>Listing your apartment takes 10 minutes. No financial information is required until we find you a subletter.</p>
          <CreateListingBox loading={this.state.loading} createListing={this.createListing}/>
          <ErrorModal message={this.state.requestErrorMessage} show={this.state.requestError} close={()=> this.setState({requestError:false})}/>
        </div>
    ); 
  }
}

const mapStateToProps = state => {
  return {
      user: state.housingApp.user,
  }
}
  
export default connect(mapStateToProps)(HomeOptionBox)
