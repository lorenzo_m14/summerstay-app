import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Card } from '@material-ui/core'
import CardActionArea from '@material-ui/core/CardActionArea'
import '../../CSS/InfoDisplayBoxes.css'
import CardMedia from '@material-ui/core/CardMedia'


const useStyles = makeStyles({
  card: {
    width: 300,
    margin: 10,
    cursor:'pointer'
    // minWidth: '240px',
  },
  media: {
    height: 200,
    borderRadius:5
  },
});

export default function FeaturedListings(props) {
  const classes = useStyles()
  const { image, name, state, body } = props

  return (
    
    <Card className={classes.card} elevation={0} onClick={props.onClick}>
      <CardMedia
        className={classes.media}
        image={image}
        title='Featured Listing'
      />
      <h3 style={{ marginBottom: 8, marginTop: 8 }}>{name}</h3>
      <p style={{ color: '#5E6C77' }}>{state}</p>
    </Card>
    // <div onClick={props.onClick} style={{maxWidth: 300,margin: 10}}>
    //   
    //   <h2 className="SoftText" style={{marginTop: 10}}>{name}</h2>
    // </div>
  );
}
