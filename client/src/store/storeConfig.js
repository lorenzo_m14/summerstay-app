import { createStore, combineReducers } from 'redux';
import reducer from './reducers/root.js' // simply another name for the reducer on the page

const rootReducer = combineReducers({
    housingApp: reducer
});

const store = createStore(rootReducer);

export { store };