import { LOGIN_USER, RESET, UPDATE_LISTINGS,SELECT_LISTING_BY_ID,FILTER_LISTINGS, RESIZE_APP} from './actionTypes.js'
import ReactGA from 'react-ga'

import { SMALL_COMPUTER_SIZE, LARGE_PHONE_SIZE, MEDIUM_PHONE_SIZE, SMALL_PHONE_SIZE, SIZE_1100, SIZE_1200, SIZE_1300, SIZE_500} from '../../constants/index.js'
import { CHECK_RESET } from '../../constants/functions.js'

export const loginUser = (user) => { // may want to add events to this so they appear when logged in
    localStorage.setItem('user', JSON.stringify(user));
    ReactGA.set({ userId: user.id });
    return {
        type: LOGIN_USER,
        user: user,
    };
}

export const reset = (mandatoryReset) => { // no payload needed
    //Needed incase you pass bad login credentials
    if (CHECK_RESET(window.location.pathname) || mandatoryReset){
        localStorage.clear()
        console.log('Cleared local storage')
    }
    
    return {
        type: RESET, 
    };
}

export const resizeApp = (windowWidth) =>{
    const width = windowWidth > window.screen.availWidth ? window.screen.availWidth : windowWidth
    return {
        type: RESIZE_APP,
        appSize:{
            isSmallComputer: width <= SMALL_COMPUTER_SIZE,
            isLargePhone: width <= LARGE_PHONE_SIZE,
            isMediumPhone: width <= MEDIUM_PHONE_SIZE,
            isSmallPhone: width <= SMALL_PHONE_SIZE,
            isSize1100: width <= SIZE_1100, 
            isSize1200: width <= SIZE_1200,
            isSize1300: width <= SIZE_1300, 
            isSize500: width <= SIZE_500
        }
    }
}

export const updateListings =(newListings)=>{
    return {
        type: UPDATE_LISTINGS,
        listings: newListings,
    }
}

export const selectListingByID = (newSelectedID)=>{
    return {
        type: SELECT_LISTING_BY_ID, 
        selectedID: newSelectedID
    }
}

export const filterListings = (newListings)=>{
    return{
        type :FILTER_LISTINGS, 
        newListings: newListings, 
        //need to add params
    }
}


