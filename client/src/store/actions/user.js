import { HOST_URL } from '../../constants/index.js';
// import { resetRoute } from './nav';
import { normalizeProfile } from '../../utils';

export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';
export const SET_PROFILE = 'SET_PROFILE';
export const SET_PAYMENT = 'SET_PAYMENT';

// store user access token for all api requests
export function setAccessToken(accessToken) {
  return {
    type: SET_ACCESS_TOKEN,
    accessToken,
  };
}

// profile needed for client side display (top right icon)
export function setProfile(profile) {
  return {
    type: SET_PROFILE,
    profile,
  };
}

// this is pending on backend
export function setPayment(payment) {
  return {
    type: SET_PAYMENT,
    payment,
  };
}

// TODO: Implement this facebook login action! 
//       The fetch request to the server works (see SignUp component)
export function loginWithFacebook(facebookAccessToken) {
    return (dispatch) => {
        console.log(dispatch);

      return fetch(`${HOST_URL}/api/v1/facebook`, {
        method: 'POST',
        body: JSON.stringify({
          facebook_access_token: facebookAccessToken,
        }),
        headers: { "content-type": "application/json" },
      })
      .then(response => response.json())
      .then(json => {
        console.log(json);
  
        if (json.access_token) {
          dispatch(setAccessToken(json.access_token));
          dispatch(setProfile(normalizeProfile(json.email, json.first_name, json.last_name, json.image)));
          dispatch(setPayment(!!json.stripe_id));
        //   dispatch(resetRoute({ routeName: 'Main' })); // may need to 
        } else {
          alert(json.error);
        }
      })
      .catch(e => alert(e));
    };
}