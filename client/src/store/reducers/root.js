import { LOGIN_USER, RESET, RESIZE_APP, UPDATE_LISTINGS, SELECT_LISTING_BY_ID, FILTER_LISTINGS } from "../actions/actionTypes.js";

import { SMALL_COMPUTER_SIZE, LARGE_PHONE_SIZE, MEDIUM_PHONE_SIZE, SMALL_PHONE_SIZE, SIZE_1100, SIZE_1200, SIZE_1300, SIZE_500 } from '../../constants/index.js'

const windowWidth = window.innerWidth > window.screen.availWidth ? window.screen.availWidth : window.innerWidth

const initialState = {
    user: null,
    listings: [], 
    filteredListings: [],
    selectedListing: null,
    appSize:{
        isSmallComputer: windowWidth <= SMALL_COMPUTER_SIZE,
        isLargePhone: windowWidth <= LARGE_PHONE_SIZE,
        isMediumPhone: windowWidth <= MEDIUM_PHONE_SIZE,
        isSmallPhone: windowWidth <= SMALL_PHONE_SIZE,
        isSize1100: windowWidth <= SIZE_1100, 
        isSize1200: windowWidth <= SIZE_1200,
        isSize1300: windowWidth <= SIZE_1300, 
        isSize500: windowWidth <= SIZE_500
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                user: action.user,  
            }

        case RESET: 
            return {
                user: null,
                listings: [], 
                filteredListings: [],
                selectedListing: null,
                temp: null, 
                appSize:{
                    isSmallComputer: windowWidth <= SMALL_COMPUTER_SIZE,
                    isLargePhone: windowWidth <= LARGE_PHONE_SIZE,
                    isMediumPhone: windowWidth <= MEDIUM_PHONE_SIZE,
                    isSmallPhone: windowWidth <= SMALL_PHONE_SIZE,
                    isSize1100: windowWidth <= SIZE_1100, 
                    isSize1200: windowWidth <= SIZE_1200,
                    isSize1300: windowWidth <= SIZE_1300,
                    isSize500: windowWidth <= SIZE_500
                }
            }
        case RESIZE_APP:
            return {
                ...state, 
                appSize: action.appSize
            }
        case UPDATE_LISTINGS: 
            return {
                ...state, 
                listings: action.listings , 
                filteredListings: action.listings//action.listings.length !== 0 ? action.listings : [...state.listings], 
            }
        case SELECT_LISTING_BY_ID: 
            return{
                ...state, 
                selectedListing: {...state.listings.find(l => l.id === action.selectedID)}
            }
        case FILTER_LISTINGS: 
            return {
                ...state, 
                filteredListings: action.newListings
            }
        

        default:
            return state
    }
};

export default reducer;