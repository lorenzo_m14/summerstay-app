import React, { Component } from 'react';

import Header from './components/header.js'
//pages 
import Home from './pages/Home'
import ListingsMain from './pages/ListingsMain.js'
import CampusManagers from './pages/CampusManagers.js'
import HowItWorks from './pages/HowItWorks.js'
import PayAsYouStay from './pages/PayAsYouStay.js'
import ListingSub from './pages/ListingSub.js'
import BookListing from './pages/BookListing.js'
import SignUp from './pages/SignUp.js'
import Login from './pages/Login.js'
import CreateListing from './pages/CreateListing.js'
import Dashboard from './pages/Dashboard.js'
import NotFound from './pages/NotFound.js'
import HandlePayment from './pages/HandlePayment.js'
import SignUpConfirmation from './pages/SignUpConfirmation.js'
import ResetPassword from './pages/ResetPassword.js'
import HostLanding from './pages/HostLanding.js'
import TermsAndConditions from './pages/TermsAndConditions.js'
import Support from './pages/Support.js'
import PrivateRoute from './constants/PrivateRoute.js'
import { GET } from './constants/requests.js'
import { Helmet } from 'react-helmet';
import ReactGA from 'react-ga';
import { REACT_APP_GA_TRACKING_ID } from './constants/index.js'

import { connect } from 'react-redux'
import { loginUser, resizeApp} from './store/actions/passThrough.js'

import { Router, Route, Link, Redirect, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import PrivacyPolicy from './pages/PrivacyPolicy.js'
import Policies from './pages/Policies.js'

export const history = createBrowserHistory()
ReactGA.initialize(REACT_APP_GA_TRACKING_ID, { standardImplementation: true });

// history.listen(location => {
//   const path = location.pathname + location.search
//   ReactGA.set({ page: path }); // Update the user's current page
//   ReactGA.pageview(path); // Record a pageview for the given page
// });

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      showRedirectMessage: false
    }
    this.getUser()
  }

  getUser=()=> {
    const user = JSON.parse(localStorage.getItem('user'))
    this.handleWindowSizeChange()
    if(user){
      GET('/api/v1/me')
      .then(data => {
        this.props.onLoginUser(data.user)
      })
      .catch(error => {
        console.log(error)
      });      
    }
  }
  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  componentWillUnmount() {
      window.removeEventListener('resize', this.handleWindowSizeChange);
  }
  handleWindowSizeChange = () => {
    this.props.onResizeApp(window.innerWidth)
  }

  // implement unauthorized function to pass to all protected routes
  // unauthorized=()=>{
  //   localStorage.clear()
  //   this.props.onLogoutUser();
  //   this.props.history.push('/login')
  // }
 
  render() {  
    const headMetadata = (
      <Helmet>
          <title>Housing for Students and Interns - SummerStay</title>
      </Helmet>
    )

    return (
      <Router history={history}>
        {headMetadata}
        <div style={{display: "flex", flex: 1,flexDirection:"column"}}>

        <Header />

        <Switch>
          <Route exact path="/" component={Home} />

          {/* listings pages */}
          <Route exact path="/listings" component={ListingsMain} />
          <Route exact path="/listings/view/:id"  component={ListingSub} />

          {/* THIS WAS PRIVATE ROUTE need to add user={this.props.user*/}
          <PrivateRoute exact path="/listings/book/:id"  component={BookListing} user={this.props.user}/>
          <PrivateRoute exact path="/listings/edit/:id" component={CreateListing} user={this.props.user}/>

          {/* static pages */}
          <Route exact path="/host" component={HostLanding} />
          <Route exact path="/campus-managers" component={CampusManagers} />
          <Route exact path="/how-it-works" component={HowItWorks} />
          <Route exact path="/payment-plans" component={PayAsYouStay} />

          {/* footer pages */}
          <Route exact path="/terms" component={TermsAndConditions} />
          <Route exact path="/support" component={Support} />
          <Route exact path="/privacy" component={PrivacyPolicy} />
          <Route path="/policies" component={Policies} />
          {/* <Route exact path="/community-guidelines" component={CommunityGuidelines} /> */}

          {/* authentication pages */}
          <Route path="/signup" component={SignUp} />
          <Route path="/confirmation" component={SignUpConfirmation}/>
          <Route path="/reset-password" component={ResetPassword}/>
          <Route path="/login" component={Login} />

          {/* dashboard pages */}
          {/* THIS WAS PRIVATE ROUTE need to add user={this.props.user}*/}
          <PrivateRoute path="/dashboard" component={Dashboard}  user={this.props.user}/>

          <Route path="/handle/payment" component={HandlePayment} />

          {/* TODO: view profile */}
          {/* <Route path="/users/:id" component={Profile} /> */}

          <Route path={'/not-found'} component={NotFound} /> 
          <Redirect to={'/not-found'} component={NotFound} /> 
        </Switch>
        </div>
      </Router>
    ); 
  }
}
const mapStateToProps = state => {
  return {
      user: state.housingApp.user 
  }
}
const mapDispatchToProps  = dispatch => {
  return{
      onLoginUser: (user) => dispatch(loginUser(user)), 
      onResizeApp: (windowWidth) => dispatch(resizeApp(windowWidth))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);

