import React from 'react'
import { Route, Redirect } from "react-router-dom"

import { store } from '../store/storeConfig.js'
import { reset } from '../store/actions/passThrough.js'
import { SAVE_ROUTE } from './functions.js'
// Private route checks if user token is present in Redux store

const checkAuth = (props) => {
    let user = JSON.parse(localStorage.getItem("user"));

    console.log(props)
    if(!user){
        // DO NOT SWITCH THE ORDER OF THESE FUNCTION CALLS
        store.dispatch(reset(false))
        SAVE_ROUTE(props.location.pathname)
        return false
    }
    return true;
}

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props => checkAuth(props)
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/login' }} />}
    />
);

export default PrivateRoute;