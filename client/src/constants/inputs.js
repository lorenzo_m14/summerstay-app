import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { FormControlLabel, Checkbox, TextField, Slider } from '@material-ui/core'
import { Helmet } from 'react-helmet';
import NLButton from '../components/utilities/NoLinkButton.js'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import '../CSS/DatePicker.css'
import { DateRangePicker, DayPickerRangeController } from 'react-dates'
import { MIN_RENTAL_DAYS, NUM_DISPLAY_MONTHS } from './index.js'
 
const CustomInput = withStyles({
    root: {
        backgroundColor: '#fff',
        borderRadius: 5,
        fontFamily: 'Helvetica Neue, sans-serif',
        '& label.Mui-focused': {
            color: '#454545',
        },
        '& label.Mui-disabled': {
            color: '#e0e0e0',
        },

        '& .MuiInput-underline:after': {
            borderBottomColor: '#454545',
        },
        '& .MuiSelect-select:focus':{
            backgroundColor:'#f9fafc',
            borderRadius: 5
        },
        '& .MuiInputBase-root':{
            '&.Mui-disabled':{
                color:'#e0e0e0'
            },
            '&.Mui-disabled fieldset':{
                borderColor: '#e0e0e0',
            },
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor:  '#e0e0e0',
            },
            '&:hover fieldset': {
                borderColor: '#d5d9e0',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#d5d9e0',
            },
            
        },

    },
})(TextField);

const AltCustomInput = withStyles({
    root: {
        backgroundColor: '#fff',
        borderRadius: 30,
        borderWidth: 0,
        textIndent: 50,
        fontFamily: 'Helvetica Neue, sans-serif',
        boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.1), 0 3px 10px 0 rgba(0, 0, 0, 0.1)',
        '& label.Mui-focused': {
            color: '#454545',
        },
        '& label.Mui-disabled': {
            color: '#fff',
        },

        '& .MuiInput-underline:after': {
            borderBottomColor: '#454545',
        },
        '& .MuiSelect-select:focus':{
            backgroundColor:'#fff',
            borderRadius: 30
        },
        '& .MuiInputBase-root':{
            '&.Mui-disabled':{
                color:'#fff'
            },
            '&.Mui-disabled fieldset':{
                borderColor: '#fff',
                borderRadius: 30
            },
        },
        '& .MuiOutlinedInput-inputAdornedStart':{
            paddingLeft: 10
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor:  '#fff',
                borderRadius: 30
            },
            '&:hover fieldset': {
                borderColor: '#fff',
                borderRadius: 30
            },
            '&.Mui-focused fieldset': {
                borderColor: '#fff',
                borderRadius: 30
            },
            
        },

    },
})(TextField);


const CustomSlide = withStyles({
    root: {
      color: '#6c4ef5',
      padding: '13px 0',
    },
    thumb: {
      height: 17,
      width: 17,
      backgroundColor: '#fff',
      border: '1px solid #6c4ef5',
    //   marginTop: -12,
    //   marginLeft: -13,
      boxShadow: '#f9fafc 0px 2px 2px',
      '&:focus,&:hover,&$active': {
        boxShadow: '#ccc 0px 2px 3px 1px',
      },
      '& .bar': {
        // display: inline-block !important;
        height: 9,
        width: 1,
        backgroundColor: '#a6a6a6',
        marginLeft: 1,
        marginRight: 1,
      },
    },
    active: {},
    valueLabel: {
    //   left: 'calc(-50% + 4px)',
    },
    track: {
      height: 5,
      
    },
    rail: {
      color: '#a6a6a6',
      opacity: .8,
      height: 5,
    },
})(Slider);

function CustomThumbComponent(props) {
return (
    <span {...props}>

    </span>
);
}
// const CustomInput = withStyles(theme => ({
//     root: {
//       'label + &': {
//         marginTop: 15, 
//       },
//       transition: theme.transitions.create(['border-color']),
//       borderRadius: 3,
//       position: 'relative',
//       backgroundColor: '#ffffff',
//       border: '1px solid #e0e0e0',
//       '&:hover': {
//         borderRadius: 3,
//         borderColor: '#1A273E',
//       },
//       '&:focus': {
//         borderRadius: 3,
//         borderColor: '#1A273E',
//       },
//     },
//     error:{
//         borderRadius: 3,
//         border: '1px solid red',
//     },
//     input: {
//       minWidth: 175,
//       position: 'relative',
//       fontSize: 15,
//       padding: '15px 20px 15px 10px',
      
//       // Use the system font instead of the default Roboto font.
//       fontFamily: [
//         '-apple-system',
//         'BlinkMacSystemFont',
//         '"Segoe UI"',
//         'Roboto',
//         '"Helvetica Neue"',
//         'Arial',
//         'sans-serif',
//         '"Apple Color Emoji"',
//         '"Segoe UI Emoji"',
//         '"Segoe UI Symbol"',
//       ].join(','),
      
//     },
    
// }))(InputBase);

const CustomCheck = withStyles({
    root: {
      color: '#e0e0e0',
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&$checked': {
        color: '#6c4ef5',
      },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

const useStyles = makeStyles(theme => ({
    rootF: {
        marginTop: 10,
        marginBottom: 10
    },
    root: {
        minWidth: 200,
        marginTop: 10,
        marginBottom: 10
    },
}));
  
export function CustomSelect(props) {
    const classes = useStyles();
    
    return (
        
            <CustomInput
                className={props.full ? classes.rootF : classes.root}
                fullWidth={props.full}
                disabled={props.disabled} 
                error={props.error}
                select
                label={props.label}
                value={props.value}
                onChange={props.onChange}
                variant="outlined"
                placeholder={props.placeholder}
            >
                {
                    props.children
                }
            </CustomInput>  
    );
  }

export function CustomCheckbox(props){
    return (
        <div style={{display:'flex', alignItems:'center'}}>
            <FormControlLabel
                control={
                    <CustomCheck checked={props.checked} onChange={props.onChange}/>
                }
                value={props.value}
            />
            <div style={{display:'flex', flexDirection:'column'}}>
                {
                    !props.children ? 
                        <p className="SoftBold" style={{margin: 0}}>{props.label}</p>
                    : props.children
                }
                
                {
                    props.subText && <p className="ExtraSmallText SoftText" style={{margin:0}}>{props.subText}</p>
                }
            </div>
        </div>
        
    );
}

export function CustomTextbox(props){

    return (
        
        <CustomInput
            style={props.style}
            fullWidth={props.full}
            // disabled={props.disabled} 
            // type={`${props.type}`}
            // error={props.error}
            // label={props.label}
            // autoComplete={`${props.autoComplete}`}
            // multiline={props.multiline}
            // rows={props.rows}
            // value={props.value}
            // required={props.required}
            onChange={props.onChange}
            // InputProps={props.InputProps}
            // onInput={props.onInput}
            variant="outlined"
            // onKeyPress={props.onKeyPress}
            // placeholder={props.placeholder}
            {...props}
        />
        
    );
}
export function AltCustomTextbox(props){

    return (
        
        <AltCustomInput
            style={props.style}
            fullWidth={props.full}
            onChange={props.onChange}
            variant="outlined"
            {...props}
        />
        
    );
}

export function CustomSlider(props){

    return(
        <CustomSlide
            ThumbComponent={CustomThumbComponent}
            onChange={props.onChange}
            // getAriaLabel={index => (index === 0 ? 'Minimum price' : 'Maximum price')}
            {...props}
        />
    )
}

export function DatePicker(props){

    return(
        <DateRangePicker
            // onChange={props.onChange}
            startDateId="startDate"
            endDateId="endDate"
            minimumNights={MIN_RENTAL_DAYS}
            numberOfMonths={props.numberOfMonths || NUM_DISPLAY_MONTHS }
            hideKeyboardShortcutsPanel={true}
            // orientation={}
            // getAriaLabel={index => (index === 0 ? 'Minimum price' : 'Maximum price')}
            {...props}
        />
    )
}

export function OpenedDatePicker(props){

    return(
        <DayPickerRangeController
            // startDateId="startDate"
            // endDateId="endDate"
            minimumNights={MIN_RENTAL_DAYS}
            numberOfMonths={props.numberOfMonths || NUM_DISPLAY_MONTHS }
            hideKeyboardShortcutsPanel={true}
            {...props}
        />

    )
}

export function CustomCount(props){
    const { title, min, max, step, value, error, disabled} = props
    const val = value ? value : 0
    return(
        <div style={{display:'flex', alignItems:'center', justifyContent: 'space-between', maxWidth: 300}}>
            <p className="SoftBold" style={{margin: 0, marginRight: 10, color: error ? '#e62929' : '#484848' }}>{title}</p>
            <div style={{display:'flex',alignItems:'center'}}>
                <div>
                    <NLButton title='-' onClick={()=>(val !== min && !disabled) ? props.onChange(val-step) : undefined} 
                        type={(val !== min && !disabled) ? 'CircleButton ExtraSmallCirlce LargeText Purple' : 'CircleButton ExtraSmallCirlce Disabled LargeText'}/>
                </div>
                <p className="SoftBold" style={{margin: 0, textAlign:'center',marginLeft: 10, marginRight: 10, 
                            width: 25, color: error ? '#e62929' : '#484848'}}>{val}</p>
                <div>
                    <NLButton title='+' onClick={()=>(val !== max && !disabled) ? props.onChange(val+step) : undefined} 
                        type={(val !== max && !disabled) ? 'CircleButton ExtraSmallCirlce LargeText Purple' : 'CircleButton ExtraSmallCirlce Disabled LargeText'}/>
                </div>
            </div>
        </div>
    )
}

// TODO: use these for Helmet page titles later
export function headMetadata(title) {
    if (title) {
        const headTitle = title + "- SummerStay"
        return (
            <Helmet>
                <title>{headTitle}</title>
            </Helmet>
        )
    }
}