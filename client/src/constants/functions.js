import { MIN_PASSWORD_LENGTH } from './index.js'

export function DISPLAY_NUMBER(x) {

    return (x/100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

export function HUMANIZE(str) {
    var frags = str.split('_');
    for (let i=0; i<frags.length; i++) {
        frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
}

// Used to make an dictanaty of arrays into an array of dictanaries.
// Arrays must be the same length
export function MERGE_ARRAYS(inputObject){
    var constructedArray = []
    var keys = Object.keys(inputObject)
    var values =  Object.values(inputObject)
    for(let j = 0; j < values[0].length; j++){
        let temp = {}
        for(let i = 0; i < keys.length; i++){
            temp[keys[i]] = values[i][j]
        }
        constructedArray.push(temp)
    }
    return constructedArray
}

export function PARSE_QUERY(queryString){
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}

export function BUILD_QUERY(parameters) {
    var qs = "";
    for(var key in parameters) {
        var value = parameters[key];
        qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
    }
    return qs;
}

export function LOCAL_EMAIL_CHECK(emailString){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(emailString).toLowerCase());
}

export function LOCAL_PASSWORD_CHECK(passwordSring){
    var re = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*[\W_]).{8,50}$/
    return re.test(String(passwordSring)) && passwordSring.length >= MIN_PASSWORD_LENGTH
}
export function SANITIZE_PHONE_NUMBER(phoneNumberString){
    var number = '+' + phoneNumberString.replace(/\D/g,'')
    return number
}

// checks for at least 11 digits, ignores nonnumbers
export function IS_VALID_PHONE(value) {
    return (/^\d{11,}$/).test(value.replace(/[\s()+\-\.]|ext/gi, ''));
}

//made incase bad login credentials are passed. I need different local storage behavior and routes
export function CHECK_RESET(route){
    if(route === '/login' || route === '/signup'){
        return false
    }
    return true
}

export function SAVE_ROUTE(route){

    if(CHECK_RESET(route)){
        const rr = {
            route: route
          }
        localStorage.setItem('returnRoute', JSON.stringify(rr))  
        console.log('Setting redirect url')
        console.log(rr)
    }
}

export function IS_FB_APP() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    console.log(ua);
    return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1) || (ua.indexOf('Instagram') > -1)
}