// Let's put reusable constants here!
// use process.env to import env variables
export const HOST_URL = process.env.REACT_APP_HOST_URL
export const CLIENT_URL = process.env.REACT_APP_CLIENT_URL
export const FB_APP_ID = process.env.REACT_APP_FACEBOOK_KEY
export const STRIPE_CLIENT = process.env.REACT_APP_STRIPE_CLIENT_ID
export const STRIPE_API_KEY = process.env.REACT_APP_STRIPE_API_KEY
export const PLAID_API_KEY = process.env.REACT_APP_PLAID_API_KEY
export const PLAID_ENV = process.env.REACT_APP_PLAID_ENV
export const PASSBASE_API_KEY = process.env.REACT_APP_PASSBASE_API_KEY
export const DISABLE_ID_VERIFICATION = Boolean(process.env.REACT_APP_DISABLE_ID_VERIFICATION)
export const SHOULD_DEBUG_FB = Boolean(process.env.REACT_APP_DEBUG_FACEBOOK)
export const REACT_APP_GA_TRACKING_ID = process.env.REACT_APP_GA_TRACKING_ID

// export const DD_CLIENT = process.env.REACT_DATA_DOG_CLIENT_KEY
export const MAX_NUM_IMAGES = 10
export const MAX_IMAGE_SIZE = 10000000

export const MIN_PASSWORD_LENGTH = 8
export const PASSWORD_ERROR = `Your password must contain at least ${MIN_PASSWORD_LENGTH} characters, a capital letter, a number, and a special character.`

export const SMALL_COMPUTER_SIZE = 1000
export const SIZE_1100 = 1100
export const SIZE_1200 = 1200
export const SIZE_1300 = 1300
export const SIZE_500 = 500
export const LARGE_PHONE_SIZE = 800
export const MEDIUM_PHONE_SIZE = 600
export const SMALL_PHONE_SIZE = 400

// Constants for Date Selection
export const DATE_FORMAT ='YYYY-MM-DD'
export const MIN_RENTAL_DAYS = 21 
export const NUM_DISPLAY_MONTHS = 2

export const GENERAL_ERROR = 'We aren\'t sure what happened here. That\'s on us. Try reloading the page or logout and back in.'
export const AMENITIES = { // if you add something make sure to add an icon to iconList.js
    none: "No Amenities",
    air: 'A/C',
    furnished: 'Furnished',
    gym: 'Gym',
    heating: 'Heating',
    internet: 'Wifi',
    kitchen: 'Kitchen',
    laundry: 'Laundry',
    parking: 'Parking',
    pool: 'Pool',
    tv: 'TV',
    pet: 'Pet Friendly'
}
export const USER_TYPES = {
    student: "Student", 
    nonStudent: "Not A Student"
}

export const LISTING_TYPES = ['Entire Place', 'Single Room', 'Shared Room']

export const SCHOOL_NAMES = [
    'American University',
    'Amhurst University',
    'Boston College',
    'Boston University',
    'Brandeis University',
    'Brown University',
    'California Institute of Technology',
    'Carnegie Mellon University',
    'Case Western Reserve University',
    'College of William and Mary',
    'Columbia University',
    'Cornell University',
    'Dartmouth College',
    'Duke University',
    'Emory University',
    'George Washington University',
    'Georgetown University',
    'Georgia Institute of Technology',
    'Harvard University',
    'Howard University',
    'Johns Hopkins University',
    'Lehigh University',
    'Massachusetts Institute of Technology',
    'New York University',
    'Northeastern University',
    'Northwestern University',
    'Pennsylvania State University-University Park',
    'Pepperdine University',
    'Princeton University',
    'Rensselaer Polytechnic Institute',
    'Rice University',
    'Rutgers University-New Brunswick',
    'Stanford University',
    'Syracuse University',
    'Texas A&M University-College Station',
    'Tufts University',
    'Tulane University',
    'University of California-Berkeley',
    'University of California-Davis',
    'University of California-Irvine',
    'University of California-Los Angeles',
    'University of California-San Diego',
    'University of California-Santa Barbara',
    'University of Chicago',
    'University of Florida',
    'University of Georgia',
    'University of Illinois-Urbana-Champaign',
    'University of Miami',
    'University of Michigan-Ann Arbor',
    'University of North Carolina-Chapel Hill',
    'University of Notre Dame',
    'University of Pennsylvania',
    'University of Rochester',
    'University of Southern California',
    'University of Texas-Austin',
    'University of Virginia',
    'University of Washington',
    'University of Wisconsin-Madison',
    'Vanderbilt University',
    'Villanova University',
    'Wake Forest University',
    'Washington University in St. Louis',
    'Worcester Polytechnic Institute',
    'Yale University',
]

// TODO: use these for Helmet page titles later
export const PAGE_TITLE = {
    Home: "Housing For Students and Interns",
    
    Login: "Login",
    Signup: "Signup",
    ResetPassword: "Reset Password",

    TermsAndConditions: "Terms and Conditions",
    Support: "Support",
    PrivacyPolicy: "Privacy Policy",
    Policies: "Policies Page",

    PayAsYouStay: "Payment Plans",
    HowItWorks: "How It Works",
    CampusManagers: "Campus Managers",
    HostLanding: "List Your Place",

    BookListing: "Book Your Stay",
    ListingsMain: "Find Your SummerStay",
    ListingsSub: "Find Your SummerStay",

    Dashboard: "Dashboard",
}
