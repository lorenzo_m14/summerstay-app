import { HOST_URL, GENERAL_ERROR } from './index.js'
import { SAVE_ROUTE } from './functions.js'
import { history } from '../App.js'
import { store } from '../store/storeConfig.js'
import { reset } from '../store/actions/passThrough.js'

const redirect=(route)=>{
  history.push(route)
}
const unauthorized=()=>{
  console.log("Unauthorized request")
  // DO NOT SWITCH THE ORDER OF THESE FUNCTIONS
  store.dispatch(reset(false))
  SAVE_ROUTE(window.location.pathname)
  history.replace('/login')
}

const getHeaders=()=>{
  return {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
}

const parseResponse=(r)=>{
  return r.json().then(data => ({status: r.status, data: data}))
}

const handleResponse=(resJson, resolve, reject)=>{
  console.log(resJson.data);
    switch (resJson.status) {
    // OK Responses
      case 200: 
        return resolve(resJson.data)
      case 201: 
        return resolve(resJson.data)
      case 304: 
        return resolve(resJson.data)

    // Failed Responses
      case 400: // BAD REQUEST
        return reject(resJson.data)
      case 401: // UNAUTHORIZED
        unauthorized()
        return reject(resJson.data)
      case 403: // FORBIDDEN
        return reject(resJson.data)
      case 404: // NOT FOUND
        return redirect('/error/404')
      case 422: // Unprocessable Entity
        return reject(resJson.data)
      case 500: // INTERNAL SERVER ERROR
        
        
        return redirect('/error/500')

      default: // GENERAL USE CASE
        return reject(GENERAL_ERROR)
    }
}

// HTTP GET Request - Returns Resolved or Rejected Promise
export const GET = (path) => {
  return new Promise((resolve, reject) => {
    fetch(path, {
        method: 'GET',
        proxy: `${HOST_URL}`,
        headers: getHeaders(),
    })
    .then( parseResponse )
    .then((resJson)=> handleResponse(resJson, resolve, reject))
  });
};

// HTTP POST Request - Returns Resolved or Rejected Promise
export const POST = (path, body) => {
    return new Promise((resolve, reject) => {
      fetch(path, {
          method: 'POST',
          proxy: `${HOST_URL}`,
          headers: getHeaders(),
          body: JSON.stringify(body)
      })
      .then( parseResponse )
      .then((resJson)=> handleResponse(resJson, resolve, reject))
    });
};

// HTTP PUT Request - Returns Resolved or Rejected Promise
export const PUT = (path, body) => {
  return new Promise((resolve, reject) => {
    fetch(path, {
        method: 'PUT',
        proxy: `${HOST_URL}`,
        headers: getHeaders(),
        body: JSON.stringify(body)
    })
    .then( parseResponse )
    .then((resJson)=> handleResponse(resJson, resolve, reject))
  });
};

// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const DELETE = (path, body) => {
  return new Promise((resolve, reject) => {
    fetch(path, {
        method: 'DELETE',
        proxy: `${HOST_URL}`,
        headers: getHeaders(),
        body: JSON.stringify(body)
    })
    .then( parseResponse )
    .then((resJson)=> handleResponse(resJson, resolve, reject))
  });
};
