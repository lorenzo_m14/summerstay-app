# # NOTE: THIS CONTROLLER IS CURRENTLY DEPRECATED IN FAVOR OF DISPLAYING CHARGES WITH RESERVATION
# class Api::V1::ChargesController < ApplicationController
#     before_action :authenticate_user_from_token!


#     # get all of a user's invoices for reservation
#     def index
#       reservation_id = params[:reservation_id]
#       reservation = Reservation.find_by_id(reservation_id)
#       if reservation.nil?
#         render json: {
#           error: "Can't find charges for reservation",
#           is_success: false
#         }, status: :bad_request 
#         return
#       end
#       rc = reservation.reservation_cost
#       charges = reservation.charges

#       if charges.count == 0
#         render json: {error: "no charges", is_success: false}, status: :bad_request
#         return
#       end

#       invoices = []

#       charge0 = charges.first
#       invoice0 = Stripe::Invoice.retrieve(charge0&.stripe_charge_id)
#       invoices.append(invoice0)

#       # display invoices for charges
#       if rc.upfront?
#         charge1 = charges.last
#         invoice1 = Stripe::Invoice.retrieve(charge0.stripe_charge_id)
#         invoices.append(invoice1)
#       elsif rc.continuous?
#         # retrieve subscription
#         charge1 = charges.last
#         sub_id = charge1.subscription_id
#         sub_invoice_data = Stripe::Invoice.list(subscription: sub_id)
#         if !sub_invoice_data.nil?
#           invoices.append(sub_invoice_data&.data)
#         end
#       end

#       invoices_serializer = invoices.map{ |i|
#         InvoiceSerializer.new(i)
#       }

#       render json: {
#         charges: invoices_serializer,
#         is_success: true
#       }, status: :ok
#     end
  
#     # show a user's charge
#     def show
#       charge_id = params[:id]
#       charge = Charge.find_by_id(charge_id)

#       if !charge.nil?
#         # check if charge belongs to user
#         if charge.user_id == current_user.id
#           charge_serializer = ChargeSerializer.new(charge)
#           render json: {
#             charge: charge_serializer,
#             is_success: true
#           }, status: :ok
#         else
#           render json: {
#             error: "Invalid charge id for user",
#             is_success: false
#           }, status: :bad_request
#         end
#       else
#         puts "CHARGE IS NIL"
#         render json: {
#           error: "Invalid charge id",
#           is_success: false
#         }, status: :bad_request
#       end
#     end

#     def update
#       # if charge has not yet been paid, allow user to change payment method
#     end

#     # TODO: list charges for given reservation
#     def charges_by_reservation
#       reservation_id = params[:id] # reservation id

#       reservation = Reservation.find_by_id(reservation_id)
#       if reservation.nil? || reservation.user_id != current_user.id
#         render json: {error: "Cannot retrieve charges for reservation", is_success: false}, status: :bad_request
#       else

#         charges = Charge.where(user_id: current_user.id, reservation_id: reservation_id)

#         charges_serializer = charges.map{ |c|
#           stripe_charge = nil
#           if !c.stripe_charge_id.nil?
#             # retrieve stripe charge
#             stripe_charge = Stripe::Charge.retrieve(c.stripe_charge_id)
#           end
#           ChargeSerializer.new(c,stripe_charge: stripe_charge)
#         }

#         render json: {
#           charges: charges_serializer,
#           is_success: true
#         }, status: :ok
#       end
#     end

#     def get_receipt_url
#       invoice_id = params[:stripe_invoice_id]
#       invoice = Stripe::Invoice.retrieve(invoice_id)

#       # user must be the customer for the invoice
#       if !invoice.nil? && current_user.stripe_id == invoice.customer
#         charge_id = invoice&.charge
#         charge = Stripe::Charge.retrieve(charge_id)
#         if !charge.nil? && !charge&.receipt_url.nil?
#           render json: {receipt_url: charge.receipt_url, is_success: true}, status: :ok
#           return
#         end
#       end
#       render json: {error: "Unable to retrieve receipt", is_success: false}, status: :bad_request
#     end
#   end
  