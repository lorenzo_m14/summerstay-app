require 'twilio-ruby'
require 'open-uri'

class Api::V1::UsersController < ApplicationController
    before_action :authenticate_user_from_token!, except: [:facebook_login, :facebook]
    before_action :set_phone_number, only: [:send_phone_token, :verify_phone_number]

    def facebook_login
        if params[:facebook_access_token]
            graph = Koala::Facebook::API.new(params[:facebook_access_token])
            user_data = graph.get_object("/me?fields=email,gender, birthday,first_name,last_name,id,picture")

            if user_data['email'].blank?
                render json: { error: "Facebook account must have a valid email!", is_success: false}, status: :bad_request
                return
            end

            reset_session
            sign_out

            user = User.find_by(email: user_data['email'])
            if user
                user.generate_authentication_token
                user.save

                sign_in(user)
                set_session(user)
                user_serializer = UserSerializer.new(user)
                render json: {
                    user: user_serializer,
                    created: false,
                    provider: "facebook",
                    is_success: true
                }, status: :ok
            else
                render json: {error: "Please sign up with your Facebook account before logging in", is_success: false}, status: :bad_request
            end
        else
            render json: { error: "Facebook authentication failed!", is_success: false}, status: :unprocessable_entity
        end
    end

    def facebook 
        if params[:facebook_access_token]
            graph = Koala::Facebook::API.new(params[:facebook_access_token])
            user_data = graph.get_object("/me?fields=email,gender, birthday,first_name,last_name,id,picture")

            if user_data['email'].blank?
                render json: { error: "Facebook account must have a valid email!", is_success: false}, status: :bad_request
                return
            end

            reset_session
            sign_out

            user = User.find_by(email: user_data['email'])
            if user
                user.generate_authentication_token
                user.save

                sign_in(user)
                set_session(user)
                user_serializer = UserSerializer.new(user)
                render json: {
                    user: user_serializer,
                    created: false,
                    provider: "facebook",
                    is_success: true
                }, status: :ok
            else
                fb_image = user_data.dig("picture", "data", "url")
                user = User.new(
                            first_name: user_data['first_name'],
                            last_name: user_data['last_name'],
                            email: user_data['email'],
                            password: Devise.friendly_token + "Aa1!", # "Aa1! is to satisfy password complexity enforcement"
                            uid: user_data['id'],
                            provider: 'Facebook',
                            fb_image: fb_image
                        )

                user.skip_confirmation!
                user.generate_authentication_token

                if user.save
                    if fb_image.present?
                        begin
                            Log.info("Saving user Facebook image to activestorage", level: "INFO", type: "Signup", action: "facebook") 
                            downloaded_image = open(fb_image)
                            user.image.attach(io: downloaded_image  , filename: "avatar.jpg")
                        rescue OpenURI::HTTPError => ex
                            Log.error("Unable to download Facebook image!", level: "ERROR", type: "Signup", action: "facebook") 
                        end 
                    end

                    Log.debug "Send welcome email to #{user.fullname}"
                    response = AccountMailer.new.welcome(user)

                    sign_in(user)
                    set_session(user)
                    user_serializer = UserSerializer.new(user)
                    render json: {
                        user: user_serializer,
                        created: true,
                        provider: "facebook",
                        is_success: true
                    }, status: :created
                else
                    render json: {error: user.errors, is_success: false}, status: 422
                end
            end
        else
            render json: { error: "Facebook authentication failed!", is_success: false}, status: :unprocessable_entity
        end
    end

    def logout
        user = User.find_by_id(session[:user_id])
        user.generate_authentication_token
        user.save
        reset_session
        sign_out
        render json: { is_success: true}, status: :ok
    end

    def me
        user_serializer = UserSerializer.new(current_user)
        render json: {user: user_serializer}, status: :ok
    end

    # TODO: Fix to change privacy of other users
    def show
        user_id = params[:id]
        user = User.find_by_id(user_id)
        if user.blank?
            render json: { error: "User not found!", is_success: false}, status: :bad_request
        else
            render json: {user: UserProfileSerializer.new(user)}, status: :ok
        end
    end

    def update_user
        new_params = user_params
        user = current_user
        if user.update(new_params)
            user_serializer = UserSerializer.new(user)
            render json: { user: user_serializer, is_success: true }, status: :ok
        else
            render json: { error: user.errors, is_success: false}, status: :bad_request
        end
    end

    # Phone number verification + update
    # should send phone number including the '+' sign
    def send_phone_token
        # first check if phone number exists in db
        if User.exists?(phone_number: @phone_number)
            render json: { error: "Phone number already exists for an account"}, status: :bad_request and return
        end

        verification = TwilioClient.new.send_token(@phone_number)
        # TODO: refactor this
        if verification.status == "pending"
            render json: { phone_number: @phone_number, is_success: true }, status: :ok
        else
            Log.error(verification, level: "ERROR", type: "Phone", action: "send_phone_token")
            render json: { error: "Unable to deliver SMS"}, status: :bad_request
        end
    end

    def verify_phone_number
        token = params[:token]
        verification_check = TwilioClient.new.check_token(@phone_number, token)

        if verification_check.valid
            Log.info("Phone number verified! Saving phone number to user", level: "INFO", type: "Phone", action: "verify_phone_number")
            current_user.update!(phone_number: @phone_number)
            render json: { message: "Successfully updated phone number", phone_number: @phone_number, is_success: true }, status: :ok
        else
            Log.error(verification, level: "ERROR", type: "Phone", action: "verify_phone_number")
            render json: { error: "Invalid verification code. Please try again."}, status: :bad_request
        end
    end

    # NOT CURRENTLY USED
    # TODO: implement this for sendgrid_emails
    def sendgrid_email_lead
        email = params[:email]
        activation = params[:activation] # activation where email is captured

        response = SendgridClient.new.capture_email_lead(email)
        render json: { is_success: true }, status: :ok
    end

    # NOTE: Currently referrals are inactive
    ### Campus Rep ###

    # # get all listings that use the current users referral code
    # def get_listings_with_ref
    #     rc = current_user.referral_code
    #     if !rc.blank?
    #         listings = Listing.where(referral_code_id: rc.id)

    #         render json: { listings: listings.map{ |listing|
    #             ListingShowSerializer.new(listing)
    #         },
    #     }, status: :ok
    #     else
    #         render json: { error: "User is not a campus representative"}, status: :bad_request
    #     end
    # end

    # # create a campus ref code for a user if none exists
    # def create_campus_ref
    #     # generate a token to be used as a referral code in listings
    #     error = current_user.generate_campus_rep
    #     if error.nil?
    #         render json: {
    #             user: UserSerializer.new(current_user),
    #             is_success: true
    #         }, status: :created
    #     else
    #         render json: { error: error}, status: :bad_request
    #     end
    # end

    private 
        # DEPRECATED IN FAVOR OF SESSION COOKIES FOR SECURITY
        # def set_response_headers(auth_token, email)
        #     response.set_header("X-User-Token", auth_token)
        #     response.set_header("X-User-Email", email)
        # end

        def set_phone_number
            @phone_number = params[:phone_number]
            if @phone_number.blank?
                render json: { error: "Invalid phone number"}, status: :bad_request
            end
        end

        def set_session(user)
            session[:user_id] = user.id
            session[:token] = user.authentication_token
          end

        def user_params
            params.require(:user).permit(
                :description, :first_name, :last_name, :image, :school_name, :user_type
            )
        end
end
