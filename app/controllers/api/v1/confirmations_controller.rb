class Api::V1::ConfirmationsController < Devise::ConfirmationsController
  
    def show
        reset_session
        self.resource = resource_class.confirm_by_token(params[:confirmation_token])
        if resource.errors.empty?
            sign_out

            # Server Login
            user = resource
            user.update(provider: 'Email')
            user.generate_authentication_token
            user.save!

            sign_in(user)
            set_session(user)

            response = AccountMailer.new.welcome(user)

            render json: {
                user: UserSerializer.new(user), 
                is_success: true
            }, status: :created
        else
            Log.error(resource.errors.to_json, level: "ERROR", type: "Confirm")
            render json: {error: resource.errors, is_success: false}, status: 400
        end
    end

    private

    # def set_response_headers(auth_token, email)
    #     response.set_header("X-User-Token", auth_token)
    #     response.set_header("X-User-Email", email)
    # end

    def set_session(user)
        session[:user_id] = user.id
        session[:token] = user.authentication_token
    end
  
  end