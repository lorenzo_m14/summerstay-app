class Api::V1::PasswordsController < Devise::PasswordsController
  before_action :authenticate_user_from_token!, except: [:create, :edit, :update]
  # skip_before_action :authenticate_user!, only: :edit

  def create
    user = User.find_by(email: params[:email])
    if user.blank?
      render json: {error: "Invalid email!"}, status: :bad_request and return
    end 
    token = user.send_reset_password_instructions
    AccountMailer.new.reset_password(user,token)
    render json: {is_success: true}, status: :created 
  end

  def edit
    user = User.with_reset_password_token(params[:reset_password_token])
    if user.blank?
      render json: {error: "Invalid reset password token", is_success: false}, status: :bad_request
    else 
      render json: {is_success: true}, status: :ok
    end
  end

  def update
    self.resource = User.reset_password_by_token(params)
    if resource.errors.empty?
        # Server Login
        user = resource
        user.generate_authentication_token

        sign_in(user)
        set_session(user)
        user.save

        render json: {
            user: UserSerializer.new(user),
            is_success: true
        }, status: :ok and return
    else
        render json: {error: resource.errors, is_success: false}, status: :bad_request and return
    end
  end

  private 

  def set_session(user)
    session[:user_id] = user.id
    session[:token] = user.authentication_token
  end
end