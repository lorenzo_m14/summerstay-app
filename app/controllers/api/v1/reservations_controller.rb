class Api::V1::ReservationsController < ApplicationController
  include Constants::Reservations
  include Constants::Events
  before_action :authenticate_user_from_token!
  before_action :set_reservation, only: [:show, :approve, :decline, :cancel, :own_stays_show, :compute_payouts]
  before_action :redis_connected?, only: [:create, :approve, :cancel]

  def show
    reservation_serializer = OwnReservationSerializer.new(@reservation, is_accepted: @reservation.accepted?)
    render json: { reservation: reservation_serializer, is_success: true }, status: :ok
  end

  # TODO: Clean this method up, move logic to model
  def create
    listing = Listing.find(params[:listing_id])
    charge_type = params[:charge_type]
    payment_method_id = params[:payment_method_id]
    request_message = params[:request_message] # TODO: make this less than designated amount of chars

    error = nil
    if listing.Waiting?
      error = "Cannot book for this listing"
    elsif current_user == listing.user
      error = "You cannot book your own property."
    elsif current_user.stripe_id.blank?
      error = "Please update your payment method."
    elsif ENV['DISABLE_ID_VERIFICATION'].blank? && (current_user.Unapproved? || current_user.Denied?)
      error = "Please verify your identity before making a booking request."
    elsif listing.reservations.find_by(user_id: current_user.id, status: [:Waiting, :Approved])
      error = "You have already made a reservation for this listing."
    end

    if !error.nil?
      render json: { error: error, is_success: false}, status: :bad_request
    else
      # TODO: Include graceful date parsing!
      start_date = Date.parse(reservation_params[:start_date])
      end_date = Date.parse(reservation_params[:end_date])

      reservations = Reservation.where(
        "listing_id = ? AND (start_date <= ? AND end_date >= ?) AND status = ?",
        listing.id, end_date, start_date, 1
      ).count

      # check if dates are valid (cannot book on current day)
      error = nil
      in_listing_dates = start_date < listing.start_date || listing.end_date < end_date
      # TODO: figure out timezones solution
      if start_date < Date.current || end_date < Date.current
        error = "Reservation must be in the future!"
      elsif end_date < start_date || in_listing_dates
        error = "Invalid dates for reservation!"
      elsif reservations > 0
        error = "Listing is booked during these dates!"
      elsif (end_date - start_date).to_i + 1 <= MIN_RESERVATION_LENGTH # can't have reservation less than 3 weeks
        error = "Reservation cannot be less than 3 weeks"
      end

      if !error.nil?
        render json: { error: error, is_success: false }, status: :bad_request
        return
      end

      # Make a reservation
      reservation = current_user.reservations.build(reservation_params)
      reservation.listing = listing
      reservation.host = listing.user
      reservation.request_message = request_message

      # Check if expiry is after start date
      if Date.current + RESERVATION_EXPIRY_DAYS.days >= start_date
        reservation.expires_at = start_date.in_time_zone.to_time
      else
        reservation.expires_at = DateTime.current + RESERVATION_EXPIRY_DAYS.days
      end

      payment_services = PaymentServices.new(reservation)
      payment_method, _customer = payment_services.update_customer_payment_method(payment_method_id)

      if payment_method.nil?
        render json: { error: "Invalid payment method for reservation!", is_success: false }, status: :bad_request
        return
      end

      # NOTE: NOT DOING RESERVATION REFERRAL CODES FOR NOW
      # update with referral code if valid
      # if params[:referral_code] && params[:referral_code] != ""
      #   puts "REFERRAL CODE PARAM"
      #   rc = ReferralCode.find_by(code: params[:referral_code])
      #   if !rc.blank?
      #     reservation.update(referral_code: rc)
      #   end
      # end

      if reservation.save
        # if reservation is created, add the reference to existing conversation
        conversation = Conversation.where(listing_id: listing.id).between(current_user, listing.user).first
        if !conversation.nil?
          conversation.reservation_id = reservation.id
        else 
          # TODO: Add in conversation creation, add automated first message
        end

        # calculate reservation costs and charges, and assign to reservation
        charges, errors = payment_services.create_reservation_charges(charge_type, payment_method.object, payment_method.id)

        # Create payment intents and charges objects
        if !errors.nil?
          puts errors
          render json: {error: errors, is_success: false}, status: :bad_request
          return
        else
          # sends emails and notification
          CreateRequestWorker.perform_async(reservation.id)

          # queue request reminder
          expiry_reminder_date = reservation.expires_at - 1.days
          if expiry_reminder_date > DateTime.current
            ExpiryReminderWorker.perform_in(expiry_reminder_date.in_time_zone.to_time, reservation.id, listing.user_id)
          else
            ExpiryReminderWorker.perform_async(reservation.id, listing.user_id)
          end

          ExpiredRequestWorker.perform_in(reservation.expires_at.in_time_zone.to_time, reservation.id)


          render json: {reservation: OwnReservationSerializer.new(reservation), is_success: true}, status: :created
        end
      else
        render json: {error: reservation.errors, is_success: false}, status: :bad_request
      end
    end
  end

  def own_stays
    stays = Reservation.where(user_id: current_user.id, status: [:Waiting, :Approved])
    reservations_serializer = stays.map { |r| 
      OwnReservationSerializer.new(r, is_accepted: r.accepted?)
    }
    render json: {stays: reservations_serializer, is_success: true}, status: :ok
  end

  def own_stays_show
    if @reservation.user != current_user
      render json: { error: "Invalid Reservation ID", is_success: false}, status: :bad_request
    else
      render json: { listing: OwnReservationSerializer.new(@reservation, is_accepted: @reservation.accepted?), is_success: true}, status: :ok
    end
  end

  def own_stays_charges
    stays = Reservation.where(user_id: current_user.id, status: [:Waiting, :Approved])
    reservations_serializer = stays.map { |r| 
      ReservationChargesSerializer.new(r, avatar_url: r.user.image) 
    }
    render json: {stays: reservations_serializer, is_success: true}, status: :ok
  end

  # return reservations by listing for host, showing only basic user info and reservation cost
  def reservations_by_listing
    reservations = Reservation.where(listing_id: params[:id]).where(status: [:Waiting, :Approved])
    reservations_serializer = reservations.map { |r| 
        ReservationSerializer.new(r, avatar_url: r.user.image) 
    }
    render json: {reservations: reservations_serializer, is_success: true}, status: :ok
  end

  # approve reservation request, charge renter, and change listing status to booked
  def approve
    if @reservation.listing.user_id == current_user.id
      listing = @reservation.listing
      error = nil
      # reservation must have status waiting to be approved
      if !@reservation.Waiting?
        error = 'Unable to approve reservation.'
      # must be valid date/not expired
      elsif @reservation.expires_at < DateTime.current
        error = 'Cannot approve expired reservation.'
      elsif ENV['DISABLE_ID_VERIFICATION'].blank? && !current_user.Approved?
        error = 'Your ID verification status must be approved. Please contact support for help.'
      elsif !current_user.is_active_host
        error = 'Please Connect to Stripe Express first.'
      elsif Reservation.where("listing_id = ? AND (start_date <= ? AND end_date >= ?) AND status = ?",listing.id, @reservation.end_date, @reservation.start_date, 1).count > 0
        # check if any other approved reservations overlap
        error = 'Listing is not available for this reservation length'
      end

      if !error.nil?
        render json: { error: error, is_success: false }, status: :bad_request
      else
        _reservation, approval_error = @reservation.approve
        unless approval_error.nil?
          render json: { error: approval_error, is_success: false }, status: :bad_request
          return
        end

        # deletes request expiry jobs,
        # adds premove, movein, moveout jobs, as well as payment reminders
        @reservation.approval_events

        # Do post processing job, check if listing still has valid windows for reservations
        if !listing.has_bookable_windows
          listing.update(bookable: false)
          Log.info("No more bookable windows for listing #{listing.id}", level: "INFO", listing_id: listing.id)
        end

        reservation_serializer = ReservationBookingSerializer.new(@reservation, last_name: true)
        render json: {reservation: reservation_serializer, is_success: true}, status: :ok
      end
    else
      render json: {error: "No Permission", is_success: false}, status: :bad_request
    end

    # rescue Stripe::CardError => e
    #   # the renter has an issue with their card
    #   Log.error(e.message, level: "ERROR", type: "Listing", action: "approve", error_type: "Stripe")
    #   render json: {error: e.message, is_success: false}, status: 400
    # rescue Stripe::InvalidRequestError => e
      # puts e.to_json
      # puts e.type
      # puts e.code
      # Log.error(e.message, level: "ERROR", type: "Listing", action: "approve")
      # render json: {error: e.message, is_success: false}, status: 400
  end

  def decline
    if @reservation.listing.user_id == current_user.id
      if @reservation.Waiting?
        reservation, error = @reservation.decline

        if !error.nil?
          render json: { error: error, is_success: false}, status: :bad_request
          return
        end

        # send email to renter
        DeclinedRequestWorker.perform_async(reservation.id)

        reservation_serializer = ReservationBookingSerializer.new(reservation)
        render json: {reservation: reservation_serializer, is_success: true}, status: :ok
      else
        render json: {error: "Cannot decline reservation", is_success: false}, status: :bad_request
      end
    else
      render json: {error: "No Permission", is_success: false}, status: :bad_request
    end
  end

  # cancel's user's reservation request
  def cancel
    if @reservation.user_id == current_user.id
      Log.info("Renter is cancelling the reservation", level: "INFO", reservation_id: @reservation.id)
      if @reservation.Waiting? ||  @reservation.Approved?
        reservation, error = @reservation.cancel(current_user.id)

        Events.new.cancelled_by_renter(reservation)

        # check if "recovered dates" from cancellation allow it to be bookable (if not bookable)
        listing = reservation.listing
        if !listing.bookable? && listing.has_bookable_windows
          Log.info("Listing is rebookable after cancellation", level: "INFO", listing_id: listing.id)
          listing.update(bookable: true) 
        end

        reservation_serializer = ReservationBookingSerializer.new(reservation)
        render json: {reservation: reservation_serializer, is_success: true}, status: :ok
      else
        render json: {error: "Cannot cancel reservation", is_success: false}, status: :bad_request
      end
    elsif @reservation.host_id == current_user.id
      Log.info("Host is cancelling the reservation", level: "INFO", reservation_id: @reservation.id)
      if @reservation.Waiting? ||  @reservation.Approved?
        reservation, error = @reservation.cancel(@reservation.host_id)

        Events.new.cancelled_by_host(reservation)

        # check if "recovered dates" from cancellation allow it to be bookable (if not bookable)
        listing = reservation.listing
        if !listing.bookable? && listing.has_bookable_windows
          Log.info("Listing is rebookable after cancellation", level: "INFO", listing_id: listing.id)
          listing.update(bookable: true) 
        end

        reservation_serializer = ReservationBookingSerializer.new(reservation)
        render json: {reservation: reservation_serializer, is_success: true}, status: :ok
      else
        render json: {error: "Cannot cancel reservation", is_success: false}, status: :bad_request
      end
    else
      render json: {error: "No Permission", is_success: false}, status: :bad_request
    end
  end

  def compute_payouts
    # should be for hosts to see payouts
    if @reservation.host.id != current_user.id
      render json: { error: "Invalid reservation ID", is_success: false}, status: :bad_request and return
    end
    payouts, errors = PaymentServices.new(@reservation).compute_payouts
    if errors.nil?
      payouts_serializer = payouts.map{|p|
        PayoutSerializer.new(p)
      }
      render json: {payouts: payouts_serializer, reservation_id: @reservation.id, is_success: true}, status: :ok
    else
      render json: {error: errors, is_success: false}, status: :bad_request
    end
  end

  private
    def set_reservation
      @reservation = Reservation.find(params[:id])
      if @reservation.nil?
        render json: { error: "Invalid reservation ID", is_success: false}, status: :bad_request and return
      end
    end

    def reservation_params
      params.require(:reservation).permit(:listing_id, :start_date, :end_date, :request_message)
    end

    # some actions require sidekiq jobs for proper processing
    def redis_connected?
      !!Sidekiq.redis(&:info)
    end
end