class Api::V1::MessagesController < ApplicationController
    before_action :authenticate_user_from_token!
    before_action :set_conversation

    class ListingSerializer < ActiveModel::Serializer
        attributes :id, :listing_name
    end

    def index
        user = current_user
        if user == @conversation.sender || user == @conversation.recipient
            other_user = user == @conversation.sender ? @conversation.recipient : @conversation.sender
            other_user_serializer = UserProfileSerializer.new(other_user)
            listing_serializer = ListingSerializer.new(@conversation.listing)
            
            messages = @conversation.messages.order("created_at ASC")

            # # If we need pagination in the future
            # if !params[:page].blank? && Integer(params[:page]) > 0
            #     messages = messages.paginate(page: params[:page])
            # else
            #     # default returns first page
            #     messages = messages.paginate(page: 1)
            # end

            messages_serializer = ActiveModelSerializers::SerializableResource.new(messages,  each_serializer: MessageSerializer)

            render json: {
                user: other_user_serializer,
                listing: listing_serializer,
                messages: messages_serializer,
                # metadata: pagination_dict(messages),
                is_success: true
            }, status: :ok
        else
            render json: {error: "You don't have permission to view this", is_success: false}, status: :bad_request
        end
    end

    def create
        pars = message_params
        pars[:user_id] = current_user.id
        message = @conversation.messages.new(pars)
        message_serializer = MessageSerializer.new(message)

        if message.save
            # ActionCable.server.broadcast "conversation_#{@conversation.id}", message: render_message(@message)
            render json: {message: message_serializer, is_success: true}, status: :ok
        else
            render json: {error: "Could not send message!", is_success: false}, status: :bad_request
        end
    end


    private
        def set_conversation
            @conversation = Conversation.find(params[:conversation_id])
        end

        def message_params
            params.require(:message).permit(:context)
        end
end