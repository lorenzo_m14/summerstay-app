class Api::V1::PayoutsController < ApplicationController
    before_action :authenticate_user_from_token!

    # get all of a user's payouts (complete and incomplete)
    def index
      reservations = Reservation.where(host_id: current_user.id, status: :Approved)
      reservations_serializer = reservations.map { |r| 
        ReservationPayoutsSerializer.new(r) 
      }
      render json: {stays: reservations_serializer, is_success: true}, status: :ok
    end
  
    # show a user's payout
    def show
      render json: {
        is_success: true
      }, status: :ok
    end

    # Stripe Connect account creation Oauth callback
    def stripe_connect_callback
      response = HTTParty.post("https://connect.stripe.com/oauth/token",
        query: {
          client_secret: ENV["STRIPE_SECRET_KEY"],
          code: params[:code],
          grant_type: "authorization_code"
        }
      )

      # check for response error
      if response.parsed_response.key?("error")
        render json: {
          error: response.parsed_response["error_description"],
          is_success: false
        }, status: :bad_request
      else
        merchant_id = response.parsed_response["stripe_user_id"]

        user = current_user
        user.update!(merchant_id: merchant_id)

        user.set_connect_status

        # Update phone number from stripe connect account
        # Note: doesn't work if number is already taken!
        is_phone_updated = update_phone_from_connect(user)

        Log.info("Unable to update phone number") if !is_phone_updated

        # TODO: Scrape more user details from Stripe Connect Account

        render json: {
          user: UserSerializer.new(user),
          is_success: true
        }, status: :created
      end
  end

  # Login link for users with Stripe Connect Account
  def stripe_connect_create_login
    user = current_user
    if user.merchant_id.present?
      login_info = Stripe::Account.create_login_link(user.merchant_id)
      render json: { login_url: login_info.url, is_success: true}, status: :ok
    else
      render json: { error: 'Please Connect to Stripe Express first.', is_success: false}, status: :bad_request
    end
  end

  private
    def update_phone_from_connect(user)
      connect_account = Stripe::Account.retrieve(user.merchant_id)
      phone_number = connect_account&.business_profile&.support_phone
      user.update(phone_number: phone_number) if !phone_number.nil?
      return user.save
    end
end
  