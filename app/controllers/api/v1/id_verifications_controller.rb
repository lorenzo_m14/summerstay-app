class Api::V1::IdVerificationsController < ApplicationController
    before_action :authenticate_user_from_token!, except: [:id_verify_webhooks]

    def verify_id
        auth_key = params[:auth_key]
        if auth_key.blank?
            error = "Invalid ID Verification!"
            Log.error(error, type: "IdVerify", action: "verify_id", level: "ERROR")
            render json: {error: error, is_success: false}, status: :bad_request
        else
            response = PassbaseClient.new.verify_id(auth_key)
            Log.debug(response)

            status = response.dig("status")
            if status.nil?
                error = "Could not process ID Verification!"
                Log.error(error, type: "IdVerify", action: "verify_id", level: "ERROR")

                render json: {
                  error: error,
                  is_success: false
                }, status: :bad_request
                
            elsif status == "error"
                message = "User verification in review."
                # TODO: Fix this when Passbase fixes this shit!
                # Mitigation: Always pass user into "InReview"
                user = current_user
                Log.info(message, type: "IdVerify", action: "verify_id", status: status, level: "INFO")
                user.update(id_verify_status: :InReview, id_verify_token: auth_key)
                user.save!
                user_serializer = UserSerializer.new(user)

                render json: { user: user_serializer, message: message, is_success: true }, status: :ok

                # error = "Could not process ID Verification!"
                # Log.error(error, type: "IdVerify", action: "verify_id", level: "ERROR")

                # render json: {
                #   error: error,
                #   is_success: false
                # }, status: :bad_request
            else
                review_status = response.dig("review_status")
                authentication_document = response.dig("review_status")
                user = current_user

                message = "User verification is unapproved."
                status = "Unapproved"
                if review_status.nil?
                    message = "User verification in review."
                    status = "InReview"
                    user.InReview!
                elsif review_status == true
                    message = "User verification denied! Please contact support."
                    status = "Denied"
                    user.Denied!
                elsif review_status == false
                    message = "User verification approved!"
                    status = "Approved"
                    user.Approved!
                end

                user.update(id_verify_token: auth_key)
                # if authentication_document.present?
                #     user.update(verify_document_type: authentication_document)
                # end
                user.save!
                
                Log.info(message, type: "IdVerify", action: "verify_id", status: status, level: "INFO")
                user_serializer = UserSerializer.new(user)

                render json: { user: user_serializer, message: message, is_success: true }, status: :ok
            end
        end
    end

    def id_verify_webhooks
        puts "ID Verification Webhook"
        if is_review_status_changed?
            # get review_status from webhook body
            # update corresponding user status (via id_verify_token)
            auth_key = params[:authentication_key] # should match the one saved in db for user
            auth_status = params[:review_status] # review statuses: true, false, null

            user = User.find_by(id_verify_token: auth_key)
            if user.nil?
                error = "Invalid user for id verification token: #{auth_key}"
                Log.error(error, type: "IdVerify", action: "id_verify_webhooks", level: "ERROR")
                render json: { error: error }, status: :bad_request
                return
            end

            status = "InReview"
            if auth_status.nil?
                user.InReview!
                Log.info("Review Status in Review.", type: "IdVerify", action: "id_verify_webhooks", level: "INFO", status: status)
            elsif auth_status == true
                status = "Approved"
                user.Approved!
                Log.info("Review Status Approved!", type: "IdVerify", action: "id_verify_webhooks", level: "INFO", status: status)
            elsif auth_status == false
                status = "Denied"
                user.Denied!
                error = "User #{user.id} failed id verification!"
                Log.error(error, type: "IdVerify", action: "id_verify_webhooks", level: "ERROR", status: status)
            else
                error = "Invalid review status!"
                Log.error(error, type: "IdVerify", action: "id_verify_webhooks", level: "ERROR")
                render json: { error: error }, status: :bad_request
                return
            end

            render json: { message: params }, status: :ok
        else
            error = "Improper webhook type!"
            Log.error(error, type: "IdVerify", action: "id_verify_webhooks", level: "ERROR")
            render json: { error: error }, status: :bad_request
        end
    end

    private

    def is_review_status_changed?
        params[:event] == 'AUTHENTICATION_REVIEW_STATUS_CHANGED'
    end

end
  