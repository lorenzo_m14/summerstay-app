class Api::V1::ReferralCodesController < ApplicationController
    before_action :authenticate_user_from_token!

    # NOTE: Currently unused
    # verify referral code based on passed string
    def verify_code
        code =  params[:referral_code]
        if code.blank?
            render json: {error: "Invalid referral code!", is_success: false}, status: :bad_request
        else
            referral_code = ReferralCode.find_by(code: code)
            if referral_code.blank?
                render json: {error: "Invalid referral code!", is_success: false}, status: :bad_request
            else
                render json: {message: "Valid referral code", code: code, is_success: true}, status: :ok
            end
        end
    end
end
  