class Api::V1::ClaimsController < ApplicationController
    before_action :authenticate_user_from_token!, except: [:index, :show]
    before_action :set_claim, except: [:index, :create]

    # Basic claims endpoints
    # TODO: WRITE TESTS FOR THIS CONTROLLER (low priority)

    # see all your claims
    def index
        claims = Claim.where(user_id: current_user.id)
        claims_serializer = ActiveModelSerializers::SerializableResource.new(claims,  each_serializer: ClaimSerializer)
        render json: {
            claims: claims_serializer,
            is_success: true
        }, status: :ok
    end

    # view your claim
    def show
        if current_user.id == @claim.user_id
            claim_serializer = ClaimSerializer.new(@claim)
            render json: { claim: claim_serializer, is_success: true}, status: :ok
        else
            render json: { error: "Invalid claim.", is_success: false}, status: :bad_request
        end
    end

    # create a claim for a reservation you are associated with.
    def create
        reservation = Reservation.find_by_id(claim_params[:reservation_id])
        if !reservation.nil? && reservation.associated_with_user?(current_user)
            params = claim_params
            params[:user_id] = current_user.id
            claim = Claim.create!(params)
            claim_serializer = ClaimSerializer.new(claim)
            render json: { claim: claim_serializer, is_success: true}, status: :ok
        else
            render json: { error: "Couldn't create claim.", is_success: false}, status: :bad_request
        end
    end

    # TODO:
    def update
    end

    private
    # set instance of claim for calls with id param
    def set_claim
        @claim = Claim.find_by(id: params[:id])
    end

    # allow params, especially for update
    def claim_params
        params.require(:claim).permit(
            :user_type, :claim_type, :content, :reservation_id
        )
    end
end
