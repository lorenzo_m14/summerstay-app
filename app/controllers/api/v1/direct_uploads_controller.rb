class Api::V1::DirectUploadsController < ActiveStorage::DirectUploadsController
    # Should only allow null_session in API context, so request is JSON format
    include TokenAuthentication
    protect_from_forgery with: :exception
    skip_before_action :verify_authenticity_token
    before_action :authenticate_user_from_token!
  
    # Also, since authenticity verification by cookie is disabled, you should implement you own logic :
    # before_action :verify_user

    private
  
    # def verify_user
    #   raise unless User.find(doorkeeper_token[:resource_owner_id])
    # end
  end