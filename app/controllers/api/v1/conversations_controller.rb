class Api::V1::ConversationsController < ApplicationController
    before_action :authenticate_user_from_token!

    def index
        conversations = Conversation.involving(current_user)

        # optional parameter to filter by listing
        if !params[:listing_id].blank?
            conversations = conversations.where(listing_id: params[:listing_id])
        end

        conversations_serializer = ActiveModel::Serializer::CollectionSerializer.new(conversations, user: current_user, each_serializer: ConversationSerializer)

        render json: {
            conversations: conversations_serializer,
            is_success: true
        }, status: :ok

    end

    def create
        current_user_id = current_user.id
        listing = Listing.find_by_id(params[:listing_id])
        recipient = User.find_by_id(params[:recipient_id])
        
        if listing.blank?
            render json: {error: "Invalid listing, cannot create conversation", is_success: false}, status: 400
        elsif recipient.blank?
            render json: {error: "Invalid recipient, cannot create conversation", is_success: false}, status: 400
        elsif listing.status == "Waiting"
            render json: {error: "Invalid listing, cannot create conversation", is_success: false}, status: 400
        elsif listing.user_id != recipient.id && listing.user_id != current_user_id
            render json: {error: "Users not associated with listing, cannot create conversation", is_success: false}, status: 400
        elsif current_user_id == recipient.id
            render json: {error: "Recipient id is current user id, cannot create conversation", is_success: false}, status: 400
        # retrieve old convo
        elsif Conversation.where(listing_id: listing.id).between(recipient.id, current_user_id).first
            conversation = Conversation.where(listing_id: listing.id).between(recipient.id, current_user_id).first
            render json: {message: "Fetched conversation", conversation: ConversationSerializer.new(conversation, user: current_user), is_success: true}, status: 200
        # new convo
        else

            # check if reservation exists, could be null
            renter = current_user == listing.user ? recipient : current_user
            reservation =  Reservation.where(listing: listing, user: renter).first

            c_params = {
                "sender_id": current_user_id,
                "recipient_id": recipient.id,
                "listing_id": listing.id,
                "reservation_id": reservation&.id,
            }
            conversation = Conversation.create(c_params)
            if conversation.save
                render json: {
                    message: "Created new conversation",
                    conversation: ConversationSerializer.new(conversation, user: current_user), 
                    is_success: true
                }, status: 200
            else
                render json: {error: conversation.errors, is_success: false}, status: 400
            end
        end
    end

    private

        def conversation_params
            params.permit(:sender_id, :recipient_id, :listing_id)
        end
end