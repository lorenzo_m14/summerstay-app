class Api::V1::WebhooksController < ApplicationController
    # protect_from_forgery except: [:payouts,:invoices]
    include Constants::Payments

    # Stripe connected account updates
    def accounts
        if is_account_updated?
            # TODO: check for account verification status update
            # check requirements hash for pending and upcoming verification
        elsif is_capability_updated?
            account_id = params.dig(:data, :object, :account)
            user = User.find_by(merchant_id: account_id)

            if user.nil?
                error = "Cannot associate account #{account_id} with user"
                Log.info(error, level: "ERROR", type: "Webhook", action: "accounts", method: "capability.updated")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            else
                user.set_connect_status

                Log.info("User status is now #{user&.connect_account_status}", level: "INFO", type: "Webhook", action: "accounts", method: "capability.updated")
                render json: {user: UserSerializer.new(user), is_success: true}, status: :ok
            end
        end
    end

    def invoices
        if is_payment_succeeded?
            user = retrieve_user

            # identify the associated charge in db, mark as paid, and add invoice id to charge
            stripe_invoice = retrieve_invoice
            charge = retrieve_charge_from_invoice(stripe_invoice)

            if charge.nil?
                error = "Stripe Invoice #{stripe_invoice&.id} couldn't be associated with a charge"
                Log.error(error, level: "ERROR", type: "Webhook", action: "invoices", method: "payment.succeeded")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
                return
            else
                Log.debug "Retrieved corresponding charge #{charge&.id}"
                # get stripe charge to associate receipt url
                stripe_charge = Stripe::Charge.retrieve(stripe_invoice.charge)
                charge.Paid!
                charge.update!(paid_at: DateTime.current, receipt_url: stripe_charge&.receipt_url)

                # change reservation to paid if charge num 2
                if charge.charge_num == 1 && charge.charge_type == CHARGE_TYPES[:upfront]
                    Log.info("Completed second upfront payment, reservation is now paid!", level: "INFO", type: "Webhook", action: "invoices", method: "payment.succeeded")

                    # reservation has completed its payments!
                end

                if !Rails.env.production?
                    Log.info("Sending [nonprod] receipt email after payment success webhook")
                    ReceiptWorker.perform_async(charge.reservation.id,charge.id)
                end

                Log.info("Updated charge #{charge&.id} with receipt and payment timestamp", level: "INFO", type: "Webhook", action: "invoices", method: "payment.succeeded")
                render json: {charge: charge, is_success: true}, status: :ok
                return
            end
        elsif is_payment_failed?
            Log.debug "Webhook: is_payment_failed"
            user = retrieve_user
            stripe_invoice = retrieve_invoice

            lines = params.dig(:data, :object, :lines, :data)

            error = "Payment failed for invoice #{stripe_invoice&.id}"
            Log.error(error, level: "ERROR", type: "Webhook", action: "invoices", method: "payment.failed")
            render json: {error: error, event_id: params[:id], is_success: false}, status: :ok
        elsif is_charge_refunded?
            # TODO: handle when deposit is returned
        end
    end

    # Payouts first take the form of Stripe::Transfer from platform account to connected account
    # Stripe::Payout is then paid out automatically after a day to the connected account
    #   - Points of failure: transfer.failed, transfer.reversed, and payout_failed
    def payouts
        if is_transfer_succeeded?
            Log.debug"Webhook: is_transfer_succeeded"
            # handle transfer succeeded
            stripe_transfer = retrieve_transfer
            payout = update_payout(stripe_transfer)
            if payout.nil?
                error = "Couldn't find corresponding payout for transfer #{stripe_transfer&.id}"
                Log.error(error, level: "ERROR", type: "Webhook", action: "payouts", method: "transfer.succeeded")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            else
                reservation = payout.reservation

                Log.info("Successful transfer #{stripe_transfer&.id} for payout #{payout&.id}", level: "INFO", type: "Webhook", action: "invoices", method: "transfer.succeeded")
                render json: {payout: payout, is_success: true}, status: :ok
            end
        elsif is_transfer_failed?
            Log.debug "Webhook: is_transfer_failed"
            stripe_transfer = retrieve_transfer

            error = "Transfer #{stripe_transfer&.id} failed!"
            Log.error(error, level: "ERROR", type: "Webhook", action: "payouts", method: "transfer.failed")
            render json: {error: error, event_id: params[:id], is_success: false}, status: :ok
        elsif is_transfer_reversed?
            Log.debug "Webhook: is_transfer_reversed"
            stripe_transfer = retrieve_transfer

            error = "Stripe transfer #{stripe_transfer&.id} reversed!"
            Log.error(error, level: "ERROR", type: "Webhook", action: "payouts", method: "transfer.reversed")
            render json: {error: error, event_id: params[:id], is_success: false}, status: :ok
        elsif is_payout_paid?
            Log.debug "Webhook: is_payout_payed"
            stripe_payout = retrieve_payout

            Log.info("Payout #{stripe_payout&.id} was paid successfully", level: "INFO", type: "Webhook", action: "invoices", method: "payout.paid")
            render json: {stripe_payout: stripe_payout, event_id: params[:id], is_success: true}, status: :ok
        elsif is_payout_failed?
            Log.debug "Webhook: is_payout_failed"

            stripe_payout = retrieve_payout
            error = "Stripe payout #{stripe_payout&.id} failed!"
            Log.error(error, level: "ERROR", type: "Webhook", action: "payouts", method: "payout.failed")
            render json: {error: "Payout failed!", event_id: params[:id], is_success: false}, status: :ok
        end
    end

    # Subscriptions first start as sub_sched objects.
    # They then release Stripe::Subscription objects on start date that generate invoices
    # Must associate released subscription with corresponding Reservation Cost
    def subscriptions
        # subscription start
        # if is_subscription_schedule_completed?
        # if is_subscription_schedule_aborted?
        if is_subscription_schedule_released?
            Log.debug "Webhook: is_subscription_schedule_released"

            sub_schedule_id = params.dig(:data, :object, :id)
            sub_id = params.dig(:data, :object, :subscription)
            rc = ReservationCost.where(sub_schedule_id: sub_schedule_id).first
            if rc.nil?
                error = "Couldn't associate schedule #{sub_schedule_id} with reservation!"
                Log.error(error, level: "ERROR", type: "Webhook", action: "subscriptions", method: "subscription_schedule.released")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            else 
                rc.update!(subscription_id: sub_id)
                Log.info("Associated subscription #{sub_id} with reservation #{rc&.reservation&.id}", level: "INFO", type: "Webhook", action: "subscriptions", method: "subscription_schedule.released")
                render json: {reservation_cost: rc, is_success: true}, status: :ok
            end
        elsif is_subscription_schedule_updated?
            Log.debug "Webhook: is_subscription_schedule_updated"

            sub_schedule_id = params.dig(:data, :object, :id)
            sub_id = params.dig(:data, :object, :subscription)
            rc = ReservationCost.where(sub_schedule_id: sub_schedule_id).first

            if rc.nil?
                error = "Couldn't associate schedule #{sub_schedule_id} with reservation"
                Log.error(error, level: "ERROR", type: "Webhook", action: "subscriptions", method: "subscription_schedule.updated")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            elsif sub_id.nil?
                error = "Couldn't retrieve schedule from subscription_schedule"
                Log.error(error, level: "ERROR", type: "Webhook", action: "subscriptions", method: "subscription_schedule.updated")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            else 
                rc.update!(subscription_id: sub_id)
                Log.info("Associated subscription #{sub_id} with reservation #{rc&.reservation&.id}", level: "INFO", type: "Webhook", action: "subscriptions", method: "subscription_schedule.updated")
                render json: {reservation_cost: rc, is_success: true}, status: :ok
            end
        elsif is_subscription_schedule_completed?
            Log.debug "Webhook: is_subscription_schedule_completed"
            sub_schedule_id = params.dig(:data, :object, :id)
            sub_id = params.dig(:data, :object, :subscription)
            rc = ReservationCost.where(sub_schedule_id: sub_schedule_id).first

            if rc.nil?
                error = "Couldn't associate schedule #{sub_schedule_id} with reservation"
                Log.error(error, level: "ERROR", type: "Webhook", action: "subscriptions", method: "subscription_schedule.completed")
                render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
            else 
                reservation = rc.reservation

                # reservation has completed its payments!
                Log.info("Subscription schedule completed! Reservation #{reservation&.id} has completed payments", level: "INFO", type: "Webhook", action: "subscriptions", method: "subscription_schedule.completed")
                render json: {reservation_cost: rc, is_success: true}, status: :ok
            end
        else
            error = "Webhook: #{params[:type]} subscription schedule webhook"
            Log.error(error, level: "ERROR", type: "Webhook", action: "subscriptions", method: params[:type])
            render json: {error: error, event_id: params[:id], is_success: false}, status: :bad_request
        end 
    end

    private

    def retrieve_user
        customer_token = params.dig(:data, :object, :customer)
        return nil if customer_token.nil?

        User.where(stripe_id: customer_token).first
    end

    def retrieve_invoice
        invoice_id = params.dig(:data, :object, :id)
        return nil if invoice_id.nil?

        invoice = Stripe::Invoice.retrieve(invoice_id)
        return invoice
    end

    # Two types of invoices - associated with Stripe::Subscription, or standalone
    #   - check if has metadata for reservation and charge num
    def retrieve_charge_from_invoice(invoice)
        # - check if has subscription
        subscription_id = invoice&.subscription
        if !subscription_id.nil?
            Log.info("Fetching charge from subscription", level: "INFO", type: "Webhook", action: "invoices")
            rc = ReservationCost.where(subscription_id: subscription_id).first

            # if we can't find via Reservation Cost subscription id,
            # try to find it in the subscription plan metadata
            if rc.nil?
                data = invoice&.lines&.data
                if data.count > 0
                    metadata = data[0]&.plan&.metadata
                    rc = ReservationCost.where(reservation_id: metadata&.reservation_id).first
                    if !rc.nil?
                        rc.subscription_id = subscription_id
                    end
                end
            end

            # if we still can't associate the reservation with the subscription, return nil
            return nil if rc.nil?

            # use index in invoice list to retrieve corresponding charge
            invoice_list = Stripe::Invoice.list({subscription: subscription_id})
            invoice_list_index = invoice_list&.data&.index(invoice)
            charges = rc.reservation.charges
            charge = charges.where(charge_num: invoice_list_index+1).first
            return charge
        # check metadata for charge info
        elsif !invoice.metadata.nil?
            Log.info("Fetching charge from invoice metadata", level: "INFO", type: "Webhook", action: "invoices")
            reservation_id = invoice&.metadata&.reservation_id
            charge_num = invoice&.metadata&.num_charge
            charge = Charge.where(reservation_id: reservation_id, charge_num: charge_num).first
            return charge
        else
            return nil
        end
    end

    def retrieve_transfer
        transfer_id = params.dig(:data, :object, :id)
        return nil if transfer_id.nil?

        Stripe::Transfer.retrieve(transfer_id)
    end

    def retrieve_payout
        account = params.dig(:account)
        payout_id = params.dig(:data, :object, :id)
        return nil if payout_id.nil? || account.nil?

        # need to retrieve payout PER stripe connect account
        Stripe::Payout.retrieve(payout_id, stripe_account: account)
    end

    # use metadata fields [reservation_id, num_payout] to associate Stripe::Transfer with payout in db
    def update_payout(stripe_payout)
        metadata = stripe_payout.metadata
        return nil if metadata.nil?

        reservation_id = metadata.reservation_id
        num_payout = metadata.num_payout

        # find associated payout in db
        payout = Payout.where(reservation_id: reservation_id, payout_num: num_payout).first
        return nil if payout.nil?
        payout.Paid!
    end

    def retrieve_sub_schedule
        sub_schedule_id = params.dig(:data, :object, :id)
        return nil if sub_schedule_id.nil?

        invoice = Stripe::Invoice.retrieve(invoice_id)
        return invoice
    end

    def is_account_updated?
        params[:type] == 'account.updated'
    end

    def is_capability_updated?
        params[:type] == 'capability.updated'
    end

    def is_payment_succeeded?
        params[:type] == 'invoice.payment_succeeded'
    end

    def is_payment_failed?
        params[:type] == 'invoice.payment_failed'
    end

    def is_transfer_succeeded?
        params[:type] == 'transfer.paid'
    end

    def is_transfer_failed?
        params[:type] == 'transfer.failed'
    end

    def is_transfer_reversed?
        params[:type] == 'transfer.reversed'
    end

    def is_payout_paid?
        params[:type] == 'payout.paid'
    end

    def is_payout_failed?
        params[:type] == 'payout.failed'
    end

    def is_subscription_schedule_released?
        params[:type] == 'subscription_schedule.released'
    end

    def is_subscription_schedule_updated?
        params[:type] == 'subscription_schedule.updated'
    end

    def is_subscription_schedule_completed?
        params[:type] == 'subscription_schedule.completed'
    end

    def is_subscription_schedule_aborted?
        params[:type] == 'subscription_schedule.aborted'
    end

    def is_charge_refunded?
        params[:type] == 'charge.refunded'
    end
end
