class Api::V1::SessionsController < Devise::SessionsController
    before_action :authenticate_user_from_token!, except: [:create]

    # generate auth token on successful login
    def create
      reset_session
      super do |user|
        if user.persisted?
          sign_out

          user.generate_authentication_token
          user.save!
          
          sign_in(user)
          set_session(user)
          
          user_serializer = UserSerializer.new(user)
          render json: {
                user: UserSerializer.new(user), 
                provider: "email",
                is_success: true
          }, status: :ok
          return
        end
      end
    end

    private 

    def set_session(user)
      session[:user_id] = user.id
      session[:token] = user.authentication_token
    end

    # DEPRECATED IN FAVOR OF SESSIONS FOR SECURITY
    def set_response_headers(auth_token, email)
        response.set_header("X-User-Token", auth_token)
        response.set_header("X-User-Email", email)
    end
end
