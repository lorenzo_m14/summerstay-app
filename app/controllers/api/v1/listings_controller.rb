require 'will_paginate/array'

class Api::V1::ListingsController < ApplicationController
    include Constants::Payments
    include Constants::Listings

    before_action :authenticate_user_from_token!, except: [:index, :show]
    before_action :set_listing, except: [:index, :create, :own_listings]
    before_action :is_authorized, only: [:update]

    # for renters: to search for listings
    def index
        # search by location
        valid_params = {}
        if params[:latitude].present? && params[:longitude].present?
            scope = params[:scope].present? ? params[:scope] : DEFAULT_SCOPE # 50 miles is default scope
            listings = Listing.where(active: true, bookable: true).near([params[:latitude],params[:longitude]], scope, order: 'distance')
            valid_params[:latitude] = params[:latitude]
            valid_params[:longitude] = params[:longitude]
            valid_params[:scope] = scope
        else
            listings = Listing.where(active: true, bookable: true)
        end

        if params[:min_price].present? && params[:max_price].present?
            # TODO: Use display_price instead
            min_price = ((params[:min_price].to_i)/1.05).round * 100 # adjust to display price
            max_price = ((params[:max_price].to_i)/1.05).round * 100 # adjust to display price
            listings = listings.where('price >= ? AND price <= ?', min_price, max_price)
            valid_params[:min_price] = params[:min_price]
            valid_params[:max_price] = params[:max_price]
        end

        if params[:beds].present? && params[:baths].present?
            beds = params[:beds]
            baths = params[:baths]
            listings = listings.where('beds > ? AND bathroom > ?', beds, baths)
            valid_params[:beds] = beds
            valid_params[:baths] = baths
        end

        if params[:stay_type].present?
            stay_types = params[:stay_type].split(',')
            filtered_stay_types = stay_types.select{|type| STAY_TYPES.include?(type)}
            listings = listings.where(listing_type: filtered_stay_types)
            valid_params[:stay_types] = filtered_stay_types
        end
        
        # search by dates
        if params[:start_date].present? && params[:end_date].present?
            start_date = Date.parse(params[:start_date])
            end_date = Date.parse(params[:end_date])

            if !start_date.nil? && !end_date.nil?
                # check for overlapping reservations
                listings = listings.select{ |listing| listing.is_available(start_date, end_date) }
                valid_params[:start_date] = params[:start_date]
                valid_params[:end_date] = params[:end_date]
            else
                # if invalid start or end date, we continue without date selection
                # render json: {
                #     error: 'Invalid start or end dates!',
                #     is_success: false
                # }, status: :bad_request
                # return
            end
        end



        # pagination page param
        if params[:page].present? && Integer(params[:page]) > 0
            listings = listings.paginate(page: params[:page], per_page: 20)
        else
            listings = listings.paginate(page: 1, per_page: 20)
        end


        
        listings_serializer = ActiveModelSerializers::SerializableResource.new(
                                listings, each_serializer: ListingIndexSerializer)

        Log.debug('Listings index', type: 'Listing', action: 'index', level: 'INFO', params: valid_params)
        render json: {
            listings: listings_serializer,
            params: valid_params,
            metadata: pagination_dict(listings),
            is_success: true
        }, status: :ok
    end

    # view a single listing
    def show
        if !@listing.active
            render json: { error: 'Invalid Listing ID', is_success: false}, status: :bad_request
            return
        end

        unavailable_dates = @listing.unavailable_dates
        
        listing_serializer = ListingShowSerializer.new(@listing, unavailable_dates: unavailable_dates)
        render json: { listing: listing_serializer, cost_params: COST_PARAMS, is_own_listing: false, is_success: true}, status: :ok
    end

    # for hosts: to make new listings
    def create 
        user = User.find(current_user.id)

        if user.listings.count >= MAX_LISTINGS_PER_USER
            error = "Cannot have more than #{MAX_LISTINGS_PER_USER} listings!"
            Log.error(error, type: 'Listing', action: 'create', level: 'ERROR')
            render json: { error: { listing: error }, is_success: false}, status: :bad_request
            return
        end

        new_params = listing_params

        listing = user.listings.build(new_params)

        if listing.save
            listing_serializer = OwnListingSerializer.new(listing)
            render json: { listing: listing_serializer, is_success: true}, status: :created
        else
            render json: { error: listing.errors, is_success: false}, status: :bad_request
        end
    end

    # for hosts: to update fields to listings
    def update
        # Should not be able to edit certain details of listinglistings if listing has any waiting/approved reservations

        # check for any waiting/approved reservations
        new_params = listing_params

        # listing with requests/reservation
        if @listing.reservations.where(status: [:Waiting, :Approved]).count > 0

            # Only able to edit images, summary and listing_name if there are active requests/bookings
            acceptable_params = ["images", "summary", "listing_name", "start_date", "end_date"]
            if new_params&.keys.present?
                invalid_params = new_params.keys.difference(acceptable_params) 

                if !invalid_params.blank?
                    error = "Cannot update #{invalid_params.first} status due to active requests and/or approved reservations"

                # date update validations for 
                elsif new_params.key?(:start_date) || new_params.key?(:end_date)
                    # get set of start and end dates for all reservation requests
                    reservation_dates = @listing.reservations.where(status: [:Waiting, :Approved]).map { |r|
                        [r[:start_date].to_date,r[:end_date].to_date]
                    }.flatten.to_set 

                    if Date.parse(new_params[:end_date]) <= Date.today + MIN_RESERVATION_LENGTH.days
                        error = "Must allow for bookable window of at least 3 weeks"
                    end

                    if Date.parse(new_params[:start_date]) > reservation_dates.min
                        error = "Invalid start date due to active requests or approved reservations"
                    end

                    if Date.parse(new_params[:end_date]) < reservation_dates.max
                        error = "Invalid end date due to active requests or approved reservations"
                    end
                end
            end

            if error.present?
                Log.error(error, type: 'Listing', action: 'update', level: 'ERROR')
                render json: { error: { listing: error }, is_success: false }, status: :bad_request
                return
            end
        else
            # adjust active tag based on listing status
            if @listing.Waiting?
                new_params = listing_params.except(:active)
            else
                # active can implicitly be changed if listing is not waiting
                new_params = listing_params
                temp_params = new_params.except(:images)
                temp_listing = @listing
                temp_listing.assign_attributes(temp_params) # doesn't save attributes yet

                # need to disallow save if listing params invalid
                unless temp_listing.is_ready_listing
                    error = "Listing #{@listing.id} cannot be updated, otherwise would no longer be ready"
                    Log.error(error, type: 'Listing', action: 'update', listing: @listing.id, level: 'ERROR')
                    temp_listing.errors.add(error)
                    render json: { errors: temp_listing.errors, is_success: false }, status: :bad_request
                    return
                end

            end
        end

        # Note: Referral system is inactive
        # update with referral code if valid
        # if new_params[:referral_code].present?
        #     rc = ReferralCode.find_by(code: new_params[:referral_code])
        #     if rc.blank?
        #         render json: { error: {referral_code: ["Invalid referral code!"]}, is_success: false}, status: :bad_request
#                 return
        #     else 
        #         new_params[:referral_code] = rc
        #     end
        # elsif new_params[:referral_code] == ""
        #     new_params.delete(:referral_code)
        # end

        listing = @listing
        if listing.update!(new_params)
            # save display price
            if new_params.key?(:price)
                listing.update(display_price: (new_params[:price] * 1.05).floor)
            end

            # if listing isn't bookable, check to see if date update allows bookable windows
            if !listing.bookable && (new_params.key?(:start_date) || new_params.key?(:end_date))
                if listing.has_bookable_windows
                    Log.info("Listing #{listing.id} is now bookable", type: "Listing", action: "update", level: "OK")
                    listing.update(bookable: true)
                end
            end

            if listing.Waiting?

                # Note: Referral system is inactive
                # # capture lead for Lead Dyno if listing added referral code
                # if new_params[:referral_code].present?
                #     listing.capture_listing_lead
                # end

                if listing.is_ready_listing

                    # Note: Referral system is inactive
                    # capture lead for Lead Dyno if listing has referral code
                    # listing.capture_listing_purchase

                    Log.info("Listing #{listing.id} is ready for public listing!", type: "Listing", action: "update", level: "OK")
                    listing.update(active: true, ready: true, bookable: true, status: :Ready, ready_at: DateTime.current) 

                    # TODO: add back when email template is completed
                    # Events.new.completed_listing(listing)

                    listing_serializer = OwnListingSerializer.new(listing)

                    render json: { listing: listing_serializer, ready: true,is_success: true }, status: :created
                    return
                end
            end

            listing_serializer = OwnListingSerializer.new(listing)

            render json: { listing: listing_serializer, is_success: true }, status: :ok
        else
            render json: { error: listing.errors, is_success: false}, status: :bad_request
        end
    end

    def destroy
        # should only be able to delete a listing if listing hasn't been booked
        if @listing.Waiting? || @listing.Ready?

            # all reservation requests will be deleted 
            @listing.Deleted!
            for r in @listing.reservations
                DeclinedRequestWorker.new.perform(r.id)
                r.destroy
            end

            render json: { is_success: true }, status: :ok
        else
            render json: { error: "Cannot delete a booked listing!", is_success: false}, status: :bad_request
        end
    end

    # give reservation cost on booking view, calculates unsaved reservation costs and charges based on params 
    def get_reservation_cost
        listing_id = params[:id]
        charge_type = params[:charge_type]  # can choose between standard and pay-as-you-stay
        start_date = params[:start_date]
        end_date = params[:end_date]
        payment_method_type = params[:payment_method_type]

        rental_days = (end_date.to_date - start_date.to_date).to_i

        if rental_days <= MIN_RESERVATION_LENGTH
            error = "Listing cannot be less than 3 weeks"
            render json: { error: error, is_success: false}, status: :bad_request
            return
        end

        reservation_cost, charges = PaymentServices.new(nil, @listing).get_reservation_prices(charge_type, start_date, end_date, payment_method_type)
        reservation_cost_serializer = ReservationCostSerializer.new(reservation_cost)

        render json: { reservation_cost: reservation_cost, charges: charges, is_success: true}, status: :ok
    end

    # for hosts: view own listings

    def own_listings
        listings = current_user.listings
        render json: {
            listings: listings.map { |listing|
                OwnListingSerializer.new(listing)
            },
            is_success: true
        }, status: :ok
    end

    def own_listings_show
        if @listing.user != current_user
            render json: { error: "Invalid Listing ID", is_success: false}, status: :bad_request
        else
            render json: { listing: OwnListingSerializer.new(@listing), is_success: true}, status: :ok
        end
    end

    def delete_image
        # don't allow deletion if listing is active and deleting an image
        # will result in less than 2 images
        if @listing.user != current_user
            render json: { error: "Invalid Listing ID", is_success: false}, status: :bad_request
        elsif @listing.Ready? && @listing.images.count <= 2
            render json: { error: "Cannot delete photo! Your listing must have at least 2 photos.", is_success: false}, status: :bad_request
        else 
            image = @listing.images.find(params[:image_id])
            image.purge
            render json: {is_success: true}, status: :ok
        end
    end

    def sort_images
        if @listing.user != current_user
            render json: { error: "Invalid Listing ID", is_success: false}, status: :bad_request
        else 
            sorted_image_ids = params[:sorted_image_ids]
            if sorted_image_ids.blank?
                render json: { error: "Cannot sort images!", is_success: false}, status: :bad_request
            else
                sorted_image_ids.each_with_index do |image_id, index|
                    @listing.images.where(id: image_id).update_all(position: index + 1)
                end

                render json: {listing: OwnListingSerializer.new(@listing), is_success: true}, status: :ok
            end
        end
    end


    private
        # set instance of listing for calls with id param
        def set_listing
            @listing = Listing.find_by(id: params[:id])
            if @listing.nil?
                render json: { error: "Invalid listing ID", is_success: false}, status: :bad_request and return
            end
        end

        # authorized user for creation/edit
        def is_authorized
            if current_user.id != @listing.user_id
                render json: { error: "Invalid listing ID", is_success: false}, status: :bad_request and return
            end
        end

        # allow params, especially for update
        def listing_params
            params.require(:listing).permit(
                :listing_type, :listing_name, :summary, :address, :apt_number,
                :bedroom, :beds, :bathroom, :longitude, :latitude, 
                :start_date, :end_date,  
                # :is_tv, :is_kitchen, :is_air, :is_heating, :is_internet,
                # :is_laundry, :is_furnished, :is_parking, :is_gym, :is_pool, :is_pet,
                :price, :active, :request_message,
                full_address: [:line_1, :line_2, :city, :state, :zip_code, :country],
                images: [], amenities: [])
        end
end
