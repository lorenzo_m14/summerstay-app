class Api::V1::NotificationsController < ApplicationController
    before_action :authenticate_user_from_token!
    before_action :set_notifications, only: [:index]
  
    def index
        if @notifications.blank?
          render json: {notifications: [], is_success: true}, status: :ok
          return
        end
        notifications = @notifications

        # # If we need pagination in the future
        # if !params[:page].blank? && Integer(params[:page]) > 0
        #   notifications = notifications.paginate(page: params[:page], per_page: 25)
        # else
        #   # default returns the first page
        #   notifications = notifications.paginate(page: 1, per_page: 25)
        # end

        notifications_serializer = ActiveModelSerializers::SerializableResource.new(
            notifications,  each_serializer: NotificationSerializer)
        render json: {
          notifications: notifications_serializer, 
          # metadata: pagination_dict(notificatoins),
          is_success: true
        }, status: :ok
    end

    def mark_as_read
        if params[:id]
          notification = Notification.where(recipient: current_user).find(params[:id])
          if notification.nil?
            render json: {is_success: false}, status: :bad_request
          elsif notification.read
            render json: {message: "Notification is already read", is_success: true}, status: :ok
          else
            notification.update(read_at: Time.current)
            render json: {is_success: true}, status: :ok
          end
        else
          unread_notifications = set_unread_notifications
          if unread_notifications.blank?
            render json: {message: "All notifications are already read!", is_success: true}, status: :ok
            return
          end

          unread_notifications.update_all(read_at: Time.current)
          render json: {message: "Read all notifications", is_success: true}, status: :ok
        end
    
      end

    def set_unread_notifications
      Notification.where(recipient: current_user).unread.order("created_at DESC")
    end

    def set_notifications
      @notifications = Notification.where(recipient: current_user).order("created_at DESC")
    end
  end
  