class Api::V1::PaymentMethodsController < ApplicationController
    include Constants::Payments
    before_action :authenticate_user_from_token!

    def index
        if current_user.stripe_id.blank?
            render json: { payment_methods: [], is_success: true}, status: :ok
        else
            customer = Stripe::Customer.retrieve(current_user.stripe_id)
            if customer.nil? || customer.sources.nil?
                error = "Can't retrieve payment methods for user"
                Log.error(error, level: "ERROR", type: "PaymentMethod", action: "index")
                render json: { error: error, is_success: false}, status: :bad_request
            else
                payment_methods = customer&.sources&.data

                # TODO: May want to see if the payment should be deleted
                # look through all payment methods and all invalid expired cards
                 valid_payment_methods = []
                 payment_methods.each do |method| 
                    if method.object == "card"
                        if Date.current > Date.new(method&.exp_year, method&.exp_month).end_of_month
                            next
                        end
                    end

                    valid_payment_methods << method
                end

                render json: { payment_methods: valid_payment_methods, default_method: customer.default_source, is_success: true}, status: :ok
            end
        end
    end

    # Adds a payment source, or if source token fingerprint already exists, return existing source
    # Has is_existing_source in return params for client to update views as necessary
    def create
        user = current_user
        if user.stripe_id.blank?
            customer = Stripe::Customer.create(
                name: "#{user.first_name} #{user.last_name}",
                email: user.email
            )
            user.stripe_id = customer.id
            user.save
        else
            customer = Stripe::Customer.retrieve(user.stripe_id)
        end

        # maximum of 5 payment sources for a user
        if customer.sources.count >= 5
            error = "Cannot have more than 5 payment sources attached to a user"
            Log.error(error, level: "ERROR", type: "PaymentMethod", action: "create")
            render json: { error: error, is_success: false}, status: :bad_request
        else
            if params[:payment_method_type] == PAYMENT_TYPES[:card]
                Log.info("Card Payment Method", level: "INFO", type: "PaymentMethod", action: "create")
                source_token = params[:token]
                source_type = :card
            elsif params[:payment_method_type] == PAYMENT_TYPES[:bank]
                Log.info("Bank Payment Method", level: "INFO", type: "PaymentMethod", action: "create")
                source_token = setup_plaid_bank_account(params[:token], params[:account_id])       
                source_type = :bank_account         
            else
                error = "Invalid payment type! Couldn't create payment method"
                Log.error(error, level: "ERROR", type: "PaymentMethod", action: "create")
                render json: { error: error, is_success: false}, status: :bad_request
                return
            end

            if source_token.nil?
                error =  "Invalid source token"
                Log.error(error, level: "ERROR", type: "PaymentMethod", action: "create")
                render json: { error: error, is_success: false}, status: :bad_request
                return
            end

            # Retrieve the card fingerprint using the stripe_card_token  
            source_fingerprint = Stripe::Token.retrieve(source_token).try(source_type).try(:fingerprint) 
            # check whether a card with that fingerprint already exists
            if source_fingerprint
                default_source = customer.sources.data.select{|source| 
                    source.object == "card" && source.fingerprint ==  source_fingerprint
                }.last 
                puts "got default source"
                puts default_source
            end

            #create new card if doesn't already exist
            if default_source
                Log.info("Source already exists for user!", level: "INFO", type: "PaymentMethod", action: "create")
                is_existing_source = true
            else
                default_source = customer.sources.create(source: source_token)
                is_existing_source = false
            end

            customer.default_source = default_source.id
            customer.save

            if !is_existing_source
                message = "Successfully attached source #{default_source.id} to customer #{customer.id}"
                Log.info(message, level: "INFO", type: "PaymentMethod", action: "create")
            end

            render json: { payment_method: default_source, is_existing_source: is_existing_source,  is_success: true}, status: :ok
 
        end
    end


  
    # show a user's charge
    def delete_payment_method
        user = current_user
        if user.stripe_id.blank?
            error = "User has no stripe account"
            Log.error(error, level: "ERROR", type: "PaymentMethod", action: "delete")
            render json: { error: error, is_success: false}, status: :bad_request
        else
            payment_method_id = params[:payment_method_id]
            customer = Stripe::Customer.retrieve(current_user.stripe_id)
            sources = customer.sources.data

            if sources.count <= 1
                error = "User must have at least one payment method"
                Log.error(error, level: "ERROR", type: "PaymentMethod", action: "delete")
                render json: { error: error, is_success: false}, status: :bad_request
            else
                payment_method = sources.select{|s|
                    s.id == payment_method_id
                }.first
        
                if payment_method
                    source = Stripe::Customer.delete_source(customer.id, payment_method.id)
                    render json: { payment_method: source, is_success: true}, status: :ok
                else
                    error = "Couldn't retrieve payment method for user"
                    Log.error(error, level: "ERROR", type: "PaymentMethod", action: "delete")
                    render json: { error: error, is_success: false}, status: :bad_request
                end
            end
        end

        rescue Stripe::CardError => e
        render json: { error: e.message, is_success: false}, status: :bad_request
    end

    private

    def setup_plaid_bank_account(plaid_token, account_id)
        Log.info "Setting up configs for plaid account"

        client = Plaid::Client.new(env: ENV['PLAID_ENV'],
                    client_id: ENV['PLAID_CLIENT_ID'],
                    secret: ENV['PLAID_SECRET'],
                    public_key: ENV['PLAID_PUBLIC_KEY'])
  
        exchange_token_response = client.item.public_token.exchange(plaid_token)
        access_token = exchange_token_response['access_token']

        stripe_response = client.processor.stripe.bank_account_token.create(access_token, account_id)
        bank_account_token = stripe_response['stripe_bank_account_token']
      end
  end
  