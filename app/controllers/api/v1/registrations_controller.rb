class Api::V1::RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user_from_token!, except: [:create, :update]

  # def after_sign_up_path_for(resource)
  #   '/an/example/path' # Or :prefix_to_your_route
  # end

  def create
    super do |user|
      if user.save
        AccountMailer.new.confirmation(user,user.confirmation_token)
        user_serializer = UserSerializer.new(user)
        render json: {
          user: UserSerializer.new(user), 
          provider: "email",
          is_success: true
        }, status: :created 
        return
      end
    end
  end

end