require 'twilio-ruby'
class TwilioClient
    VERIFY_SID = ENV['TWILIO_VERIFY_SID']

    def initialize(service = nil)
        @client = Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
        @service = service.nil? ? VERIFY_SID : service
    end

    def send_token(number)
        verification = @client.verify
                              .services(@service)
                              .verifications
                              .create(to: number, channel: 'sms')
        return verification
    end

    def check_token(number, token)
        verification_check = @client.verify
                                    .services(@service)
                                    .verification_checks
                                    .create(to: number, code: token)
        return verification_check
    end
end
