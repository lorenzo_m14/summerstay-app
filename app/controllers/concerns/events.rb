# Event class is designed for sending notifications and emails
class Events
	include NotificationsHelper::Reservations
	include NotificationsHelper::Listings
	include NotificationsHelper::Messages
	include NotificationsHelper::Payments

	# Reservation Events
	def create_request(reservation)
		create_request_notification(reservation)

		ReservationMailer.new.booking_request(reservation)
	end

	def request_expiry(reservation)
		request_expiry_notification(reservation)

		ReservationMailer.new.request_expiry_host(reservation)
	end

	def accepted_request(reservation)
		accepted_request_notification(reservation)

		mailer = ReservationMailer.new
		mailer.accepted_booking_host(reservation)
		if reservation.reservation_cost.upfront?
				mailer.accepted_standard_booking_renter(reservation)
		else
				mailer.accepted_pays_booking_renter(reservation)
		end
	end

	def declined_request(reservation)
		declined_request_notification(reservation)

		ReservationMailer.new.declined_booking_renter(reservation)
	end

	# TODO: cancelation by different users
	def cancelled_by_renter(reservation)
		cancelled_by_renter_notification(reservation)

		mailer = ReservationMailer.new
		mailer.cancelled_reservation_by_renter(reservation, reservation.host)
		mailer.cancelled_reservation_by_renter(reservation, reservation.user)
	end

	def cancelled_by_host(reservation)
		cancelled_by_host_notification(reservation)

		mailer = ReservationMailer.new
		mailer.cancelled_reservation_by_host(reservation, reservation.host)
		mailer.cancelled_reservation_by_host(reservation, reservation.user)
	end

	def pre_move(reservation)
		pre_move_notification(reservation)

		mailer = ReservationMailer.new
		mailer.pre_move_renter(reservation)
		mailer.pre_move_host(reservation)
	end

	def move_in(reservation)
		move_in_notification(reservation)

		mailer = ReservationMailer.new
		mailer.move_in_renter(reservation)
		mailer.move_in_host(reservation)
	end

	def move_out(reservation)
		move_out_notification(reservation)

		mailer = ReservationMailer.new
		mailer.move_out_renter(reservation)
		mailer.move_out_host(reservation)
	end

	# TODO: add deposit returned event in future
	def deposit_returned(reservation)
		deposit_returned_notification(reservation)

		mailer = ReservationMailer.new
		mailer.deposit_returned_renter(reservation) # placeholder
	end

	# Listing
	def completed_listing(listing)
		completed_listing_notification(listing)

		mailer = ListingMailer.new
		mailer.completed_listing(listing)
	end

	def inactive_listing(listing)
		inactive_listing_notification(listing)

		# TODO: add mailer for inactive listings in future
	end

	# TODO: add this event in future
	def unfinished_listing(listing)
		unfinished_listing_notification(listing)

		mailer = ListingMailer.new
		mailer.unfinished_listing(listing)
	end

	# Messages
	def send_message(message)
		message_notification(message)

		AccountMailer.new.message(message)
	end

	# Payments
	def upcoming_payment(charge)
		upcoming_payment_notification(charge)

		PaymentMailer.new.upcoming_payment(charge)
	end

	def payout_success(payout)
		PaymentMailer.new.payout_success_email(payout)
	end

	# def payout_host_verification(user)
	# 	payout_verification_notification(user)

	# 	PaymentMailer.new.payout_verification_email(user)
	# end

	def connect_status_update(user)
		if user.Verified?
				connect_status_verified_notification(user)
		elsif user.Due?
				connect_status_due_notification(user)
		elsif user.PastDue? || user.Disabled?
				connect_status_blocked_notification(user)
		else
				puts "Should not send status update for #{user.connect_account_status} "
				return false
		end

		AccountMailer.new.connect_status_update(user)
	end

	# TODO: Add this
	def payment_declined(charge)
		payment_declined_notification(charge)

		PaymentMailer.new.payment_declined(charge)
	end

	# TODO: Payment events
	def payment_due(charge)
	end

	def payment_overdue()
	end
end