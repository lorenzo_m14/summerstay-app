# PaymentServices class handles interfacing with Stripe
class PaymentServices
  include Utilities
  include Constants::Payments
  include Constants::Reservations

  # PaymentServices constructor from Reservation and Listing
  def initialize(reservation, listing = nil)
    @reservation = reservation
    if listing.nil? && @reservation.present?
      @listing = @reservation.listing
    else
      @listing = listing
    end
  end

  ### PaymentServices API ###

  # Retrieve payment method based on payment_method_id
  # If payment_method_id exists with Customer, use it
  # Otherwise, try to create it on the customer 
  def update_customer_payment_method(payment_method_id)
    # payment_method = fetch_payment_method(payment_method_id)
    customer = find_customer
    sources = customer&.sources&.data
    payment_method = sources.select{|s| s.id == payment_method_id}.first

    # check if payment method is new payment method
    # if we can't fetch a payment method, but a payment token was passed, create new source for customer
    if payment_method.nil? && !payment_method_id.nil?
      Log.debug 'Creating new source here'
      payment_method = Stripe::Customer.create_source(
        customer.id,
        {
          source: payment_method_id,
        }
      )
    end

    if payment_method.nil?
      payment_method = sources.select{|s| s.id == customer.default_source}.first if payment_method.nil?
      if payment_method.nil?
        Log.debug 'Cannot update payment method'
        return nil, nil
      end
    else
      Log.debug 'Update default payment method'

      customer = Stripe::Customer.update(customer.id, {
          default_source: payment_method.id,
      })
    end
    return payment_method, customer
  end

  # Returns (unsaved) ReservationCost and Charges computed for params
  def get_reservation_prices(charge_type, start_date, end_date, payment_method_type=nil, payment_method_id=nil)
    # Check for if frontend returns strings instead of integers for charge_type enum
    if charge_type == 'upfront'
      charge_type = 0
    elsif charge_type == 'continuous'
      charge_type = 1
    end
    rc = compute_reservation_cost(charge_type, start_date, end_date, payment_method_type, payment_method_id)
    charges = compute_reservation_charges(rc, start_date, end_date)
    if charges.map(&:total_cost).sum != rc.total_cost
      rc.stripe_fee = charges.map(&:stripe_fee).sum
      rc.total_cost = charges.map(&:total_cost).sum
    end
    return rc, charges
  end

  # Creates and saves ReservationCost and Charges computed for Reservation params
  def create_reservation_charges(charge_type = 0, payment_method_type = nil, payment_method_id = nil)
    if @reservation.nil?
      return nil, 'Invalid reservation for charges'
    end

    rc = compute_reservation_cost(charge_type, @reservation.start_date, @reservation.end_date, payment_method_type, payment_method_id)
    charges = compute_reservation_charges(rc, @reservation.start_date, @reservation.end_date)
    if charges.map(&:total_cost).sum != rc.total_cost
      rc.stripe_fee = charges.map(&:stripe_fee).sum
      rc.total_cost = charges.map(&:total_cost).sum
    end

    charges.each {|c| c.save! }
    @reservation.reservation_cost = rc
    @reservation.charges = charges

    return charges, nil
  end

  # Creates an initial Stripe::Invoice and either final Stripe::Invoice or Stripe::SubscriptionSchedule
  # 1. Creates initial Stripe::Invoice with based on ReservationCost, marks as paid and saves stripe_charge_id
  # 2. depending on charge type, creates remaining charges
  # 2a. if standard, creates second charge and then finalizes it, saves hosted invoice url as well
  # 2b. if continuous, creates subscription schedule and saves fields to ReservationCost object
  def approve_reservation
    return nil, 'Invalid reservation' if @reservation.nil?

    reservation = @reservation

    charges = reservation.charges
    rc = reservation.reservation_cost
    init_charge = charges.where(charge_num: 0).first

    # Create initial invoice

    # Idempotency check to see if charge already has stripe_charge_id
    if init_charge.stripe_charge_id.nil?
      Log.debug 'Initial charge does not yet have a stripe_charge_id'

      # instantiate invoices/subscriptions based on rc charge type
      invoice1, error = create_initial_invoice

      if error.present?
        return nil, error
      end

      # Handle if stripe can't fetch invoice
      if invoice1.nil?
        error = 'Could not create initial invoice'
        Log.info(error, level: 'ERROR', module: self.class.name, method: __callee__)
        return nil, error
      else
        # assign created invoice id to charge
        Log.info('Successfully created initial invoice', level: 'INFO', module: self.class.name, method: __callee__)
        init_charge.Paid!
        init_charge.update!(stripe_charge_id: invoice1.id)

        # update rc deposit_status to paid
        rc.update!(deposit_status: :Paid)
      end
    else
      Log.warn("Charge #{init_charge.id} already has a stripe_charge_id", level: 'WARN', module: self.class.name, method: __callee__)
    end

    # Standard payment with emailed stripe invoice
    if rc.upfront?
      # Idempotency check to see if charge already has stripe_charge_id
      second_charge = charges.where(charge_num: 1).first
      if second_charge.stripe_charge_id.nil?
        Log.debug 'Standard second charge does not yet have a stripe_charge_id'
        invoice2 = create_second_standard_invoice

        if invoice2.nil?
          error = 'Could not create standard second invoice'
          return nil, error
        else
          Log.info("Successfully created standard second invoice", level: "INFO", module: self.class.name, method: __callee__)
          second_charge.update!(invoice_url: invoice2.hosted_invoice_url, stripe_charge_id: invoice2.id)
        end
      else
        Log.info("Charge #{second_charge.id} already has a stripe_charge_id", level: "WARN", module: self.class.name, method: __callee__)
      end
      
    elsif rc.continuous? # continuous charge
      # Idempotency check to see if reservation already has sub_schedule_id
      if rc.sub_schedule_id.nil?
        sub_sched_charge = create_subscription_schedule

        if sub_sched_charge.nil?
          error = "Could not create subscription schedule"
          return nil, error
        end
        Log.info("Successfully create subscription schedule and updated reservation cost", level: "WARN", module: self.class.name, method: __callee__)
      else
        Log.info("Reservation #{rc.reservation.id} already has an associated subscription schedule #{rc.sub_schedule_id}.", level: "WARN", module: self.class.name, method: __callee__)
      end
    end

    return charges, nil
  end

  # Computes payouts starting (start_date + CHARGE_PERIOD) (week 2)
  # payouts occur on a biweekly basis
  # For odd week reservations, last payout will occur after moveout on even week, paying out the last part of the stay
  def compute_payouts
    reservation = @reservation 
    return nil, "Reservation is nil" if reservation.nil?

    Log.debug "Computing payouts for reservation #{reservation.id}"

    error = nil
    rc = reservation.reservation_cost

    # calculate dates and cost params
    payout_per_week = (rc.destination_amount/reservation.num_weeks).ceil
    num_payouts = (reservation.num_weeks/(CHARGE_PERIOD).to_f).ceil

    listing_user = reservation.listing.user

    # initial payout should start at week 2 of stay
    initial_payout_date = reservation.start_date.to_date + (CHARGE_PERIOD).weeks

    payouts = []
    0.upto(num_payouts-1) do |i|
      # Check if it's the last payout and we have odd num weeks
      # if this is the case, only pay out 1 week's worth
      payout_date = initial_payout_date + (i * CHARGE_PERIOD).weeks
      if payout_date > reservation.end_date.to_date && reservation.num_weeks % 2 == 1
        Log.debug "Payout for final week of rent (odd # of weeks)"
        payout_weeks = 1
      else
        Log.debug "Normal payout (even # of weeks)"
        payout_weeks = PAYOUT_WEEKLY_INTERVAL
      end

      payout = Payout.new({
        # status defaults to unpaid
        due_by: payout_date,

        # cost params
        payout_amount: payout_per_week * payout_weeks,
        payout_num: i,

        merchant_id: listing_user.merchant_id,
        reservation_id: reservation&.id,
        user_id: listing_user.id
      })
      payouts.append(payout)
    end

    return payouts, error
  end

  # Create Payouts and setup jobs to perform Stripe::Transfers
  def instantiate_payouts
    payouts, error = compute_payouts
    if !error.nil?
      return nil, error
    end
    payouts.each {|p| p.save! }

    # create jobs for each of the payouts
    @reservation.payouts = payouts
    for p in payouts do
      if p.jid.nil?
        Log.debug "Creating job for payout #{p.id}"
        jid = PayoutWorker.perform_in(p.due_by.in_time_zone.to_time, @reservation.id, p.id)
        p.update!(jid: jid)
      else
        # For idempotency of payout jobs
        Log.info("Payout #{p.id} already has job with id #{p.jid}", level: "WARN", module: self.class.name, method: __callee__)
      end
    end

    return payouts, nil
  end

  # Capture Stripe::Transfer for Payout based on payout_id
  # Called when PayoutWorker executes for a Payout
  def capture_transfer_payout(payout_id)
    payout = Payout.find_by_id(payout_id)

    # capture the stripe payout and update with the returned stripe payout
    if payout.nil?
      Log.info("Cannot capture payout with id #{payout_id}", level: "INFO")
      return nil
    elsif payout.stripe_transfer_id.blank?
      payout = create_stripe_transfer_from_payout(payout)
      if payout.nil?
        return nil
      end
    else
      Log.info("Payout with id #{payout_id} already has an associated Stripe::Transfer", level: "INFO")
      return nil
    end

    return payout
  end

  # Cancels all remaining Stripe Payments (invoice or subscription) for a given reservation
  # 1. Void all unpaid Stripe::Invoices for Charges (standard)
  # 2. Cancel Stripe::Subscription or Stripe::SubscriptionSchedule for Reservation (continuous)
  def cancel_stripe_payments_for_reservation
    r = @reservation
    rc = r.reservation_cost
    if rc.upfront?
      # find all unpaid Stripe::Invoices and void them
      unpaid_charges = r.charges.where(status: :Unpaid)
      for c in unpaid_charges
        if !c.stripe_charge_id.nil?
          invoice = Stripe::Invoice.void_invoice(c.stripe_charge_id)
          Log.info("Canceling unpaid invoice #{c.stripe_charge_id}", level: "INFO")
          c.Cancelled!
        end
      end
      return nil
    else
      # if subscription_id is nil, subscription is still a schedule
      # so we want to cancel the schedule
      if rc.subscription_id.nil?
        # TODO: Use sdk when Stripe releases for SubSched
        # Cancel the schedule
        if rc.sub_schedule_id.nil?
          error = "No subscription to cancel!"
          return error
        else
          options = {
            basic_auth: {
              username: ENV["STRIPE_SECRET_KEY"],
              password:'',
            }
          }
          response = HTTParty.post("https://api.stripe.com/v1/subscription_schedules/#{rc.sub_schedule_id}/cancel", options)
    
          # check for response error
          if response.parsed_response.key?("error")
            return response.parsed_response["error_description"]
          else
            Log.info("Successfully cancelled subscription schedule #{rc.sub_schedule_id}", level: "INFO")
            rc.update!(sub_status: :Cancelled)
            return nil
          end
        end
      else 
        # cancel the subscription
        sub = Stripe::Subscription.delete(rc.subscription_id)
        if sub.nil?
          error = "Could not cancel subscription #{rc.subscription_id}"
          return error
        else
          Log.info("Successfully cancelled subscription #{rc.subscription_id}", level: "INFO")
          rc.update!(sub_status: :Cancelled)
          return nil
        end
      end
    end
  end

  # TODO: Currently unused because doesn't actually charge money, just charges negative balance in Stripe Connect
  # Collects host deposit from source (currently Stripe Connect account)
  def collect_host_deposit
    host = @reservation.listing.user
    if !host.merchant_id.blank?
      charge = Stripe::Charge.create({
        amount: HOST_DEPOSIT,
        currency: DEFAULT_CURRENCY,
        source: host.merchant_id,
        metadata: {
          type: 'host',
          reservation_id: @reservation.id
        }
      })
    else
      return nil
    end
  end

  private
  
  def find_customer
    user = @reservation&.user
    if user.stripe_id
      Stripe::Customer.retrieve(user.stripe_id)
    else
      nil
    end
  end

  ### ReservationCost and Charges helper methods ###

  # Compute and return ReservationCost params from given input params
  # NOTE: the ReservationCost is not a saved object yet, so can be used for reservation preview
  def compute_reservation_cost(charge_type, start_date, end_date, payment_method_type=nil,payment_method_id=nil)
    if payment_method_id.nil?
      payment_method_type = payment_method_type.nil? ? PAYMENT_TYPES[:card] : payment_method_type
    else
      payment_method = fetch_payment_method(payment_method_id)
      payment_method_type = payment_method.object
      payment_method_id = payment_method.id
    end

    num_days = (start_date.to_date..end_date.to_date).count
    num_weeks = (num_days/7.0).ceil
    if charge_type == CHARGE_TYPES[:continuous]
      num_charges = (num_weeks/CHARGE_PERIOD).to_i
    else
      num_charges = 2 # 2 charges for standard payment type
    end

    # price and date params
    listing = @listing
    weekly_rent = listing.price
    total_rent = num_weeks * weekly_rent
    due_by = start_date

    # fee calculations
    deposit = weekly_rent * DEPOSIT_WEEKS
    continuous_fee_perc = charge_type == CHARGE_TYPES[:continuous] ? SS_CONTINUOUS_PERC : 0
    renter_service_fee = total_rent * SS_RENTER_PERC
    continuous_fee = total_rent * continuous_fee_perc
    # discount = is_discount ? DISCOUNT : 0

    # stripe fee either 3% with card or +$5.25 with ACH deposit
    non_stripe_payment = total_rent + renter_service_fee + continuous_fee + deposit # + discount
    stripe_fee = calculate_stripe_payment_fee(non_stripe_payment, payment_method_type, num_charges).ceil
    tax = ((non_stripe_payment + stripe_fee) * TAX_RATE).ceil
    
    # total cost and payout calculations
    total_cost = non_stripe_payment + stripe_fee + tax
    destination_amount = total_rent * (1 - SS_HOST_PERC - STRIPE_ADJ_PAYOUT_PERC)

    reservation_cost_params = {
      price: weekly_rent,
      renter_service_fee: renter_service_fee,
      continuous_fee: continuous_fee,
      total_rent: total_rent,
      # platform_cost: total_rent + renter_service_fee,
      # discount: discount,
      deposit: deposit,
      stripe_fee: stripe_fee,
      tax: tax, 
      total_cost: total_cost,
      destination_amount: destination_amount,
      charge_type: charge_type,
      num_weeks: num_weeks,

      payment_method_type: payment_method_type,
      payment_method_id: payment_method_id
    }

    # Return ReservationCost object
    ReservationCost.new(reservation_cost_params)
  end

  # Constructs charges based on ReservationCost object, and Reservation length (start and end)
  # Number of charges and cost varies depending on charge_type (upfront or continuous)
  # NOTE: these charges are not saved objects yet, so can be used for reservation preview
  def compute_reservation_charges(rc, start_date, end_date)

    # calculate dates and num charges
    num_days = (start_date.to_date..end_date.to_date).count
    num_weeks = (num_days/7.0).ceil
    due_by = start_date
    num_charges = (num_weeks/CHARGE_PERIOD).to_i

    # cost calculation
    initial_charge = rc.price * INIT_CHARGE_WEEKS 
    additional_fees = rc.service_fees # + rc.discount

    charge_type = rc.upfront? ? CHARGE_TYPES[:upfront] : CHARGE_TYPES[:continuous]

    charges = []
    deposit = rc.deposit
    charge0 = create_charge(
                charge_type,
                0,
                start_date,
                initial_charge + additional_fees,
                rc.deposit,
                rc.payment_method_type,
                rc.payment_method_id
              )
    charges.append(charge0)

    if rc.upfront? # pay upfront
      Log.debug "Upfront charge type"
      upfront_cost = rc.total_rent - initial_charge

      # second charge is due 2 weeks before move in by default
      second_charge_due_date = start_date.to_date - STANDARD_PAYMENT_DUE_DAYS.days

      charge1 = create_charge(
        charge_type,
        1, # payment num 2
        second_charge_due_date,
        upfront_cost,
        0,
        rc.payment_method_type,
        rc.payment_method_id
      )
      charges.append(charge1)

    elsif rc.continuous? # pay as you stay
      cost_per_charge = ((rc.total_rent-initial_charge)/(num_charges-1)).ceil

      1.upto(num_charges-1) do |num_charge|
        due_date = start_date.to_date + (num_charge * CHARGE_PERIOD + DELAY_PERIOD).weeks
        charge = create_charge(
                  charge_type,
                  num_charge,
                  due_date,
                  cost_per_charge,
                  0,
                  rc.payment_method_type,
                  rc.payment_method_id
                  )
        charges.append(charge)
      end
    end

    return charges
  end

  # Create a charge in the database from params
  def create_charge(charge_type, charge_num, due_by, cost, deposit, payment_method_type=nil, payment_method_id=nil)
    # default method right now is card payment, will have bank in future
    payment_method_type = PAYMENT_TYPES[:card] if payment_method_type.nil?

    stripe_fee = calculate_stripe_payment_fee(cost + deposit, payment_method_type, 1)
    total_cost = deposit + cost + stripe_fee

    charge_params = {
      # status defaults to unpaid
      due_by: due_by,

      # cost params
      deposit: deposit,
      stripe_fee: stripe_fee,
      platform_cost: cost,
      total_cost: total_cost,

      # metadata params
      charge_type: charge_type,
      charge_num: charge_num,
      payment_method_type: payment_method_type,

      reservation_id: @reservation&.id,
      user_id: @reservation&.user&.id,
    }

    Charge.new(charge_params)
  end


  ### Payout helper methods ###

  # Payouts are associated with Stripe::Transfer.
  # Save Stripe::Tranfer id as stripe_payout_id for Payout
  def create_stripe_transfer_from_payout(payout)

    # Idempotency check for payout
    if !payout.stripe_transfer_id.nil?
      Log.info("Payout #{payout.id} already has an associated Stripe::Transfer #{payout.stripe_transfer_id}", level: "INFO")
      return nil
    end

    r = payout.reservation
    transfer = Stripe::Transfer.create({
      amount: payout.payout_amount,
      currency: DEFAULT_CURRENCY,
      destination: payout.merchant_id,
      metadata: {
        due_by: payout.due_by,
        num_payout: payout.payout_num,
        reservation_id: payout.reservation_id
      },
      transfer_group: "reservation_#{r.id}",
    })

    if transfer.nil?
      Log.info("Unable to create Stripe::Transfer with id #{transfer.id} for reservation #{r.id}, payout ##{payout.payout_num}", level: "INFO")
      return nil
    else
      # save stripe payout id to payout object
      payout.update!(status: :Paid, stripe_transfer_id: transfer.id)
      Log.info("Created Stripe::Transfer with id #{transfer.id} for reservation #{r.id}, payout ##{payout.payout_num}", level: "INFO")

      return payout
    end
  end


  ### Stripe::Invoice/Subscription helper methods ###

  # Creates the initial invoice for both charge types
  def create_initial_invoice
    customer = find_customer
    reservation = @reservation
    rc = reservation.reservation_cost
    start_date = reservation.start_date
    num_weeks = reservation.num_weeks 

    num_charges = (num_weeks/CHARGE_PERIOD).to_i
    initial_rent_amount = rc.price * INIT_CHARGE_WEEKS

    # Delete any existing InvoiceItems to prevent accidental requests
    delete_floating_invoice_items(customer.id)
  
    # invoice item 1: deposit
    # invoice item 2: initial rent payment (2 weeks + renter service fee)
    # invoice item 3: [optional] PAYS fees
    # invoice item 4: processing fees
    Stripe::InvoiceItem.create({
      customer: customer.id,
      amount: rc.deposit,
      currency: 'usd',
      description: 'Security Deposit'
    })

    # Initial Rent Payment = 2 weeks rent + renter service fee
    Stripe::InvoiceItem.create({
      customer: customer.id,
      amount: initial_rent_amount + rc.renter_service_fee,
      currency: 'usd',
      description: 'Initial Rent Payment'
    })

    # add PAYS line item only if continuous
    if rc.continuous?
      Stripe::InvoiceItem.create({
        customer: customer.id,
        amount: rc.continuous_fee,
        currency: 'usd',
        description: 'Pay-As-You-Stay Fee'
      })
    end

    # stripe fee is on top of total platform fee
    total_platform_fee = rc.service_fees + initial_rent_amount + rc.deposit
    stripe_fee = calculate_stripe_payment_fee(total_platform_fee, rc.payment_method_type)

    Stripe::InvoiceItem.create({
      customer: customer.id,
      amount: stripe_fee,
      currency: 'usd',
      description: 'Processing Fee'
    })

    # initial invoice
    stripe_invoice = Stripe::Invoice.create({
      customer: customer.id,
      collection_method: 'charge_automatically',
      metadata: {
        type: 'renter',
        num_charge: 0,
        reservation_id: reservation.id,
      },
      description: "Reservation for #{reservation.listing.listing_name}: Initial Payment"
      # auto_advance: true,
      # application_fee_amount: rc.renter_service_fee
    })

    if !stripe_invoice.nil?
      begin
        payed_stripe_invoice = Stripe::Invoice.pay(stripe_invoice.id)

      # if invoice charge fails, void invoice and log error
      rescue Stripe::InvalidRequestError
        void_invoice = Stripe::Invoice.void_invoice(stripe_invoice.id)
        Log.error("Voided invoice due to Stripe InvalidRequestError", level: "ERROR", type: "Stripe", module: self.class.name, method: __callee__)
        raise
        # return nil, "Payment failed due to bad payment method!"
      rescue Stripe::CardError
        void_invoice = Stripe::Invoice.void_invoice(stripe_invoice.id)
        Log.error("Voided invoice due to Stripe CardError", level: "ERROR", type: "Stripe", module: self.class.name, method: __callee__)
        raise
        # return nil, "Payment failed due to card payment error!"
      end

      if !payed_stripe_invoice&.charge.nil?
        stripe_charge = Stripe::Charge.retrieve(payed_stripe_invoice.charge)
        charge = reservation.charges.where(charge_num: 0).first          
        charge.Paid!
        charge.update!(paid_at: DateTime.current, receipt_url: stripe_charge&.receipt_url)
        Log.debug "Updated charge #{charge.id} with receipt and payment timestamp" 
      else 
        error = "Could not update charge with receipt and payment timestamp"
        Log.info(error, level: "ERROR", module: self.class.name, method: __callee__)
      end
    else
      Log.debug "Initial stripe invoice already exists!"
    end
    return payed_stripe_invoice, error
  end

  # Creates the second (and final) invoice for a standard payment type
  # for charge type 0: upfront payment second charge
  def create_second_standard_invoice
    Log.debug "Creating second invoice"
    customer = find_customer
    reservation = @reservation
    rc = reservation.reservation_cost

    start_date = reservation.start_date
    num_weeks = reservation.num_weeks    

    num_charges = (num_weeks/CHARGE_PERIOD).to_i
    initial_rent_amount = rc.price * INIT_CHARGE_WEEKS
    rem_rent_amount = rc.total_rent - initial_rent_amount
    stripe_fee = calculate_stripe_payment_fee(rem_rent_amount, rc.payment_method_type)

    # Delete any existing InvoiceItems to prevent accidental requests
    delete_floating_invoice_items(customer.id)
    Log.debug "Adding invoice items"

    Stripe::InvoiceItem.create({
      customer: customer.id,
      amount: rem_rent_amount,
      currency: 'usd',
      description: 'Final Rent Payment'
    })

    Stripe::InvoiceItem.create({
      customer: customer.id,
      amount: stripe_fee,
      currency: 'usd',
      description: 'Processing Fee'
    })

    # create second standard invoice
    # If we are within two weeks of start date, automatically charge for second payment
    # Else create a charge due within two weeks of the start date
    if Date.current >= start_date - STANDARD_PAYMENT_DUE_DAYS.days
      # automatically charge for second payment as well
      stripe_invoice = Stripe::Invoice.create({
        customer: customer.id,
        collection_method: 'charge_automatically',
        metadata: {
          type: 'renter',
          num_charge: 1,
          reservation_id: reservation.id,
        },
        description: "Reservation for #{reservation.listing.listing_name}: Second Payment"
      })
      if !stripe_invoice.nil?
        stripe_invoice = Stripe::Invoice.pay(stripe_invoice.id)
        if !stripe_invoice&.charge.nil?
          stripe_charge = Stripe::Charge.retrieve(stripe_invoice.charge)
          charge = reservation.charges.where(charge_num: 1).first
          charge.Paid!
          charge.update!(paid_at: DateTime.current, receipt_url: stripe_charge&.receipt_url)
          Log.debug "Updated charge #{charge.id} with receipt and payment timestamp"

          # reservation has completed its payments!
        else 
          error = "Could not update charge #{charge.id} with receipt and payment timestamp"
          Log.error(error, level: 'ERROR', module: self.class.name, method: __callee__) 
        end
      else
        Log.debug 'Second stripe invoice already exists!'
      end
    else
      due_date = start_date - STANDARD_PAYMENT_DUE_DAYS.days
      # send stripe_invoice email and create link
      stripe_invoice = Stripe::Invoice.create({
        customer: customer.id,
        collection_method: 'send_invoice',
        metadata: {
          type: 'renter',
          num_charge: 1,
          reservation_id: reservation.id,
        },
        description: "Reservation for #{reservation.listing.listing_name}: Second Payment",
        due_date: due_date.in_time_zone.to_time.to_i,
        auto_advance: true
      })
      if !stripe_invoice.nil?
        stripe_invoice = Stripe::Invoice.finalize_invoice(stripe_invoice.id)
      else
        Log.debug 'Could not create second stripe invoice!'
      end
    end

    return stripe_invoice
  end

  # Called before creation of invoices for idempotency
  # deletes any invoice items created from an incomplete invoice
  def delete_floating_invoice_items(customer_id)
    floating_invoice_items = Stripe::InvoiceItem.list( {
      customer: customer_id,
      pending: true
    })
    if !floating_invoice_items.nil?
      for i in floating_invoice_items&.data
        Stripe::InvoiceItem.delete(i.id)
      end
    end
  end

  # Create Stripe::SubscriptionSchedule and save as sub_schedule_id in ReservationCost
  # This schedule will release a Stripe::Subscription via a webhook and that will be saved to subscription_id
  def create_subscription_schedule
    customer = find_customer

    reservation = @reservation
    rc = reservation.reservation_cost
    num_weeks = reservation.num_weeks    

    num_rem_charges = (num_weeks/CHARGE_PERIOD - 1).to_i
    initial_rent_amount = rc.price * INIT_CHARGE_WEEKS
    rem_rent_cost = rc.total_rent - initial_rent_amount
    charge_amount = (rem_rent_cost/num_rem_charges).ceil

    # TODO: solve off by one errors for plan charges

    # calculate fees incurred for plan
    stripe_fee = calculate_stripe_payment_fee(charge_amount, rc.payment_method_type)
    plan_charge_amount = charge_amount + stripe_fee

    # total cost for subscription
    total_plan_cost = rem_rent_cost + calculate_stripe_payment_fee(rem_rent_cost, rc.payment_method_type, num_rem_charges)

    Log.debug "Creating plan for reservation #{reservation.id}"
    # create plan with biweekly charges
    plan = Stripe::Plan.create({
      amount: plan_charge_amount,
      interval: 'week',
      interval_count: CHARGE_PERIOD,
      product: {
        name: "Rent and processing fees for #{reservation.listing.listing_name} (Reservation ID: #{reservation.id})",
      },
      currency: 'usd',
      metadata: {
        type: 'renter',
        reservation_id: reservation.id,
        num_charges: num_rem_charges,
        total_plan_cost: total_plan_cost
      }
    })


    sub_start_date = reservation.start_date.to_date + (CHARGE_PERIOD + DELAY_PERIOD).weeks
    cancel_date = sub_start_date + ((num_rem_charges-1) * CHARGE_PERIOD).weeks

    Log.info("Creating subscription schedule from #{sub_start_date} to #{cancel_date} for #{convert_to_dollars(total_plan_cost)}")
    ## NOTE: TO BE DEPRECATED ONCE STRIPE FEATURE IS OUT OF BETA
    ## Call to create scheduled subscription
    options = {
      basic_auth: {
        username: ENV['STRIPE_SECRET_KEY'],
        password: ''
      },
      body: {
        customer: customer.id,
        start_date: sub_start_date.in_time_zone.to_time.to_i,
        phases: [
          {
            plans: [
              {
                plan: plan.id,
                quantity: 1
              }
            ],
            iterations: num_rem_charges
          }
        ],
        renewal_behavior: 'none',
        metadata: {
          type: 'renter',
          reservation_id: reservation.id
        }
      }
    }

    # create sub schedule
    response = HTTParty.post('https://api.stripe.com/v1/subscription_schedules', options)

    # check for response error
    if response.parsed_response.key?('error')
      Log.error(response.parsed_response['error_description'], level: 'ERROR', module: self.class.name, method: __callee__)
      return response.parsed_response['error_description']
    end

    # update reservation cost with subscription details 
    rc.update!(
      sub_start_date: sub_start_date,
      sub_schedule_id: response.parsed_response['id'],
    )
    rc.Scheduled!

    return response.parsed_response
  end


  ### Payment helper Methods ###

  # Fetches payment method
  # Retrieve all sources from customer to find payment_method_id
  def fetch_payment_method(payment_method_id=nil)
    customer = find_customer
    sources = customer&.sources&.data
    payment_method = sources.select{|s| s.id == payment_method_id}.first
    # if payment method is nil (payment_method_id was either invalid or nil), get default source
    payment_method = sources.select{|s| s.id == customer.default_source}.first if payment_method.nil?
    return payment_method
  end

  # Calculates stripe payment depending on charge type
  # and, if continuous charge type, num_charges
  def calculate_stripe_payment_fee(amount, payment_method_type = PAYMENT_TYPES[:card], num_charges = 1)
    if payment_method_type == PAYMENT_TYPES[:card]
      return (amount * STRIPE_CARD_PERC).ceil
    elsif payment_method_type == PAYMENT_TYPES[:bank]
      charge_fee = STRIPE_ACH_FEE + TRANSACTION_FEE # should be $5.25
      return (num_charges * charge_fee).ceil
    else
      error = 'Unable to calculate stripe fee'
      Log.error(error, level: 'ERROR', module: self.class.name, method: __callee__)
      return 0
    end
  end

  ### Unused Helper Methods ###
  def get_invoices_for_subscription(subscription_id)
    Stripe::Invoice.list(subscription: subscription_id)
  end

  def get_invoices_for_customer(customer_id)
    Stripe::Invoice.list(customer: customer_id)
  end

  def get_invoices_for_reservation(reservation_id)
  end
end