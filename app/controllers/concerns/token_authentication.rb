module TokenAuthentication
    # include Devise::Controllers::Helpers
    # extend ActiveSupport::Concern
    # include ActionController::Helpers
    include ActionController::Cookies

    # included do
    #     private :authenticate_user_from_token!
    #     # This is our new function that comes before Devise's one
    #     before_action :authenticate_user_from_token!
    #     # This is Devise's authentication
    #     before_action :authenticate_user!
    # end

    def current_user
        # DEPRECATE TOKEN AUTH IN FAVOR OF SESSION AUTH FOR SECURITY REASONS

        # Set the authentication params if not already present
        # if user_token = params[:user_token].blank? && request.headers["X-User-Token"]
        #   params[:user_token] = user_token
        # end
        # if user_email = params[:user_email].blank? && request.headers["X-User-Email"]
        #   params[:user_email] = user_email
        # end

        # user_email = params[:user_email].presence
        # user = user_email && User.find_by(email: user_email)

        # Notice how we use Devise.secure_compare to compare the token
        # in the database with the token given in the params, mitigating
        # timing attacks.

        # Use session tokens to perform authentication
        # TODO: Research how to prevent CSRF attacks
        token = session[:token]
        user_id = session[:user_id]

        # puts "current user"
        # puts session[:token]
        # puts session[:user_id]
        
        user = User.find_by_id(user_id)

        if user && Devise.secure_compare(user.authentication_token, token)
            # Notice we are passing store false, so the user is not
            # actually stored in the session and a token is needed
            # for every request. If you want the token to work as a
            # sign in token, you can simply remove store: false.
            @current_user = user
        end
    end

    def authenticate_user_from_token!
        render json: { error: 'Not Authenticated', is_success: false}, status: :unauthorized unless current_user.present?
    end
end