class PassbaseClient
  def verify_id(auth_key)
    options = {
        headers: {
            "Authorization": ENV["PASSBASE_PRIVATE_KEY"],
        }
    }
    # create sub schedule
    response = HTTParty.get("https://app.passbase.com/api/v1/authentications/by_key/#{auth_key}", options)
    response.parsed_response
  end
end
