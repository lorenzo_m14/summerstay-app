# Error module to Handle errors globally
# include Error::ErrorHandler in application_controller.rb
require 'twilio-ruby'
module Error
  module ErrorHandler
    def self.included(klass)
      klass.class_eval do
        rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
        rescue_from ActiveRecord::RecordInvalid, with: :invalid_record
        rescue_from Redis::CannotConnectError, with: :redis_error

        rescue_from Twilio::REST::RestError, with: :twilio_error

        # Stripe and Plaid Errors
        rescue_from Plaid::PlaidAPIError, with: :plaid_error

        rescue_from Stripe::InvalidRequestError, with: :stripe_error
        rescue_from Stripe::CardError, with: :stripe_error
        rescue_from Stripe::APIConnectionError, with: :stripe_error
        rescue_from Stripe::StripeError, with: :stripe_error

        rescue_from Koala::Facebook::ClientError, with: :facebook_key_error

        rescue_from ActionController::RoutingError, with: :route_not_found_error
        rescue_from ActionController::BadRequest, with: :bad_request_error

        # rescue_from StandardError, with: :bad_request_error
      end
    end

    private

    def bad_request_error(e)
      Log.error(e.to_s, level: 'ERROR', type: 'BadRequest', action: 'bad_request_error')
      json = error_to_json('Invalid Request!', e.to_s)
      render json: json, status: :bad_request
    end

    def facebook_key_error(e)
      error = 'Facebook login is down. Sorry for the inconvenience.'
      Log.error(e.to_s, level: 'ERROR', type: 'Facebook', action: 'facebook_key_error')
      json = error_to_json(error, e.to_s)
      render json: json, status: :bad_request
    end

    def redis_error(e)
      Log.fatal(e&.to_s, level: 'FATAL', type: 'Redis', action: 'redis_error')
      json = error_to_json('Server Connection Error!', e.to_s)
      render json: json, status: :bad_request
    end

    def plaid_error(e)
      Log.error(e&.error_code, level: 'ERROR', type: 'Plaid', action: 'plaid_error')
      json = error_to_json(e.error_code, e.error_message)
      render json: json, status: :bad_request
    end

    def stripe_error(e)
      Log.error(e&.error&.message)

      action = e&.error&.type
      case action
      when 'card_error'
        if e.error.decline_code.present?
          # TODO: we should send an message to the renter if this occurs
          error = 'We encountered an issue processing the payment.'
        else
          error = 'We encountered an issue with card payment.'
        end
      when 'invalid_request_error'
        error = 'We encountered an issue processing the payment.'
      when 'api_connection_error'
        error = 'We are currently encountering Stripe connection issues.'
      else
        error = 'Sorry, we encountered a Stripe error.'
      end

      Log.error(error, level: 'ERROR', type: 'Stripe', action: action)
      json = error_to_json(error, action)
      render json: json, status: :bad_request
    end

    def twilio_error(e)
      if e.code == 60200 # invalid phone
        action = 'invalid_phone'
        error = 'Invalid phone number!'
      elsif e.code == 20404 # invalid verification
        action = 'invalid_code'
        error = 'Invalid verification code!'
      else
        action = 'twilio_error'
        error = 'Twilio verification error!'
      end

      json = error_to_json(error, e.to_s)
      Log.error(error, level: 'ERROR', type: 'Twilio', action: action)
      render json: json, status: :bad_request
    end

    def route_not_found_error(e)
      error = e.to_s
      Log.error(error, level: 'ERROR', type: 'Route', action: 'route_not_found')
      json = error_to_json('Invalid Route!', e.to_s)
      render json: json, status: :not_found
    end 

    def record_not_found(e)
      error = e.to_s
      Log.error(error, level: 'ERROR', type: 'Record', action: 'record_not_found')
      json = error_to_json('Record not found!', e.to_s)
      render json: json, status: :not_found
    end

    def invalid_record(e)
      error = e&.record&.errors
      Log.error(error, level: 'ERROR', type: 'Record', action: 'invalid_record')
      json = error_to_json(error, e.to_s)
      render json: json, status: :bad_request
    end

    def error_to_json(error, service_error)
      {
        error: error,
        error_message: service_error,
        is_success: false
      }.as_json
    end
  end
end