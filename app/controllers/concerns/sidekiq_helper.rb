# Sidekiq Helper
# NOTE: reservation_id will ALMOST ALWAYS be first arg
class SidekiqHelper

  # Helper method to see all jobs scheduled for reservation
  def view_reservation_jobs(reservation)
    puts "Job info for Reservation #{reservation.id}"
    jobs = []
    scheduled = Sidekiq::ScheduledSet.new
    scheduled.each do |job|
      if job.args.first == reservation.id
        puts "ID: #{job.jid}"
        puts "Class: #{job.klass}"
        puts "Scheduled Time: #{job.at}"
        puts
        jobs.append(job)
      end
    end
    return jobs
  end

  def remove_expiry_jobs(reservation)
    scheduled = Sidekiq::ScheduledSet.new
    scheduled.each do |job|
      if job.args.first == reservation.id && (job.klass == "ExpiredRequestWorker" || job.klass == "ExpiryReminderWorker" )
        Log.info("Deleting #{job.klass} for reservation #{reservation.id}", level: "INFO")
        job.delete
      end
    end
  end

  def delete_reservation_jobs(reservation)
    scheduled = Sidekiq::ScheduledSet.new
    scheduled.each do |job|
      if job.args.first == reservation.id 
        puts "Deleting #{job.klass} for reservation #{reservation.id}"
        job.delete
      end
    end
  end
end