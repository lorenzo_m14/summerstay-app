class Url
    # extend self
    def initialize(is_relative = true)
        # notifications use relative paths, emails use absolute paths
        @base_url = is_relative ? '' : "#{ENV['CLIENT_BASE_URL']}"
        @dashboard_url = "#{@base_url}/dashboard"
    end

    # Listings
    def create_listing_url
        "#{@base_url}/host"
    end

    def listings
        "#{@base_url}/listings"
    end

    def view_listing(listing_id)
        "#{@base_url}/listings/view/#{listing_id}"
    end

    def edit_listing(listing_id)
        "#{@base_url}/listings/edit/#{listing_id}"
    end

    # NOT PRESENT: Feedback (for feedback form)
    def feedback(reservation_id = nil)
        query = "?reservation_id=#{reservation_id.presence}" if reservation_id.present?
        "#{@base_url}/feedback#{query.presence}"
    end

    def host_claims(listing_id, reservation_id)
        query = "?claim=true&reservation_id=#{reservation_id.presence}"
        "#{@dashboard_url}/listings/#{listing_id}/reservations#{query}"
    end

    def renter_claims(reservation_id)
        query = "?claim=true"
        "#{@dashboard_url}/stays/#{reservation_id}/info#{query}"
    end

    ### DASHBOARD URLS ###

    def dashboard
        "#{@dashboard_url}"
    end

    def dashboard_listings
        "#{@dashboard_url}/listings"
    end

    def stays
        "#{@dashboard_url}/stays"
    end

    # Reservations + listings
    # TODO: can we link to a particular reservation?
    def host_reservation(listing_id, reservation_id=nil)
        "#{@dashboard_url}/listings/#{listing_id}/reservations"
    end

    def renter_reservation(reservation_id)
        "#{@dashboard_url}/stays/#{reservation_id}"
    end

    def host_listing(listing_id)
        "#{@dashboard_url}/listings/#{listing_id}/info"
    end

    # Payments
    def payments
        "#{@dashboard_url}/payments"
    end

    def charge(charge_id)
        "#{@dashboard_url}/payments/#{charge_id}"
    end

    def payouts
        "#{@dashboard_url}/payouts"
    end
    
    # Messages
    def conversations
        "#{@dashboard_url}/chats"
    end

    def message(conversation_id)
        "#{@dashboard_url}/chats/#{conversation_id}"
    end

    # Notifications
    def notifications
        "#{@dashboard_url}/notifications"
    end
    

    # User Settings
    def settings
        "#{@dashboard_url}/settings"
    end

    def confirmation(token)
        "#{@base_url}/confirmation?confirmation_token=#{token}"
    end

    def reset_password(token)
        "#{@base_url}/reset-password?reset_password_token=#{token}"
    end

    # Static
    def how_it_works
        "#{@base_url}/how-it-works"
    end

    def pay_as_you_stay
        "#{@base_url}/pay-as-you-stay"
    end

    def campus_managers
        "#{@base_url}/campus-managers"
    end

    def support
        "#{@base_url}/support"
    end

    # Legal
    def tnc
        "#{@base_url}/terms"
    end

    def privacy
        "#{@base_url}/privacy"
    end
end
