# NOTE: LEADDYNO CLASS IS DEPRECATED!
# class LeadDyno

#   LISTING_COMMISION = 5
#   BOOKING_COMMISSION = 25

#   # TYPES
#   LISTING = "listing"
#   HOST_BOOKING = "booking-host"
#   RENTER_BOOKING = "booking-renter"

#   def create_affiliate(user)
#     response = HTTParty.post("https://api.leaddyno.com/v1/affiliates",
#     query: {
#         key: ENV["LEAD_DYNO_PRIVATE_KEY"],
#         email: user.email,
#         first_name: user.first_name,
#         last_name: user.last_name,
#         affiliate_code: user.get_referral_code,
#     })
#     return response.parsed_response
#   end

#   # Host creates a listing, associate purchase afterwards
#   def capture_listing_lead(host, listing)
#     type = "listing"
#     affiliate_code = listing.get_referral_code
#     tracking_code = "#{type}_#{listing.id}"
#     response = lead_request(host, affiliate_code, tracking_code)
#   end

#   # Renter books a place
#   def capture_booking_lead(renter, reservation)
#     type = "booking"
#     listing = reservation.listing
#     affiliate_code = listing.get_referral_code
#     tracking_code = "#{type}_#{listing.id}"
#     response = lead_request(renter, affiliate_code, tracking_code)
#   end

#   # Host creates a verified listing with code, payout to CM
#   # $5 to CM
#   def capture_listing_purchase(host, listing)
#     type = "listing"
#     purchase_code = "#{type}_#{listing.id}"
#     response = purchase_request(host, purchase_code, LISTING_COMMISION)
#   end

#   # Host successfully converts a verified listing with code, payout to host and CM
#   # $25 to host, CM
#   def capture_booking_purchase_host(host, reservation)
#     type = "host-booking"
#     purchase_code = "#{type}_#{reservation.listing.id}"
#     response = purchase_request(host, purchase_code, BOOKING_COMMISSION)
#   end

#   # Renter successfully books a place, payout to host and CM
#   # $25 to host, CM
#   def capture_booking_purchase_renter(renter, reservation)
#     type = "renter-booking"
#     purchase_code = "#{type}_#{reservation.id}"
#     response = purchase_request(renter, purchase_code, BOOKING_COMMISSION)
#   end
  
#   # TODO: modify cancel purchase based on referral type
#   def cancel_purchase(listing)
#     response = HTTParty.delete("https://api.leaddyno.com/v1/purchases/listing_#{listing.id}",
#     query: {
#         key: ENV["LEAD_DYNO_PRIVATE_KEY"],
#         source: "API"
#     })
#     return response.parsed_response
#   end

#   private
#     def lead_request(user, affiliate_code, tracking_code)

#       response = HTTParty.post("https://api.leaddyno.com/v1/leads",
#       query: {
#           key: ENV["LEAD_DYNO_PRIVATE_KEY"],
#           email: user.email,
#           code: affiliate_code,
#           tracking_code: tracking_code
#       })
#       return response.parsed_response
#     end

#     def purchase_request(user,purchase_code, commission)
#       # TODO: change type and commission amount 
#       # type = referral_type == "renter" ? "renter" : "host"
#       # commission_amount = type == "renter" ? 50 : 50
#       Log.info "LeadDyno purchase code=#{purchase_code}"

#       response = HTTParty.post("https://api.leaddyno.com/v1/purchases",
#       query: {
#           key: ENV["LEAD_DYNO_PRIVATE_KEY"],
#           email: user.email,
#           purchase_code: purchase_code,
#           commission_amount_override: commission
#       })
#       return response.parsed_response
#     end

# end