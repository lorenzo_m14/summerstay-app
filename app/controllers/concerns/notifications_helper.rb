module NotificationsHelper
  module Reservations
    include Constants::NotificationTypes

    def create_request_notification(reservation)
      recipient = reservation.host
      actor = reservation.user

      Notification.create(content: "New booking request from #{actor.first_name}",
      notification_type: RESERVATION_REQUEST,
      recipient_id: recipient.id,
      actor_id: actor.id,
      notifiable_id: reservation.id,
      notifiable_type: reservation.class.name,
      notification_url: Url.new.host_reservation(reservation.listing.id)
      )
    end

    def request_expiry_notification(reservation)
      recipient = reservation.host
      actor = reservation.user

      Notification.create(content: "The booking request from #{actor.first_name} will soon expire on #{reservation.expires_at.strftime("%B %d, %Y")}",
        notification_type: RESERVATION_EXPIRING,
        recipient_id: recipient.id,
        actor_id: actor.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.host_reservation(reservation.listing.id)
      )
    end
    
    def accepted_request_notification(reservation)
      recipient = reservation.user
      actor = reservation.host

      Notification.create(content: "#{actor.first_name} has accepted your booking request for #{reservation&.listing&.listing_name}",
        notification_type: RESERVATION_ACCEPTED,
        recipient_id: recipient.id,
        actor_id: actor.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.renter_reservation(reservation.id)
      )
    end

    def declined_request_notification(reservation)
      recipient = reservation.user
      actor = reservation.host

      Notification.create(content: "#{actor.first_name} has declined your booking request for #{reservation&.listing&.listing_name}",
        notification_type: RESERVATION_ACCEPTED,
        recipient_id: recipient.id,
        actor_id: actor.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.stays
      )
    end

    def cancelled_by_host_notification(reservation)
      recipient = reservation.user
      actor = reservation.host

      Notification.create(content: "#{actor.first_name} has cancelled your stay for #{reservation.listing.listing_name}. Our team will be in touch, we're sorry for the inconvenience.",
        notification_type: RESERVATION_ACCEPTED,
        recipient_id: recipient.id,
        actor_id: actor.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.stays
      )
    end

    def cancelled_by_renter_notification(reservation)
      recipient = reservation.host
      actor = reservation.user

      Notification.create(content: "#{actor.first_name} has cancelled their stay at your listing: #{reservation.listing.listing_name}. Our team will be in touch, we're sorry for the inconvenience.",
        notification_type: RESERVATION_ACCEPTED,
        recipient_id: recipient.id,
        actor_id: actor.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.dashboard_listings
      )
    end

    def pre_move_notification(reservation)
      renter = reservation.user
      host = reservation.host

      # need notification to host and to renter, just like emails

      # for the renter
      Notification.create(content: "Your stay at #{reservation.listing.listing_name} is coming up on #{reservation.start_date.strftime("%B %d, %Y")}",
        notification_type: REMINDER_PREMOVE,
        recipient_id: renter.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.renter_reservation(reservation.id)
      )

      # for the host 
      Notification.create(content: "Stay at #{reservation.listing.listing_name} is coming up on #{reservation.start_date}",
        notification_type: REMINDER_PREMOVE,
        recipient_id: host.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.host_reservation(reservation.listing.id)
      )
    end

    def move_in_notification(reservation)
      renter = reservation.user
      host = reservation.host
  
      # for the renter
      Notification.create(content: "It's move-in day! Be prepared for your stay at #{reservation.listing.listing_name}",
        notification_type: REMINDER_MOVEIN,
        recipient_id: renter.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.renter_reservation(reservation.id)
      )

      # for the host 
      Notification.create(content: "It's move-in day! #{reservation.user.first_name} is moving into #{reservation.listing.listing_name}",
        notification_type: REMINDER_MOVEIN,
        recipient_id: host.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.host_reservation(reservation.listing.id)
      )
    end

    def move_out_notification(reservation)
      renter = reservation.user
      host = reservation.host
  
      # for the renter
      Notification.create(content: "It's move-out day! We hope you enjoyed your stay at #{reservation.listing.listing_name}",
        notification_type: REMINDER_MOVEOUT,
        recipient_id: renter.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.dashboard # TODO: Change this to claims/feedback
      )

      # for the host 
      Notification.create(content: "It's move-out day! Thank your for subletting with SummerStay",
        notification_type: REMINDER_MOVEOUT,
        recipient_id: host.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.dashboard # TODO: Change this to claims/feedback
      )
    end

    # TODO: add deposit returned event
    def deposit_returned_notification(reservation)
      renter = reservation.user
      Notification.create(content: "It's move-out day! Thank your for subletting with SummerStay",
        notification_type: REMINDER_MOVEOUT,
        recipient_id: renter.id,
        notifiable_id: reservation.id,
        notifiable_type: reservation.class.name,
        notification_url: Url.new.dashboard # TODO: Change this to claims/feedback
      )
    end
  end

  module Listings
    include Constants::NotificationTypes

    def completed_listing_notification(listing)
      Notification.create(content: "Congratulations on completing your listing! We will alert you of all booking requests via email",
        notification_type: LISTING_COMPLETED,
        recipient_id: listing.user_id,
        notifiable_id: listing.id,
        notifiable_type: listing.class.name,
        notification_url: Url.new.host_listing(listing.id)
      )
    end

    def inactive_listing_notification(listing)
      Notification.create(content: "We've inactivated your listing at #{listing.listing_name} because of too many expired booking requests",
        notification_type: LISTING_UNFINISHED,
        recipient_id: listing.user_id,
        notifiable_id: listing.id,
        notifiable_type: listing.class.name,
        notification_url: Url.new.host_listing(listing.id)
      )
    end

    # NOTE: CURRENTLY UNUSED
    # TODO: update the text for this notification
    def unfinished_listing_notification(listing)
      Notification.create(content: "We noticed you haven't finished your listing at #{listing.listing_name}",
        notification_type: LISTING_UNFINISHED,
        recipient_id: listing.user_id,
        notifiable_id: listing.id,
        notifiable_type: listing.class.name,
        notification_url: Url.new.host_listing(listing.id)
      )
    end
  end

  module Payments
    include Constants::NotificationTypes

    def upcoming_payment_notification(charge)
      reservation = charge.reservation
  
      # for the renter
      Notification.create(content: "Your upcoming payment of #{charge&.total_cost} is due by #{charge.due_by.strftime("%B %d, %Y")}",
        notification_type: PAYMENT_UPCOMING,
        recipient_id: charge.user_id,
        notifiable_id: charge.id,
        notifiable_type: charge.class.name,
        notification_url: Url.new.payments
      )
    end

    def payment_declined_notification(charge)
      # for the renter
      Notification.create(content: "Your payment for your booking request could not be processed! Please update your payment method.",
        notification_type: PAYMENT_DECLINED,
        recipient_id: charge.user_id,
        notifiable_id: charge.id,
        notifiable_type: charge.class.name,
        notification_url: Url.new.payments
      )
    end

    def payout_success_notification(payout)
      reservation = payout.reservation
  
      # for the renter
      Notification.create(content: "Your payout of #{payout&.payout_amount} has been transferred to your Stripe Connect account",
        notification_type: PAYOUT_SUCCESS,
        recipient_id: payout.user_id,
        notifiable_id: payout.id,
        notifiable_type: payout.class.name,
        notification_url: Url.new.payments
      )
    end

    def connect_status_blocked_notification(user)
      Notification.create(content: "Stripe has blocked your Stripe Connect account. Update your info to continue receiving payouts",
        notification_type: CONNECT_BLOCKED,
        recipient_id: user.id,
        notifiable_id: user.id,
        notifiable_type: user.class.name,
        notification_url: Url.new.payouts
      )
    end

    def connect_status_due_notification(user)
      Notification.create(content: "Additional information is required to verify your Stripe Connect account. This information is required to continue receiving payouts",
        notification_type: CONNECT_DUE,
        recipient_id: user.id,
        notifiable_id: user.id,
        notifiable_type: user.class.name,
        notification_url: Url.new.payouts
      )
    end

    def connect_status_verified_notification(user)
      Notification.create(content: "Stripe has verified your account information! You're set to start receiving payouts",
        notification_type: CONNECT_VERIFIED,
        recipient_id: user.id,
        notifiable_id: user.id,
        notifiable_type: user.class.name,
        notification_url: Url.new.payouts
      )
    end
  end

  module Messages
    include Constants::NotificationTypes

    def message_notification(message)
      conversation = message.conversation

      if conversation.sender_id == message.user_id
        actor = User.find(conversation.sender_id)
        recipient_id = conversation.recipient_id
      else
        actor = User.find(conversation.recipient_id)
        recipient_id = conversation.sender_id
      end

      Notification.create(content: "New message from #{actor.first_name}",
        notification_type: MESSAGE_SENT,
        recipient_id: recipient_id,
        actor_id: actor.id,
        notifiable_id: message.id,
        notifiable_type: message.class.name,
        notification_url: Url.new.message(message.id)
      )
    end
  end
end