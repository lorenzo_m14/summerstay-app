class ApplicationController < ActionController::API
    include ActionController::MimeResponds
    include ActionController::Cookies
    include Error::ErrorHandler
    include TokenAuthentication

    before_action :configure_permitted_parameters, if: :devise_controller?
    respond_to :json

    def fallback_index_html
        # renders index html
        respond_to do |format|
            format.html { render body: Rails.root.join('public/index.html').read }
        end
    end

    def route_not_found
        render json: { error: "Not Found", is_success: false}, status: :not_found
    end

    def render_not_found
        render json: { error: "Invalid ID", is_success: false}, status: :not_found
    end

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email, :password])
    end

    def pagination_dict(collection)
        {
          per_page: collection.per_page,
          offset: collection.offset,
          current_page: collection.current_page,
        #   next_page: collection.next_page,
        #   prev_page: collection.previous_page,
          total_pages: collection.total_pages,
          total_count: collection.total_entries
        }
    end

    def append_info_to_payload(payload)
        super
        payload[:user_id] = current_user.try(:id)
        # payload[:request_id] = request.uuid
        payload[:user_agent] = request.headers['HTTP_USER_AGENT']
        if payload[:status]
            payload[:status_code] = payload[:status]
            case 
            when payload[:status] >= 200 && payload[:status] < 300
                payload[:level] = "INFO"
            when payload[:status] >= 300 && payload[:status] < 400
                payload[:level] = "WARN"
            when payload[:status] >= 400 && payload[:status] < 500
                payload[:level] = "ERROR"
            when payload[:status] > 500
                payload[:level] = "FATAL"
            else
                payload[:level] = "EMERGENCY"
            end
        end
  end
end
