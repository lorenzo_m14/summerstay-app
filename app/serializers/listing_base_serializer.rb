class ListingBaseSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

             # basic info
  attributes :id, :listing_name,
             :listing_type, :bedroom, :beds, :bathroom, 
             :created_at, :updated_at, 
             :summary,

             # location info (no address)
             :latitude, :longitude,

             # amenities
             :amenities,
            #  :is_tv, :is_kitchen, :is_air, :is_heating, :is_internet, :is_laundry, 
            #  :is_furnished, :is_parking, :is_gym, :is_pool, :is_pet,

             # dates
             :start_date, :end_date, 

             # state and status info
             :active, :ready, :bookable, :status,

             :price, :images

  def images
    return unless object.images.attached?
    # TODO: add in variant logic later
    # variant = object.image.variant(resize: "100x100")
    return object.images.order(:position).map{|i|
      {
        "id": i.id,
        "url": rails_blob_url(i, only_path: true),
        "position": i.position
      }
    }
  end

  # Shortened lat + long
  def latitude
    object.latitude.round(4)
  end

  def longitude
    object.longitude.round(4)
  end

  belongs_to :user, serializer: UserProfileSerializer, key: :host

end
