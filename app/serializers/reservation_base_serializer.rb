# Serializer for reservations viewed by HOST
class ReservationBaseSerializer < ActiveModel::Serializer
  attributes :id, :status, :start_date, :end_date

  def start_date
    object.start_date.strftime("%Y-%m-%d")
  end

  def end_date
    object.end_date.strftime("%Y-%m-%d")
  end
end
