# Reservation charges seen by renter
class ReservationChargesSerializer < ReservationBaseSerializer
  attributes :expires_at, :charges, :reservation_cost

  def charges
    res_charges = object.charges.order("created_at ASC")
    res_charges.map{|c|
      ChargeSerializer.new(c)
    }
  end 

  belongs_to :host, serializer: UserProfileSerializer

  belongs_to :listing, serializer: ListingInfoSerializer

  has_one :reservation_cost, serializer: ReservationCostSerializer

end
