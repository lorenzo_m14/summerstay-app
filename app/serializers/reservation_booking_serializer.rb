# Serializer for reservations viewed by HOST
class ReservationBookingSerializer < ReservationBaseSerializer
  attributes :expires_at, :guest, :request_message

  def guest
    if @instance_options[:last_name].present?
      UserProfileSerializer.new(object.user, last_name: true)
    else
      UserProfileSerializer.new(object.user)
    end
  end
  
  has_one :reservation_cost, serializer: HostReservationCostSerializer
end
