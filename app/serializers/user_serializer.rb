class UserSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers

    attributes  :id, :created_at, :updated_at, :confirmed_at, :email, 
                :first_name, :last_name, :provider, :uid, :image, 
                :phone_number, :description, 
                :stripe_id, :merchant_id, :connect_account_status, :id_verify_status,
                :school_name, :user_type
                # :referral_code

    # def referral_code
    #     if object.referral_code
    #         object.referral_code.code
    #     else
    #         return nil
    #     end
    # end

    def image
        # TODO: add in variant logic later
        # variant = object.image.variant(resize: "100x100")

        if object.image.attached?
            {
                "id": object.image.id,
                "url": rails_blob_url(object.image, only_path: true)
            }
        elsif !object.fb_image.blank?
            object.fb_image
            {
                "id": nil,
                "url": object.fb_image
            }
        else
            return nil
        end
    end

end