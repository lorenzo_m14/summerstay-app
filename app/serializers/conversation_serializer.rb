# TODO: Integrate ActionCable
class ConversationSerializer < ActiveModel::Serializer
  attributes :id, :listing_id, :last_message, :user
  
  class MessageSerializer < ActiveModel::Serializer
    attributes :id, :context, :user_id, :created_at, :updated_at
  end

  def last_message
    if !object.messages.blank?
      MessageSerializer.new(object.messages.order("created_at").last)
    else
      return nil
    end
  end

  def user
    a_user = @instance_options[:user]
    return nil if a_user.nil?
    if object.sender_id != a_user.id
      UserProfileSerializer.new(object.sender)
    else
      UserProfileSerializer.new(object.recipient)
    end
  end

  # belongs_to :sender, serializer: UserSerializer
  # belongs_to :recipient, serializer: UserSerializer

end
