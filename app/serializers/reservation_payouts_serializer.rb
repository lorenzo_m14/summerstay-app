# Reservation payouts seen by host
class ReservationPayoutsSerializer < ReservationBaseSerializer
  attributes :payouts

  def payouts
    res_payouts = object.payouts.order("created_at ASC")
    res_payouts.map{|p|
      PayoutSerializer.new(p)
    }
  end 

  belongs_to :listing, serializer: ListingInfoSerializer


end
