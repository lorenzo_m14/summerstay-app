# TODO: Integrate ActionCable
class MessageSerializer < ActiveModel::Serializer
  attributes :id, :context, :user_id, :conversation_id, :created_at, :updated_at
end