# Show basic listing info in reservation charges and reservation payouts
class ListingInfoSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :cover_image, :listing_name, :address, :listing_type

  def cover_image

    return unless object.images.attached?
    # TODO: add in variant logic later
    # variant = object.image.variant(resize: "100x100")

    return rails_blob_url(object.images.order(:position).first, only_path: true)
  end
end