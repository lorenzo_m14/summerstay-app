class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :status, :start_date, :end_date, :expires_at, :listing_id, :guest

  def start_date
    object.start_date.strftime("%Y-%m-%d")
  end

  def end_date
    object.end_date.strftime("%Y-%m-%d")
  end

  def guest
    if @instance_options[:last_name].present?
      UserProfileSerializer.new(object.user, last_name: true)
    else
      UserProfileSerializer.new(object.user)
    end
  end

  # belongs_to :user, serializer: UserProfileSerializer, key: :guest

  has_one :reservation_cost, serializer: HostReservationCostSerializer
end
