class ChargeSerializer < ActiveModel::Serializer
  attributes  :id, :status, :charge_type, :charge_num, 
              :due_by, :total_cost, :paid_at, :invoice_url, :receipt_url
end
