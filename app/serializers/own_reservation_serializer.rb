class OwnReservationSerializer < ReservationSerializer

  attribute :stay_info, if: :is_accepted

  belongs_to :listing, serializer: ListingBookingSerializer

  has_one :reservation_cost, serializer: ReservationCostSerializer

  belongs_to :host, serializer: UserProfileSerializer

  def stay_info
    {
      address: object.listing.address,
      apt_number: object.listing.apt_number,
      phone_number: object.host.phone_number
    }
  end

  def is_accepted
    return true 
    if @instance_options[:is_waiting] == false
      return true
    end
    return false
  end
end
