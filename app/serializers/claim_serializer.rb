class ClaimSerializer < ActiveModel::Serializer
  attributes  :id, :user_type, :claim_type, :content, :user_id, :reservation_id, :created_at
end
