class HostReservationCostSerializer < ActiveModel::Serializer
  attributes :total_rent, :destination_amount, :charge_type
end
  