# Listing view (non-dashboard)
class ListingShowSerializer < ListingBaseSerializer
  include Rails.application.routes.url_helpers

  attributes :unavailable_dates

  def unavailable_dates
    @instance_options[:unavailable_dates]
  end

  belongs_to :user, serializer: UserProfileSerializer, key: :host

end
