class PayoutSerializer < ActiveModel::Serializer
  attributes  :id, :status, :due_by, :payout_amount, :payout_num
end
