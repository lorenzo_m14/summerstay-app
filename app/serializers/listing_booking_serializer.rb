class ListingBookingSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :listing_name,
             :summary,
             :listing_type, :bedroom, :beds, :bathroom, :amenities,
             :latitude, :longitude,
             :start_date, :end_date,
             :price, :images

  def images
    return unless object.images.attached?
    # TODO: add in variant logic later
    # variant = object.image.variant(resize: "100x100")
    return object.images.order(:position).map{|i|
      {
        "id": i.id,
        "url": rails_blob_url(i, only_path: true)
      }
    }
  end

  def price
    # TODO: Return display price
    #return object.display_price if object.display_price?

    (object.price * 1.05).round
  end

  def latitude
    if @instance_options[:is_accepted]
      object.latitude
    else
      object.latitude.round(4)
    end
  end

  def longitude
    if @instance_options[:is_accepted]
      object.longitude
    else
      object.longitude.round(4)
    end
  end
end
