class UserProfileSerializer < ActiveModel::Serializer
    include Rails.application.routes.url_helpers

    attributes :id, :first_name, :image
    attribute :email, if: :is_self?
    attribute :last_name, if: :show_last_name?

    def image
      # TODO: add in variant logic later
      # variant = object.image.variant(resize: "100x100")

      if object.image.attached?
          {
              "id": object.image.id,
              "url": rails_blob_url(object.image, only_path: true)
          }
      elsif !object.fb_image.blank?
          object.fb_image
          {
              "id": nil,
              "url": object.fb_image
          }
      else
          return nil
      end
    end

    def show_last_name?
        @instance_options[:last_name].present?
    end

    # TODO: if we need profile serialization for own user
    def is_self?
    end
  end