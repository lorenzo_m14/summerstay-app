class NotificationSerializer < ActiveModel::Serializer
  attributes  :id, :notification_type, :created_at, :read, :content, :notification_url
end
