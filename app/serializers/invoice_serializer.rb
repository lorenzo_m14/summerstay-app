class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :paid, :amount_due, :charge_num, :due_date, :status_transitions

  def hosted_invoice_url 
    if !object&.hosted_invoice_url.blank?
      object.hosted_invoice_url
    end
  end

  def payment_date 
    if object.paid
      stripe_charge = get_stripe_charge
      Time.at(stripe_charge.created).utc
    else
      return nil
    end
  end

  def payment_info
    if object.paid
      stripe_charge = get_stripe_charge
      return stripe_charge.payment_method_details
    else
      return nil
    end
  end

  private
    def get_stripe_charge
      stripe_charge = Stripe::Charge.retrieve(object.charge) if !object.charge.nil?
      return stripe_charge
    end
end
