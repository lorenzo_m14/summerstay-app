class ReservationCostSerializer < ActiveModel::Serializer
  attributes  :price, :total_rent, :total_cost, 
              :renter_service_fee, :continuous_fee, :deposit, :stripe_fee, :tax, 
              :destination_amount,
              :charge_type,
              :payment_method_type, :payment_method_id
end
  