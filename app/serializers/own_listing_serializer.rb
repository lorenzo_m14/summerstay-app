# Listing object on host dashboard
class OwnListingSerializer < ListingBaseSerializer
  include Rails.application.routes.url_helpers
  
  # TODO: re-add full_address 
  attributes :address, :apt_number, :latitude, :longitude,
             :reservations              

    def reservations
      return [] if object.Waiting?
      
      my_reservations = object.reservations.select{|r|
        r.Waiting? || r.Approved?
      }

      my_reservations.map { |r|
        ReservationBookingSerializer.new(r, last_name: true)
      }
    end

    # displays exact lat and long 
    def latitude
      object.latitude
    end
  
    def longitude
      object.longitude
    end

    # def referral_code
    #   if object.referral_code
    #     object.referral_code.code
    #   else
    #     return nil
    #   end
    # end
end

  