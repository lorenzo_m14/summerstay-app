class ListingIndexSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :listing_name, :location_metadata,
             :summary,
             :listing_type, :bedroom, :beds, :bathroom, :amenities,
             :latitude, :longitude,
             :start_date, :end_date, 
             :price, :images

  def images
    return unless object.images.attached?
    # TODO: add in variant logic later
    # variant = object.image.variant(resize: "100x100")

    # NOTE: only include the first image for index
    cover_image = object.images.order(:position).first
    return [{
        "id": cover_image.id,
        "url": rails_blob_url(cover_image, only_path: true)
    }]
  end

  def price
    # TODO: Return display price
    # return object.display_price if object.display_price?

    (object.price * 1.05).round
  end

  def latitude
    object.latitude.round(4)
  end

  def longitude
    object.longitude.round(4)
  end

  def location_metadata
    {
      city: object&.city,
      state: object&.state,
      zip_code: object&.zip_code
    }      
  end
end
