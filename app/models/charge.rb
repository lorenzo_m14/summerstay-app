class Charge < ApplicationRecord
    enum status: {Unpaid: 0, Paid: 1, Cancelled: 2, Refunded: 3}

    belongs_to :reservation     # reservation_id
    belongs_to :user            # user_id

    has_many :notifications, as: :notifiable, :dependent => :destroy

    validates :total_cost, presence: true
    validates :due_by, presence: true
end
