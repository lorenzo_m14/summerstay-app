class Message < ApplicationRecord
  belongs_to :user
  belongs_to :conversation

  validates_presence_of :context, :conversation_id, :user_id
  validates :context, length: { maximum: 9999 }
  after_create_commit :create_notification

  has_one :notifications, as: :notifiable, :dependent => :destroy

  def message_time
    self.created_at.strftime("%v")
  end

  private

  def create_notification
    # avoid spam notifications
    if self.conversation.should_send_notif
      MessageWorker.perform_async(self.id)
    end
  end
end
