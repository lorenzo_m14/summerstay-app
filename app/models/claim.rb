class Claim < ApplicationRecord
    enum user_type: {host: 0, renter: 1}
    enum claim_type: {None: 0, Message: 1, Reservation: 2, Payment: 3, Claim: 4}

    belongs_to :user
    belongs_to :reservation

    has_one :notifications, as: :notifiable, :dependent => :destroy

    validates_presence_of :content, :user_id, :reservation_id, :claim_type
    after_create_commit :create_notification
  
    private
  
    def create_notification
  
      if self.reservation.user_id == self.user_id
        sender = User.find(self.reservation.user_id)
        recipient_id = self.reservation.listing.user_id
      else
        sender = User.find(self.reservation.listing.user_id)
        recipient_id = self.reservation.user_id
      end
  
      Notification.create(content: "#{sender.fullname} submitted a claim against you. To find out more, please contact info@thesummerstay.com",
      notification_type: :Claim,
      user_id: recipient_id,
      listing_id: self.reservation.listing_id,
      reservation_id: self.reservation.id)
    end
end
