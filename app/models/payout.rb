class Payout < ApplicationRecord
    # need InProgress state for Stripe::Transfers that are in progress, until they get paid out.
    enum status: {Unpaid: 0, Pending: 1, Paid: 2, Cancelled: 3, Refunded: 4, Other: 5}

    has_many :notifications, as: :notifiable, :dependent => :destroy

    belongs_to :reservation     # reservation_id
    belongs_to :user            # user_id
end
