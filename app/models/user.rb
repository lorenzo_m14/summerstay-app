class User < ApplicationRecord
  # this status is based on the requirements hash from webhooks
  # defaults to enabled when account is created, and changes based on capability.updated webhooks
  enum connect_account_status: {Enabled: 0, Due: 1, Pending: 2, Verified: 3, PastDue: 4, Disabled: 5}

  # this status is based on government id verification status, defaults to Unapproved
  enum id_verify_status: {Unapproved: 0, InReview: 1, Approved: 2, Denied: 3, Other: 4}

  # user_type defines whether or not the person is a student or not

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :omniauthable

  has_many :listings
  has_many :reservations
  has_many :notifications, foreign_key: :recipient_id

  has_one :referral_code, dependent: :destroy

  has_one_attached :image

  validates :first_name, presence: true, length: {maximum: 50}
  validates :last_name, presence: true, length: {maximum: 50}
  validates :phone_number, uniqueness: true,  :allow_blank => true

  validate :password_complexity



  # simple Stack Overflow solution for enforcing password complexity
  def password_complexity
    return if password.blank? || password =~ /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*[\W_]).{8,50}$/

    errors.add :password, "Complexity requirement not met. Length should be 8-50 characters and include: 1 uppercase letter, 1 number and 1 special character."
  end

  def is_active_host
    !self.merchant_id.blank?
  end

  def unread_notifications
    self.notifications.unread.count
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def get_referral_code
    self.referral_code.code if !self.referral_code.blank?
  end

  # generate random ReferralCode if token is not passed 
  def generate_referral_code(token=nil)
    token = SecureRandom.base58(4).upcase if token.nil?
    referral_code = ReferralCode.create!(user_id: self.id, code: token)
    
    if !self.referral_code_id.blank?
      self.referral_code.destroy 
      self.referral_code = nil
    end
    self.referral_code = referral_code
  end

  # if referral code already exists, pass in as param
  def generate_campus_rep(token=nil)
    if !self.referral_code.blank?
      error = "User is already a campus rep!"
      return error
    end

    self.generate_referral_code(token)

    # response = LeadDyno.new.create_affiliate(self)

    # if response.key?("error")
    #   error = response["error_description"]
    #   return error
    # else
    #   Log.info("Created LeadDyno Affiliate for user #{current_user.id}", level: "INFO", type: "LeadDyno", referral_code: self.get_referral_code)

    #   referral_url = response["affiliate_dashboard_url"]
    #   self.update(referral_url: referral_url)
    #   self.save
    #   return nil
    # end
  end

  def set_connect_status
    return nil if self.merchant_id.nil?

    account = Stripe::Account.retrieve(self.merchant_id)
    return nil if account.nil?

    requirements = account&.requirements
    if !requirements.disabled_reason.blank?
      self.Disabled!
      ConnectUpdateWorker.perform_async(self.id)
    elsif !requirements&.past_due.blank?
      self.PastDue!
      ConnectUpdateWorker.perform_async(self.id)
    elsif !requirements&.pending_verification.blank?
      self.Pending!
    elsif !requirements&.eventually_due.blank? || !requirements&.currently_due.blank?
      self.Due!
      ConnectUpdateWorker.perform_async(self.id)
    else
      self.Verified!
      ConnectUpdateWorker.perform_async(self.id)
    end
  end

  def generate_authentication_token
    token = nil
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
    self.authentication_token = token
  end

  private


end
