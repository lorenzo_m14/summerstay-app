class Notification < ApplicationRecord
    # For realtime notification via ActionCable
    # after_create_commit { NotificationJob.perform_later self }
    
    after_create_commit :increment_unread

    belongs_to :recipient, class_name: "User"
    belongs_to :actor, class_name: "User", optional: true
    belongs_to :notifiable, polymorphic: true, optional: true

    scope :unread, -> { where(read_at: nil) }

    def read
        self.read_at.present?
    end

    private

        def increment_unread
            self.recipient.increment!(:unread)
        end
  end
  