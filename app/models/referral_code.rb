class ReferralCode < ApplicationRecord
    belongs_to :user
    has_many :listing

    validates :code, presence: true, length: {maximum: 10}
end
