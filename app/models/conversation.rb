class Conversation < ApplicationRecord
    belongs_to :sender, class_name: "User", foreign_key: :sender_id
    belongs_to :recipient, class_name: "User", foreign_key: :recipient_id
    belongs_to :listing
    belongs_to :reservation, optional: true

    has_many :messages, dependent: :destroy
    # validates_uniqueness_of :sender_id, :recipient_id

    scope :involving, -> (user) {
        where("conversations.sender_id = ? OR conversations.recipient_id = ?", user.id, user.id)
    }

    scope :between, -> (user_A, user_B) {
        where("(conversations.sender_id = ? AND conversations.recipient_id = ?) OR (conversations.sender_id = ? AND conversations.recipient_id = ?)", user_A, user_B, user_B, user_A)
    }

    def most_recent_message
        self.messages.order("created_at")&.last
    end

    # if there's less than 1 hr between messages wait to send message notif
    def should_send_notif
        last_messages = self.messages.order("created_at DESC").limit(3)
        # make sure we always send first response
        if last_messages.count < 3
            return true
        elsif last_messages.count == 3
            if last_messages.first.created_at > last_messages.second.created_at + 3.minutes
                return true
            end
        end
        return false
    end
end
