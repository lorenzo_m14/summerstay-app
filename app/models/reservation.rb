class Reservation < ApplicationRecord
  include Constants::Reservations
  include Constants::Events
  # status of Completed means paid and done, other is for if there are certain claims, etc.
  enum status: { Waiting: 0, Approved: 1, Completed: 2, Declined: 3, Expired: 4, Cancelled: 5, Other: 6 } 

  belongs_to :user
  belongs_to :listing
  belongs_to :host, class_name: "User", foreign_key: :host_id
  belongs_to :referral_code, optional: true

  has_one :reservation_cost, dependent: :destroy
  has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :charges, dependent: :destroy
  has_many :payouts, dependent: :destroy
  has_many :claims, dependent: :destroy

  def accepted?
    self.Approved? || self.Completed?
  end

  def associated_with_user?(user)
    self.user.id == user.id || self.host.id == user.id
  end

  def num_weeks
    num_days = (end_date.to_date - start_date.to_date).to_i + 1
    num_weeks = (num_days / 7.0).ceil
  end

  def num_charges
    charges.count
  end

  def charges_sum
    charges.map(&:total_cost).sum
  end

  def stripe_fees_charges_sum
    charges.map(&:stripe_fee).sum
  end

  def initial_charge
    charges.where(charge_num: 0).first
  end

  def second_charge
    charges.where(charge_num: 1).first
  end

  def subscription_charges
    charges.where.not(charge_num: 0).order(charge_num: :asc)
  end

  def subscription_charges_sum
    charges.where.not(charge_num: 0).map(&:total_cost).sum
  end

  def unpaid_charges
    charges.where(paid_at: nil).order(charge_num: :asc)
  end

  def num_payouts
    payouts.count
  end

  def is_completed
    all_charges_paid? && all_payouts_paid?
  end

  def all_charges_paid?
    charges.where(status: :Paid).count == charges.count
  end

  def all_payouts_paid?
    payouts.where(status: :Paid).count == payouts.count
  end

  ### Reservation Events ###

  # TODO: Move logic from controller into here
  def create_reservation
  end

  def approve
    return nil, 'Cannot approve reservation charges.' if charges.nil?

    payment_services = PaymentServices.new(self)
    # charge payment to stripe
    _charges, errors = payment_services.approve_reservation
    return nil, errors if errors.present?

    # create payouts for reservation
    _payouts, errors = payment_services.instantiate_payouts
    return nil, errors if errors.present?

    # NOTE: Referral code is inactive
    # do referral code logic here
    # referral_error = self.on_approval_referral
    # if !referral_error.nil?
    #   Log.info(referral_error, level: "ERROR", type: "LeadDyno")
    # end

    # set listing to Booked status if needed
    listing = self.listing
    unless listing.booked
      listing.update(booked: true, first_booked_at: DateTime.current)
    end

    # change reservation status to approved
    Log.debug 'Changing reservation status to approved'
    self.update!(approved_at: DateTime.current)
    self.Approved!

    # invalidate all other reservations for that listing and cancel associated charges
    # check for overlapping unapproved reservations
    reservations_to_decline = Reservation.where(
      "listing_id = ? AND (start_date <= ? AND end_date >= ?) AND status = ?",
      listing.id, self.end_date, self.start_date, 0
    )
    reservations_to_decline.map { |r|
      r.decline
    }

    return self, errors
  end

# Can only decline reservation if waiting status
def decline
  unless self.Waiting?
    return nil, "Reservation is not in Waiting status, cannot decline"
  end

  # change status of a reservation to declined
  Log.debug "Declining reservation #{self.id}"

  # delete associated charges
  Log.info("Deleting charges for reservation #{self.id}", level: "INFO")
  Charge.where(reservation_id: self.id).destroy_all

  # remove all mailer and payout jobs from the queue for this reservation
  Log.info("Deleting mailer and payout jobs for declined reservation #{self.id}", level: "INFO")
  SidekiqHelper.new.delete_reservation_jobs(self)

  self.update!(declined_at: DateTime.current)
  self.Declined!

  return self, nil
end

def cancel(actor_id)
  # invalidate all pending charges/subscriptions
  Log.info("Canceling future payments for reservation #{self.id}", level: "INFO")
  error = PaymentServices.new(self).cancel_stripe_payments_for_reservation
  if error.present?
    Log.error("Issue canceling payments: #{error}", level: "ERROR")
    return nil, error 
  end

  # remove all mailer and payout jobs from the queue for this reservation
  Log.info("Deleting mailer and payout jobs for cancelled reservation #{self.id}", level: "INFO")
  SidekiqHelper.new.delete_reservation_jobs(self)

  self.update!(canceller_id: actor_id)
  self.Cancelled!

  return self, nil
end

def approval_events
  # remove expiry jobs
  SidekiqHelper.new.remove_expiry_jobs(self)

  # send job for successfully booking for both renter and host
  ApprovedRequestWorker.perform_async(self.id)

  # Create jobs for premove in (1 week before or now), move in (start date), move out (end date)
  pre_move_email_date = (self.start_date - PRE_MOVE_EMAIL_DAYS.days).in_time_zone.to_time + EMAIL_OFFSET_TIME
  move_in_email_date = self.start_date.in_time_zone.to_time + EMAIL_OFFSET_TIME
  move_out_email_date = self.end_date.in_time_zone.to_time + EMAIL_OFFSET_TIME

  # if reservation starts in sooner than threshold, queue job immediately
  if Date.current + PRE_MOVE_EMAIL_DAYS.days > self.start_date.to_date
    Log.info("PreMove email sent at approval. Reservation booked within #{PRE_MOVE_EMAIL_DAYS} of move-in.", level: "WARN")
    PreMoveWorker.perform_async(self.id)
  else
    # queue job for move in email
    PreMoveWorker.perform_in(pre_move_email_date, self.id)
  end

  # queue job for move in and move out email
  MoveInWorker.perform_in(move_in_email_date, self.id)
  MoveOutWorker.perform_in(move_out_email_date, self.id)

  # Only send payment reminders emails for recurring payments (subscription)
  if self.reservation_cost.continuous?
    Log.info("Creating payment reminder jobs for continuous booking", level: "INFO")
    1.upto(self.charges.count - 1) do |num_charge|
      charge = self.charges.where(charge_num: num_charge).first
      if charge.nil?
        Log.error("Cannot find charges to create reminders!", level: "ERROR", type: "Listing", action: "approve", listing_id: listing.id)
        break 
      end
      reminder_date = charge.due_by - UPCOMING_PAYMENT_DAYS.days
      reminder_time = reminder_date.in_time_zone.to_time + EMAIL_OFFSET_TIME

      # send immediately if reminder date is passed
      if Date.current > reminder_date
        Log.info("Payment reminder email sent at approval. Reservation booked within #{UPCOMING_PAYMENT_DAYS} of move-in.", level: "WARN")
        PaymentReminderWorker.perform_async(self.id, charge.id)
      else 
        PaymentReminderWorker.perform_in(reminder_time, self.id, charge.id)
      end
    end
  end
end

# private

  # NOTE: Deprecated
  # on approval, capture 3 possible actions for leaddyno
  # 1. capture renter booking lead
  # 2. capture renter booking purchase (associated with booking lead)
  # 3. capture host booking purchase (associated with lead made with listing creation)
  # def on_approval_referral
  #   ld = LeadDyno.new

  #   if !self.Waiting?
  #     return "Reservation has already approved"
  #   end

  #   # check if listing has referral_code
  #   if self.listing.referral_code.present?
  #     # can only "redeem" referral on first booking
  #     if listing.Ready?
  #       puchase_response = ld.capture_booking_purchase_host(self.host, self)
  #       if puchase_response.key?("error")
  #         return puchase_response["error_description"]
  #       end
  #     end
  #   end

    # check if reservation has referral_code
    # if self.referral_code.present?
    #   # capture lead and purchase simultaneously for renter
    #   purchase_response = ld.capture_booking_lead(self.user, self)
    #   if purchase_response.key?("error")
    #     puts "Capture booking lead error"
    #     puts purchase_response["error_description"]
    #   end
    #   purchase_response = ld.capture_booking_purchase_renter(self.user, self)
    #   if purchase_response.key?("error")
    #     puts "Capture booking purchase (renter) error"
    #     puts purchase_response["error_description"]
    #   end
    # end
  # end

end
  