# Listing class provides validations for listing update
# bookable = allowed to be viewed in map, has windows to be booked (default true)
# ready = all listing fields are completed
# active = user defined field (always false if not ready)
class Listing < ApplicationRecord
  include Constants::Reservations

  enum status: {Waiting: 0, Ready: 1, Archived: 2, Deleted: 3, Other: 4}
  
  belongs_to :user
  belongs_to :referral_code, optional: true
  has_many_attached :images, :dependent => :destroy
  has_many :reservations, :dependent => :destroy
  has_many :notifications, as: :notifiable, :dependent => :destroy

  # Address is stored as jsonb object, validate presence of internal keys  
  store_accessor :full_address, :line_1, :line_2, :city, :state, :zip_code, :country
  validate :check_full_address
  
  # TODO: check serverside geocode 
  geocoded_by :formatted_address
  after_validation :geocode, if: :full_address_changed?

  # changed validations for initial creation of listing
  validates :listing_type, :address, :longitude, :latitude,
            presence: true,
            on: :create

  # check that all the necessary fields exist
  validates :listing_type, :listing_name, :summary,
            :bedroom, :beds, :bathroom, :longitude, :latitude,
            :start_date, :end_date, :price,
            presence: true,
            on: :is_ready_listing

  # general validations 
  validates :beds, :bedroom, numericality: {greater_than_or_equal_to: 1, less_than_or_equal_to: 10}, allow_blank: true

  validates :bathroom, numericality: {greater_than_or_equal_to: 0.5, less_than_or_equal_to: 5}, allow_blank: true

  validates :listing_name, length: { in: 8..70 }, allow_blank: true

  validates :apt_number, length: { maximum: 20 }, allow_blank: true

  validates :summary, length: { maximum: 99999 }, allow_blank: true

  # validates :price, numericality: {only_integer: true, greater_than_or_equal_to: 10 * 100, less_than_or_equal_to: 10000 * 100}, allow_blank: true
  validate :check_price

  validate :valid_dates

  def check_price
    if self.price.blank?
      return true
    end
    if self.price < 10 * 100
      self.errors.add(:price, "Price must be greater than $10")
    elsif self.price > 10000 * 100
      self.errors.add(:price, "Price must be less than $10000")
    end
  end

  def check_full_address
    is_full_address = self.full_address.present? && self.line_1.present? \
                    && self.city.present? && self.state.present? \
                    && self.zip_code.present? && self.country.present?
    if !is_full_address
      self.errors.add(:address, "Please enter a valid street address")
    end
    is_full_address
  end

  def is_ready_listing
    detail = !self.price.blank? && !self.listing_name.blank?
    self.errors.add(:details, "Invalid details") if !detail
    # puts "missing details" if !detail

    beds = !self.bedroom.blank? && !self.beds.blank? && !self.bathroom.blank?
    self.errors.add(:beds, "Invalid rooms info") if !beds
    # puts "missing beds" if !beds

    loc =  !self.latitude.blank? && !self.longitude.blank? && !self.address.blank?
    self.errors.add(:location, "Invalid address") if !loc
    # puts "missing loc" if !loc

    amens = !self.amenities.blank?
    self.errors.add(:amenities, "Please select amenities") if !amens

    dates = self.valid_dates
    self.errors.add(:dates, "Invalid dates") if !dates
    # puts "missing dates" if !dates

    ims = self.valid_images
    self.errors.add(:ims, "Must have at least two photos") if !ims
    # puts "missing images" if !ims

    if ENV['DISABLE_ID_VERIFICATION'].blank?
      user_verified = !self.user.Unapproved? && !self.user.Denied?
    else
      user_verified = true
    end
    # puts "user is not verified" if !user_verified

    # puts "Ready status:"
    # puts detail && beds && loc && dates && ims
    detail && beds && loc && amens && dates && ims && user_verified
  end

  def valid_dates
    if !self.Waiting? # don't validate dates again if already ready, we shouldn't be able to change at this point
      return true 
    elsif self.start_date.nil? && self.end_date.nil? # dates shouldn't be nil
      return false
    # elsif self.start_date < Date.current
    #   self.errors.add(:dates, "Date cannot be in the past!")
    elsif self.start_date >= self.end_date
      self.errors.add(:dates, "Start date must precede end date!")
    elsif  (self.end_date - self.start_date).to_i <= 21
      self.errors.add(:dates, "Listing must be at least 3 weeks long!")
    elsif (self.end_date - self.start_date).to_i >= 365 * 2
      self.errors.add(:dates, "Cannot create a listing with duration greater than 2 years!")
    else 
      return true
    end
    return false
  end

  # should have at least two images
  def valid_images
    if !self.images.attached?
      errors.add(:images, "no images present!")
    elsif self.images.attached? && self.images.count < 2
      errors.add(:image, "listing needs 2 or more images") 
    else
      return true
    end
    return false
  end

  def is_bathroom_thresholds
    if self.bathroom
      errors.add(:bathroom, "must be in 0.5 intervals")
    end
  end

  def cover_photo(size)
    if self.images.length > 0
      url_for(self.images[0])
    else
      "blank.jpg"
    end
  end

  # check for overlapping reservations
  def is_available(start_date, end_date)
      if self.start_date.blank? || end_date.blank?
        return false
      end
      reservations = Reservation.where(
          "listing_id = ? AND (start_date <= ? AND end_date >= ?) AND status = ?",
          self.id, end_date, start_date, 1
      ).count

      # within the timeframe of the listing
      calendar = self.start_date <= start_date && end_date <= self.end_date

      reservations == 0 && calendar
  end

  # check for overlapping reservation dates
  def unavailable_dates
    today = Date.current
    reservations = Reservation.where(
      "listing_id = ? AND (end_date >= ?) AND status = ?",
      self.id, today, 1
    )

    unavailable_dates = reservations.map { |r|
        (r[:start_date].to_datetime..r[:end_date].to_datetime).map { |day| day.strftime("%Y-%m-%d") }
    }.flatten.to_set
  end

  # we want to check if a listing has any MIN_RESERVATION_LENGTH windows
  def has_bookable_windows
    today = Date.current
    bookable_start_date = [Date.today, self.start_date].max
    return false if bookable_start_date + MIN_RESERVATION_LENGTH > self.end_date

    reservations = Reservation.where(
      "listing_id = ? AND (end_date >= ?) AND status = ?",
      self.id, today, 1
    )

    unavailable_dates = reservations.map { |r|
        (r[:start_date].to_date..r[:end_date].to_date)
    }.flatten.to_set 
      
    counter = 0
    (bookable_start_date..self.end_date).each do |day|
      # if there is a valid window period, we return true
      return true if counter >= MIN_RESERVATION_LENGTH

      if unavailable_dates.any?{ |u_day_range| u_day_range.include?(day) }
        counter = 0
      else
        counter += 1
      end
    end

    return false
  end

  # formats full address into string
  def formatted_address
    return nil if self.full_address.blank?

    line_2 = "#{self.full_address['line_2']} " if self.full_address['line_2'].present?
    "%s%s, %s, %s %s" % [line_2, self.full_address['line_1'], self.full_address['city'], self.full_address['state'], self.full_address['zip_code']]
  end

  # TODO: include validation for referral code
  # validates :referral_code, on: :check_referral_code

  # NOTE: Currently unused
  # def check_referral_code(code)
  #   rc = ReferralCodes.find_by(code: code)
  #   if rc.present?
  #     puts "Valid referral code!"
  #     self.code = rc
  #     return true
  #   else
  #     puts "Invalid referral code!"
  #     return false
  #   end
  # end

  # def get_referral_code
  #   if !self.referral_code.nil?
  #     self.referral_code.code
  #   end
  # end
    
  # NOTE: Deprecated
  # def capture_listing_lead
  #   # check if listing has referral_code
  #   if self.referral_code.present?
  #     Log.info "Creating referral lead for LeadDyno"
  #     # can only "redeem" referral on first booking
  #     if self.Waiting?
  #       lead_response = LeadDyno.new.capture_listing_lead(self.user, self)
  #       if lead_response.key?("error")
  #         Log.info(lead_response["error_description"], level: "ERROR", type: "LeadDyno", action: "capture_listing_lead")
  #       else
  #         Log.info("Sucessfully captured lead for listing #{self.id}, with referral #{self.referral_code}", level: "INFO", type: "LeadDyno", action: "capture_listing_lead")
  #       end
  #     end
  #   end
  # end

  # NOTE: Deprecated
  # def capture_listing_purchase
  #   # check if listing has referral_code
  #   if self.referral_code.present?
  #     Log.info "Creating referral purchase for LeadDyno"
  #     # can only "redeem" referral on first booking
  #     if self.Waiting? && !self.active
  #       purchase_response = LeadDyno.new.capture_listing_purchase(self.user, self)
  #       if purchase_response.key?("error")
  #         Log.info(lead_response["error_description"], level: "ERROR", type: "LeadDyno", action: "capture_listing_purchase")
  #       else
  #         Log.info("Sucessfully captured purchase for listing #{self.id}, with referral #{self.referral_code}", level: "INFO", type: "LeadDyno", action: "capture_listing_purchase")
  #       end
  #     end
  #   end
  # end
end
