class ReservationCost < ApplicationRecord
    enum charge_type: {upfront: 0, continuous: 1}
    enum sub_status: {Scheduled: 0, Started: 1, Ended: 2, Cancelled: 3, Other: 4}
    enum deposit_status: {Unpaid: 0, Paid: 1, Returned: 2, Defaulted: 3}
    belongs_to :reservation, optional: true

    def service_fees
        self.renter_service_fee + self.continuous_fee
    end
end
