class AccountMailer < ApplicationMailer

    include Constants::EmailId
    include Constants::EmailCategory

    def welcome(recipient)
        template_data = {
            "first_name": recipient.first_name,
            "create_listing_url": Url.new(false).create_listing_url,
            "listings_url": Url.new(false).listings
        }
        template_id = ACCOUNT_WELCOME_ID
        categories = [PRIORITY_LOW, USER_TYPE_GENERAL, PRODUCT_AREA_ACCOUNT]
        response = send_email(recipient,template_data,template_id,categories)
    end

    def confirmation(recipient, token)
        template_data = {
            "first_name": recipient.first_name,
            "confirmation_url": Url.new(false).confirmation(token)
        }

        template_id = ACCOUNT_CONFIRMATION_ID
        categories = [PRIORITY_LOW, USER_TYPE_GENERAL, PRODUCT_AREA_ACCOUNT]
        response = send_email(recipient,template_data,template_id,categories)
    end

    def reset_password(recipient, token)
        template_data = {
            "first_name": recipient.first_name,
            "reset_password_url": Url.new(false).reset_password(token)
        }

        template_id = ACCOUNT_RESET_PASSWORD_ID
        categories = [PRIORITY_MEDIUM, USER_TYPE_GENERAL, PRODUCT_AREA_ACCOUNT]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def message(message)
        conversation = message.conversation
        sender = message.user
        recipient = message.user == conversation.recipient ? conversation.sender : conversation.recipient
        template_data = {
            # "first_name": recipient.first_name,
            "sender_first_name": sender.first_name,
            "message_content": message.context,
            "message_url": Url.new(false).message(conversation.id)
        }

        template_id = ACCOUNT_MESSAGE_ID
        categories = [PRIORITY_HIGH, USER_TYPE_GENERAL, PRODUCT_AREA_LISTING, PRODUCT_AREA_BOOKING]
        response = send_email(recipient,template_data,template_id,categories)
    end

    # Stripe Connect
    def connect_status_update(host)
        template_data = {
            "first_name": host.first_name,
            "payout_url": Url.new(false).payouts
        }

        template_id = ''
        if host.Verified?
            template_id = ACCOUNT_CONNECT_VERIFIED_ID
        elsif host.PastDue? || host.Disabled?
            template_id = ACCOUNT_CONNECT_BLOCKED_ID
        elsif host.Due?
            template_id = ACCOUNT_CONNECT_DUESOON_ID
        end
        categories = [PRIORITY_CRITICAL, USER_TYPE_HOST, PRODUCT_AREA_PAYMENT]
        response = send_email(host,template_data,template_id,categories)
    end
end