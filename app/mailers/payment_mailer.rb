class PaymentMailer < ApplicationMailer
    include Utilities
    include Constants::EmailId
    include Constants::EmailCategory

    # STRIPE TAKES CARE OF THESE GUYS
    def upcoming_payment(charge)
        puts "Sending Payment Reminder Email"
        reservation = charge.reservation
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter
        charge_amount = charge.total_cost
        template_data = {
            "first_name": recipient.first_name,
            "payment_url": Url.new(false).payments,
            "charge_amount": "#{convert_to_dollars(charge_amount)}"
        }
        template_id = PAYMENT_RENTER_UPCOMING_ID
        categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_PAYMENT, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    # TODO: Add this
    def payment_declined(charge)
        puts "Sending Payment Declined Email"
        reservation = charge.reservation
        renter = reservation.user
        recipient = renter
        template_data = {
            "first_name": recipient.first_name,
            "payment_url": Url.new(false).payments,
        }
        template_id = PAYMENT_RENTER_DECLINED_ID
        categories = [PRIORITY_CRITICAL, USER_TYPE_RENTER, PRODUCT_AREA_PAYMENT, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    # TODO: Placeholder receipt email
    def receipt_email(charge)
        puts "Sending Receipt Email"
        reservation = charge.reservation
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter
        charge_amount = charge.total_cost
        template_data = {
            "first_name": recipient.first_name,
            "receipt_url": charge&.receipt_url,
            "charge_amount": "#{convert_to_dollars(charge_amount)}"
        }
        template_id = RECEIPT_EMAIL_ID
        categories = [PRIORITY_LOW, USER_TYPE_RENTER, PRODUCT_AREA_PAYMENT, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def payout_success_email(payout)
        puts "Sending Payout Success Email"
        reservation = payout.reservation
        host = reservation.host
        recipient = host
        template_data = {
            "first_name": recipient.first_name,
            "payout_url": Url.new(false).payouts
        }
        template_id = PAYOUT_HOST_SUCCESS_ID
        categories = [PRIORITY_MEDIUM, USER_TYPE_HOST, PRODUCT_AREA_PAYMENT, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    # TODO: remove as no longer used
    def payout_verification_email(user)
        puts "Sending Payout Verification Email"
        recipient = user
        template_data = {
            "first_name": recipient.first_name,
            "payout_url": Url.new(false).payouts
        }
        template_id = PAYOUT_HOST_VERIFICATION_ID
        categories = [PRIORITY_MEDIUM, USER_TYPE_HOST, PRODUCT_AREA_PAYMENT, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    # def overdue_payment_reminder
    # end

    # def remaining_payment_reminder
    # end
end