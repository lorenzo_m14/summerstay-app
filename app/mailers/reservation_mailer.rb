class ReservationMailer < ApplicationMailer
    include Utilities
    include Constants::EmailId
    include Constants::EmailCategory

    def booking_request(reservation)
        puts "Sending Booking Request Email"
        listing = reservation.listing
        host = reservation.host
        renter = reservation.user
        recipient = host

        template_data = {
            "first_name": host.first_name,
            "reservation_url": Url.new(false).host_reservation(listing.id,reservation.id)
        }
        template_id = BOOKING_HOST_REQUEST_ID
        categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end
    
    # reservation status change
    def accepted_booking_host(reservation)
        puts "Sending Accepted Booking Host Email"

        listing = reservation.listing
        host = reservation.host
        renter = reservation.user
        recipient = host
        charge2 = reservation.second_charge
        total_cost = reservation.reservation_cost.total_cost

        template_data = {
            "first_name": recipient.first_name,
            "renter_full_name": renter.fullname,
            "address": listing.address,
            "total_payment": "$#{convert_to_dollars(total_cost)}",
            "tnc_url": Url.new(false).tnc,
            "messages_url": Url.new(false).conversations,
            "dashboard_url": Url.new(false).dashboard,
            "reservation_url": Url.new(false).host_reservation(listing.id, reservation.id)
        }
        template_id = BOOKING_HOST_ACCEPTED_ID
        categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def accepted_standard_booking_renter(reservation)
        puts "Sending Accepted Booking Renter Email"
        listing = reservation.listing
        host = reservation.host
        renter = reservation.user
        recipient = renter

        charge1 = reservation.initial_charge
        charge2 = reservation.second_charge
        total_cost = reservation.reservation_cost.total_cost
        payment1 = charge1&.total_cost
        payment2 = charge2&.total_cost
        payment_date = charge2&.due_by

        template_data = {
            "first_name": recipient.first_name,
            "host_full_name": host.fullname,
            "address": listing.address,
            "total_payment": "$#{convert_to_dollars(total_cost)}",
            "payment1": "$#{convert_to_dollars(payment1)}",
            "payment2": "$#{convert_to_dollars(payment2)}",
            "payment_date": format_date(payment_date),
            "payment_url": Url.new(false).payments,
            "tnc_url": Url.new(false).tnc,
            "messages_url": Url.new(false).conversations,
            "reservation_url": Url.new(false).renter_reservation(reservation.id)
            # "reservation_name": reservation.listing.listing_name,
            # "dashboard_url": Url.new(false).dashboard
        }
        template_id = BOOKING_RENTER_ACCEPTED_STANDARD_ID
        categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def accepted_pays_booking_renter(reservation)
        puts "Sending Accepted Booking Renter Email"
        listing = reservation.listing
        host = reservation.host
        renter = reservation.user
        recipient = renter

        charge1 = reservation.initial_charge
        total_cost = reservation.reservation_cost.total_cost
        payment1 = charge1&.total_cost
        paymentpays = reservation.subscription_charges_sum

        template_data = {
            "first_name": recipient.first_name,
            "host_full_name": host.fullname,
            "address": listing.address,
            "total_payment": "$#{convert_to_dollars(total_cost)}",
            "payment1": "$#{convert_to_dollars(payment1)}",
            "paymentpays": "$#{convert_to_dollars(paymentpays)}",
            "payment_url": Url.new(false).payments,
            "tnc_url": Url.new(false).tnc,
            "messages_url": Url.new(false).conversations,
            "reservation_url": Url.new(false).renter_reservation(reservation.id)
            # "reservation_name": reservation.listing.listing_name,
            # "dashboard_url": Url.new(false).dashboard
        }
        template_id = BOOKING_RENTER_ACCEPTED_PAYS_ID
        categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def declined_booking_renter(reservation)
        puts "Sending Declined Booking Renter Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter

        template_data = {
            "first_name": renter.first_name,
            "listings_url": Url.new(false).listings
        }
        template_id = BOOKING_RENTER_DECLINED_ID
        categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)

    end

    def request_expiry_host(reservation)
        puts "Sending Request Expiration Email"
        listing = reservation.listing
        host = reservation.listing.user
        renter = reservation.user
        recipient = host

        template_data = {
            "first_name": renter.first_name,
            "reservation_url": Url.new(false).host_reservation(listing.id,reservation.id)
        }

        puts template_data
        template_id = REQUEST_EXPIRY_HOST_ID
        categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)

    end

    def cancelled_reservation_by_renter(reservation, recipient = nil)
        host = reservation.host
        renter = reservation.user
        recipient = host if recipient.nil?

        if recipient == host
            puts "Sending Cancelled Reservation Renter Email to host"
            claim_url = Url.new(false).host_claims(reservation.listing.id,reservation.id)
            template_id = RESERVATION_HOST_RENTERCANCEL_ID
            categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]
        else
            puts "Sending Cancelled Reservation Renter Email to renter"
            claim_url = Url.new(false).renter_claims(reservation.id)
            template_id = RESERVATION_RENTER_RENTERCANCEL_ID
            categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]
        end

        template_data = {
            "first_name": recipient.first_name, 
            "claim_url": claim_url
        }

        response = send_email(recipient,template_data,template_id,categories)
    end

    def cancelled_reservation_by_host(reservation, recipient = nil)
        host = reservation.host
        renter = reservation.user
        recipient = renter if recipient.nil?

        if recipient == host
            puts "Sending Cancelled Reservation Host Email to host"
            claim_url = Url.new(false).host_claims(reservation.listing.id,reservation.id)
            template_id = RESERVATION_HOST_HOSTCANCEL_ID
            categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]
        else
            puts "Sending Cancelled Reservation Host Email to renter"
            claim_url = Url.new(false).renter_claims(reservation.id)
            template_id = RESERVATION_RENTER_HOSTCANCEL_ID
            categories = [PRIORITY_HIGH, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]
        end

        template_data = {
            "first_name": recipient.first_name,
            "claim_url": claim_url
        }

        response = send_email(recipient,template_data,template_id,categories)
    end

    # move in/move out
    def pre_move_renter(reservation)
        puts "Sending pre move Renter Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter
        template_data = {
            "first_name": renter.first_name,
            "reservation_url": Url.new(false).renter_reservation(reservation.id)
            # "claim_url": Url.new(false).claims
        }
        template_id = PRE_MOVE_RENTER_ID
        categories = [PRIORITY_LOW, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def pre_move_host(reservation)
        puts "Sending pre move Host Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = host
        template_data = {
            "first_name": renter.first_name,
            "reservation_url": Url.new(false).host_reservation(reservation.listing.id,reservation.id)
            # "claim_url": Url.new(false).claims
        }
        template_id = PRE_MOVE_HOST_ID
        categories = [PRIORITY_LOW, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def move_in_renter(reservation)
        puts "Sending Move in Renter Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter
        template_data = {
            "first_name": renter.first_name,
            "claim_url": Url.new(false).renter_claims(reservation.id)
        }
        template_id = MOVE_IN_RENTER_ID
        categories = [PRIORITY_LOW, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def move_in_host(reservation)
        puts "Sending Move in Host Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = host
        template_data = {
            "first_name": host.first_name,
            "claim_url": Url.new(false).host_claims(reservation.listing.id, reservation.id)
        }
        template_id = MOVE_IN_HOST_ID
        categories = [PRIORITY_LOW, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)

    end

    def move_out_renter(reservation)
        puts "Sending Move out Renter Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = renter
        template_data = {
            "first_name": renter.first_name,
            "tnc_url": Url.new(false).tnc,
            "claim_url": Url.new(false).renter_claims(reservation.id)
        }
        template_id = MOVE_OUT_RENTER_ID
        categories = [PRIORITY_LOW, USER_TYPE_RENTER, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    def move_out_host(reservation)
        puts "Sending Move out Host Email"
        host = reservation.listing.user
        renter = reservation.user
        recipient = host
        template_data = {
            "first_name": host.first_name,
            "tnc_url": Url.new(false).tnc,
            "claim_url": Url.new(false).host_claims(reservation.listing.id, reservation.id)
        }
        template_id = MOVE_OUT_HOST_ID
        categories = [PRIORITY_LOW, USER_TYPE_HOST, PRODUCT_AREA_BOOKING]

        response = send_email(recipient,template_data,template_id,categories)
    end

    # TODO: add deposit returned email
    def deposit_returned_renter(reservation)
        puts "Sending Deposit Returned to Renter Email"
    end
end