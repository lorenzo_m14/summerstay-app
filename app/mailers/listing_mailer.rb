class ListingMailer < ApplicationMailer
    include Utilities
    include Constants::EmailId
    include Constants::EmailCategory

    def completed_listing(listing)
        puts "Sending Completed Booking Email"
        recipient = listing.user

        template_data = {
            "first_name": recipient.first_name,
            "listing_url": Url.new(false).host_listing(listing.id),
            "payout_url": Url.new(false).payouts
        }
        template_id = COMPLETED_LISTING_ID # NOT VALID YET
        categories = [PRIORITY_LOW, USER_TYPE_HOST, PRODUCT_AREA_LISTING]
        response = send_email(recipient,template_data,template_id,categories)
    end

    def unfinished_listing(listing)
        puts "Sending Unfinished Booking Email"
        recipient = listing.user

        template_data = {
            "first_name": recipient.first_name,
            "listing_url": Url.new(false).host_listing(listing.id)
        }
        template_id = UNFINISHED_LISTING_ID # NOT VALID YET
        categories = [PRIORITY_HIGH, USER_TYPE_HOST, PRODUCT_AREA_LISTING]
        response = send_email(recipient,template_data,template_id,categories)
    end
end