class NewDeviseMailer < Devise::Mailer   
    # helper :application # gives access to all helpers defined within `application_helper`.
    include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
    # default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views
    default from:   'Summer Stay <no-reply@summerstay.com>'
    default reply_to: 'Summer Stay <no-reply@summerstay.com>'

    # NOTE: We handle email sending via sendgrid
    # TODO: fully remove devise mailer integrations, these are here to prevent devise from sending emails
    def confirmation_instructions(record, token, opts={})
    end

    def reset_password_instructions(record, token, opts={})
    end
end