class ApplicationMailer
  include Constants::Mailer
  
  def initialize(attributes = {})
    @send_test = attributes[:send_test]
  end

  def send_email(recipient,template_data, template_id, categories)
    if should_send 

      Log.debug "Sending mail to #{recipient.email}"

      mail = SendGrid::Mail.new
      mail.template_id = template_id
      mail.from = SendGrid::Email.new(email: NO_REPLY_MAILER, name: DEFAULT_MAILER_NAME)
      personalization = SendGrid::Personalization.new
      personalization.add_to(SendGrid::Email.new(email: recipient.email, name: recipient.fullname ))
      personalization.add_dynamic_template_data(template_data)
      mail.add_personalization(personalization)
      for category in categories
        mail.add_category(SendGrid::Category.new(name: category))
      end

      sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'] || '', host: 'https://api.sendgrid.com')
      response = sg.client.mail._('send').post(request_body: mail.to_json)

      if is_success_email(response.status_code)
        Log.info("Successfully sent mail!", level: "INFO", type: "Email", recipient: recipient&.email, action: caller[0][/`([^']*)'/, 1], template_id: template_id)
      else
        Log.error("Failed to send email with status #{response.status_code}", level: "ERROR", type: "Email", recipient: recipient&.email, action: caller[0][/`([^']*)'/, 1]) 
      end

      return response
    end
  end

  private
    def should_send
      # don't send actual mail if in test mode unless specific config
      !Rails.env.test? || @send_test.present?
    end

    def is_success_email(status)
      status == "200" || status == "202"
    end
end
