class CronExpiredRequestsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :cron

  def perform
    Log.info("Running CronExpiredRequestWorker", level: "INFO", type: "Scheduler", action: "expire_old_reservations")
    # auto expires reservations
    Reservation.where(status: :Waiting).where("expires_at < ? OR start_date <= ?", DateTime.current, Date.current).each do |r|
      Log.info("Expiring reservation #{r.id}", level: "INFO", type: "Scheduler", action: "expire_old_reservations")
      r.decline
      r.Expired! # set status to expired
      DeclinedRequestWorker.perform_async(r.id)

      listing = r.listing
      # if total expired reservations for an active listing > 10, make the listing inactive
      if listing.reservations.where(status: :Expired).count >= 10
        Log.info("Setting listing #{listing.id} to inactive--too many expired reservations", level: "INFO", type: "Scheduler", action: "inactivate_stale_listing")

        InactiveListingWorker.perform_async(r.listing.id)
        listing.update!(active: false)
      end
    end


  end
end
