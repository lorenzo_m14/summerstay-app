class PaymentReminderWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(reservation_id, charge_id)
    # send emails via sendgrid to host and recipient
    reservation = Reservation.find_by_id(reservation_id)
    charge = Charge.find_by_id(charge_id)

    if reservation.nil? || charge.nil?
      Log.info("Cannot create PaymentReminder job ", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
      return nil
    end

    Events.new.upcoming_payment(charge)
  end
end
