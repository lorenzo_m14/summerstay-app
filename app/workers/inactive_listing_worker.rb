class InactiveListingWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  # TODO: implement inactive list worker event
  def perform(listing_id)
        # send emails via sendgrid to host and recipient
        listing = Listing.find_by_id(listing_id)

        if listing.nil?
          Log.info("Cannot find listing", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name)
          return nil
        end

        Events.new.inactive_listing(listing)
  end
end
