class PayoutWorker
  include Sidekiq::Worker
  sidekiq_options queue: :payout

  def perform(reservation_id, payout_id, referral_id=nil)
        # send emails via sendgrid to host and recipient
        payout = Payout.find_by_id(payout_id)

        # create platform notification to host
        if payout.nil?
          Log.info("Cannot create job: can't find payout #{payout_id}", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
        elsif !payout.stripe_transfer_id.nil?
          Log.info("Cannot create job: payout #{payout_id} has already been paid!", level: "WARN", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
        else
          Log.info("Capturing transfer for payout #{payout_id}")
          payout = PaymentServices.new(payout.reservation).capture_transfer_payout(payout_id)

          # payout notification
          if payout.nil?
            Log.info("Could not capture Stripe payout transfer for payout #{payout_id}", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
          else
            Events.new.payout_success(payout)
          end
        end
  end
end
