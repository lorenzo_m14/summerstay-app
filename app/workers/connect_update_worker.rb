class ConnectUpdateWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(user_id)
        # send emails via sendgrid to host and recipient
        user = User.find_by_id(user_id)

        if user.nil?
          Log.info("Cannot create ConnectUpdate job ", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name)
          return nil
        end

        Events.new.connect_status_update(user)
  end
end
