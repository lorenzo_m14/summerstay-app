class PreMoveWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(reservation_id)
        # send emails via sendgrid to host and recipient
        reservation = Reservation.find_by_id(reservation_id)

        if reservation.nil?
          Log.info("Cannot create PreMove job ", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
          return nil
        end

        Events.new.pre_move(reservation)
  end
end
