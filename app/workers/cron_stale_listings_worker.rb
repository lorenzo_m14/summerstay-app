class CronStaleListingsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :cron

  def perform
    Log.info("Running CronStaleListingsWorker", level: "INFO", type: "Scheduler", action: "check_stale_listings")
    # check if listing still has valid windows for reservations
    Listing.where(status: [:Ready]).each do |listing|
      if listing&.end_date.nil?
          Log.error("Listing #{listing.id} is invalid", level: "ERROR", type: "Scheduler", action: "check_stale_listings")
      elsif listing&.end_date < Date.today
          Log.info("Archiving listing #{listing.id}", level: "INFO", type: "Scheduler", action: "check_stale_listings")
          listing.update!(bookable: false, status: :Archived) 
      elsif listing.bookable && !listing.has_bookable_windows
          Log.info("Listing #{listing.id} has no more bookable windows", level: "INFO", type: "Scheduler", action: "check_stale_listings")
          listing.update!(bookable: false)
      end
    end
  end
end
