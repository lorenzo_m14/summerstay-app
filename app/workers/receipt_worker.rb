class ReceiptWorker
    include Sidekiq::Worker
    sidekiq_options queue: :mailer
  
    def perform(reservation_id, charge_id)
        # send emails via sendgrid to host and recipient
        charge = Charge.find_by_id(charge_id)

        # create platform notification to host
        if charge.nil? 
            Log.info("Cannot create job: couldn't find charge with id #{charge_id}", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
        else
            PaymentMailer.new.receipt_email(charge)  
        end
    end
  end
  

