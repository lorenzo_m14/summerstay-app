class MessageWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(message_id)
        # send emails via sendgrid to host and recipient
        message = Message.find_by_id(message_id)

        if message.nil?
          Log.info("Cannot create Message job ", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name)
          return nil
        end

        Events.new.send_message(message)
  end
end
