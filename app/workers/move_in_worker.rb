class MoveInWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(reservation_id)
        # send emails via sendgrid to host and recipient
        reservation = Reservation.find_by_id(reservation_id)

        if reservation.nil?
          Log.info("Cannot create MoveIn job ", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
          return nil
        end

        Events.new.move_in(reservation)
  end
end
