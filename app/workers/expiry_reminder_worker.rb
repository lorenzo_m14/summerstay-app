class ExpiryReminderWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(reservation_id, host_id)
        # send emails via sendgrid to host and recipient
        user = User.find_by_id(host_id)
        reservation = Reservation.find_by_id(reservation_id)

        if reservation.nil?
          Log.info("Cannot create RequstExpiryReminder job", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
          return nil
        end

        Events.new.request_expiry(reservation)
  end
end
