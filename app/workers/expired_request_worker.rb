class ExpiredRequestWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailer

  def perform(reservation_id)
      # send emails via sendgrid to host and recipient
      r = Reservation.find_by_id(reservation_id)

      if r.nil?
        Log.info("Cannot create ExpiredRequest job", level: "ERROR", job_id: self.jid, type: "Worker", action: self.class.name, reservation_id: reservation_id)
        return nil
      end

      Log.info("Expiring reservation #{r.id}", level: "INFO", type: "Scheduler", action: "expire_old_reservations")
      r.decline
      r.Expired! # set status to expired
      DeclinedRequestWorker.perform_async(r.id)

      listing = r.listing
      # if total expired reservations for an active listing > 10, make the listing inactive
      if listing.reservations.where(status: :Expired).count >= 10
        Log.info("Setting listing #{listing.id} to inactive--too many expired reservations", level: "INFO", type: "Scheduler", action: "inactivate_stale_listing")

        InactiveListingWorker.perform_async(listing.id)
        listing.update!(active: false)
      end
  end
end
