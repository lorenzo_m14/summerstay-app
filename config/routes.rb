Rails.application.routes.draw do
  scope :api, defaults: {format: :json} do
    scope :v1 do
        # custom user endpoints
        get '/users/logout' => 'api/v1/users#logout'
        post '/users/facebook_login' => 'api/v1/users#facebook_login'
        post '/users/facebook' => 'api/v1/users#facebook'
        put '/users/update_user' => 'api/v1/users#update_user'
        get '/me' => 'api/v1/users#me'

        # referral endpoints (Currently unused)
        # post '/users/referrals' => 'api/v1/users#create_campus_ref'
        # get '/users/referrals' => 'api/v1/users#get_listings_with_ref'
        # get '/verify_referral_code' => 'api/v1/referral_codes#verify_code'

        # twilio phone verification
        post '/users/send_phone_token' => 'api/v1/users#send_phone_token'
        post '/users/verify_phone_number' => 'api/v1/users#verify_phone_number'

        # dashboard listings
        get '/my_listings' => 'api/v1/listings#own_listings'
        get '/my_listings/:id' => 'api/v1/listings#own_listings_show'

        # dashboard stays
        get '/stays' => 'api/v1/reservations#own_stays'
        get '/stays/charges' => 'api/v1/reservations#own_stays_charges'
        get '/stays/:id' => 'api/v1/reservations#own_stays_show'

        # Stripe Connect endpoints
        post '/payouts/connect_callback' => 'api/v1/payouts#stripe_connect_callback'
        get '/payouts/connect_login' => 'api/v1/payouts#stripe_connect_create_login'

        # webhooks
        post '/webhooks/payouts' => 'api/v1/webhooks#payouts'
        post '/webhooks/invoices' => 'api/v1/webhooks#invoices'
        post '/webhooks/subscriptions' => 'api/v1/webhooks#subscriptions'
        post '/webhooks/accounts' => 'api/v1/webhooks#accounts'

        # ID Verification
        post '/users/verify_id' => 'api/v1/id_verifications#verify_id'
        post '/webhooks/id_verify' => 'api/v1/id_verifications#id_verify_webhooks'
        # TODO: include this endpoint when we get webhooks access
        # post '/users/update_id_verify_status' => 'api/v1/users#update_id_verify_status'

        # Email Lead (via Sendgrid)
        # TODO: add this endpoint for email leads
        # post 'users/email_lead' => 'api/v1/users#sendgrid_email_lead'

        devise_for :users, controllers: { 
          sessions: 'api/v1/sessions',
          registrations: 'api/v1/registrations',
          confirmations: 'api/v1/confirmations',
          passwords: 'api/v1/passwords'
        }

        resources :listings, controller: 'api/v1/listings' do
          member do
            get '/reservations' => 'api/v1/reservations#reservations_by_listing'
            get '/reservation_cost' => 'api/v1/listings#get_reservation_cost'
            put '/sort_images' => 'api/v1/listings#sort_images'
            delete '/delete_image' => 'api/v1/listings#delete_image'
          end
        end

        resources :reservations, controller: 'api/v1/reservations' do
          member do
            post '/approve' => 'api/v1/reservations#approve'
            post '/decline' => 'api/v1/reservations#decline'
            post '/cancel' => 'api/v1/reservations#cancel'
            get '/charges' => 'api/v1/charges#charges_by_reservation'
            get '/compute_payouts' => 'api/v1/reservations#compute_payouts'
          end
        end

        resources :conversations, controller: 'api/v1/conversations', only: [:index, :create] do
          resources :messages, controller: 'api/v1/messages', only: [:index, :create]
        end

        resources :charges, controller: 'api/v1/charges' do
          member do
            post '/pay_charge' => 'api/v1/charges#pay_charge'
            get '/get_receipt_url' => 'api/v1/charges#get_receipt_url'
          end
        end

        resources :payment_methods, controller: 'api/v1/payment_methods', only: [:index, :create]
        delete '/delete_payment_method' => 'api/v1/payment_methods#delete_payment_method'

        resources :payouts, controller: 'api/v1/payouts'

        resources :claims, controller: 'api/v1/claims'

        resources :direct_uploads, controller: 'api/v1/direct_uploads'

        resources :notifications, controller: 'api/v1/notifications', only: [:index] do
          post :mark_as_read, on: :collection
          post :mark_as_read, on: :member
        end

        # In production we will host Sidekiq WebUI on a separate node
        # https://github.com/honeypotio/sidekiq_dashboard
        if !Rails.env.production?
          require 'sidekiq/web'
          require 'sidekiq/cron/web'
          mount Sidekiq::Web => '/sidekiq'
        end

        # For Realtime chat and notifications
        # mount ActionCable.server => '/cable'
    end
  end

  get '*path', to: 'application#fallback_index_html', constraints: lambda { |request|
    !request.xhr? && request.format.html?
  }

  # get '*unmatched_route', to: 'application#route_not_found'
end
