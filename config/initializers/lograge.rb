Rails.application.configure do
  if Rails.env.staging? || Rails.env.production?
    if ENV['RAILS_LOG_TO_STDOUT'].present?
      logger           = ActiveSupport::Logger.new(STDOUT)
      logger.formatter = config.log_formatter
      config.logger    = logger
    end

    config.lograge.enabled = true
    config.lograge.formatter = Lograge::Formatters::Json.new
    config.lograge.logger = config.logger
    config.colorize_logging = false

    config.lograge.custom_options = lambda do |event|
      if ENV['DD_APM_ENABLED'].present?
        correlation = Datadog.tracer.active_correlation
      end
      {
        dd: { trace_id: correlation&.trace_id },
        ddsource: ['ruby'],
        params: event.payload[:params].reject { |k| %w(controller action).include? k },
        level: event.payload[:level],
        status: event.payload[:level],
        status_code: event.payload[:status_code],
        request_info: {
          user_id: event.payload[:user_id],
          # request_id: event.payload[:request_id],
          user_agent: event.payload[:user_agent]
        },
        env: ENV['RAILS_ENV']
      }
    end

    config.lograge.ignore_actions = [
        'ActiveStorage::DiskController#show',
        'ActiveStorage::BlobsController#show',
        'ActiveStorage::RepresentationsController#show'
    ]
  end
end