Sidekiq.configure_server do |config|
    config.log_formatter = Sidekiq::Logger::Formatters::JSON.new
    config.logger.level = ::Logger::INFO
    config.logger = ActiveSupport::Logger.new(STDOUT)
    
    # add cron jobs
    schedule_file = "config/schedule.yml"
    if File.exist?(schedule_file) && Sidekiq.server?
        Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
    end
end
