if Rails.env.staging? || Rails.env.production?
  Datadog.configure do |c|
      c.use :rails, service_name: ENV['RAILS_ENV']
  end  
end